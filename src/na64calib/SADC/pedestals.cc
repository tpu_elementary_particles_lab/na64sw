#include "na64calib/SADC/pedestals.hh"
#include "na64util/str-fmt.hh"

//#include <msgpack.hpp>

namespace na64dp {
namespace calib {

void
PedestalsCSVDataIndex::append_index_from_tuple( Pedestals & p
                                              , const Tuple & t ) {
    std::string detName = std::get<0>(t)
              , pedType = std::get<1>(t)
              ;
    DetID did;
    auto & names = *_naming;
    try {
        did = names[detName];
    } catch( std::exception & e ) {
        _log.error( "Unable to retrieve detector ID for name \"%s\": %s"
                  , detName.c_str()
                  , e.what() );
        throw;
    }
    assert( names[did] == detName );  // ensure reverse conversion returns same name
    _log.debug( "Loaded pedestals for \"%s\"", names[did].c_str() );
    auto ir = p.emplace( did, std::make_pair( NDValue()
                                            , NDValue()
                                            )
                       );
    NDValue * ndvPtr;
    if( pedType == "Odd" ) {
        ndvPtr = &(ir.first->second.first);
    } else if( pedType == "Even" ) {
        ndvPtr = &(ir.first->second.second);
    } else {
        NA64DP_RUNTIME_ERROR( "Can not interpret pedestal order type."
                " Expected \"Odd\",\"Even\", got: \"%s\".", pedType.c_str() );
    }
    ndvPtr->mean = std::get<2>(t);
    ndvPtr->stdDev = std::get<3>(t);
    ndvPtr->n = std::get<4>(t);
}

}
}

