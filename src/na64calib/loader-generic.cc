#include "na64calib/loader-generic.hh"

namespace na64dp {
namespace calib {

/** Used registered calibration data type index to retrieve and set the
 * calibration data. Throws `CalibDataTypeIsNotDefined` if data type is not
 * found. */
void
GenericLoader::load_data( EventID eventID
                        , Dispatcher::CIDataID cidID
                        , Dispatcher & d ) {
    auto it = _dataIndexes.find( cidID );
    if( _dataIndexes.end() == it ) {
        throw errors::CalibDataTypeIsNotDefined( cidID, this );
    }
    DispatcherProxy proxy(d, cidID);
    it->second->put( eventID, proxy );
    if( !proxy.was_used() ) {
        NA64DP_RUNTIME_ERROR( "Data index did not update the calibration data." );
    }
};

bool
GenericLoader::has( EventID eventID
                  , Dispatcher::CIDataID cidID ) {
    auto it = _dataIndexes.find(cidID);
    if( _dataIndexes.end() == it ) {
        log4cpp::Category::getInstance( "calib" ).debug(
                   "GenericLoader:%p -- no entries for calib data of type %s"
                 , this
                 , util::calib_id_to_str(cidID).c_str() );
        return false;
    }
    return it->second->has( eventID );
}

void
GenericLoader::add_data_index( Dispatcher::CIDataID cidID
                             , AbstractCalibDataIndex * aiPtr ) {
    auto ir = _dataIndexes.emplace( cidID, aiPtr );
    if( !ir.second ) {
        NA64DP_RUNTIME_ERROR( "Double data indexing objects inserted for"
                " single calibration data type." );
    }
}

}

namespace errors {

CalibDataTypeIsNotDefined::CalibDataTypeIsNotDefined( calib::Dispatcher::CIDataID cdid
                                                    , calib::GenericLoader * loaderPtr ) throw()
        : GenericRuntimeError( "Calibration data type is"
                " not defined in loader instance." )
        , _cdid(cdid)
        , _loaderPtr(loaderPtr)
        {}

}

}

