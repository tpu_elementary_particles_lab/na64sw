#include "na64calib/range-override-run-index.hh"

namespace na64dp {
namespace calib {

void
RangeOverrideRunIndex::add_entry( EventID bgnEvID
                                , const Dispatcher::CIDataID & typeKey
                                , const std::string & loaderName
                                ) {
    // find or insert typed entry (ranges index)
    //auto typeKey = Dispatcher::CIDataID{ti, calibType};
    auto tIt = _types.find( typeKey );
    if( _types.end() == tIt ) {
        auto ir = _types.emplace( typeKey
                                , RangeIndex<EventID, CalibDataDescription>() );
        tIt = ir.first;
    }
    // emplace new entry
    RangeIndex< EventID, CalibDataDescription> & rangeIndex
        = tIt->second;
    auto ir = rangeIndex.emplace(bgnEvID, CalibDataDescription{ loaderName });
    if( ! ir.second ) {
        if( ir.first->second.loaderName != loaderName ) {
            throw errors::CalibrationDataConflict( bgnEvID, typeKey.first, typeKey.second,
                    loaderName, ir.first->second.loaderName );
        }
        log4cpp::Category::getInstance("calibrations").warn( 
                    "Doubling specification of loader \"%s\" for calibration"
                    " data %s on event %s."
                    , loaderName.c_str()
                    , util::calib_id_to_str(typeKey).c_str()
                    , bgnEvID.to_str().c_str()
                    );
    }
}

void
RangeOverrideRunIndex::updates( EventID oldEventID
                              , EventID newEventID
                              , UpdatesList & ul ) {
    for( auto it = _types.begin()
       ; _types.end() != it
       ; ++it ) {
        auto cidID = it->first;
        auto & runIdx = it->second;
        auto oldEntryIt = runIdx.get_valid_entry_for( oldEventID )
           , newEntryIt = runIdx.get_valid_entry_for( newEventID )
           ;
        if( oldEntryIt == newEntryIt ) continue;
        if( runIdx.end() == newEntryIt ) {
            throw errors::NoValidForRange();
        }
        const CalibDataDescription & cdd = newEntryIt->second;
        ul.enqueue_update( it->first
                         , cdd.loaderName
                         , newEntryIt->first );
    }
}

}
}

