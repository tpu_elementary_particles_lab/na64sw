#include "na64calib/config-yaml.hh"

#include "na64detID/TBName.hh"
#include "na64util/str-fmt.hh"
#include "na64calib/range-override-run-index.hh"

#include "na64detID/cellID.hh"
#include "na64detID/wireID.hh"

#include <fstream>

namespace na64dp {
namespace calib {

/**\brief Appends run index / data index configuration wrt YAML document
 *
 * This utility function fills run index and data loader indexes according
 * to information provided in YAML node at the runtime. The given node has to
 * be a list of entries each of that contains the validity period
 * (`validFromRun`) and type string key referencing particular data loader
 * instance.
 * */
void
configure_from_YAML( YAML::Node & rootNode
                   , RangeOverrideRunIndex * runIndex
                   , GenericLoader * loader
                   , const std::string & loaderName
                   , std::unordered_map<std::string, YAMLCalibInfoCtr> & ciCtrs
                   ) {
    auto & L = log4cpp::Category::getInstance( "calibrations.yaml" );
    for( YAML::const_iterator nodeIt = rootNode.begin()
       ; rootNode.end() != nodeIt
       ; ++nodeIt ) {
        const std::string calibTypeName = nodeIt->first.as<std::string>();
        // Get the constructor node
        auto ctrIt = ciCtrs.find( calibTypeName );
        if( ciCtrs.end() == ctrIt ) {
            if( "APIVersion" != calibTypeName ) {
                L.warn( "Unable to retrieve calibration data type for node \"%s\"."
                      , calibTypeName.c_str() );
            }
            continue;
        }
        const YAML::Node & node = nodeIt->second;
        YAMLCalibInfoCtr & ctr = ctrIt->second;
        // Construct the data index if needed
        if( ! ctr.dataIndexPtr ) {
            assert( ctr.ctr );
            // Construct new data index
            ctr.dataIndexPtr = ctr.ctr();
            L.info( "New calibration data index constructed %s: %p"
                  , util::calib_id_to_str(ctr.ciDataID).c_str()
                  , ctr.dataIndexPtr );
            loader->add_data_index( ctr.ciDataID, ctr.dataIndexPtr );
        }
        // Add into both indexes
        for( const auto & cdNode : node ) {
            EventID validFromEvent( cdNode["validFromRun"].as<na64sw_runNo_t>()
                                  , 0, 0 );
            // It is important to keep this two calls covariant:
            // - add run index entry
            runIndex->add_entry( validFromEvent
                               , ctr.ciDataID
                               , loaderName );
            // - add data entry
            try {
                ctr.dataIndexPtr->append( validFromEvent, cdNode );
            } catch( std::exception & e ) {
                L.error( "While appending entry for %s event of \"%s\" data"
                        " an error occured."
                        , validFromEvent.to_str().c_str(), calibTypeName.c_str() );
            }
            L.info( "Run (%p) and data (%p) indexes %s added from YAML on event %s."
                  , runIndex
                  , ctr.dataIndexPtr
                  , util::calib_id_to_str(ctr.ciDataID).c_str()
                  , validFromEvent.to_str().c_str() );
        }
    }
}

nameutils::DetectorNaming
mappings_from_yaml_API02( const YAML::Node & root ) {
    auto & L = log4cpp::Category::getInstance( "calibrations" );
    nameutils::DetectorNaming r;
    for( auto kv : root["chips"] ) {
        const YAML::Node & pl = kv.second;
        const std::string chipName = kv.first.as<std::string>();
        // add chip with name and id
        nameutils::DetectorNaming::ChipFeatures & ct
            = r.chip_add( chipName, pl["id"].as<int>());
        // get basic fields from the YAML chip description
        // - description string is rarely used, human-readable description of
        //   the chip
        ct.description = pl["desc"].as<std::string>();

        const YAML::Node & pats = pl["patterns"];
        // - pathFormat is slash-separated path of objects, used to organaize
        //   various artifacts related to the detector entity: files,
        //   histograms, etc.
        ct.pathFormat = pats["path"].as<std::string>();
        ct.nameFormat = pats["name"].as<std::string>();
        ct.dddNameFormat = pats["tbname"].as<std::string>();
        if( "SADC" == chipName ) {
            ct.append_completion_context = CellID::append_completion_context;
            ct.to_string = CellID::to_string;
            ct.from_string = CellID::from_string;
        } else if( "APV" == chipName ) {
            ct.append_completion_context = WireID::append_completion_context;
            ct.to_string = WireID::to_string;
            ct.from_string = WireID::from_string;
        } else if( "NA64TDC" == chipName ) {
            ct.append_completion_context = WireID::append_completion_context;
            ct.to_string = WireID::to_string;
            ct.from_string = WireID::from_string;
        /* ... add other chips here ... */
        } else {
            L.warn( "No to-/from-string conversion and text context appender"
                    " functions will be defined for chip \"%s\"."
                    , chipName.c_str() );
        }
    }
    for( auto kv : root["kins"] ) {
        const YAML::Node & pl = kv.second;
        try {
            if( pl["pattenrs"] ) {
                const YAML::Node & pats = pl["pattenrs"];
                auto & kinFts = r.define_kin( kv.first.as<std::string>()
                                            , pl["id"].as<int>()
                                            , pl["chip"].as<std::string>()
                                            , pats["name"].as<std::string>()
                                            , pl["desc"].as<std::string>()
                                            , pats["path"].as<std::string>()
                                            );
                kinFts.dddNameFormat = pats["tbname"].as<std::string>();
            } else {
                r.define_kin( kv.first.as<std::string>()
                            , pl["id"].as<int>()
                            , pl["chip"].as<std::string>()
                            , ""
                            , pl["desc"].as<std::string>()
                            , ""
                            );
            }
        } catch(std::exception & e) {
            L.error( "Exception occured on node \"%s\"."
                   , kv.first.as<std::string>().c_str() );
            throw;
        }
    }
    return r;
}

}  // namespace na64dp::calib
}  // namespace na64dp

