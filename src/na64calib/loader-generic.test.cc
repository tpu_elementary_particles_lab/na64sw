#include "na64calib/loader-generic.hh"

#include <gtest/gtest.h>

namespace na64dp {
namespace calib {

class CalibDataIndexStub : public GenericLoader::AbstractCalibDataIndex {
public:
    virtual void put( EventID eid, GenericLoader::DispatcherProxy & p ) override {
        p = acquire(eid);
    }

    virtual std::string acquire( EventID eid ) {
        if( eid.run_no() == 3 ) {
            return "three";
        } else if( eid.run_no() == 4 ) {
            return "four";
        }
        assert(false);
    }
    virtual bool has( EventID eid ) override {
        return  eid.run_no() > 2 && eid.run_no() < 5;
    }
};

class MockObserver : public na64dp::util::Observable<std::string>::iObserver {
public:
    std::string value;
    MockObserver() {}
    virtual void handle_update(const std::string & v) override {
        value = v;
    }
};

class CalibGenericLoader : public ::testing::Test {
public:
    Dispatcher dispatcher;
    GenericLoader loader;
    CalibDataIndexStub * stub;
    Dispatcher::CIDataID k;
    CalibGenericLoader() : stub(nullptr)
                         , k(Dispatcher::info_id<std::string>("tstStub")) {
        stub = new CalibDataIndexStub();
        loader.add_data_index( Dispatcher::info_id<std::string>("tstStub")
                             , stub );
    }
    virtual ~CalibGenericLoader() {
        if( stub ) delete stub;
    }
};

TEST_F( CalibGenericLoader, DataRequestLookupIsCorrect ) {
    // we don't "see" any non-existing data
    ASSERT_FALSE( loader.has( EventID(3, 1, 1)
                            , Dispatcher::info_id<int>("tstStub")
                            )
                );
    ASSERT_FALSE( loader.has( EventID(3, 1, 1)
                            , Dispatcher::info_id<std::string>("nonexisting")
                            )
                );
    ASSERT_FALSE( loader.has( EventID(1, 1, 1)
                            , Dispatcher::info_id<std::string>("tstStub")
                            )
                );
    // but we "see" existing one
    ASSERT_TRUE(  loader.has( EventID(3, 1, 1)
                            , Dispatcher::info_id<std::string>("tstStub")
                            )
               );
}

TEST_F( CalibGenericLoader, DataIndexQueryCorrect ) {
    // we "see" existing data
    ASSERT_TRUE( loader.has(EventID(3, 0, 0), k) );
    // fail to retrieve non-existing data
    EXPECT_THROW( loader.load_data( EventID(3, 0, 0)
                                  , Dispatcher::info_id<std::string>("nonexisting")
                                  , dispatcher
                                  )
                , errors::CalibDataTypeIsNotDefined );
    EXPECT_THROW( loader.load_data( EventID(3, 0, 0)
                                  , Dispatcher::info_id<int>("tstStub")
                                  , dispatcher
                                  )
                , errors::CalibDataTypeIsNotDefined );
}

TEST_F( CalibGenericLoader, DataIndexForwarded ) {
    MockObserver observer;
    // data was retrieved indeed
    dispatcher.subscribe<std::string>( observer, "tstStub" );
    ASSERT_TRUE( observer.value.empty() );
    loader.load_data( EventID(3, 0, 0)
                    , k
                    , dispatcher );
    ASSERT_STREQ( observer.value.c_str()
                , "three" );
    loader.load_data( EventID(3, 2, 333)
                    , k
                    , dispatcher );
    ASSERT_STREQ( observer.value.c_str()
                , "three" );
    // and this data differs as expected
    loader.load_data( EventID(4, 2, 333)
                    , k
                    , dispatcher );
    ASSERT_STREQ( observer.value.c_str()
                , "four" );
    dispatcher.unsubscribe<std::string>( observer, "tstStub" );
}

}
}

