#include "na64util/namedFileIterator.hh"

#include <TROOT.h>
#include <TKey.h>

namespace na64dp {
namespace util {

NamedFileIterator::NamedFileIterator( TFile * file
                 , const char * clName
                 , const char * regex )
        : _file(file), _className(clName), _rx(regex) {
    //assert( _rx.IsValid() ); // always false; what this method is intended for?!
    _dirStack.push( StackEntry(TIter(file->GetListOfKeys()), _file) );
}

TObject *
NamedFileIterator::get_match() {
    if( _dirStack.empty() ) return nullptr;
    TKey * key;
    while((key = (TKey*) _dirStack.top().first.Next())) {
        TClass * cl = ROOT::GetROOT()->GetClass(key->GetClassName());
        // if current is dir
        if( cl->InheritsFrom("TDirectory") ) {
            TDirectory * dir = (TDirectory*) key->ReadObj();
            assert(dir);
            _dirStack.push(StackEntry(TIter(dir->GetListOfKeys()), dir));
            return get_match();  // recursive forwarding here
        }
        // if current is look-up target
        if( cl->InheritsFrom(_className)
         && _rx.Match(key->GetName() ) ) {
            // TODO: check here name vs regex match
            TObject * res = key->ReadObj();
            return res;
        }
    }
    _dirStack.pop();
    return get_match();  // recursive forwarding here
}

}
}
