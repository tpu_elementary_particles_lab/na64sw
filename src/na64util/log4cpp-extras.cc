#include "na64util/log4cpp-extras.hh"
#include "na64util/Geant4UIAppender.hh"

#include <log4cpp/Category.hh>
#include <log4cpp/AppendersFactory.hh>
#include <log4cpp/OstreamAppender.hh>

#include <stdexcept>  // XXX
#include <memory>

namespace na64dp {
namespace util {

static Log4cppAppendersFactoryResult_t
create_ostream_appender(const log4cpp::AppendersFactory::params_t & p) {
    std::string name;
    p.get_for("stdout appender").required("name", name);
    return Log4cppAppendersFactoryResult_t(new log4cpp::OstreamAppender(name, &std::cout));
}

void
inject_extras_to_log4cpp() {
    log4cpp::AppendersFactory::getInstance().registerCreator( "stdout"
                , create_ostream_appender );

    #ifdef Geant4_FOUND
    log4cpp::AppendersFactory::getInstance().registerCreator( "Geant4 UI"
                , Geant4UISessionAppender::create );
    assert(log4cpp::AppendersFactory::getInstance().registered("Geant4 UI"));
    #endif
}

}
}

