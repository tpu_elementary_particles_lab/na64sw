#include "na64util/uri.hh"

#include <wordexp.h>

namespace na64dp {
namespace util {

std::vector<std::string>
expand_names( const std::string & expr ) {
    std::vector<std::string> r;
    wordexp_t p;
    char ** w;
    wordexp( expr.c_str(), &p, 0x0 );
    w = p.we_wordv;
    if( 0 == p.we_wordc ) {
        wordfree( &p );
        return r;
    }
    r.reserve(p.we_wordc);
    for( size_t i=0; i < p.we_wordc; i++ ) {
        r.push_back(w[i]);
    }
    wordfree( &p );
    return r;
}

#ifdef UriParser_FOUND

namespace uriparser {

URI::URI(std::string uri) : uri_(uri) {
    UriParserStateA state_;
    state_.uri = &uriParse_;
    isValid_   = uriParseUriA(&state_, uri_.c_str()) == URI_SUCCESS;
}

URI::~URI() {
    uriFreeUriMembersA(&uriParse_);
}

std::string
URI::from_range(const UriTextRangeA & rng) const {
    return std::string(rng.first, rng.afterLast);
}

std::string
URI::from_list(UriPathSegmentA * xs, const std::string & delim) const {
    UriPathSegmentStructA * head(xs);
    std::string accum;
    while (head) {
        accum += delim + from_range(head->text);
        head = head->next;
    }
    return accum;
}

}

#endif

}
}
