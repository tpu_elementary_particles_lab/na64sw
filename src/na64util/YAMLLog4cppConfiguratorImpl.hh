#pragma once

#include <log4cpp/FactoryParams.hh>
#include <log4cpp/AppendersFactory.hh>
#include <log4cpp/LayoutsFactory.hh>
#include <log4cpp/Category.hh>

#include <yaml-cpp/yaml.h>

namespace na64dp {
namespace util {

class YAMLLog4cppConfiguratorImpl {
public:
    YAMLLog4cppConfiguratorImpl() {}
    virtual ~YAMLLog4cppConfiguratorImpl() {}
    void doConfigure(const std::string & filename);
    void doConfigure(const YAML::Node & cfg);
protected:
    void _configure_category_from_YAML( const YAML::Node & catCfg
                                      , log4cpp::Category & cat
                                      , std::list<std::string> & catNames
                                      );
private:
    std::map<std::string, log4cpp::Appender*> appenders;
};

}
}
