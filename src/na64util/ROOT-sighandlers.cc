#include "na64util/ROOT-sighandlers.hh"

#ifdef ROOT_FOUND
#include <TSystem.h>

void
disable_ROOT_sighandlers() {
    gSystem->ResetSignals();
}
#endif
