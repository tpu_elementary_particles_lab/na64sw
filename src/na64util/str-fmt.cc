#include "na64util/str-fmt.hh"

#include <cstdarg>
#include <vector>
#include <map>
#include <cassert>

namespace na64dp {
namespace util {

const StringSubstFormat gDefaultStrFormat = { "{", "}" };

/** This function reproduces pure C `printf()` behaviour, accepting a format
 * string and arbitrary number of arguments to produce output as an
 * `std::string` instance.
 *
 * Although the output string is allocated on heap, initial buffer is restrcted
 * by number defined by `NA64DP_STR_FMT_LENGTH` macro. It will be extended, but
 * this fact may have importance for performance concerns.
 * */
std::string
format(const char *fmt, ...) throw() {
    va_list args;
    va_start(args, fmt);
    std::vector<char> v(NA64DP_STR_FMT_LENGTH);
    while(true) {
        va_list args2;
        va_copy(args2, args);
        int res = vsnprintf(v.data(), v.size(), fmt, args2);
        if ((res >= 0) && (res < static_cast<int>(v.size()))) {
            va_end(args);
            va_end(args2);
            return std::string(v.data());
        }
        size_t size;
        if (res < 0)
            size = v.size() * 2;
        else
            size = static_cast<size_t>(res) + 1;
        v.clear();
        v.resize(size);
        va_end(args2);
    }
}

std::string
str_subst( const std::string & tmplt
         , const std::map<std::string, std::string> & context
         , bool requireCompleteness
         , const StringSubstFormat * fmt
         ) {
    std::string r(tmplt);
    // Iterate over all entries within a context (we assume context to be
    // short).
    for( auto entry : context ) {
        std::size_t pos;
        const std::string key = fmt->bgnMarker + entry.first + fmt->endMarker
                        , & val = entry.second;
        // Unil there is no more occurances of the string, perform search and
        // substitution
        while( std::string::npos != (pos = r.find(key)) ) {
            r = r.replace( pos, key.length(), val );
        }
    }
    if( requireCompleteness ) {
        assert_str_has_no_fillers(r, fmt);
    }
    return r;
}

/** We consider any of curly brackets `{}` to be a remnant of template string
 * markup and return `true` if any of them is found in string
 *
 * \todo shall we imply some advanced logic here?
 * \todo Support `StringSubstFormat` (doesn't work for non-default)
 * */
bool
str_has_no_fillers( const std::string & s
                  , const StringSubstFormat * fmt ) {
    return std::string::npos == s.find_first_of("{}");
}

/** Uses `str_has_no_fillers()` to check if string is consistent and raises
 * `StringIncomplete` exception if expectation is not fullfilled. */
void
assert_str_has_no_fillers( const std::string & s
                         , const StringSubstFormat * fmt ) {
    if( str_has_no_fillers(s) ) return;
    throw errors::StringIncomplete(s);
}

/** Examples:
 *      "one two" -> ("one", "two")
 *      ""some another" vlah" -> ("some another", "blah")
 * */
std::vector<std::string>
tokenize_quoted_expression(const std::string & strExpr) {
    std::vector<std::string> tokens;
    const char * tokBgn = nullptr;
    for( const char *c = strExpr.c_str(); '\0' != *c; ++c ) {
        if( ' ' == *c ) {
            if( !tokBgn ) continue;  // omit spaces between tokens
            // otherwise, push token
            tokens.push_back( std::string(tokBgn, c - tokBgn) );
            tokBgn = nullptr;
        } else if( '"' == *c && !tokBgn ) {
            tokBgn = ++c;
            // comma starts escaped sequence -- traverse for next comma
            while( *c != '"' ) {
                if( *c == '\0' )
                    throw std::runtime_error("Unbalanced quotes.");
                ++c;
            }
            // Push token
            tokens.push_back( std::string(tokBgn, c - tokBgn) );
            tokBgn = nullptr;
        } else {
            if( !tokBgn ) tokBgn = c;
        }
    }
    // push back last token in string
    if(tokBgn)
        tokens.push_back( tokBgn );
    return tokens;
}

}  // namespace ::na64dp::util
}  // namespace na64dp

