#include "na64util/selector.hh"

#include <cstring>

namespace na64dp {
namespace errors {

SelectorBuildupFailure::SelectorBuildupFailure( int rc
                                              , const char * errBf ) throw() : std::runtime_error("DSuL error.")
                                                                             , _rc(rc) {
    strncpy( _errBf, errBf, sizeof(_errBf) );
    snprintf( _errBf, sizeof(_errBf)
            , "Selector translation failed. Return code: %d, error: %s"
            , rc, errBf );
}

}
}

