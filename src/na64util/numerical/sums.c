#include "na64util/numerical/sums.h"

#include <math.h>
#include <assert.h>
#include <stdlib.h>

/*
 * Klein's Sum
 * **********/

void
na64dp_sum_klein_init(na64dp_KleinScorer_t * S) {
    assert(S);
    S->s = S->cs = S->ccs = 0.;
}

void
na64dp_sum_klein_add( na64dp_KleinScorer_t * S
                    , double v ) {
    assert(S);
    assert( !isnan(v) );
    double c, cc, t = S->s + v;
    if( fabs(S->s) >= fabs(v) ) {
        c = (S->s - t) + v;
    } else {
        c = (v - t) + S->s;
    }
    S->s = t;
    t = S->cs + c;
    if( fabs(S->cs) >= fabs(c) ) {
        cc = (S->cs - t) + c;
    } else {
        cc = (c - t) + S->cs;
    }
    S->cs = t;
    S->ccs += cc;
}

double
na64dp_sum_klein_result(const na64dp_KleinScorer_t * S) {
    assert(S);
    return S->s + S->cs + S->ccs;
}

/*
 * Pearson's Correlation
 * ********************/

void
na64dp_cov_init( na64dp_Covariance_t * C ) {
    assert(C);
    C->n = 0;
    na64dp_sum_klein_init( &(C->xMean) );
    na64dp_sum_klein_init( &(C->yMean) );
    na64dp_sum_klein_init( &(C->d2x) );
    na64dp_sum_klein_init( &(C->d2y) );
    na64dp_sum_klein_init( &(C->rS) );
}

void
na64dp_cov_account( na64dp_Covariance_t * C
                  , double x, double y ) {
    /* Increase value count */
    ++(C->n);
    const double dx1 = x - na64dp_sum_klein_result(&(C->xMean))
               , dy1 = y - na64dp_sum_klein_result(&(C->yMean))
               ;
    na64dp_sum_klein_add( &(C->xMean), dx1/(C->n) );
    na64dp_sum_klein_add( &(C->yMean), dy1/(C->n) );

    const double dx2 = x - na64dp_sum_klein_result(&(C->xMean))
               , dy2 = y - na64dp_sum_klein_result(&(C->yMean))
               ;
    na64dp_sum_klein_add( &(C->d2x), dx1*dx2 );
    na64dp_sum_klein_add( &(C->d2y), dy1*dy2 );
    /* Compute and update co-moment sum */
    na64dp_sum_klein_add( &(C->rS), dx1*dy2 );
}

double
na64dp_covariance_get( const na64dp_Covariance_t * C ) {
    if( C->n < 2 ) {
        return nan("0");
    }
    return na64dp_sum_klein_result(&(C->rS))/(C->n-1.);
}

void
na64dp_mcov_init( na64dp_CovarianceMatrix_t * ptr
                , unsigned short d ) {
    ptr->d = d;
    ptr->n = 0;
    ptr->means = malloc( d*sizeof(na64dp_KleinScorer_t) );
    ptr->sqds  = malloc( d*sizeof(na64dp_KleinScorer_t) );
    for( int i = 0; i < d; ++i ) {
        na64dp_sum_klein_init( ptr->means + i );
        na64dp_sum_klein_init( ptr->sqds  + i );
    }
    const unsigned int nComponents = d*(d+1)/2;
    ptr->covs = malloc( nComponents*sizeof(na64dp_KleinScorer_t) );
    for( int i = 0; i < nComponents; ++i ) {
        na64dp_sum_klein_init( ptr->covs + i );
    }
}

void
na64dp_mcov_account( na64dp_CovarianceMatrix_t * M
                   , const double * v ) {
    /* Increase value count */
    ++(M->n);
    /* Compute deviations.
     * Note: variable length array here. Alternatively one may involve
     * alloca() call here if current compiler does not support this extension */
    double dV1[M->d]
         , dV2[M->d]
         ;
    for( int i = 0; i < M->d; ++i ) {
        /* Deviation prior to mean value modification */
        dV1[i] = v[i] - na64dp_sum_klein_result(M->means + i);
        /* Modify mean value */
        na64dp_sum_klein_add( M->means + i, dV1[i]/(M->n) );
        /* Deviation after to mean value modification */
        dV2[i] = v[i] - na64dp_sum_klein_result(M->means + i);
        /* Increase cumulative squared deviation sum */
        na64dp_sum_klein_add( M->sqds + i, dV1[i]*dV2[i] );
    }
    /* For each unique value pair within a vector (15) */
    int k = 0;
    for( int i = 0; i < M->d; ++i ) {
        for( int j = i; j < M->d; ++j ) {
            na64dp_sum_klein_add( M->covs + k, dV1[i]*dV2[j] );
            ++k;
        }
    }
}

double
na64dp_mcov( na64dp_CovarianceMatrix_t * M, int i, int j ) {
    if( M->n < 2 ) {
        return nan("0");
    }
    size_t offset = j + i*(M->d * 2 - 1 - i)/2;
    return na64dp_sum_klein_result( M->covs + offset )/(M->n - 1);
}

void
na64dp_mcov_free( na64dp_CovarianceMatrix_t * M ) {
    free(M->means);
    free(M->sqds);
    free(M->covs);
}

