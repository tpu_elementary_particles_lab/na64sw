#include "na64util/numerical/online.hh"

#include <iostream>  // XXX

namespace na64dp {
namespace numerical {

namespace aux {

template<> void
reset_summation_scorer<double>(double & scorer) {
    scorer = 0.;
}

template<> void
reset_summation_scorer<KleinScorer>(KleinScorer & scorer) {
    scorer.reset();
}

}

}  // namespace ::na64dp::numerical
}  // namespace na64dp

