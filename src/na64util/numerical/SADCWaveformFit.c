# include "na64util/numerical/SADCWaveFormFit.h"

# ifdef GSL_FOUND

# include <stdio.h>
# include <stdint.h>
# include <stdlib.h>
# include <math.h>

# include <gsl/gsl_vector.h>
# include <gsl/gsl_rng.h>
# include <gsl/gsl_randist.h>
# include <gsl/gsl_blas.h>
# include <gsl/gsl_multifit_nlin.h>

// Model function shortcut.
double moyal( union SADCWF_FittingFunctionParameters * gp, double x ) {
    const double lambda = (x - gp->moyalPars.p[1])/gp->moyalPars.p[2],
                 exp1   = exp(-lambda),
                 exp2   = exp( -(lambda + exp1)/2 );
    return exp2*gp->moyalPars.p[0];
}

// Error calculation function --- instance to be minimized.
int moyal_f( const gsl_vector * p,
             void * data_, 
             gsl_vector * f) {
    struct SADCWF_FittingInput * data = (struct SADCWF_FittingInput *) data_;
    const double * y = data->samples;
    const double * sigma = data->sigma;

    double p1 = gsl_vector_get(p, 0),
           p2 = gsl_vector_get(p, 1),
           p3 = gsl_vector_get(p, 2);
    
    uint8_t i;
    for( i = 0; i < data->n; i++ ) {
        double x        = i,
               lambda   = ( x - p2 ) / p3,
               Yi       = p1*exp(- (lambda + exp(-lambda))/2);

        gsl_vector_set( f, i, (Yi - y[i])/sigma[i] );
    }
    return GSL_SUCCESS;
}

// Jacobian matrix.
int moyal_df( const gsl_vector * p,
              void * data_, 
              gsl_matrix * J) {
    struct SADCWF_FittingInput * data = (struct SADCWF_FittingInput *) data_;
    const double p1 = gsl_vector_get(p, 0),
                 p2 = gsl_vector_get(p, 1),
                 p3 = gsl_vector_get(p, 2);

    uint8_t i;
    for( i = 0; i < data->n; i++ ) {
        // Jacobian matrix J(i,j) = dfi / dpj,
        // where fi = (Yi - yi)/sigma[i],
        //       Yi = A * exp(-lambda * i) + b
        // and the pj are the parameters (A,lambda,b).
        
        const double x = i,
                     sigma = data->sigma[i],
                     lambda = (x - p2)/p3,

                     exp2 = exp(-lambda),
                     exp1 = exp(-(lambda + exp2)/2);   

        gsl_matrix_set( J, i, 0, exp1 );
        gsl_matrix_set( J, i, 1, (p1/(2*p3*sigma))*(1 - exp2)*exp1 );
        gsl_matrix_set( J, i, 2, (p1/(2*p3*p3*sigma))*(x-p2)*(1 - exp2)*exp1 );
    }
    return GSL_SUCCESS;
}

// Function and matrix simultaneous calculation shortcut.
int moyal_fdf( const gsl_vector * p,
               void * data_, 
               gsl_vector * f,
               gsl_matrix * J ) {
    moyal_f(p, data_, f);
    moyal_df(p, data_, J);

    return GSL_SUCCESS;
}

void print_state( FILE * logfile, size_t iter, gsl_multifit_fdfsolver * s) {
    if( !logfile ) return;
    fprintf(logfile, "\n # iter: %u Parametrs = { %7.2e %7.2e %7.2e } \n",
            (unsigned) iter,
            gsl_vector_get( s->x, 0 ),
            gsl_vector_get( s->x, 1 ),
            gsl_vector_get( s->x, 2 ) );

    //fprintf(logfile, " f = {\n");
    fprintf(logfile, "# dx = {%7.2e %7.2e %7.2e}\n",
            gsl_vector_get( s->dx, 0 ),
            gsl_vector_get( s->dx, 1 ),
            gsl_vector_get( s->dx, 2 )
        );
}

int fit_SADC_samples( const struct SADCWF_FittingInput * srcData,
                      SADCWF_FittingFunction fitting_function,
                      union SADCWF_FittingFunctionParameters * uFitPars,
                      FILE * logfile ) {
    gsl_multifit_fdfsolver *s;
    int status;
    unsigned int iter = 0;
    uint8_t p;
    gsl_matrix * covar;
    gsl_multifit_function_fdf f;
    //double p_init[3] = { 1e3, 10, 3 };
    gsl_vector_view P;

    if( moyal == fitting_function ) {
        p = 3;
        covar = gsl_matrix_alloc( p, p );
        P = gsl_vector_view_array( uFitPars->moyalPars.p, p );
        /* covar mx computation pars */
        f.f = &moyal_f;
        f.df = &moyal_df;
        f.fdf = &moyal_fdf;
        /* dimensions, etc. */
        f.n = srcData->n;
        f.p = p;
        f.params = (/*const*/ struct SADCWF_FittingInput *) srcData;
    } else {
        return -1;
    }

    s = gsl_multifit_fdfsolver_alloc( gsl_multifit_fdfsolver_lmsder, f.n, p );
    gsl_multifit_fdfsolver_set( s, &f, &P.vector );
    print_state( logfile, iter, s ); 

    do {
        iter++;
        status = gsl_multifit_fdfsolver_iterate( s );
        if( logfile ) {
         //   fprintf(logfile, "# multifit status = %s\n", gsl_strerror (status));
        }
        //print_state(logfile, iter, s);
        /*fprintf(logfile, "XXX: %e, %e\n", s->x, s->dx );*/
        if(status){
            break;
        }
        status = gsl_multifit_test_delta( s->dx, s->x, 1e-4, 1e-4 );
        if( logfile ) {
           // fprintf(logfile, "# multifit test delta status = %s\n", gsl_strerror(status));
        }
    } while(status == GSL_CONTINUE && iter < 2000);

    // gsl_multifit_covar( s->J, 0.0, covar );

    if( logfile ) {
        fprintf(logfile, "# ****** Fitting exit ************\n");
    }

    # define FIT(i) gsl_vector_get(s->x, i)
    # define ERR(i) sqrt(gsl_matrix_get(covar,i,i)) 
    {
        double chi = gsl_blas_dnrm2(s->f);
        double dof = f.n - p;
        double c = GSL_MAX_DBL( 1, chi/sqrt(dof) ); 
        
        if( logfile ) {
            fprintf( logfile, "# chisq/dof = %g\n",  pow(chi, 2.0)/dof );
            fprintf( logfile, "# p1 = %.5f +/- %.5f\n", FIT(0), c*ERR(0) );
            fprintf( logfile, "# p2 = %.5f +/- %.5f\n", FIT(1), c*ERR(1) );
            fprintf( logfile, "# p3 = %.5f +/- %.5f\n", FIT(2), c*ERR(2) );
        }
        uFitPars->moyalPars.p[0] = FIT(0);
        uFitPars->moyalPars.p[1] = FIT(1);
        uFitPars->moyalPars.p[2] = FIT(2);
        uFitPars->moyalPars.err[0] = c*ERR(0);
        uFitPars->moyalPars.err[1] = c*ERR(1);
        uFitPars->moyalPars.err[2] = c*ERR(2);
        uFitPars->moyalPars.chisq_dof = pow(chi, 2.0)/dof;
    }
    # if 1
    // Testing printout for obtained parameters
    if( logfile ) {  
        gsl_vector * tmpF = gsl_vector_alloc(f.n)
                   ;
        gsl_matrix * derivCache = gsl_matrix_alloc( f.n, p );
        moyal_f( s->x, (void *) srcData, tmpF );
        moyal_df( s->x, (void *) srcData, derivCache );

        fprintf( logfile, "%3s %10s %10s %10s %10s %10s %10s \n", 
                           "#i", "Y_i", "|Y_i - y_i|", "dY/dp1", "dY/dp2", "dY/dp3", "Y(i)" );

        for( int i = 0; i < (unsigned int) srcData->n; ++i ) {
            fprintf( logfile, "%3d %10.4f %10.4f %10.4f %10.4f %10.4f %10.4f \n",
                     (int) i,
                     srcData->samples[i],
                     gsl_vector_get(tmpF, i),
                     gsl_matrix_get( derivCache, i, 0 ),
                     gsl_matrix_get( derivCache, i, 1 ),
                     gsl_matrix_get( derivCache, i, 2 ),
                     fitting_function( uFitPars, i ) );
        }
        gsl_vector_free( tmpF );
    }
    # endif

    gsl_multifit_fdfsolver_free (s);
    gsl_matrix_free (covar);

    return 0;
}

# if 0
void fill_data_for_fit(SADCWF_FittingInput *data, double *wave, double *sigm, int channel) {
    data->n = channel;
    data->samples = wave;
    data->sigma = sigm;
}

int main ()  {
    FILE * logfile;
    logfile = fopen("logfile.txt", "w");

    double wave[32] = { 0, 0,  0, 0.00223187, 1.46463, 40.8029,
                        150.515, 271.428, 312.383, 285.484, 200.774, 177.073, 135.125,
                        100.5573, 69.0666, 49.7094, 35.6999, 35.6102, 18.3616, 17.1607,
                        9.43156, 6.75856, 9.84292, 3.47018, 2.48652, 1.78168, 1.27663,
                        0.914749, 0.655447, 0.469648, 0.336518};
    double sigm[32];
    for(int i = 0; i < 32; i++){
        sigm[i] = 1;        
    }

    SADCWF_FittingInput dataForFitting;
    fill_data_for_fit(&dataForFitting, wave, sigm, 32); 
 
    //gsl_vector * p = gsl_vector_alloc (3);
    //gsl_vector_set_all(p, 2);
    //gsl_vector * f = gsl_vector_alloc (32);
    //gsl_vector_set_all(f, 0);
    //void *data_ = &dataForFitting;
    //gsl_matrix * J = gsl_matrix_alloc (32, 3);
    //fprintf(logfile, "%f", gsl_vector_get(p, 2) );
    
    //moyal_df( p, &dataForFitting, J);

    SADCWF_FittingFunction fitting_function = moyal;
    union SADCWF_FittingFunctionParameters uFitPars;

    uFitPars.moyalPars.p[0] = 500;
    uFitPars.moyalPars.p[1] = 1.5;
    uFitPars.moyalPars.p[2] = 7;
    
    fit_SADC_samples(&dataForFitting, fitting_function, &uFitPars, logfile );

    fclose(logfile);
    return 0;
}

# endif

# endif  /* GSL_FOUND */

