#include "na64util/str-fmt.hh"

#include <gtest/gtest.h>

namespace na64dp {

TEST(StrUtilsQuotedTokens, splitsOrdinaryTokens) {
    auto v = util::tokenize_quoted_expression( "one   two" );
    ASSERT_EQ( v.size(), 2 );
    ASSERT_FALSE( v[0].empty() );
    EXPECT_EQ( v[0], "one" );
    ASSERT_FALSE( v[1].empty() );
    EXPECT_EQ( v[1], "two" );
}

TEST(StrUtilsQuotedTokens, splitsCommaEscapedTokens) {
    auto v = util::tokenize_quoted_expression( "\"some another\" three" );
    ASSERT_EQ( v.size(), 2 );
    ASSERT_FALSE( v[0].empty() );
    EXPECT_EQ( v[0], "some another" );
    ASSERT_FALSE( v[1].empty() );
    EXPECT_EQ( v[1], "three" );
}

TEST(StrUtilsQuotedTokens, handlesEmptyAndShortTokens) {
    auto v = util::tokenize_quoted_expression( "\"\" 1   _ \"\"" );
    ASSERT_EQ( v.size(), 4 );
    EXPECT_TRUE( v[0].empty() );
    EXPECT_EQ( v[1], "1" );
    EXPECT_EQ( v[2], "_" );
    EXPECT_TRUE( v[3].empty() );
}

TEST(StrUtilsQuotedTokens, errorOnUnbalancedQuotes) {
    EXPECT_THROW( util::tokenize_quoted_expression( "\"" )
                , std::runtime_error );
    EXPECT_THROW( util::tokenize_quoted_expression( "\"one two\" blah \"some three" )
                , std::runtime_error );
}

}

