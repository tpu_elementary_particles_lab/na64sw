#include "na64event/event.hh"



#ifdef GenFit_FOUND
#include <Track.h>
#endif

namespace na64dp {
 
EvFieldTraits<Track>::MapType &
EvFieldTraits<Track>::map( Event & e ) { return e.tracks; }

std::vector<EvFieldTraits<Track>::MapType*>
EvFieldTraits<Track>::maps_for( Event & e
                              , DetID did
                              , const nameutils::DetectorNaming & ) {
    std::vector<MapType*> v;
    v.push_back( &e.tracks );
    return v;
}

void
EvFieldTraits<Track>::reset_hit( Track & track ) {
    track.seed[0] = track.seed[1] = track.seed[2] = 0;
    track.mom[0] = track.mom[1] = track.mom[2] = 0;
    track.momentum = 0;
    track.pdg = 0;
    track.edep = 0;
    track.chi2ndf = 0;
    track.trackPoints.clear();
   
    if(track.genfitTrackRepr) {
        #if defined(GenFit_FOUND) && GenFit_FOUND
        delete track.genfitTrackRepr;
        #endif
        track.genfitTrackRepr = nullptr;
    }
}


// ///////
// Getters
/////////
static double _track_chi2( const Track & track ) { return track.chi2ndf; }
static double _track_mom( const Track & track ) { return track.momentum; }
// ...

static EvFieldTraits<Track>::GetterEntry _getters[] = {
    { _track_chi2, "chi2",      "returns fitted chi2" },
    { _track_mom,  "momentum",  "returns fitted momentum" },

    // ...
    {nullptr, nullptr, nullptr}
};

EvFieldTraits<Track>::GetterEntry * EvFieldTraits<Track>::getters = _getters;
   
}
