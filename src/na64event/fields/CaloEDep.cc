#include "na64event/fields/CaloEDep.hh"
#include "na64util/str-fmt.hh"

#include "na64event/event.hh"

namespace na64dp {

EvFieldTraits<CaloEDep>::MapType &
EvFieldTraits<CaloEDep>::map( Event & e ) {
    return e.caloEDeps;
}

std::vector<EvFieldTraits<CaloEDep>::MapType*>
EvFieldTraits<CaloEDep>::maps_for( Event & e
                                 , DetID
                                 , const nameutils::DetectorNaming & ) {
    std::vector<EvFieldTraits<CaloEDep>::MapType*> r;
    r.push_back( &e.caloEDeps );
    return r;
}

void
EvFieldTraits<CaloEDep>::reset_hit( CaloEDep & hit ) {
    hit.stationHits.clear();
    hit.station = 0x0;
    hit.eDepMeV = 0;
}

DetID_t
EvFieldTraits<CaloEDep>::uniq_detector_id(DetID did) {
    return did.id;
}

void
EvFieldTraits<CaloEDep>::remove_from_event( Event & e
                                          , MapType::Iterator it
                                          , const nameutils::DetectorNaming & nm ) {
	std::pair< DetID_t, PoolRef<CaloEDep> > pr = *it;
	// Remove from subsidiary maps
	for( auto subMapPtr : maps_for(e, DetID(it.offsetIt->first), nm) ) {
		// Locate all the sadc hits corresponding to detector
		auto er = subMapPtr->container().equal_range(pr.first);

		for( auto it = er.first; it != er.second; ++it ) {
			if( it->second != pr.second.offset_id() ) continue;
			subMapPtr->container().erase(it);
			break;
		}
	}
}

//
// Getters
/////////

static double _calo_edep_MeV( const CaloEDep & caloEDep ) { return caloEDep.eDepMeV; }

static EvFieldTraits<CaloEDep>::GetterEntry _getters[] = {
    { _calo_edep_MeV,  "eDepMeV",    "returns energy deposition in MeVs" },
    // ...
    {nullptr, nullptr, nullptr}
};

EvFieldTraits<CaloEDep>::GetterEntry * EvFieldTraits<CaloEDep>::getters = _getters;

}
