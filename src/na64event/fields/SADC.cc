#include "na64event/fields/SADC.hh"

#include "na64event/hitTraits.hh"
#include "na64dp/abstractEventSource.hh"
#include "na64detID/TBName.hh"
#include "na64event/serialize.hh"
#include "na64util/str-fmt.hh"

// Detector-specific IDs
#include "na64detID/wireID.hh"
#include "na64detID/cellID.hh"

#include <cmath>

namespace na64dp {

HitsMap<SADCHit> &
EvFieldTraits<SADCHit>::map( Event & e ) { return e.sadcHits; }

std::vector<HitsMap<SADCHit>*>
EvFieldTraits<SADCHit>::maps_for( Event & e
                                , DetID
                                , const nameutils::DetectorNaming & ) {
    std::vector<HitsMap<SADCHit>*> r;
    r.push_back( &e.sadcHits );
    # if 0
    if(  ) {  // ECAL
        e.push_back( &e.byDetector.ECAL );
    } else if(  ) {  // HCAL
    }  // ... SRD, LYSO, VETO, etc.
    # endif
    return r;
}

void
EvFieldTraits<SADCHit>::remove_from_event( Event & e
                                         , HitsMap<SADCHit>::Iterator it
                                         , const nameutils::DetectorNaming & nm ) {
    // Remember detector ID as `it' will not be valid after erasing it from
    // main map
    DetID did(it.offsetIt->first);
    // Erase entry from each subsidiary map
    for( auto subMapPtr : maps_for(e, did, nm) ) {
        subMapPtr->container().erase( did );
    }
}

HitsInserter<SADCHit> &
EvFieldTraits<SADCHit>::inserter( AbstractEventSource & src ) {
    return src._inserterSADC;
}

#if 0
void
EvFieldTraits<SADCHit>::append_subst_dict_for( DetID did,
        std::map<std::string, std::string> & m,
        const nameutils::DetectorNaming & nm ) {
    char bf[64];
    CellID cid(did.payload());
    snprintf(bf, sizeof(bf), "%d", cid.get_x());
    m["xIdx"] = bf;
    snprintf(bf, sizeof(bf), "%d", cid.get_y());
    m["yIdx"] = bf;
    snprintf(bf, sizeof(bf), "%d", cid.get_z());
    m["zIdx"] = bf;
    if( std::string("ECAL") == nm.kin_name(did) ) {
        m["partName"] = cid.get_z() ? "preshower" : "main";
    }
}
#endif

void
EvFieldTraits<SADCHit>::reset_hit( SADCHit & hit ) {
    for(int i = 0; i < 32; ++i) {
        hit.wave[i] = std::nan("0");
    }
    hit.sum = std::nan("0");
    hit.eDep = std::nan("0");
    hit.time = std::nan("0");
    hit.timeError = std::nan("0");
    hit.maxSample = hit.maxValue = std::nan("0");
    hit.maxCell = false;
    hit.pedestals[0] = std::nan("0");
    hit.pedestals[1] = std::nan("0");
    hit.timeBegin = std::nan("0");
    hit.angle = std::nan("0");
    hit.maxima.clear();
    hit.fittingData.clear();
}

DetID_t
EvFieldTraits<SADCHit>::uniq_detector_id(DetID did) {
    return did.id;
}

/// SADC sum getter
static double _sadc_sum_getter( const SADCHit & h ) { return h.sum; }
/// SADC energy deposition getter
static double _sadc_edep_getter( const SADCHit & h ) { return h.eDep; }
/// SADC max waveform position getter
static double _sadc_maxsample_getter( const SADCHit & h ) { return h.maxSample; }
/// SADC max waveform amplitude getter
static double _sadc_maxvalue_getter( const SADCHit & h ) { return h.maxValue; }
/// Returns odd pedestal value
static double _sadc_ped0_getter( const SADCHit & h ) { return h.pedestals[0]; }
/// Returns even pedestal value
static double _sadc_ped1_getter( const SADCHit & h ) { return h.pedestals[1]; }
/// Returns fitting RMS value
static double _sadc_fit_rms_getter( const SADCHit & h ) {
    if( !h.fittingData.is_valid() ) {
        return std::nan("0");
    }
    return h.fittingData->rms;
}
/// Returns rms divided by waveform area
static double _sadc_rms_div_area_getter( const SADCHit & h ) { 
    return _sadc_fit_rms_getter(h) /
           _sadc_sum_getter(h);
}
/// Returns gitting ChiSq value
static double _sadc_fit_chisq_getter( const SADCHit & h ) {
    if( !h.fittingData.is_valid() ) {
        return std::nan("0");
    }
    return h.fittingData->chisq;
}


/// Returns Moyal area value
static double _sadc_fit_moyal_area_getter( const SADCHit & h ) {
    if( !h.fittingData.is_valid() ) {
        return std::nan("0");
    }
    return h.fittingData->fitPars.moyalPars.p[0];
}
/// Returns Moyal max value
static double _sadc_fit_moyal_max_getter( const SADCHit & h ) {
    if( !h.fittingData.is_valid() ) {
        return std::nan("0");
    }
    return h.fittingData->fitPars.moyalPars.p[1];
}
/// Returns Moyal width value
static double _sadc_fit_moyal_width_getter( const SADCHit & h ) {
    if( !h.fittingData.is_valid() ) {
        return std::nan("0");
    }
    return h.fittingData->fitPars.moyalPars.p[2];
}
/// Returns begin time hit value
static double _sadc_timeBegin_getter( const SADCHit & h ) { return h.timeBegin; }
/// Returns angle begin hit
static double _sadc_angle_getter( const SADCHit & h ) { return h.angle; }
///
static double _sadc_countPipeUp_getter( const SADCHit & h ) { return h.countPipeUp; }

//
// Getters
/////////

static EvFieldTraits<SADCHit>::GetterEntry _getters[] = {
    { _sadc_sum_getter,             "sum", "Waveform sum" },
    { _sadc_edep_getter,            "edep", "Energy deposition" },
    { _sadc_maxsample_getter,       "maxSample", "Number of maximal sample" },
    { _sadc_maxvalue_getter,        "maxValue", "Value of maximal sample" },
    { _sadc_ped0_getter,            "pedestalOdd", "Odd pedestal" },
    { _sadc_ped1_getter,            "pedestalEven", "Event pedestal" },
    //
    { _sadc_timeBegin_getter,       "timeBegin", "Begining time hit" },
    { _sadc_angle_getter,           "angle", "Angle begin hit" },
    { _sadc_countPipeUp_getter,     "countPipeUp", "Freq pileup" },
    // fitting
    { _sadc_fit_rms_getter,         "fitRMS", "RMS of fitting" },
    { _sadc_rms_div_area_getter,    "moyalRMStoSum", "RMS divided by waveform area"},
    { _sadc_fit_chisq_getter,       "fitChiSq", "Squared chi to ndof of fitting" },
    // Moyal-specific
    { _sadc_fit_moyal_area_getter,  "moyalArea", "Area parameter of Moyal function" },
    { _sadc_fit_moyal_max_getter,   "moyalMax", "Max parameter of Moyal function" },
    { _sadc_fit_moyal_width_getter, "moyalWidth", "Width parameter of Moyal function" },
    // ...
    { nullptr, nullptr, nullptr }
};
EvFieldTraits<SADCHit>::GetterEntry * EvFieldTraits<SADCHit>::getters
    = _getters;


//
// Serialization
///////////////

#if defined(CapnProto_FOUND) && CapnProto_FOUND
void
EvFieldTraits<SADCHit>::pack( const SADCHit & hit
                            , PackedType::Builder bHit
                            , serialization::StatePacking & ) {
    // write SADC hit specific fields
    auto wave = bHit.initWave(32);
    for( int i = 0; i < 32; ++i ) {
        wave.set(i, hit.wave[i]);
    }
    bHit.setSum( hit.sum );
    bHit.setEDep( hit.eDep );
    bHit.setMaxSample( hit.maxSample );
    bHit.setMaxValue( hit.maxValue );
    bHit.setPedestalOdd(  hit.pedestals[0] );
    bHit.setPedestalEven( hit.pedestals[1] );
    if( hit.fittingData.is_valid() ) {
        auto bFittingData = bHit.initFittingData();

        bFittingData.setChisq( hit.fittingData->chisq );
        bFittingData.setRms( hit.fittingData->rms );
        auto bFitPars = bFittingData.initParameters();

        if( WfFittingData::moyal == hit.fittingData->fitType ) {
            auto bm = bFitPars.initMoyal();
            auto & m = hit.fittingData->fitPars.moyalPars;

            // write SADC waveform fitting specific fields
            bm.setArea(         m.p[0] );
            bm.setMaximum(      m.p[1] );
            bm.setWidth(        m.p[2] );
            bm.setErrArea(      m.err[0] );
            bm.setErrMaximum(   m.err[1] );
            bm.setErrWidth(     m.err[2] );
            bm.setChisq(        m.chisq_dof );
        } /* else if( ... == hit.fittingData->fitType ) */ else {
            NA64DP_RUNTIME_ERROR( "Unknown fitting type for SADC." );
        }
    }
    // ...
}

void
EvFieldTraits<SADCHit>::unpack( PackedType::Reader rHit
                              , SADCHit & hit
                              , serialization::StateUnpacking & state ) {
    for( int i = 0; i < 32; ++i ) {
        hit.wave[i] = rHit.getWave()[i];
    }
    hit.sum = rHit.getSum();
    hit.eDep = rHit.getEDep();
    hit.maxSample = rHit.getMaxSample();
    hit.maxValue = rHit.getMaxValue();
    hit.pedestals[0] = rHit.getPedestalOdd();
    hit.pedestals[1] = rHit.getPedestalEven();
    if( rHit.hasFittingData() ) {
        auto rFD = rHit.getFittingData();
        hit.fittingData = state.banks.of<WfFittingData>().create();
        hit.fittingData->chisq = rFD.getChisq();
        hit.fittingData->rms = rFD.getRms();
        auto rPFD = rFD.getParameters();
        if( rPFD.isMoyal() ) {
            hit.fittingData->fitType = WfFittingData::moyal;
            SADCWF_FittingFunctionParameters::MoyalParameters & mp
                        = hit.fittingData->fitPars.moyalPars;
            auto rMp = rPFD.getMoyal();
            mp.p[0] = rMp.getArea();
            mp.p[1] = rMp.getMaximum();
            mp.p[2] = rMp.getWidth();
            mp.err[0] = rMp.getErrArea();
            mp.err[1] = rMp.getErrMaximum();
            mp.err[2] = rMp.getErrWidth();
            mp.chisq_dof = rMp.getChisq();
        } /* else if( ...other fitting type... ) { ... } */ else {
            NA64DP_RUNTIME_ERROR( "Unknown fitting data type for SADC." );
        }
    }
}

#endif

}
