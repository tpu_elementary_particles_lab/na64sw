#include "na64event/fields/trackPoint.hh"
#include "na64event/serialize.hh"
#include "na64util/str-fmt.hh"

#ifdef GenFit_FOUND
#include <TrackPoint.h>
#endif

namespace na64dp {
//
// Getters
/////////

static double _apv_trpt_lr0( const TrackPoint & tp ) { return tp.lR[0]; }
static double _apv_trpt_lr1( const TrackPoint & tp ) { return tp.lR[1]; }
static double _apv_trpt_lr2( const TrackPoint & tp ) { return tp.lR[2]; }
static double _apv_trpt_gr0( const TrackPoint & tp ) { return tp.gR[0]; }
static double _apv_trpt_gr1( const TrackPoint & tp ) { return tp.gR[1]; }
static double _apv_trpt_gr2( const TrackPoint & tp ) { return tp.gR[2]; }
// ...

static EvFieldTraits<TrackPoint>::GetterEntry _getters[] = {
    { _apv_trpt_lr0,  "localR0",    "returns local R_0 (plane's X)" },
    { _apv_trpt_lr1,  "localR1",    "returns local R_1 (plane's Y)" },
    { _apv_trpt_lr2,  "localR2",    "returns local R_2 (plane's Z)" },
    { _apv_trpt_gr0,  "globalR0",   "returns global R_0 (world's X)" },
    { _apv_trpt_gr1,  "globalR1",   "returns global R_1 (world's Y)" },
    { _apv_trpt_gr2,  "globalR2",   "returns global R_2 (world's Z)" },
    // ...
    {nullptr, nullptr, nullptr}
};

EvFieldTraits<TrackPoint>::GetterEntry * EvFieldTraits<TrackPoint>::getters = _getters;

EvFieldTraits<TrackPoint>::MapType &
EvFieldTraits<TrackPoint>::map( Event & e ) {
    return e.trackPoints;
}

std::vector<EvFieldTraits<TrackPoint>::MapType*>
EvFieldTraits<TrackPoint>::maps_for( Event & e
                                   , DetID
                                   , const nameutils::DetectorNaming & ) {
    std::vector<EvFieldTraits<TrackPoint>::MapType*> r;
    r.push_back( &e.trackPoints );
    return r;
}

void
EvFieldTraits<TrackPoint>::reset_hit( TrackPoint & hit ) {
    hit.lR[0] = hit.lR[1] = hit.lR[2] = 0;
    hit.gR[0] = hit.gR[1] = hit.gR[2] = 0;
    hit.clusterRefs.clear();
    hit.station = 0;
    hit.charge = 0;
}

void
EvFieldTraits<TrackPoint>::remove_from_event( Event & e
                                            , MapType::Iterator it
                                            , const nameutils::DetectorNaming & nm ) {
    //NA64DP_RUNTIME_ERROR("TODO: implement remove track point from event");  // TODO
	#if 1
	std::pair< DetID_t, PoolRef<TrackPoint> > pr = *it;
	// Remove from subsidiary maps
	for( auto subMapPtr : maps_for(e, DetID(it.offsetIt->first), nm) ) {
		// Locate all the clusters corresponding to detector
		auto er = subMapPtr->container().equal_range(pr.first);
		// From these clusters, find and remove entry for certain
		// cluster using by-value comparison (note that we do stop
		// on first match -- doubtful)
		// TODO: relies only on offsets leading to certain error in case
		// of mixing pools
		for( auto it = er.first; it != er.second; ++it ) {
			if( it->second != pr.second.offset_id() ) continue;
			subMapPtr->container().erase(it);
			break;
		}
	}
	#else  // TODO: XXX, wrong implem
    // Remember detector ID as `it' will not be valid after erasing it from
    // main map
    DetID did(it.offsetIt->first);
    // Erase entry from each subsidiary map
    for( auto subMapPtr : maps_for(e, did, nm) ) {
        subMapPtr->container().erase( did );
    }
    #endif    

}

//
// Serialization
///////////////

#if defined(CapnProto_FOUND) && CapnProto_FOUND
void
EvFieldTraits<TrackPoint>::pack( const TrackPoint & tp
                               , PackedType::Builder bTp
                               , serialization::StatePacking & state ) {
    // Write track point specific fields
    bTp.setLocalR0( tp.lR[0] );
    bTp.setLocalR1( tp.lR[1] );
    bTp.setLocalR2( tp.lR[2] );
    bTp.setGlobalR0( tp.gR[0] );
    bTp.setGlobalR1( tp.gR[1] );
    bTp.setGlobalR2( tp.gR[2] );
    bTp.setStationID( tp.station );
    // ...
    // Pipulate APV clusters's references with hit numbers (relies on insertion
    // order written in state)
    if( !tp.clusterRefs.empty() ) {
        auto bClustersRefs = bTp.initClusterRefs( tp.clusterRefs.size() );
        size_t nCluster = 0;
        for( auto cRefPair : tp.clusterRefs ) {
            const DetID_t did = cRefPair.first;
            auto clusPtr = cRefPair.second;
            // Find APV hit associated with cluster among APV hits that have been
            // serializaed already
            auto clusOffsetIt = state.apvClusters.find( clusPtr );
            if( state.apvClusters.end() == clusOffsetIt ) {
                NA64DP_RUNTIME_ERROR( "Invalid serialization state:"
                        " failed to address track points's APV cluster." );
            }
            auto bClustersRef = bClustersRefs[nCluster];
            bClustersRef.setDetectorID(did);
            bClustersRef.setClusterNo(clusOffsetIt->second );
            ++nCluster;
        }
    }
    // ...

    auto ir = state.trackPoints.emplace( state.banks.of<TrackPoint>().get_ref_of( tp )
                                       , state.trackPoints.size() );
    assert( ir.second );
}

void
EvFieldTraits<TrackPoint>::unpack( PackedType::Reader rC
                                 , TrackPoint & tp
                                 , serialization::StateUnpacking & state ) {
    tp.lR[0] = rC.getLocalR0();
    tp.lR[1] = rC.getLocalR1();
    tp.lR[2] = rC.getLocalR2();
    tp.gR[0] = rC.getGlobalR0();
    tp.gR[1] = rC.getGlobalR1();
    tp.gR[2] = rC.getGlobalR2();
    tp.station = rC.getStationID();
    // ...
    // Populate cluster with hit references (relies on hits previosuly being
    // de-serialized with hit unpacking and transmitted here by the state)
    auto rClusRefs = rC.getClusterRefs();
    for( size_t i = 0; i < rClusRefs.size(); ++i ) {
        auto rClusRef = rClusRefs[i];
        DetID_t did = rClusRef.getDetectorID();
        size_t clusNo = rClusRef.getClusterNo();
        auto it = state.apvClusters.find( clusNo );
        if( state.apvClusters.end() == it ) {
            NA64DP_RUNTIME_ERROR( "Can not bind cluster with track point by"
                    " stored reference: bad cluster ID in"
                    " de-serialization state." );
        }
        auto ir = tp.clusterRefs.emplace(did, it->second);
        if( ! ir.second ) {
            NA64DP_RUNTIME_ERROR( "Can not bind track point with APV"
                    " cluster by stored reference: duplicating entry." );
        }
    }
    // ...
    auto ir = state.trackPoints.emplace( state.apvClusters.size()
                                       , state.banks.of<TrackPoint>().get_ref_of( tp )
                                       );
    assert( ir.second );
}
#endif

# if 0
EvFieldTraits<TrackPoint>::MapType &
EvFieldTraits<TrackPoint>::map( Event & e ) {
    return e.trackPoints;
}

std::vector<EvFieldTraits<TrackPoint>::MapType*>
EvFieldTraits<TrackPoint>::maps_for( Event & e
                                   , DetID
                                   , const nameutils::DetectorNaming & ) {
    std::vector<EvFieldTraits<TrackPoint>::MapType*> r;
    r.push_back( &e.trackPoints );
    return r;
}

void
EvFieldTraits<TrackPoint>::reset_hit( TrackPoint & hit ) {
    hit.lR[0] = hit.lR[1] = hit.lR[2] = 0;
    hit.gR[0] = hit.gR[1] = hit.gR[2] = 0;
    hit.clusterRefs.clear();
    hit.station = 0;
    hit.charge = 0;
    #ifdef GenFit_FOUND
    delete hit.genfitTrackPoint;
    #endif
}

void
EvFieldTraits<TrackPoint>::remove_from_event( Event & e
                                            , MapType::Iterator it
                                            , const nameutils::DetectorNaming & nm ) {
    //NA64DP_RUNTIME_ERROR("TODO: implement remove track point from event");  // TODO
	#if 1
	std::pair< DetID_t, PoolRef<TrackPoint> > pr = *it;
	// Remove from subsidiary maps
	for( auto subMapPtr : maps_for(e, DetID(it.offsetIt->first), nm) ) {
		// Locate all the clusters corresponding to detector
		auto er = subMapPtr->container().equal_range(pr.first);
		// From these clusters, find and remove entry for certain
		// cluster using by-value comparison (note that we do stop
		// on first match -- doubtful)
		// TODO: relies only on offsets leading to certain error in case
		// of mixing pools
		for( auto it = er.first; it != er.second; ++it ) {
			if( it->second != pr.second.offset_id() ) continue;
			subMapPtr->container().erase(it);
			break;
		}
	}
	#else  // TODO: XXX, wrong implem
    // Remember detector ID as `it' will not be valid after erasing it from
    // main map
    DetID did(it.offsetIt->first);
    // Erase entry from each subsidiary map
    for( auto subMapPtr : maps_for(e, did, nm) ) {
        subMapPtr->container().erase( did );
    }
    #endif    

}
#endif

}


