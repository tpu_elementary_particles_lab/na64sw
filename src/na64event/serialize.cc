#include "na64event/serialize.hh"

#if defined(CapnProto_FOUND) && CapnProto_FOUND

# include "event.capnp.h"
# include <capnp/message.h>
# include <capnp/serialize-packed.h>

namespace na64dp {
namespace serialization {

void
serialize_event( Event & e, HitBanks & banks ) {
    namespace cpn = ::capnp::na64dp;

    StatePacking S(banks);

    // initialize malloc() message builder
    ::capnp::MallocMessageBuilder msg;

    // event message builder proxy instance
    cpn::Event::Builder bEv = msg.initRoot<cpn::Event>();

    // set event id
    EventID enID = e.id;
    bEv.setId( enID );

    // Copy SADC hits data if any
    if( !e.sadcHits.empty() ) {
        auto bSadcHits = bEv.initSadcHits( e.sadcHits.size() );
        size_t n = 0;
        for( auto hitEntry : e.sadcHits ) {
            const DetID did(hitEntry.first);
            const SADCHit & hit = *hitEntry.second;
            bSadcHits[n].setDetectorID( did.id );
            EvFieldTraits<SADCHit>::pack(hit, bSadcHits[n], S);
            ++n;
        }
    }

    // Copy APV hits data if any
    std::map<APVStripNo_t, size_t> storedHitsIdx;  // stores APV hits by insertion order
    if( !e.apvHits.empty() ) {
        auto bApvHits = bEv.initApvHits( e.apvHits.size() );
        size_t n = 0;
        for( auto hitEntry : e.apvHits ) {
            const DetID did(hitEntry.first);
            const APVHit & hit = *hitEntry.second;

            bApvHits[n].setDetectorID( did.id );
            EvFieldTraits<APVHit>::pack(hit, bApvHits[n], S);
            ++n;
        }
    }
    
    // Copy StwTDC hits data if any
    if( !e.stwtdcHits.empty() ) {
        auto bStwtdcHits = bEv.initStwtdcHits( e.stwtdcHits.size() );
        size_t n = 0;
        for( auto hitEntry : e.stwtdcHits ) {
            const DetID did(hitEntry.first);
            const StwTDCHit & hit = *hitEntry.second;

            bStwtdcHits[n].setDetectorID( did.id );
            EvFieldTraits<StwTDCHit>::pack(hit, bStwtdcHits[n], S);
            ++n;
        }
    }

    // Copy APV clusters data if any
    std::map<ConstPoolRef<APVCluster>, size_t> storedClustersIdx;
    if( !e.apvClusters.empty() ) {
        auto bApvClusters = bEv.initClusters( e.apvClusters.size() );
        size_t n = 0;
        for( auto clusEntry : e.apvClusters ) {
            DetID did( clusEntry.first );
            ConstPoolRef<APVCluster> c = clusEntry.second;
            auto bC = bApvClusters[n];

            EvFieldTraits<APVCluster>::pack(*c, bC, S); //?

            storedClustersIdx.emplace( c, n );  // TODO: do it within a pack()
            ++n;
        }
    }

    # if 0
    // Fill track & points if any (TODO: move to traits?)
    if( !e.tracks.empty() ) {
        auto bTracks = bEv.initTracks( e.tracks.size() );
        size_t nTrack = 0;
        for( auto trackPair : e.tracks ) {
            const TrackID_t tid = trackPair.first;
            const Track & track = trackPair.second;
            auto bTrack = bTracks[nTrack];

            // write track-specific fields
            auto bPoints = bTrack.initPoints( track.size() );
            size_t nPoint = 0;
            for( const TrackPoint & tp : track ) {
                auto bTP = bPoints[nPoint];
                // write track point specific fields
                bTP.setLocalR0( tp.lR[0] );
                bTP.setLocalR1( tp.lR[1] );
                bTP.setLocalR2( tp.lR[2] );
                bTP.setGlobalR0( tp.gR[0] );
                bTP.setGlobalR1( tp.gR[1] );
                bTP.setGlobalR2( tp.gR[2] );
                auto bClusRefs = bTP.initClusterRefs(tp.clusterRefs.size());
                size_t nCRef = 0;
                for( auto clusPair : tp.clusterRefs ) {
                    auto clusIt = storedClustersIdx.find( clusPair.second );
                    assert(storedClustersIdx.end() != clusIt);
                    bClusRefs.set(nCRef, clusIt->second);
                    ++nCRef;
                }
                bTP.setStationID( tp.station );
                // ...

                ++nPoint;
            }
            // ...

            ++nTrack;
        }
    }
    # endif
}

}
}
#endif

