#include "na64event/pools.hh"
#include "na64event/event.hh"

#include <log4cpp/Category.hh>

namespace na64dp {

HitBanks::HitBanks( size_t nSADCHits
                  , size_t nAPVHits
                  , size_t nStwTDCHits
                  , size_t nSADCFittingEntries
                  , size_t nAPVClusters
                  , size_t nCaloEDeps
                  , size_t nTrackPoints
                  , size_t nTracks
                  ) : bankSADC(nSADCHits)
                    , bankAPV(nAPVHits)
                    , bankStwTDC(nStwTDCHits)
                    , bankSADCFitting(nSADCFittingEntries)
                    , bankAPVClusters(nAPVClusters)
                    , bankCaloEDeps(nCaloEDeps)
                    , bankTrackPoints(nTrackPoints)
                    , bankTracks(nTracks) {
    #ifndef NDEBUG
    auto & L = log4cpp::Category::getInstance("banks");
    L.info( "%zu SADC hits pre-allocated", bankSADC.capacity() );
    L.info( "%zu APV hits pre-allocated", bankAPV.capacity() );
    L.info( "%zu StwTDC hits pre-allocated", bankStwTDC.capacity() );
    L.info( "%zu SADC fitting entries pre-allocated", bankSADCFitting.capacity() );
    L.info( "%zu APV clusters pre-allocated", bankAPVClusters.capacity() );
    L.info( "%zu Calo energy depositions pre-allocated", bankCaloEDeps.capacity() );
    L.info( "%zu track points pre-allocated", bankTrackPoints.capacity() );
    L.info( "%zu tracks pre-allocated", bankTracks.capacity() );
    // ...
    #endif
}

HitBanks::HitBanks() : HitBanks(10, 10, 10, 10, 10, 10, 10, 10) {}

void
HitBanks::clear() {
    bankSADC.clear();
    bankAPV.clear();
    bankStwTDC.clear();
    bankSADCFitting.clear();
    bankAPVClusters.clear();
    bankCaloEDeps.clear();
    bankTrackPoints.clear();
    bankTracks.clear();
    // ...
    #ifndef NDEBUG
    auto & L = log4cpp::Category::getInstance("banks");
    // XXX, to frequent, creates flood
    #if 1
    msg_debug(L, "Memory banks cleared.");
    msg_debug(L, "SADC bank capacity after clearing: %zu", bankSADC.capacity());
    msg_debug(L, "APV bank capacity after clearing: %zu", bankAPV.capacity());
    msg_debug(L, "StwTDC bank capacity after clearing: %zu", bankStwTDC.capacity());
    msg_debug(L, "SADC fitting bank capacity after clearing: %zu", bankSADCFitting.capacity());
    msg_debug(L, "APV clusters bank capacity after clearing: %zu", bankAPVClusters.capacity());
    msg_debug(L, "CaloEDep bank capacity after cleaning: %zu", bankCaloEDeps.capacity() );
    msg_debug(L, "track points bank capacity after clearing: %zu", bankTrackPoints.capacity());
    // ...
    #endif
    #endif
}

}
