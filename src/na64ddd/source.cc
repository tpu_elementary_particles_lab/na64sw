#include "na64ddd/source.hh"

#ifdef DaqDataDecoding_FOUND

#include "na64event/hitTraits.hh"
#include "na64ddd/chipTraits.hh"
#include "na64detID/TBName.hh"
#include "na64detID/TBNameErrors.hh"
#include "na64util/str-fmt.hh"

#include <stdexcept>

namespace na64dp {

namespace ddd {

static struct EvTypeCodeEntry {
    CS::DaqEvent::EventType code;
    const char * name;
} _evTypeCodeEntries[] = {
    { CS::DaqEvent::START_OF_RUN        , "START_OF_RUN" },
    { CS::DaqEvent::END_OF_RUN          , "END_OF_RUN" },
    { CS::DaqEvent::START_OF_RUN_FILES  , "START_OF_RUN_FILES" },
    { CS::DaqEvent::END_OF_RUN_FILES    , "END_OF_RUN_FILES" },
    { CS::DaqEvent::START_OF_BURST      , "START_OF_BURST" },
    { CS::DaqEvent::END_OF_BURST        , "END_OF_BURST" },
    { CS::DaqEvent::PHYSICS_EVENT       , "PHYSICS_EVENT" },
    { CS::DaqEvent::CALIBRATION_EVENT   , "CALIBRATION_EVENT" },
    { CS::DaqEvent::END_OF_LINK         , "END_OF_LINK" },
    { CS::DaqEvent::EVENT_FORMAT_ERROR  , "EVENT_FORMAT_ERROR" },
};

DDDEventSource::DDDEventSource( HitBanks & banks
                              , calib::Manager & mgr
                              , const std::string & coralMapsDir
                              , const std::vector<std::string> & inputs
                              , std::vector<std::string> permittedEvTypes
                              , const std::set<std::string> & tbnames2ignore )
                                            : AbstractEventSource(banks)
                                            , calib::Handle<nameutils::DetectorNaming>("default", mgr)
                                            , _calibMgr(mgr)
                                            , _decodingFailures(0)
                                            , _tbnames2ignore(tbnames2ignore) {
    // Instantiate DDD manager object
    _dddMgr = new CS::DaqEventsManager();
    // Set the CORAL maps directory providing vital decoding info
    _dddMgr->SetMapsDir(coralMapsDir);
    // Initialize the manager object with all the data files
    for( const std::string & ifName : inputs ){
        _dddMgr->AddDataSource(ifName);
    }
    // Retain permit event types
    std::map<std::string, CS::DaqEvent::EventType> typeMap;
    for( size_t i = 0
       ; i < sizeof(_evTypeCodeEntries)/sizeof(EvTypeCodeEntry)
       ; ++i ) {
        const EvTypeCodeEntry & ee = _evTypeCodeEntries[i];
        typeMap[ee.name] = ee.code;
    }
    for( auto strType : permittedEvTypes ) {
        auto it = typeMap.find(strType);
        if( typeMap.end() == it ) {
            NA64DP_RUNTIME_ERROR (
                    , "Unknown event type specification for permission: \"%s\""
                    , strType.c_str() );
        }
        _permitTypes.insert( it->second );
    }
}

void
DDDEventSource::handle_update( const nameutils::DetectorNaming & names ) {
    calib::Handle<nameutils::DetectorNaming>::handle_update(names);
    _detIDsCache.kSADC = names.chip_id( "SADC" );
    _detIDsCache.kAPV = names.chip_id( "APV" );
    _detIDsCache.kStwTDC = names.chip_id( "NA64TDC" );

    _detIDsCache.kECAL = names.kin_id("ECAL").second;
    
    _detIDsCache.kSt = names.kin_id("ST").second;
    _detIDsCache.kStt = names.kin_id("STT").second;

    _detIDsCache.kMM = names.kin_id("MM").second;
    _detIDsCache.kGEM = names.kin_id("GM").second;
    // ...
}

void
DDDEventSource::_event_fill( Event & e, CS::Chip::Digits & digits ) {
    assert( e.id.run_no() );  // event ID has to be set prior to get_mappings() call.
    //_log.info( "Reading %d digits", (int) digits.size() );  // TODO: debug
    const nameutils::DetectorNaming & nm = **this;
    for( auto it = digits.begin(); digits.end() != it; ++it  ) {
        DetID did;
        const std::string dddPlaneName = it->second->GetDetID().GetName();
        if( _tbnames2ignore.end() != _tbnames2ignore.find(dddPlaneName) ) {
			// ^^^ TODO: perform the partial/wildcard comparison here
			continue;  // omit decoding
		}
        // Attempt first conversion stage
        try {
            did.id = nm[dddPlaneName];
        } catch( errors::TBNameParsingFailure & err ) {
            auto unknownDetIt = _unknownDets.find( err.culprit() );
            if( _unknownDets.end() == unknownDetIt ) {
                _unknownDets.emplace( err.culprit(), 1 );
                _log.warn( "Failed to transcode TBName \"%s\". Other"
                        " occurrences will be quietly ignored."
                     , it->second->GetDetID().GetName().c_str() );
            } else {
                ++(unknownDetIt->second);
            }
            continue;
        }
        // ^^^ Value of `did' is not yet complete detector ID actually -- the
        // name itself does not not always fully identify a particular
        // detector part for certain hit. We have to fill ID's part called
        // `payload' w.r.t. the hit content.
        DetChip_t chipID = did.chip();
        DetID_t ir;
        char suppBf[256] = "";
        try {
            if( _detIDsCache.kSADC == chipID) {
                ir = _fill_hit<CS::ChipSADC::Digit>( e, did, _inserterSADC
                                                   , it->second );                           
                if( !ir ) {
                    // insertion failed -- gain some common information on the
                    // SADC digit for user's consideration.
                    const CS::ChipSADC::Digit & sadc
                        = dynamic_cast<const CS::ChipSADC::Digit &>(*(it->second));
                    snprintf( suppBf, sizeof(suppBf)
                            , "overflow=%s, suppression=%s, sourceID=%#x,"
                              " port=%#x, chip=%#x, channel=%#x, x=%d, y=%d"
                            , sadc.GetChannel() ? "yes" : "no"
                            , sadc.GetSuppression() ? "yes" : "no"
                            , sadc.GetSourceID()
                            , sadc.GetPort()
                            , sadc.GetChip()
                            , sadc.GetChannel()
                            , sadc.GetX()
                            , sadc.GetY()
                            );
                    _log.error( "Hit insertion failed; %s", suppBf );
                }
            } else if( _detIDsCache.kAPV == chipID ) {

                ir = _fill_hit<CS::ChipAPV::Digit>( e, did, _inserterAPV
                                                  , it->second );
              
                
                if( !ir ) {
                    _log.error( "APV hit insertion failed." );
                }
            } else if( _detIDsCache.kStwTDC == chipID ) {
				ir = _fill_hit<CS::ChipNA64TDC::Digit>( e, did, _inserterStwTDC
                                                      , it->second );
                if( !ir ) {
					// FIXIT! Condition just to kill an error with STT naming
					if ( did.kin() == _detIDsCache.kStt ) {}
					else {
						_log.error( "StwTDC hit insertion failed." ); 
                    }
				}
			/* } else if( ... other chips ... ) { */       
            
            } else {
                NA64DP_RUNTIME_ERROR(
                        , "Translation of data from chip \"%s\" (code %#x) is"
                          " not yet implemented (detector \"%s\")."
                        , nm.chip_features(chipID).name.c_str()
                        , chipID, it->second->GetDetID().GetName().c_str() );
            }
        } catch( const errors::HitKeyIsNotUniqe & err ) {
            _log.error( "Non-unique detector mapping revealed for"
                    " digit on \"%s\" (chip %#x \"%s\", kin %#x \"%s\")."
                    , it->second->GetDetID().GetName().c_str()
                    , chipID, nm.chip_features(chipID).name.c_str()
                    , did.kin()
                    , nm.kin_features(std::make_pair(did.chip(), did.kin())).name.c_str()
                    );
            throw;
        }
        if( !ir ) {
			// TODO: Dirty place to kill  concerning STT0. FIXIT
			if ( did.kin() == _detIDsCache.kStt ) {
			} else {
				_log.error( "Failed to impose hit of TBName \"%s\" (chip=%#x \"%s\","
						" kin=%#x \"%s\" due to incomplete traits or bad digit data."
						" DDD info: {%s}."
						, it->second->GetDetID().GetName().c_str()
						, did.chip(), nm.chip_features(did.chip()).name.c_str()
						, did.kin(), nm.kin_features(std::make_pair(did.chip(), did.kin())).name.c_str()
						, suppBf
						);
            }
        }
    }
}

bool
DDDEventSource::read( Event & evRef ) {
    bool decoded = false;
    do {
        if( ! _dddMgr->ReadEvent() ) {
            return false;  // end of raw input
        }
        decoded = _dddMgr->DecodeEvent();
        if( !decoded ) {
            // NOTE: internal counter of the manager is used here to identify
            // event's number
            _log.warn( "Event source %p: failed to decode an event #%zu."
                     , this, _dddMgr->GetEventsCounter() );
            ++_decodingFailures;
            continue;  // skip events with decoding problems
        }
        auto & dddEvRef = _dddMgr->GetEvent();

        // skip unpermitted event types, if at least one type is set (otherwise,
        // pass all)
        if( !_permitTypes.empty() ) {
            auto it = _permitTypes.find(dddEvRef.GetType());
            if( it == _permitTypes.end() ) {
                continue;
            }
        }

        if( dddEvRef.GetRunNumber() > na64sw_g_runNo_max )
            NA64DP_RUNTIME_ERROR( "Run number exceeds expected maximum value"
                    " (%lu > %lu)."
                    , (unsigned long) dddEvRef.GetRunNumber()
                    , (unsigned long) na64sw_g_runNo_max );
        if( dddEvRef.GetBurstNumber() > na64sw_g_spillNo_max )
            NA64DP_RUNTIME_ERROR( "Spill number exceeds expected maximum value"
                    " (%lu > %lu)."
                    , (unsigned long) dddEvRef.GetBurstNumber()
                    , (unsigned long) na64sw_g_spillNo_max );
        if( dddEvRef.GetEventNumberInBurst() > na64sw_g_eventNo_max )
            NA64DP_RUNTIME_ERROR( "Event-in-spill number exceeds expected"
                    " maximum value (%lu > %lu)."
                    , (unsigned long) dddEvRef.GetEventNumberInBurst()
                    , (unsigned long) na64sw_g_eventNo_max );
        evRef.id = EventID( dddEvRef.GetRunNumber()
                          , dddEvRef.GetBurstNumber()
                          , dddEvRef.GetEventNumberInBurst()
                          );
        evRef.time = dddEvRef.GetTime();
        // This may cause updates on all calibration subscribers including
        // the current data source object:
        _calibMgr.event_id( evRef.id );
        _event_fill( evRef, _dddMgr->GetEventDigits() );
    } while( !decoded );
    return decoded;
}

}  // namespace ddd
}  // namespace na64dp

using na64dp::ddd::DDDEventSource;
REGISTER_SOURCE( DDDEventSource
               , banks
               , calibManager
               , yamlNode
               , ids
               , "A COMPASS DaqDataDecoding lib data format used by NA64 for"
                 " storing raw events data."
               ) {
    if( ! yamlNode["CORALMapsDir"] ) {
        NA64DP_RUNTIME_ERROR("Mandatory \"CORALMapsDir\" parameter is not"
                " provided for DaqDataDecoding data source object");
    }
    auto mapsDirs = na64dp::util::expand_names(yamlNode["CORALMapsDir"].as<std::string>());
    if( mapsDirs.size() != 1 ) {
        NA64DP_RUNTIME_ERROR( "CORAL maps dir \"%s\" expanded to %zu path strings; has to be"
                              " exactly one."
                , yamlNode["CORALMapsDir"].as<std::string>().c_str()
                , mapsDirs.size() );
    }
    std::string coralMapsDir = mapsDirs[0];
    std::vector<std::string> permitEvTypes;
    if( yamlNode["permitEvTypes"] ) {
        permitEvTypes = yamlNode["permitEvTypes"].as<std::vector<std::string>>();
    }
    std::set<std::string> tbnames2ignore;
    if( yamlNode["tbnames2ignore"] ) {
		auto v = yamlNode["tbnames2ignore"].as<std::vector<std::string>>();
		tbnames2ignore = std::set<std::string>(v.begin(), v.end());
	}

    char bf[1024], permdEvTypesStr[1024];
    size_t cPos = 0;
    for(auto entityID : ids) {
        cPos += snprintf( bf + cPos, sizeof(bf) - cPos
                        , "\"%s\", "
                        , entityID.c_str() );
    }
    cPos = 0;
    for( auto petId : permitEvTypes ) {
        cPos += snprintf( permdEvTypesStr + cPos, sizeof(permdEvTypesStr) - cPos
                        , "\"%s\", "
                        , petId.c_str() );
    }
    DDDEventSource * src;
    try {
        src = new DDDEventSource( banks
                                , calibManager
                                , coralMapsDir
                                , ids
                                , permitEvTypes
                                , tbnames2ignore
                                );
    } catch( std::exception & e ) {
        log4cpp::Category::getInstance("sources").error( "Unable to instantiate"
                " DaqDataDecoding data source object with arguments:"
                " {calibManagerPtr=%p, CORALMapsDir=\"%s\", inputFiles=[%s],"
                " permittedEventTypes=[%s]}"
                , &calibManager
                , coralMapsDir.c_str()
                , bf
                , permdEvTypesStr
                );
        throw;
    }
    log4cpp::Category::getInstance("sources").info( "Instantiated"
                " DaqDataDecoding data source object with arguments:"
                " {calibManagerPtr=%p, CORALMapsDir=\"%s\", inputFiles=[%s],"
                " permittedEventTypes=[%s]}; object ptr is %p"
                , &calibManager
                , coralMapsDir.c_str()
                , bf
                , permdEvTypesStr
                , src
                );
    return src;
}

#else

#error "DDD disabled"

#endif  // DAQ_DATA_DECODING
