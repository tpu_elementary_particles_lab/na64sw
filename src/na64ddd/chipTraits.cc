#include "na64ddd/chipTraits.hh"

#include "na64detID/cellID.hh"
#include "na64detID/wireID.hh"

#ifdef DaqDataDecoding_FOUND

namespace na64dp {
namespace ddd {

bool
CSDigitTraits<CS::ChipSADC::Digit>::complete_detector_id( const DDDEventSource::NameCache & c
                                                        , DetID & did
                                                        , const CS::ChipSADC::Digit & dgt ) {
    assert( c.kSADC == did.chip() );
    CellID cID;
    cID.set_x(dgt.GetX());
    cID.set_y(dgt.GetY());
    if( did.is_payload_set() && CellID(did.payload()).is_z_set() ) {
        cID.set_z(CellID(did).get_z());
    }
    // no z set except for ECAL for now (TODO: hodoscopes?)
    if( c.kECAL == did.kin() ) {
        assert( (!did.is_payload_set()) || !CellID(did.payload()).is_z_set() );
        assert( did.number() < 2 );  // shall be either main part, either preshower
        cID.set_z( 0 == did.number() ? 0 : 1 );
        did.number(0);  // both parts of the ECAL are now single station
    }
    did.payload( cID.cellID );
    return true;
}
    /// Copies waveform
void
CSDigitTraits<CS::ChipSADC::Digit>::impose_hit_data( const CS::ChipSADC::Digit & sadc
                                                   , SADCHit & sadcHit ) {
    assert(sadc.GetSamples().data());
    std::copy( sadc.GetSamples().data()
             , sadc.GetSamples().data() + 32  // todo: hardcoded value -- # of samples
             , sadcHit.wave );
}

bool
CSDigitTraits<CS::ChipAPV::Digit>::complete_detector_id( const DDDEventSource::NameCache & c
                                                       , DetID & did
                                                       , const CS::ChipAPV::Digit & apv ) {
    assert( c.kAPV == did.chip() );
    if( c.kMM == did.kin() ) {
        // MM02X, MM03Y, etc.
        WireID wID(did.payload());
        assert(wID.proj_is_set());
        wID.wire_no( apv.GetChannel() );
        did.payload( wID.id );
        return true;
    } else if( c.kGEM == did.kin() ) {
        // GM01Y1__, GM02X1__, etc.
        // The last 3 chars are always '1__'
        WireID wID(did.payload());
        assert(wID.proj_is_set());
        wID.wire_no( apv.GetChannel() );
        did.payload( wID.id );
        return true;
    }
    return false;  // shall be considered as not being implemented behaviour
}

void
CSDigitTraits<CS::ChipAPV::Digit>::impose_hit_data( const CS::ChipAPV::Digit & apv
                                                  , APVHit & apvHit ) {
    apvHit.rawData.wireNo = apv.GetChannel();
    std::copy( apv.GetAmplitude()
             , apv.GetAmplitude() + 3  // sizeof(APVRawData::samples)
             , apvHit.rawData.samples );
}

bool
CSDigitTraits<CS::ChipNA64TDC::Digit>::complete_detector_id( const DDDEventSource::NameCache & c
                                                           , DetID & did
                                                           , const CS::ChipNA64TDC::Digit & stwtdc ) {
    assert( c.kStwTDC == did.chip() );
    if( c.kSt == did.kin() ) {
		WireID wID(did.payload());
        assert(wID.proj_is_set());
        wID.wire_no( stwtdc.GetChannel() );
        did.payload( wID.id );
        return true;
    }

	if( c.kStt == did.kin() ) {
		WireID wID(did.payload());
        assert(wID.proj_is_set());
        wID.wire_no( stwtdc.GetChannel() );
        did.payload( wID.id );
        return true;
    }
    
    return false;  // shall be considered as not being implemented behaviour
}

void
CSDigitTraits<CS::ChipNA64TDC::Digit>::impose_hit_data( const CS::ChipNA64TDC::Digit & stwtdc
                                                      , StwTDCHit & stwtdcHit ) {
    // TODO: Design type of straw hit
    stwtdcHit.rawData.wireNo = stwtdc.GetChannel();
    stwtdcHit.rawData.time   = stwtdc.GetTime();
}

}  // namespace na64::ddd
}  // namespace na64

#endif  // SUPPORT_DDD_FORMAT

