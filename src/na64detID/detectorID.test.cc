#include "na64detID/detectorID.hh"
#include "na64detID/cellID.hh"
#include "na64detID/wireID.hh"

#include <gtest/gtest.h>

namespace na64dp {

// Checks that by default detector ID is initialized to zero.
TEST(DetectorsID, defaultInitializationIsZero) {
    DetID id;
    EXPECT_FALSE(id.is_chip_set());
    EXPECT_FALSE(id.is_kin_set());
    EXPECT_FALSE(id.is_number_set());
    EXPECT_FALSE(id.is_payload_set());
}

// Checks that there are no overlaps within detector ID bitfields
TEST(DetectorsID, noFieldsOverlap) {
    DetID id;

    id.chip(0x1);
    id.kin(0x1);
    id.number(0x1);
    id.payload(0x1);

    EXPECT_EQ(1, id.chip());
    EXPECT_EQ(1, id.kin());
    EXPECT_EQ(1, id.number());
    EXPECT_EQ(1, id.payload());

    id.unset_chip();
    id.kin(aux::gKinIDMax);
    id.unset_number();
    id.payload(aux::gPayloadMax);

    EXPECT_FALSE(id.is_chip_set());
    EXPECT_EQ(aux::gKinIDMax, id.kin());
    EXPECT_FALSE(id.is_number_set());
    EXPECT_EQ(aux::gPayloadMax, id.payload());

    id.chip(aux::gChipIDMax);
    id.unset_kin();
    id.number(aux::gDetNumMax);
    id.unset_payload();

    EXPECT_EQ(aux::gChipIDMax, id.chip());
    EXPECT_FALSE(id.is_kin_set());
    EXPECT_EQ(aux::gDetNumMax, id.number());
    EXPECT_FALSE(id.is_payload_set());
}

}  // namespace na64dp

