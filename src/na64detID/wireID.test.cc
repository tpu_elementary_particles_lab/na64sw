#include "na64detID/wireID.hh"

#include <gtest/gtest.h>

namespace na64dp {

// Tests WireID for wire-type detectors
TEST(WireID, getSet) {
    //ASSERT_GT( WireID::WireMax, 64 );  // enough to store MM hits
    //ASSERT_GT( WireID::WireMax, 64 );  // enough to store GM hits
    const char projs[] = "XYUV";
    for( size_t i = 0; i <= WireID::WireMax; ++i ) {
        for( const char * prj = projs; '\0' != *prj; ++prj ) {
            WireID wid;
            EXPECT_FALSE( wid.proj_is_set() );
            EXPECT_FALSE( wid.wire_no_is_set() );

            wid.proj(WireID::proj_code(*prj));
            wid.wire_no(i);
            EXPECT_EQ( wid.wire_no(), i );
            EXPECT_EQ( WireID::proj_label(wid.proj()), *prj );

            WireID::Projection conjProjCode =
                WireID::conjugated_projection_code(wid.proj());
            char conjProjLabel = WireID::proj_label(conjProjCode);
            switch( conjProjLabel ) {
                case 'X' : EXPECT_EQ( 'Y', *prj ); break;
                case 'Y' : EXPECT_EQ( 'X', *prj ); break;
                case 'U' : EXPECT_EQ( 'V', *prj ); break;
                case 'V' : EXPECT_EQ( 'U', *prj ); break;
            };

            WireID wid2;
            wid2.wire_no(i);
            wid2.proj(WireID::proj_code(*prj));
            EXPECT_EQ( wid2.wire_no(), i );
            EXPECT_EQ( WireID::proj_label(wid2.proj()), *prj );

            wid.unset_wire_no();
            EXPECT_TRUE( wid.proj_is_set() );
            EXPECT_FALSE( wid.wire_no_is_set() );

            wid.wire_no(i);
            wid.unset_proj();
            EXPECT_FALSE( wid.proj_is_set() );
            EXPECT_TRUE( wid.wire_no_is_set() );
        }
    }
}

TEST(WireID, payloadStrConversions) {
    char buffer[32] = "";
    WireID::to_string( 0x0, buffer, sizeof(buffer) );
    EXPECT_EQ( '\0', buffer[0] );

    const char projections[] = "XYUV";
    uint16_t wNos[] = { 0, 123, 42, WireID::WireMax };
    for( const char * prj = projections; *prj != '\0' ; ++prj ) {
        for( size_t i = 0; i < sizeof(wNos)/sizeof(*wNos); ++i ) {
            WireID wid;
            wid.proj( WireID::proj_code( *prj ) );
            wid.wire_no( wNos[i] );
            WireID::to_string( wid.id, buffer, sizeof(buffer) );
            WireID ctrlWid( WireID::from_string(buffer) );
            EXPECT_EQ( ctrlWid.id, wid.id );
        }
    }
}

TEST(WireID, strConvErrors) {
    // unkown projection
    EXPECT_THROW( WireID::from_string("Z-12")
                , std::runtime_error );
    // exceeded limit
    char bf[32];
    snprintf( bf, sizeof(bf), "X-%d", WireID::WireMax + 1 );
    EXPECT_THROW( WireID::from_string(bf)
                , std::runtime_error );
    // extra symbols on tail
    EXPECT_THROW( WireID::from_string("Y-12a")
                , std::runtime_error );
}

}

