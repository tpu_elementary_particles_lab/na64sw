#include "na64detID/wireID.hh"

#include <cstdio>
#include <stdexcept>
#include <cstring>

#include "na64util/str-fmt.hh"

namespace na64dp {

char
WireID::proj_label( Projection p ) {
    switch(p) {
        case kX:
            return 'X';
        case kY:
            return 'Y';
        case kU:
            return 'U';
        case kV:
            return 'V';
        default:
            return '?';
    }
}

WireID::Projection
WireID::proj_code( char p ) {
    switch(p) {
        case 'X':
            return kX;
        case 'Y':
            return kY;
        case 'U':
            return kU;
        case 'V':
            return kV;
        default:
            return kUnknown;
    }
}

/**By "conjugated projection planes" we imply correspoance between two (usually
 * ortogonal) detector planes like X&Y and U&V.
 *
 * Currently, this function assumes that conjugated planes are pairwise, i.e.
 * there is no detector composed with, say, X, U and V planes triplet (NA58
 * actually uses it like so).
 *
 * @param pjCode projection code
 * @throws std::runtime_error if given numerical ID is not one of
 * the WireID::Projection values.
 * */
WireID::Projection
WireID::conjugated_projection_code( WireID::Projection pjCode ) {
    if( kX == pjCode ) return kY;
    if( kY == pjCode ) return kX;

    if( kU == pjCode ) return kV;
    if( kV == pjCode ) return kU;

    NA64DP_RUNTIME_ERROR(
            , "Not a projection code enum: %d"
            , pjCode );
}

void
WireID::append_completion_context( DetID did
                                 , std::map<std::string, std::string> & m ) {
    char bf[64];
    if( ! did.is_payload_set() )
        return;
    WireID wID(did.payload());

    if( wID.wire_no_is_set() ) {
        snprintf(bf, sizeof(bf), "%d", wID.wire_no() );
        m["wireNo"] = bf;
    }

    if( wID.proj_is_set() ) {
        snprintf(bf, sizeof(bf), "%c", WireID::proj_label(wID.proj()) );
        m["proj"] = bf;

        WireID::to_string( did.payload(), bf, sizeof(bf) );
        m["idPayload"] = bf;
    }
}

void
WireID::to_string( DetIDPayload_t pl
                 , char * buf
                 , size_t available ) {
    WireID wid(pl);
    if( !pl ) return;
    int used;
    if( wid.proj_is_set() && wid.wire_no_is_set() ) {
        used = snprintf( buf, available
                       , "%c-%d"
                       , proj_label(wid.proj())
                       , wid.wire_no() );
    } else if( wid.proj_is_set() && !wid.wire_no_is_set() ) {
        used = snprintf( buf, available
                       , "%c"
                       , proj_label(wid.proj())
                       );
    } else {
        NA64DP_RUNTIME_ERROR( "Bad form of wire identifier for to-string"
                              " conversion: projection is %sset, wire number is %sset."
                            , wid.proj_is_set() ? "" : "not "
                            , wid.wire_no_is_set() ? "" : "not "
                            );
    }
    if( (size_t) used >= available ) {
        NA64DP_RUNTIME_ERROR( "Buffer of length %zu is"
                              " insufficient for WireID string, result truncated."
                            , available );
    }
}

DetIDPayload_t
WireID::from_string( const char * widStr ) {
    /* Expected format:
     *  "Y-123"  -- standard, projection char and wire no
     *  "Y1__"   -- kludgy, only first letter must be interpreted
     *  "Y1__-123" -- kludgy, with wire number
     */
    assert(widStr);
    if('\0' == *widStr) return 0x0;
    WireID wid;
    // projection
    wid.proj(proj_code( widStr[0] ));
    if( (!wid.proj_is_set()) || kUnknown == wid.proj() ) {
        NA64DP_RUNTIME_ERROR( "Projection code label '%x' can not be"
                " interpreted.", widStr[0] );
    }
    // NOTE: for GEMs we have a somewhat weird naming scheme in ~2017, when
    // TBNames had a weird suffix "{proj}1__". To circumvent this feature, whe
    // do here a direct string comparison
    if( '-' != widStr[1] && '\0' != widStr[1] ) {
        if( strncmp( "1__", widStr+1, 3 ) ) {
            // comparison failure
            NA64DP_RUNTIME_ERROR( "Unable to interpret tail portion of string"
                " \"%s\" as wire number.", widStr+1 );
        }
        widStr += 3;
    }
    if('\0' == widStr[1]) return wid.id;  // only projection char was given
    if('-' != widStr[1]) {
        NA64DP_RUNTIME_ERROR( "Unable to interpret tail portion of string"
                " \"%s\" as wire number. Expected \"-<wireno>\"", widStr );
    }
    // wire no, if given
    char * tail;
    long int wireNo = strtol( widStr + 2, &tail, 10 );
    if( '\0' != *tail || tail == widStr + 2 ) {
        NA64DP_RUNTIME_ERROR( "Unable to interpret tail portion \"%s\" of string"
                " \"%s\" as wire number.", tail, widStr );
    }
    if( 0 > wireNo || wireNo > WireID::WireMax ) {
        NA64DP_RUNTIME_ERROR( "Wire number %ld exceed definition limits [0:%d)."
                , wireNo, WireMax );
    }
    wid.wire_no( (uint16_t) wireNo );
    return wid.id;
}

}

