#include "na64detID/cellID.hh"

#include <gtest/gtest.h>

namespace na64dp {

// Tests ECAL's CellID type get/set
TEST(SADCCellID, decipherCheck) {
    for( int x = 0; x < 0x1f; ++x ) {
        for( int y = 0; y < 0x1f; ++y ) {
            for( int i = 1; i < 0x1f; ++i ) {
                CellID cellID;
                cellID.set_x( x );
                cellID.set_y( y );
                cellID.set_z( i%2 );
                // check
                EXPECT_EQ( x, cellID.get_x() );
                EXPECT_EQ( y, cellID.get_y() );
                EXPECT_EQ( (i%2), cellID.get_z() );
            }
        }
    }
}

TEST(SADCCellID, payloadStrConversions) {
    char bf[32] = "";
    EXPECT_FALSE( CellID::from_string(bf) );

    const int idxs[] = {0, 1, 17, CellID::idxMax-1, CellID::idxMax};
    for( size_t ix = 0; ix < sizeof(idxs)/sizeof(*idxs); ++ix ) {
        for( size_t iy = 0; iy < sizeof(idxs)/sizeof(*idxs); ++iy ) {
            for( size_t iz = 0; iz < sizeof(idxs)/sizeof(*idxs); ++iz ) {
                CellID cid(idxs[ix], idxs[iy], idxs[iz]);
                CellID::to_string(cid.cellID, bf, sizeof(bf));
                CellID ctrl(CellID::from_string(bf));
                EXPECT_EQ( cid.cellID, ctrl.cellID ) << " cid={"
                    << cid.get_x() << ", " << cid.get_y()
                    << ", " << cid.get_z() << "} -> \""
                    << bf << "\" ->"
                    << " {" << ctrl.get_x() << ", " << ctrl.get_y()
                    << ", " << ctrl.get_z() << "}";
            }
        }
    }
    //ASSERT_EQ( CellID::from_string(":"), CellID(0,0,0).cellID );  // xxx? -- obscure feature
}

TEST(SADCCellID, payloadStrErrors) {
    //EXPECT_THROW( CellID::from_string("1-2-3")
    //            , std::exception
    //            ) << "(absence of prefix ignored)";
    // ^^^ TODO: I doubt that this really has to throw exception since for some
    //          kins a column suffix separator is really an optional
    EXPECT_THROW( CellID::from_string(":a")
                , std::exception
                ) << "(non-digit passed)";
    EXPECT_THROW( CellID::from_string(":1-a")
                , std::exception
                ) << "(non-digit passed)";
    EXPECT_THROW( CellID::from_string(":1-2-3a")
                , std::exception
                ) << "(tail symbols ignored)";
}

}
