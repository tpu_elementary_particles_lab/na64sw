#include "na64mc/notifications.hh"

#include <gtest/gtest.h>

namespace na64dp {
namespace mc {

class TestNotificationReciever : public iNotificationDestination {
public:
    Notification::Flags_t _recvd;
protected:
    virtual void receive_notification( Notification::Flags_t f ) override {
        _recvd |= f;
    }
public:
    TestNotificationReciever( Notification::Flags_t ff, Notifier & src )
        : iNotificationDestination(ff, src), _recvd(0x0) {}
};

TEST( MCNotifications, notificationsReceived ) {
    Notifier src;
    TestNotificationReciever rcv1( 
              Notification::eventSimulationEnded
            | Notification::eventProcessingEnded
            | Notification::runSimulationEnded
            , src )
        , rcv2(
              Notification::eventSimulationStarted
            | Notification::eventProcessingStarted
            | Notification::runSimulationEnded
            , src )
        ;
    // assure no notifications received so far
    ASSERT_TRUE( (!rcv1._recvd) && (!rcv2._recvd) );
    // Emit a notification and check that signal was received only by
    // subscriber who is interested in it
    src.notify( Notification::eventSimulationStarted );
    EXPECT_FALSE( rcv1._recvd );
    EXPECT_EQ( rcv2._recvd, Notification::eventSimulationStarted );
    src.notify( Notification::eventSimulationEnded );
    EXPECT_EQ( rcv1._recvd, Notification::eventSimulationEnded );
    EXPECT_EQ( rcv2._recvd, Notification::eventSimulationStarted );
    // Reset receiver stubs
    rcv1._recvd = rcv2._recvd = 0x0;
    // Assure the simulataneous notification is received properly (at once)
    src.notify( Notification::eventSimulationStarted
              | Notification::eventSimulationEnded );
    EXPECT_EQ( rcv1._recvd, Notification::eventSimulationEnded );
    EXPECT_EQ( rcv2._recvd, Notification::eventSimulationStarted );
    // ... (done?)
}

}
}
