#include "na64mc/g4api/UISession.hh"

#include <regex>

namespace na64dp {
namespace mc {

UISession::UISession( const std::string & sysCat) 
        : _logCat(log4cpp::Category::getInstance(std::string(sysCat))) {}

static G4String
trim_trailing_newline(const G4String& msg) {
    std::regex newlines_re("\n+$");
    return std::regex_replace(msg, newlines_re, "");
}

G4int
UISession::ReceiveG4cout( const G4String& msg ) {
    _logCat << log4cpp::Priority::INFO << trim_trailing_newline(msg);
    return 0;
}

G4int
UISession::ReceiveG4cerr( const G4String& msg ) {
    _logCat << log4cpp::Priority::WARN << trim_trailing_newline(msg);
    return 0;
}

}
}

