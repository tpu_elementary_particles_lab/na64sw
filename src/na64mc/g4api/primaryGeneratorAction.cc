#include "na64mc/g4api/primaryGeneratorAction.hh"

#include <G4GeneralParticleSource.hh>

namespace na64dp {
namespace mc {

const std::string UIModuleTraits<G4VUserPrimaryGeneratorAction>::command
    = "usePrimaryGeneratorAction";
const std::string UIModuleTraits<G4VUserPrimaryGeneratorAction>::description
    = "Creates a Geant4 primary generator action instance of one of predefined"
      " types: \"GPS\" is the default for this library.";
const std::string UIModuleTraits<G4VUserPrimaryGeneratorAction>::default_
    = "GPS";

G4VUserPrimaryGeneratorAction *
UIModuleTraits<G4VUserPrimaryGeneratorAction>::instantiate( const G4String & name 
                                                          , ModularConfig & //msgr
                                                          ) {
    if( "GPS" != name )
        throw std::runtime_error( "Currently, only \"GPS\" "
                " primary generator action is supported" );
    return new PrimaryGeneratorAction();
}


PrimaryGeneratorAction::PrimaryGeneratorAction() {
    _gps = new G4GeneralParticleSource();
}

PrimaryGeneratorAction::~PrimaryGeneratorAction() {
    delete _gps;
}

void
PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent) {
    _gps->GeneratePrimaryVertex(anEvent);
}

}
}
