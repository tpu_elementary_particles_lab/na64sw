#include "na64mc/g4api/run.hh"

namespace na64dp {
namespace mc {

Run::Run( const std::string & runConfig
        , const std::string & onlineMonitDest
        , calib::Manager & mgr
        , Notifier & nfr
        )
            : MCEventProcessor(runConfig, onlineMonitDest, mgr)
            , EventBuilder( MCEventProcessor::banks()
                          , mgr
                          , MCEventProcessor::online_monit_ptr() )
            , _mcNotifier(nfr)
            {}

void
Run::RecordEvent(const G4Event * mcEvent) {
    auto & L = log4cpp::Category::getInstance("na64mc.notifications");

    L.debug( "Notifying of \"event post-processing started\".");
    _mcNotifier.notify(Notification::eventProcessingStarted);
    if( MCEventProcessor::process_event(mcEvent) ) {
        ++G4Run::numberOfEvent;

        EventID eid = calib_mgr().event_id();
        eid.event_no( eid.event_no() + 1 );
    }
    L.debug( "Notifying of \"event post-processing ended\".");
    _mcNotifier.notify(Notification::eventProcessingEnded);
}

}
}

