#include "na64mc/stepClassifier.hh"

#include <log4cpp/Category.hh>

namespace na64dp {
namespace mc {

const util::StringSubstFormat gGeant4StrPatFormat = { "$(", ")" };

StepClassifier::StepClassifier( const G4String & name
                              , const G4String & path )
        : GenericG4Messenger(path)
        , _name(name) {
    dir( name
       , "Commands responsible for eponymous MC step classifier instance" )
        .cmd<G4String>( "add"
                      , "Adds extractor to a classifier."
                      , "classifierType"
                      , "Extractor type name."
                      , ui_cmd_add_extractor
                      )
    .end(name);
}

StepHandlerKey
StepClassifier::operator()(const G4Step & step) {
    if( _extractors.empty() ) {
        throw std::runtime_error("Empty step classifier in use.");  // TODO: dedicated exception
    }
    StepHandlerKey k(_extractors.size(), {0x0});
    size_t n = 0;
    for( auto & ex : _extractors ) {
        k[n++] = ex->get_feature(step);
    }
    return k;
}

std::string
StepClassifier::format_string( const std::string & fmt
                             , const StepHandlerKey & key
                             ) {
    util::StrSubstDict context;
    size_t n = 0;
    char namebf[32], valbf[64];
    for(auto ePtr : _extractors) {
        ePtr->key_to_str(key[n], valbf, sizeof(valbf));
        snprintf(namebf, sizeof(namebf), "key%zu", n+1);
        context[namebf] = valbf;
        ++n;
    }
    return util::str_subst(fmt, context, true, &gGeant4StrPatFormat);
}

void
StepClassifier::ui_cmd_add_extractor( GenericG4Messenger * msgr_
                                    , const G4String & extractorTypeName
                                    ) {
    StepClassifier & classifier = dynamic_cast<StepClassifier&>(*msgr_);
    std::string basePath = classifier.GetRootPath() + classifier._name + "/";
    char namebf[32];
    snprintf(namebf, sizeof(namebf), "key%zu", classifier._extractors.size() + 1);
    // TODO: check for stateful
    auto extractorPtr = VCtr::self()
            .make<mc::StepClassifier::iStepFeatureExtractor>(extractorTypeName, namebf, basePath);
    classifier._extractors.push_back( extractorPtr );
    log4cpp::Category::getInstance("na64mc.stepping.classifiers")
        << log4cpp::Priority::INFO
        << "Extractor \"" << namebf << "\":" << extractorTypeName
        << " added to step classifier \"" << classifier._name << "\".";
}

void
StepClassifier::ui_cmd_add_multipart( GenericG4Messenger * //msgr_
                                    , const G4String &
                                    ) {
    //StepClassifier & classifier = dynamic_cast<StepClassifier&>(*msgr_);
    //classifier._extractors.push_back( ... );
    throw std::runtime_error("TODO: add multipart classifier");
}

StepClassifier &
StepClassifiers::add_classifier(const std::string & name) {
    auto it = find(name);
    if( end() != it ) {
        NA64DP_RUNTIME_ERROR("Classifier \"%s\" already exists.", name.c_str());
    }
    return emplace( std::piecewise_construct
                  , std::forward_as_tuple( name )
                  , std::forward_as_tuple( name
                                         , uiCmdPathPrefix
                                         )
                  ).first->second;

}

}
}

