#include "na64mc/messenger.hh"

#include "na64mc/messenger.hh"

//#include "na64mc/gdml/auxinfo/visStyle.hh"
//#include "na64mc/gdml/auxinfo/sensDet.hh"
//#include "na64mc/gdml/auxinfo/exportDefinition.hh"

#include "na64mc/g4api/sensitiveDetector.hh"
#include "na64mc/g4api/primaryGeneratorAction.hh"
#include "na64mc/g4api/eventAction.hh"
#include "na64mc/g4api/runAction.hh"
#include "na64mc/g4api/detectorConstruction.hh"
#include "na64mc/g4api/physicsList.hh"

#include "na64mc/g4api/steppingAction.hh"  // XXX

#include "na64app/app.hh"  // for modules loading

#include <G4UIcmdWithAString.hh>
#include <G4UIcmdWithoutParameter.hh>
#include <G4SDManager.hh>
#include <G4RunManager.hh>
#include <G4VModularPhysicsList.hh>
#include <G4ParallelWorldPhysics.hh>
#include <G4UnitsTable.hh>

#ifdef ROOT_FOUND
#include <TFile.h>
#endif

#include <cassert>

namespace na64dp {
namespace mc {

Messenger::Messenger( ModularConfig & cfg
                    , const G4String & thisDir
                    , const G4String & rootUIPath
                    )
        : GenericG4Messenger( rootUIPath )
        , _cfg(cfg)
        //, _physicsList(nullptr)
        //, _detectorConstruction(detectorConstruction)
        //, _pga(nullptr)
        //, _eventAction(nullptr)
        //, _runAction(nullptr)
        , _L(log4cpp::Category::getInstance("na64mc.messenger"))
        {
    // Create basic interface
    dir( thisDir, "UI extensions defined by na64sw::mc library." )
        .cmd<G4String>( "loadModule"
                      , "Loads a shared library object file. Typically used to"
                        " load NA64SW plugins containing handlers, GDML"
                        " extensions, sensitive detectors and other extensions."
                        " Searches in colon-separated paths list provided by"
                        " \"NA64SW_MODULES_DIRS_ENVVAR\" environment variable."
                      , "moduleName"
                      , ui_cmd_load_module
                      , preinit | init | idle
                      )
        .cmd<G4int>( "setHEPRandomSeed"
                   , "Sets the CLHEP random seed using"
                     " CLHEP::HepRandom::setTheSeed interface."
                   , "seed"
                   , ui_cmd_set_heprandom_seed
                   , preinit
                   )
    .end( thisDir )
    ;
    const G4String modulesBasePath = rootUIPath + thisDir + "/";
    // Populate modular configruation
    cfg.enable_module<G4VUserDetectorConstruction>( modulesBasePath );
    cfg.enable_module<G4VModularPhysicsList>( modulesBasePath );
    cfg.enable_module<G4UserSteppingAction>( modulesBasePath );
    cfg.enable_module<G4VUserPrimaryGeneratorAction>( modulesBasePath );
    cfg.enable_module<G4UserEventAction>( modulesBasePath );
    cfg.enable_module<G4UserRunAction>( modulesBasePath );
    // ...
    #if 0
    stepClassifiersPtr = new StepClassifiers("/na64sw/stepping/classifiers/");
    detectorConstructionPtr = detectorConstruction_;
    //                                                    _____________________
    // _________________________________________________/ UI messenger entries
    dir( "na64sw", "Common NA64SW environment commands." )
        .dir( "sysAPI"
            , "Commands related to base Geant4 system API: hooks, actions, etc."
            )
            .cmd<G4String>( "usePGA"
                          , "Creates and assigns primary generator action"
                            " instance. Must be called after physics list is"
                            " assigned."
                          , "PGAName"
                          , "GPS"
                          , ui_cmd_instantiate_primary_action
                          , preinit
                          )
            .cmd<G4String>( "useRunAction"
                          , "Sets the run action to use. Called after physics"
                            " list is choosen."
                          , "runAction"
                          , "na64sw"
                          , ui_cmd_use_run_action
                          , preinit )
            .cmd<G4String>( "useEventAction"
                          , "Creates and assigns event action instance. Must be called"
                            " after physics list is choosen."
                          , "evActionName"
                          , "na64sw"
                          , ui_cmd_instantiate_event_action
                          , preinit
                          )
            .cmd<G4String>( "usePhysicsList"
                          , "Use physics list instance. Besides a number of"
                            " standard supported, the special extensible"
                            " \"local\' type may be specified providing"
                            " runtime interface for assembling list with"
                            " various physics constructors. Note, that"
                            " parallel worlds are supported by all the"
                            " variants."
                          , "phListName"
                          , ui_cmd_use_ph_list
                          , preinit
                          )
            .cmd<G4String>( "useSteppingAction"
                  , "Creates and assigns extendable stepping action instance."
                    " Must be called after physics list is assigned."
                  , "stepActionName"
                  , "na64sw"
                  , ui_cmd_instantiate_stepping_action
                  , preinit
                  )
        .end( "sysAPI" )
        .dir( "processing", "Options for setting up a data processing pipeline." )
            .cmd<G4String>( "useCalibConfig"
                          , "Set the calibration data source."
                          , "calibCfg"
                          , ui_cmd_use_calibrations
                          , preinit | init | idle
                          )
        .end( "processing" )
        .dir( "gdmlAuxInfo", "NA64SW GDML auxinfo-management commands." )
            .cmd_nopar( "list"
                      , "Prints list of available aux info processors."
                      , ui_cmd_list_auxinfo
                      , anyAppState )
            .cmd<G4String>( "enable"
                          , "Enables one of the available auxiliary info processors"
                            " of the NA64SW."
                          , "processor"
                          , ui_cmd_enable_auxinfo_processor
                          , preinit | init | idle )
            .cmd<G4String>( "ignore"
                          , "Makes the app ignore certain auxinfo tag quietly."
                          , "processor"
                          , ui_cmd_ignore_auxinfo_processor
                          , preinit | init | idle )
        .end( "gdmlAuxInfo" )
        .dir( "physics", "NA64SW supplementary physics options" )
            .cmd_nopar( "createParallelPhysics"
                      , "Creates transportation physics for parallel worlds."
                      , ui_cmd_create_parallel_physics
                      , preinit
                      )
            // TODO .cmd<G4String>("listLists", "Prints list of available physics lists" ... )
            .cmd<G4String>( "add"
                          , "Adds module to physics list."
                          , "physicsName"
                          , ui_cmd_add_physics
                          , preinit
                          )
            // TODO: .cmd<G4String>( "listModules", "Prints list of available physics" ... )
        .end( "physics" )
        .dir( "checks", "Various checks hooks." )
            
        .end("checks")
        #ifdef ROOT_FOUND
        .cmd<G4String>( "openROOTFile"
                      , "Opens ROOT file with given name in \"recreate\" mode."
                      , "filename"
                      , ui_cmd_open_root_file
                      , preinit | init | idle )
        .cmd_nopar( "closeROOTFile"
                  , "Closes previously opened ROOT file."
                  , ui_cmd_close_root_file
                  , anyAppState )
        #endif
    .end( "na64sw" );
    //
    // Calibrations
    calibrationsManager.add_run_index( &_fsi );
    #endif
    
}

Messenger::~Messenger() {
}

//                                                                 ____________ 
// ______________________________________________________________/ UI Commands

void
Messenger::ui_cmd_load_module( GenericG4Messenger * //msgr_
                             , const G4String & strVal ) {
    //Messenger & msgr = *static_cast<Messenger*>(msgr_);
    log4cpp::Category & L = log4cpp::Category::getInstance("na64mc.messenger");
    L.info("Loading module \"%s\"...", strVal.c_str());
    std::set<std::string> mdls;
    mdls.insert(strVal);
    na64dp::util::load_modules( mdls, getenv(NA64SW_MODULES_DIRS_ENVVAR) );
}

void
Messenger::ui_cmd_set_heprandom_seed( GenericG4Messenger *
                                    , const G4String & strVal ) {
    std::string::size_type sz;
    long seedValue = std::stol( strVal, &sz );
    // See definition at CLHEP/Random/Random.h
    CLHEP::HepRandom::setTheSeed(seedValue);

    log4cpp::Category & L = log4cpp::Category::getInstance("na64mc.messenger");
    L << log4cpp::Priority::INFO
      << "HepRandom seed has been set to " << seedValue;
}

#if 0

void
Messenger::ui_cmd_use_run_action( GenericG4Messenger * //msgr_
                                , const G4String & //strVal
                                ) {
    Messenger & msgr = *static_cast<Messenger*>(msgr_);
    assert("na64sw" == strVal); // TODO: consider strVal to choose run action?
    if( msgr._runAction ) {
        G4Exception( __FUNCTION__
                   , "NA64SW197" 
                   , FatalErrorInArgument
                   , "An instance of run action already exists."
                   );
    }
    G4RunManager::GetRunManager()->SetUserAction(
            msgr._runAction = new RunAction( msgr._cfgRef
                    , msgr.calibrationsManager
                    , msgr.notifier ) );
}

void
Messenger::ui_cmd_use_calibrations( GenericG4Messenger * msgr_
                                  , const G4String & strVal ) {
    Messenger & msgr = *static_cast<Messenger*>(msgr_);
    // TODO: check file available

    std::unordered_map<std::string, na64dp::calib::YAMLCalibInfoCtr> calibDataIdxCtrs;
    msgr.calibrationsManager.add_loader( "generic", &msgr._genericCalibLoader );
    na64dp::util::setup_default_calibration_indexes( calibDataIdxCtrs
                                                   , msgr._genericCalibLoader
                                                   , msgr.calibrationsManager );
    try {
        YAML::Node root = YAML::LoadFile( strVal );
        configure_from_YAML( root
                           , &msgr._fsi
                           , &msgr._genericCalibLoader
                           , "generic"
                           , calibDataIdxCtrs
                           );
    } catch( std::exception & e ) {
        char errBf[128];
        snprintf( errBf, sizeof(errBf)
                , "Failed to configure calibration manager from document \"%s\"."
               , strVal.c_str() );
        G4Exception( __FUNCTION__
                   , "NA64SW301" 
                   , JustWarning
                   , errBf
                   );
        throw;
    }
}

void
Messenger::ui_cmd_list_auxinfo( GenericG4Messenger *
                              , const G4String &
                              ) {
    //Messenger & msgr = *static_cast<Messenger *>(msgr_);
    // TODO: make the `_listAuxInfoProcessorCmd` to accept an optional
    // parameter meaning that list must be related to "available" or
    // "enabled" sets
    G4cout << "Available GDML aux info processors names: "
           << "style, sensitive-detector."   // TODO: add here new processor names
           << std::endl;
    return;
}

void
Messenger::ui_cmd_enable_auxinfo_processor( GenericG4Messenger * msgr_
                                          , const G4String & strVal
                                          ) {
    Messenger & msgr = *static_cast<Messenger *>(msgr_);

    iAuxInfoProcessor * newProc = nullptr;
    auto it = msgr.detectorConstructionPtr->find(strVal);
    if( msgr.detectorConstructionPtr->end() != it ) {
        char errBf[128];
        snprintf( errBf, sizeof(errBf)
                , "Omitting repeated enable for aux info processor: \"%s\"."
                , strVal.c_str() );
        G4Exception( __FUNCTION__
                   , "NA64SW191" 
                   , JustWarning
                   , errBf
                   );
        return;
    }
    // Instantiate
    newProc = VCtr::self().make<iAuxInfoProcessor>( strVal
            , "/na64sw/gdmlAuxInfo/"
            , static_cast<RuntimeState&>(msgr)
            );
    msgr.detectorConstructionPtr->emplace(strVal, newProc);
    return;
}

void
Messenger::ui_cmd_ignore_auxinfo_processor( GenericG4Messenger * msgr_
                                          , const G4String & strVal
                                          ) {
    Messenger & msgr = *static_cast<Messenger *>(msgr_);
    assert(msgr.detectorConstructionPtr);
    msgr.detectorConstructionPtr->ignore_auxinfo(strVal);
}

void
Messenger::ui_cmd_create_parallel_physics( GenericG4Messenger * msgr_
                                         , const G4String & ) {
    Messenger & msgr = *static_cast<Messenger*>(msgr_);
    for( auto pParWorld : msgr.detectorConstructionPtr->geode_cfg().parallelWorlds ) {
        const std::string & pWorldName = pParWorld.first
                        //, & pWorldSetup = pParWorld.second
                        ;
        msgr._physicsList->RegisterPhysics( new G4ParallelWorldPhysics(pWorldName, false) );
        // ^^^ TODO: second argument defines whether the materials of the parallel
        // world must be considered. Possibly add this option to messenger?
        msgr._L << log4cpp::Priority::INFO
                << "New parallel physics instance \"" << pWorldName
                << "\" created.";
    }
}

void
Messenger::ui_cmd_use_ph_list( GenericG4Messenger * msgr_
                             , const G4String & strVal
                             ) {
    Messenger & msgr = *static_cast<Messenger*>(msgr_);
    if( msgr._physicsList ) {
        G4Exception( __FUNCTION__
                   , "NA64SW194" 
                   , FatalErrorInArgument
                   , "Physics list is already instantiated."
                   );
    }
    msgr._physicsList = VCtr::self().make<G4VModularPhysicsList>(strVal);
    G4RunManager::GetRunManager()->SetUserInitialization(msgr._physicsList);
}

void
Messenger::ui_cmd_add_physics( GenericG4Messenger * msgr_
                             , const G4String & strVal
                             ) {
    Messenger & msgr = *static_cast<Messenger*>(msgr_);
    PhysicsList * phlPtr = dynamic_cast<PhysicsList *>(msgr._physicsList);
    if( !phlPtr ) {
        G4Exception( __FUNCTION__
                   , "NA64SW193" 
                   , FatalErrorInArgument
                   , "Unable to add physics constructor module to physics list"
                     " that is not managed by NA64SW."
                   );
    }
    phlPtr->add_physics_ctr(strVal);
}

void
Messenger::ui_cmd_instantiate_primary_action( GenericG4Messenger * msgr_
                                          , const G4String & strVal
                                          ) {
    Messenger & msgr = *static_cast<Messenger*>(msgr_);
    assert("GPS" == strVal); // TODO: consider strVal to choose some other PGA?
    if( msgr._pga ) {
        G4Exception( __FUNCTION__
                   , "NA64SW195" 
                   , FatalErrorInArgument
                   , "An instance of primary generator action already exists."
                   );
    }
    G4RunManager::GetRunManager()->SetUserAction(
            msgr._pga = new PrimaryGeneratorAction() );
}

void
Messenger::ui_cmd_instantiate_event_action( GenericG4Messenger * msgr_
                                          , const G4String & strVal
                                          ) {
    Messenger & msgr = *static_cast<Messenger*>(msgr_);
    assert("na64sw" == strVal);  // TODO: consider strVal to choose event action?
    if( msgr._eventAction ) {
        G4Exception( __FUNCTION__
                   , "NA64SW196" 
                   , FatalErrorInArgument
                   , "An instance of event action already exists."
                   );
    }
    G4RunManager::GetRunManager()->SetUserAction(
            msgr._eventAction = new EventAction(msgr.notifier) );
}

#ifdef ROOT_FOUND
void
Messenger::ui_cmd_open_root_file( GenericG4Messenger * msgr_
                                , const G4String & strVal ) {
    Messenger & msgr = *static_cast<Messenger*>(msgr_);
    // TODO check no file is opened yet, print warning if opened
    msgr._rootFile = TFile::Open( strVal, "RECREATE" );
}

void
Messenger::ui_cmd_close_root_file( GenericG4Messenger * msgr_
                                 , const G4String & ) {
    Messenger & msgr = *static_cast<Messenger*>(msgr_);
    // Close ROOT file if need
    if( msgr._rootFile ) {
        msgr._rootFile->Write();
        msgr._rootFile->Close();
        msgr._rootFile = nullptr;
    }  // TODO else warning
}
#endif
#endif

}
}

