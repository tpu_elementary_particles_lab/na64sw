#include "na64mc/notifications.hh"

#include <algorithm>

namespace na64dp {
namespace mc {

//
// Destination

iNotificationDestination::iNotificationDestination( Notification::Flags_t nFlags
        , Notifier & nf ) : _flags(nFlags), _notifierRef(nf) {
    _notifierRef.subscribe(this);
}

iNotificationDestination::~iNotificationDestination() {
    _notifierRef.remove_subscription(this);
}

//
// Notifier

void
Notifier::subscribe( iNotificationDestination * ptr ) {
    _destinations.push_back(ptr);
}

void
Notifier::remove_subscription(iNotificationDestination * ptr) {
    auto it = std::find( _destinations.begin(), _destinations.end(), ptr );
    if( _destinations.end() != it )
        _destinations.erase( it );
}

void
Notifier::notify( Notification::Flags_t flags ) {
    for( auto destPtr : _destinations ) {
        Notification::Flags_t filteredFlags = destPtr->notification_flags() & flags;
        if( filteredFlags ) {
            destPtr->receive_notification( filteredFlags );
        }
    }
}

}
}

