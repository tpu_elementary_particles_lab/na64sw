#include "na64mc/genericMessenger.hh"

#include <G4ApplicationState.hh>
#include <G4UIcmdWithoutParameter.hh>

#include <cassert>

namespace na64dp {
namespace mc {

const GenericG4Messenger::AllowedState
             GenericG4Messenger::preinit = 1 << G4State_PreInit
           , GenericG4Messenger::init = 1 << G4State_Init
           , GenericG4Messenger::idle = 1 << G4State_Idle
           , GenericG4Messenger::geomClosed = 1 << G4State_GeomClosed
           , GenericG4Messenger::eventProc = 1 << G4State_EventProc
           , GenericG4Messenger::quit = 1 << G4State_Quit
           , GenericG4Messenger::abort = 1 << G4State_Abort
           , GenericG4Messenger::default_ = 0xff  // TODO
           , GenericG4Messenger::anyAppState = 0xff  // TODO
           ;


GenericG4Messenger::~GenericG4Messenger() {
    assert( _stack.empty() );
    for( auto cmdPtr : *static_cast<std::vector<G4UIcommand *>*>(this) ) {
        delete cmdPtr;
    }
}

G4String
GenericG4Messenger::_path(const G4String & lastToken) const {
    std::string result = _root;
    for(auto entry : _stack) {
        result += entry.first + "/";
    }
    return result + lastToken;
}

GenericG4Messenger &
GenericG4Messenger::dir(const G4String & name, const G4String & descr) {
    auto p = new G4UIdirectory(_path(name + "/"));
    p->SetGuidance( descr );
    _stack.push_back( std::make_pair(name, p) );
    this->push_back(p);
    return *this;
}

GenericG4Messenger &
GenericG4Messenger::cmd( const G4String & name
                       , const G4String & descr
                       , CallableTarget target
                       , AllowedState states ) {
    auto p = new G4UIcommand(_path(name), this  );
    p->SetGuidance( descr );
    set_permitted_states( p, states );
    _stack.push_back( std::make_pair(name, p) );
    this->push_back(p);
    _callables[p] = target;
    return *this;
}

GenericG4Messenger &
GenericG4Messenger::cmd_nopar( const G4String & name
                             , const G4String & descr
                             , CallableTarget target
                             , AllowedState s
                             ) {
    auto p = new G4UIcmdWithoutParameter(_path(name), this  );
    p->SetGuidance( descr );
    set_permitted_states( p, s );
    _callables[p] = target;
    return *this;
}

GenericG4Messenger &
GenericG4Messenger::end( const G4String & name ) {
    assert(!_stack.empty());  // integrity error

    if( _stack.rbegin()->first != name ) {
        char errBf[128];
        snprintf( errBf, sizeof(errBf)
                , "Wrong closing end(): \"%s\", \"%s\" expected."
                , name.c_str()
                , _stack.rbegin()->first.c_str()
                );
        G4Exception( __FUNCTION__
                   , "GEODE001"
                   , FatalErrorInArgument
                   , errBf
                   );
    }

    _stack.pop_back();
    return *this;
}

void
GenericG4Messenger::SetNewValue( G4UIcommand * cmd, G4String strval ) {
    auto calIt = _callables.find(cmd);

    if( calIt == _callables.end() ) {
        char errBf[128];
        snprintf( errBf, sizeof(errBf)
                , "No callable associated for command \"%s\"."
                , cmd->GetCommandPath().c_str()
                );
        G4Exception( __FUNCTION__
                   , "GEODE002"
                   , FatalErrorInArgument
                   , errBf
                   );
    }

    calIt->second(this, strval);
}

void
GenericG4Messenger::set_permitted_states( G4UIcommand * cmd
                                        , AllowedState states ) {
    std::vector<G4ApplicationState> * sl = cmd->GetStateList();
    sl->clear();
    for( G4int i = 0; i < 7; ++i ) {
        if( states & (1 << i) ) {
            sl->push_back( (G4ApplicationState) i );
        }
    }
}

}
}

