/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64_event_id.hpp"

# ifdef SUPPORT_DDD_FORMAT

# include "na64ee_readout.hpp"
# include "na64ee_remote_fetch.hpp"
# include "na64ee_exception.hpp"

# include <iosfwd>
# include <map>
# include <cstring>
# include <vector>
# include <DaqEvent.h>

// See references in https://curl.haxx.se/libcurl/c/getinmemory.html

namespace na64ee {

std::shared_ptr<CS::DaqEvent>
fetch_event( const char * url,
             DDDIndex::RunNo_t runNo,
             DDDIndex::SpillNo_t spillNo,
             DDDIndex::EventNo_t eventNo ) {
    NA64_UEventID eid;
    std::vector<uint8_t> evReadBuffer;
    bzero( &eid, sizeof(eid) );
    eid.chunklessLayout.runNo = runNo;
    eid.chunklessLayout.spillNo = spillNo;
    eid.chunklessLayout.eventNo = eventNo;

    char streid[EVENTID_STR_BUFLENGTH];
    eid2hex_str( eid, streid );

    char eventUrl[256];
    snprintf( eventUrl, sizeof(eventUrl),
              "%s" SINGLE_EVENT_PREFIX "%s/",
              url, streid );

    fetch_remote_data( eventUrl, evReadBuffer );

    # if 0
    std::cout << "Fetched " << evReadBuffer.size() << " bytes." << std::endl;
    std::cout << "  " << std::hex
              << ((int*) evReadBuffer.data())[0] << " "
              << ((int*) evReadBuffer.data())[1] << " "
              << ((int*) evReadBuffer.data())[2] << "..."
              << std::dec << std::endl
              ;
    # endif

    std::cout.flush();

    return std::make_shared<CS::DaqEvent>( evReadBuffer.data(),
                                           /*do copy buffer*/ true );
}

}  // namespace na64ee

# endif  // SUPPORT_DDD_FORMAT

