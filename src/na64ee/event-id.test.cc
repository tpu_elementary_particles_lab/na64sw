#include "na64ee/event-id.hh"
#include <gtest/gtest.h>

namespace na64dp {

// TODO: exception-throw tests

TEST( EventID, ToStringConversion ) {
    EventID eid( 124, 735, 1094102 );
    std::string txt = eid.to_str();
    EventID eid2 = EventID::from_str( txt );
    ASSERT_STREQ( eid.to_str().c_str(), eid2.to_str().c_str() );
}

TEST( EventID, ComparisonOperations ) {
    struct EvStub {
        struct id { int run, spill, event; } a, b;
        bool expectedResult;
    } stubs[] = {
        { { 0,  0,  0}, { 0,  0,  0}, false },

        { { 1,  0,  0}, { 0,  0,  0}, false },
        { { 0,  1,  0}, { 0,  0,  0}, false },
        { { 0,  0,  1}, { 0,  0,  0}, false },

        { { 0,  0,  0}, { 1,  0,  0},  true },
        { { 0,  0,  0}, { 0,  1,  0},  true },
        { { 0,  0,  0}, { 0,  0,  1},  true },

        { { 1,  1,  1}, { 1,  1,  1}, false },

        { { 2,  1,  1}, { 1,  1,  1}, false },
        { { 1,  2,  1}, { 1,  1,  1}, false },
        { { 1,  1,  2}, { 1,  1,  1}, false },

        { { 2,  1,  1}, { 3,  1,  1},  true },
        { { 1,  2,  1}, { 1,  3,  1},  true },
        { { 1,  1,  2}, { 1,  1,  3},  true },
    };

    for( unsigned int i = 0; i < sizeof(stubs)/sizeof(EvStub); ++i ) {
        const EvStub & stub = stubs[i];
        EventID a( stub.a.run, stub.a.spill, stub.a.event )
              , b( stub.b.run, stub.b.spill, stub.b.event );
        EXPECT_EQ( stub.expectedResult, a < b );
    }
}

// Controls basic order, simple stubs
TEST( EventID, MapIndexOrderBasic ) {
    std::map<EventID, int> map;
    map.emplace( EventID( 5,  0, 12), 1 );
    map.emplace( EventID(10,  0,  0), 2 );
    auto it1 = map.find( EventID( 5,  0, 12) );
    ASSERT_NE( it1, map.end() );
    auto it2 = map.find( EventID(10,  0,  0) );
    ASSERT_NE( it2, map.end() );
    ASSERT_LT( it1->second, it2->second );
}

// Controls ordering, combinatiorial test
TEST( EventID, MapIndexOrderFull ) {
    // Three bits from ctrl byte are used, steering the run, spill and event
    // numbers
    std::map<EventID, int> map;
    for( uint8_t ctrlByte = 0x0
       ; ctrlByte < (1 << 4)
       ; ++ctrlByte ) {
        int runNo = ctrlByte & (1 << 2) ? 10 : 0
          , spillNo = ctrlByte & (1 << 1) ? 10 : 0
          , eventNo = ctrlByte & 0x1 ? 10 : 0
          ;
        map.emplace( EventID(runNo, spillNo, eventNo)
                   , ctrlByte );
    }
    std::pair<EventID, int> prevPair( EventID(0, 0, 0), -1 );
    for( auto p : map ) {
        if( prevPair.second > -1 ) {
            EXPECT_GT( p.first, prevPair.first );
        }
        EXPECT_GT( p.second, prevPair.second )
            << "current event ID is " << p.first
            << " (0x" << std::hex << p.first << std::dec << ") "
            << ", index is " << p.second
            << "; previous event ID was: " << prevPair.first
            << " (0x" << std::hex << prevPair.first << std::dec << ") "
            << ", with index " << prevPair.second
            ;
        prevPair = p;
    }
}

//  1010(0) [0000 0000 0000 0000]
//       

}

