#include "na64dp/abstractHandler.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {

AbstractHandler::AbstractHandler()
            : _log(log4cpp::Category::getInstance(std::string("handlers"))) {
    msg_debug( _log
             , "New handler instance created: %p"
             , this );
}

void
AbstractHandler::finalize() {
    msg_debug( _log
             , "Handler %p done."
             , this );
}

}  // namespace na64dp

