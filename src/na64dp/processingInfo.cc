#include "na64dp/processingInfo.hh"
#include "na64util/str-fmt.hh"
#include "na64event/pools.hh"
#include "na64event/event.hh"

#ifdef CapnProto_FOUND
#include "metainfo.capnp.h"
#include <capnp/message.h>
#include <capnp/serialize-packed.h>
#endif

#include <cassert>

namespace na64dp {

void
iEvProcInfo::register_handler( const AbstractHandler * hPtr
                             , const std::string & hName ) {
    emplace( hPtr, aux::HandlerStatisticsEntry( hName ) );
    _ordered.push_back( &stats_for(hPtr) );
}

aux::HandlerStatisticsEntry &
iEvProcInfo::stats_for( const AbstractHandler * hPtr ) {
    auto it = find(hPtr);
    if( end() == it ) {
        NA64DP_RUNTIME_ERROR( "Can not find registered handler by given pointer." );
    }
    return it->second;
}

bool
iEvProcInfo::notify_event_read() {
    if(!_started) { _started = clock(); }
    ++_nEvsProcessed;
    return _nEvsToProcess > 0 ? _nEvsProcessed < _nEvsToProcess : true;
}

void
iEvProcInfo::notify_event_discriminated( const AbstractHandler * hPtr ) {
    ++stats_for(hPtr).nDiscriminated;
}

void
iEvProcInfo::notify_handler_starts( const AbstractHandler * hPtr ) {
    stats_for(hPtr)._started = clock();
}

void
iEvProcInfo::notify_handler_done( const AbstractHandler * hPtr ) {
    auto & s = stats_for(hPtr);
    s.elapsed += clock() - s._started;
}


void
EvProcInfoDispatcher::_dispatch_f() {
    while( _keep ) {
        std::this_thread::sleep_for(std::chrono::milliseconds(_rrMSec));
        _m.lock();
        _update_event_processing_info();
        _m.unlock();
    }
}

EvProcInfoDispatcher::EvProcInfoDispatcher( size_t nMaxEvs
                                          , unsigned int refreshTime
                                          , const HitBanks & banks )
                                              : iEvProcInfo( nMaxEvs )
                                              , _rrMSec(refreshTime)
                                              , _dispatcherThread(nullptr)
                                              , _keep(false)
                                              , _banks(banks)
                                              , _processingStartTime(clock()) {
}
EvProcInfoDispatcher::~EvProcInfoDispatcher() {
    if( _dispatcherThread ) {
        _m.lock();
        _keep = false;
        _m.unlock();
        _dispatcherThread->join();
        delete _dispatcherThread;
        _dispatcherThread = nullptr;
    }
}

bool
EvProcInfoDispatcher::notify_event_read() {
    std::lock_guard<std::mutex> lock(_m);
    return iEvProcInfo::notify_event_read();
}

void
EvProcInfoDispatcher::notify_event_discriminated( const AbstractHandler * hPtr ) {
    std::lock_guard<std::mutex> lock(_m);
    iEvProcInfo::notify_event_discriminated(hPtr);
}

void
EvProcInfoDispatcher::notify_handler_starts( const AbstractHandler * hPtr ) {
    std::lock_guard<std::mutex> lock(_m);
    iEvProcInfo::notify_handler_starts(hPtr);
}

void
EvProcInfoDispatcher::notify_handler_done( const AbstractHandler * hPtr ) {
    std::lock_guard<std::mutex> lock(_m);
    iEvProcInfo::notify_handler_done(hPtr);
}

void
EvProcInfoDispatcher::start() {
    assert(!_dispatcherThread);
    assert(!_keep);
    _keep = true;
    _dispatcherThread = new std::thread( &EvProcInfoDispatcher::_dispatch_f
                                       , this );
}

void
EvProcInfoDispatcher::finalize() {
    {
        _m.lock();
        _keep = false;
        _m.unlock();
    }
    if(_dispatcherThread) {
        _dispatcherThread->join();
        delete _dispatcherThread;
        _dispatcherThread = nullptr;
    }
}


void
PrintEventProcessingInfo::_update_event_processing_info() {
    float elapsed = ((float) clock() - started())/CLOCKS_PER_SEC
        , rate = ((float) n_events_processed())/(elapsed ? elapsed : 1)
        ;

    static char bf[1024];
    // Note: following escape sequences are used (ANSI terminal):
    // \033[s -- save current cursor position
    // \033[u -- restore cursor position previously saved
    // \033[K -- clear line, from current position to end
    size_t nPrinted = snprintf( bf, sizeof(bf)
            , "\033[K\033[1m%zu\033[0m events processed in"
              " \033[1m%.2f\033[0m sec (\033[1m%.1f\033[0m event/sec)\n"
            , n_events_processed(), elapsed, rate );
    size_t nHandler = 1;
    for( auto handlerPair : *this ) {
        const AbstractHandler * handlerPtr = handlerPair.first;
        const aux::HandlerStatisticsEntry & hse = handlerPair.second;
        float perc = 100.*hse.nDiscriminated/((float) n_events_processed());
        nPrinted += snprintf( bf + nPrinted, sizeof(bf) - nPrinted
                            , "\033[K  %2zu : %p %5zu (%4.1f%%) : %s\n"
                            , nHandler, handlerPtr, hse.nDiscriminated
                            , perc, hse.name.c_str() );
        if( nPrinted >= sizeof(bf) ) {
            break;
        }
        ++nHandler;
    }
    fprintf(stdout, "\033[%zuA", nHandler);
    fputs(bf, stdout);
    fflush(stdout);
}

void
EvProcInfoDispatcher::excerpt( std::vector<char> & buffer ) {
    #if CapnProto_FOUND
    //std::lock_guard<std::mutex> lock(_m);
    // TODO: this objects could be probably made reentrant
    ::capnp::MallocMessageBuilder msg;
    EventProcessingState::Builder state = msg.initRoot<EventProcessingState>();
    ::capnp::List<HandlerStats>::Builder hStates = state.initHandlers( size() );

    // Fill handlers infor
    int i = 0;
    std::map<unsigned short, const EventProcessingState *> sorted;
    for( const aux::HandlerStatisticsEntry * hse : ordered_stats() ) {
        HandlerStats::Builder hState = hStates[i];

        hState.setEventsDiscriminated( hse->nDiscriminated );
        hState.setElapsedTime( 1e3*float(hse->elapsed)/CLOCKS_PER_SEC );
        hState.setName( hse->name );
        ++i;
    }
    state.setElapsedTime( 1e3*float(clock() - started())/CLOCKS_PER_SEC );
    state.setEventsPassed( n_events_processed() );
    state.setNEventsExpected( n_events_to_process() );
    // Fill banks info
    auto banks = state.initBanks(); {
        banks.setNSADCHits( _banks.bankSADC.capacity() );
        banks.setNAPVHits( _banks.bankAPV.capacity() );
        banks.setNSADCFittingEntries( _banks.bankSADCFitting.capacity() );
        banks.setNAPVClusters( _banks.bankAPVClusters.capacity() );
        banks.setNTrackPoints( _banks.bankTrackPoints.capacity() );
        banks.setNTracks( _banks.bankTrackPoints.capacity() );
    }

    // serialize
    kj::Array<capnp::word> dataArr = capnp::messageToFlatArray(msg);
    kj::ArrayPtr<kj::byte> bytes = dataArr.asBytes();

    buffer.resize( bytes.size() );
    std::copy( bytes.begin(), bytes.end(), buffer.begin() );
    #else
    throw std::runtime_error("No Cap'n'Proto was supproted on build.");
    #endif
}

}  // namespace na64

