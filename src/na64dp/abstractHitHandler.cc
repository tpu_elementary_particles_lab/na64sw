#include "na64event/event.hh"
#include "na64dp/abstractHitHandler.hh"

namespace na64dp {

SelectiveHitHandler::SelectiveHitHandler( calib::Dispatcher & dspt
                                        , const std::string & selExpr )
            : calib::Handle<nameutils::DetectorNaming>("default", dspt)
            , _selector( selExpr, nullptr ) {
}

void
SelectiveHitHandler::handle_update(const nameutils::DetectorNaming & nm) {
    calib::Handle<nameutils::DetectorNaming>::handle_update(nm);
    if( _selector.first.empty() ) return;
    if( ! _selector.second ) {
        _selector.second = new DetSelect( _selector.first.c_str()
                                        , utils::gDetIDGetters
                                        , nm );
    } else {
        _selector.second->reset_context( nm );
    }
}

bool
SelectiveHitHandler::matches( DetID did ) const {
    if( _selector.first.empty() ) return true;
    assert( _selector.second );  // handle_update() wasn't called yet
    return _selector.second->matches(did);
}

SelectiveHitHandler::~SelectiveHitHandler() {
    if( _selector.second ) {
        delete _selector.second;
    }
}

const nameutils::DetectorNaming &
SelectiveHitHandler::naming() const {
    return static_cast<const calib::Handle<nameutils::DetectorNaming>*>(this)->operator*();
}

namespace aux {

std::string
retrieve_det_selection( const YAML::Node &cfg ) {
    if( cfg["applyTo"] ) {
        //YAML_ASSERT_STR( cfg, "applyTo" );
        return cfg["applyTo"].as<std::string>();
    }
    return "";
}

}
}

