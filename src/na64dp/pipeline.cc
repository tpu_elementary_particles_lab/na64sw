# include "na64dp/pipeline.hh"
# include "na64dp/processingInfo.hh"

namespace na64dp {

Pipeline::Pipeline(iEvProcInfo * pi) : _log(log4cpp::Category::getInstance("pipeline"))
                                     , _procInfoPtr(pi) {
    msg_debug( _log, "Empty pipeline object %p instantiated." );
}

Pipeline::Pipeline( YAML::Node cfg
                  , HitBanks & banks
                  , calib::Dispatcher & dspPtr
                  , iEvProcInfo * pi ) : _log(log4cpp::Category::getInstance("pipeline"))
                                       , _procInfoPtr(pi) {
    size_t nHandler = 0;
    for( auto cHandlerNode : cfg ) {
        std::string handlerName;
        AbstractHandler * hPtr;
        try {
            handlerName = cHandlerNode["name"].as<std::string>();
            hPtr = VCtr::self().make<AbstractHandler>( handlerName
                                                     , banks, dspPtr
                                                     , cHandlerNode );
        } catch(const std::exception & e) {
            _log.error( "An exception has been thrown during"
                    " instantiation of handler %zu: %s", nHandler, e.what() );
            { // TODO: mark with padding or something...
                std::ostringstream yos;
                yos << cHandlerNode;
                _log.error( "Configuration of the problematic piece: \n%s", yos.str().c_str() );
            }
            throw;
        }
        push_back( hPtr );
        if( _procInfoPtr ) {
            _procInfoPtr->register_handler( hPtr, handlerName );
        }
        ++nHandler;
        msg_debug( _log
                 , "Pipeline object %p with %zu handlers instantiated."
                 , this->size() );
    }
}

Pipeline::~Pipeline() {
    for( auto & handlerPtr : *this ) {
        handlerPtr->finalize();
    }
    for( auto & handlerPtr : *this ) {
        delete handlerPtr;
    }
}

bool
Pipeline::process(Event & event) {
    bool processIngStoppedByHandler = false;
    if( _procInfoPtr ) {
        // Note: the notify_event_read() returns `false' when events counter
        // exceeds maximum number and probably may return it as feedback
        // mechanism for rudimentary remote control over running processing.
        // Nevertheless we do not use this result now.
        _procInfoPtr->notify_event_read();
        for( auto & handlerPtr : *this ) {
            _procInfoPtr->notify_handler_starts( handlerPtr );
            AbstractHandler::ProcRes lpr = handlerPtr->process_event( &event );
            processIngStoppedByHandler |= (lpr & AbstractHandler::kStopProcessing);
            if( lpr & AbstractHandler::kDiscriminateEvent ) {
                _procInfoPtr->notify_event_discriminated( handlerPtr );
                break;
            } else {
                _procInfoPtr->notify_handler_done( handlerPtr );
            }
        }
    } else {
        for( auto & handlerPtr : *this ) {
            AbstractHandler::ProcRes lpr = handlerPtr->process_event( &event );
            processIngStoppedByHandler |= (lpr & AbstractHandler::kStopProcessing);
            if( lpr & AbstractHandler::kDiscriminateEvent ) {
                break;
            }
        }
    }
    return processIngStoppedByHandler;
}

}  // namespace na64dp

