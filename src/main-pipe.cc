#include "na64app/app.hh"
#include "na64event/event.hh"

#include "na64dp/abstractEventSource.hh"
#include "na64dp/pipeline.hh"
#include "na64dp/processingInfo.hh"
#include "na64calib/range-override-run-index.hh"

#include "na64event/fields/APV.hh"
#include "na64event/fields/APVCluster.hh"
#include "na64event/fields/SADC.hh"
#include "na64event/fields/StwTDC.hh"
#include "na64event/fields/CaloEDep.hh"

#ifdef ZMQ_FOUND
#   include "na64dp/processingInfoNw.hh"
#endif

#include "na64util/log4cpp-extras.hh"
#include "na64util/YAMLLog4cppConfigurator.hh"

#include <log4cpp/Priority.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/PropertyConfigurator.hh>

#ifdef ROOT_FOUND
#include "na64util/ROOT-sighandlers.hh"
#include <TFile.h>
#endif

#include <yaml-cpp/yaml.h>

#include <unistd.h>
#include <getopt.h>

#include <iostream>

// General application-relevant settings and options set from command line
struct ApplicationConfig {
    // .yaml file used to configure the pipeline
    std::string runConfigFile;
    // Input configuration file name
    std::string inputConfigFile;
    // List of input sources
    std::vector<std::string> inputs;
    # ifdef ROOT_FOUND
    // Name of the ROOT output file
    std::string ROOTOutputFile;
    # endif
    // Output stream descriptor for ASCII display
    std::string display;
    // Max number of events
    size_t nMaxEvs;
    // Calibration config file
    std::string calibCfgURI;
    // Logger configuration file
    std::string logCfg;
    // Loadable modules list
    std::set<std::string> modulesFiles;
    // ... more runtime options may be added here
};

// Sets some default values for the app
static void
initialize_defaults( ApplicationConfig & appCfg ) {
    auto cfgRootDirs = na64dp::util::expand_names("${" NA64SW_CONFIG_ROOT_ENVVAR "}");
    std::string cfgRootDir;
    if(cfgRootDirs.size() == 1) {
        cfgRootDir = cfgRootDirs[0];
    }
    if( cfgRootDir.empty() ) {
        cfgRootDir = "../na64sw/";
    }
    appCfg.runConfigFile = cfgRootDir + "presets/pipe-configs/default.yaml";
    appCfg.inputConfigFile = cfgRootDir + "presets/default-DDD-input.yaml";
    // TODO: default input has to become the stdin here ^^^
    # ifdef ROOT_FOUND
    appCfg.ROOTOutputFile = "processed.root";
    # endif
    appCfg.nMaxEvs = 0;
    appCfg.calibCfgURI = cfgRootDir + "presets/calibrations.yaml";
    //appCfg.vLvl = log4cpp::Priority::INFO;
    appCfg.logCfg = cfgRootDir + "presets/logging.yaml";
    appCfg.modulesFiles.insert( "na64StdHs" );  // standard handlers
    appCfg.modulesFiles.insert( "na64ddd" );  // DDD data source
    // ... more default values for runtime options may be added here
}

void
usage_info( const char * appName
          , std::ostream & os
          , const ApplicationConfig & appCfg ) {
    std::string dftModules;
    {
        std::ostringstream oss;
        for( auto mf : appCfg.modulesFiles ) {
            oss << mf << ",";
        }
        oss << "\b";
        dftModules = oss.str();
    }
    os << "NA64SW pipeline execution util for command shells."
       << "Usage:" << std::endl
       << "  $ " << appName
       << " [-h,--help] [-l,--list [<type>]]"
          #ifdef ROOT_FOUND
          " [-o,--root-file outputROOTFile]"
          #endif
          " [-r,--run runConfigFile]"
          " [-D,--display display]"
          " [-N,--max-events maxEventsNo]"
          " [-c,--calibrations calibCfgURI]"
          " [-m,--load-module module]"
          " [-J,--log-cfg log4cppConfig]"
          " [file1] [file2 ...]"
       << std::endl;
    os << "where:" << std::endl
       << "  -h,--help causes app to print out this message and exit." << std::endl
       << "  -l,--list [=handlers|sources|getters-SADC|getters-APV|detector-id-definitions] causes"
          " app to print the lists of known handlers, sources and various event"
          " getters available to use within the run-config. If given in a long"
          " form with =<arg> part, only the corresponding sort of entities"
          " will be printed (e.g. only handlers if provided as"
          " \"--list=handlers\"."
          " Important note: due to C library limitations (getopt_long), the"
          " argument for this"
          " option (e.g. \"hanlers\", \"sources\", etc.) will be recognized"
          " only if given with equal sign (=); space separation won't work."
       << std::endl
       << "  -r,--run <runConfigFile=" << appCfg.runConfigFile
       << "> points to the YAML file configuring the data treatment pipeline."
       << std::endl
       << "  -i, --input-cfg <input-config-file=" << appCfg.inputConfigFile
       << "> is the input configuration .YMAL description." << std::endl
       #ifdef ROOT_FOUND
       << "  -o,--root-file <outputROOTFile=" << appCfg.ROOTOutputFile
       << "> refers the ROOT output file where processed data has to be written"
       << std::endl
       #endif
       << "  -D,--display <arg> sets the output stream for ASCII display "
          " monitoring the reading/treatment progrss. Use `-' for stdout."
       #ifdef ZMQ_FOUND
       << " Use port number prefixed with `@' to expose network connection." 
       #endif
       << std::endl
       << "  -c,--calibrations <calibCfgURI=" << appCfg.calibCfgURI << "> sets the YAML file"
          " describing static calibrations." << std::endl
       << "  -N,--max-events <maxEventsNo=" << appCfg.nMaxEvs << "> sets the number of events"
          " to be read. Set to 0 to read till the end of input source(s)." << std::endl
       << "  -J,--log-cfg <log4cppConfig=" << appCfg.logCfg << "> is log4cpp configuration"
          " \"properties\" file that sets up logging configuration (sinks,"
          " layouts, etc)." << std::endl
       // TODO: default values for load-module
       << "  -m,--load-module <moduleFile=" << dftModules << "> dynamically loads an extending"
          " module file (typically, a shared object file). The module path may"
          " be given in three forms (by precedence): full or relative path to"
          " the ELF file, the name of the loadable without leading `lib` and"
          " trailing .so to be found in one of the directories given by"
          " " NA64SW_MODULES_DIRS_ENVVAR " environment variable."
       << std::endl
       << "If no input file(s) given, the application expects binary input"
          " from stdin to be a result of `ECALEventWrite' handler."
       << std::endl
       << "Base directory for most configuration include paths may be"
          " overriden with \"" NA64SW_CONFIG_ROOT_ENVVAR "\" environment"
          " variable." << std::endl
       << "The loadable module look-up paths may be provided by \"" NA64SW_MODULES_DIRS_ENVVAR
          "\" environment variable. By default this variable is set to CWD."
          " One may override it with colon-separated list of directories"
          " where modules look-up shall be performed. Empty tokens between"
          " colons will be ignored."
       << std::endl
       ;
}

// Sets the application config based on command line arguments. Returns
// non-zero in case of problems.
int set_app_config( int argc, char * argv[]
                  , ApplicationConfig & appCfg ) {

    static struct option longOpts[] = {
        { "help",           no_argument,       NULL, 'h' },
        { "list-registered",optional_argument, NULL, 'l' },
        { "root-file",      required_argument, NULL, 'o' },
        { "input-cfg",      required_argument, NULL, 'i' },
        { "run",            required_argument, NULL, 'r' },
        { "display",        required_argument, NULL, 'D' },
        { "max-events",     required_argument, NULL, 'N' },
        { "calibrations",   required_argument, NULL, 'c' },
        { "load-module",    required_argument, NULL, 'm' },
        { "log-cfg",        required_argument, NULL, 'J' },
        { NULL, 0x0, NULL, 0x0 },
    };

    int c, optIdx;
    bool hadError = false, listRegistered = false;
    // Iterate over command line options to set fields in appCfg
    while((c = getopt_long( argc, argv, "c:o:r:D:C:N:m:n:i:J:hl"
                          , longOpts, &optIdx )) != -1) {
        switch(c) {
            case '0' :
                std::cerr << "Unable to parse option \""
                          << longOpts[optIdx].name << "\""  << std::endl;
                break;
            case 'r' : appCfg.runConfigFile = optarg;
                break;
            #ifdef ROOT_FOUND
            case 'o' :
                appCfg.ROOTOutputFile = optarg;
                break;
            #endif
            case 'D' :
                appCfg.display = optarg;
                break;
            case 'N' :
                appCfg.nMaxEvs = atol(optarg);
                break;
            case 'c' :
                appCfg.calibCfgURI = optarg;
                break;
            case 'i' :
                appCfg.inputConfigFile = optarg;
                break;
            case 'm' :
                appCfg.modulesFiles.insert(optarg);
                break;
            case 'j' :
                appCfg.logCfg = optarg;
                break;
            case 'l' :
                listRegistered = true;
                break;
            // ... more runtime options may be added here
            case 'h' :
                usage_info( argv[0], std::cout, appCfg );
                return 1;
            case ':' :
                std::cerr << "Option -" << optopt << " requires an argument."
                          << std::endl;
                hadError = true;
                break;
            case '?' :
                std::cerr << "Unrecognized option '-" << optopt << "'."
                          << std::endl;
                hadError = true;
        }
    }
    if( hadError )
        return -1;
    for ( ; optind < argc; ++optind ) {
        appCfg.inputs.push_back(argv[optind]);
    }
    // Before any sybsystems initialized, load subsidiary modules
    na64dp::util::load_modules( appCfg.modulesFiles
                               , getenv(NA64SW_MODULES_DIRS_ENVVAR) );
    if( listRegistered ) {
        na64dp::util::list_registered( std::cout, optarg );
        return 1;
    }
    return 0;
}

int
main(int argc, char *argv[]) {
    //                                                               __________
    // ____________________________________________________________/ App infra
    #ifdef ROOT_FOUND
    // disable ROOT signal handlers
    {
        const char * v = getenv("KEEP_ROOT_SIGHANDLERS");
        if( !v || !(v && ('1' == v[0] && '\0' == v[1] )) ) {
            disable_ROOT_sighandlers();
        } else {
            std::cerr << "ROOT signal handlers are kept." << std::endl;
        }
    }
    #endif

    ApplicationConfig appCfg;
    initialize_defaults( appCfg );
    int rc = set_app_config( argc, argv, appCfg );
    if( rc < 0 ) {  // error occured during initial application configuration
        usage_info( argv[0], std::cerr, appCfg );
        return EXIT_FAILURE;
    }
    if( rc > 0 ) {  // one of the "single-action" options is given (-l, -h)
        return EXIT_SUCCESS;
    }
    //                                                   ______________________
    // ________________________________________________/ Runtime configuration
    // Load to the YAML::Node object describing pipeline
    YAML::Node cfgRoot;
    try {
        cfgRoot = YAML::LoadFile( appCfg.runConfigFile );
    } catch( std::exception & e ) {
        std::cerr << "Error occured while accessing, reading or parsing file \""
                  << appCfg.runConfigFile
                  << "\": " << e.what() << std::endl;
        throw;
    }

    //                                                                    _____
    // _________________________________________________________________/ Logs
    // Initialize logging subsystem
    na64dp::util::inject_extras_to_log4cpp();
    if( ! appCfg.logCfg.empty() ) {
        na64dp::util::YAMLLog4cppConfigurator::configure(appCfg.logCfg);
    } else {
        log4cpp::Appender * consoleAppender
                = new log4cpp::OstreamAppender("console", &std::cout);
        consoleAppender->setLayout(new log4cpp::BasicLayout());
        log4cpp::Category & L = log4cpp::Category::getRoot();
        //L.setPriority( appCfg.vLvl );
        L.addAppender( consoleAppender );
        L.info( "Using zero-conf logging." );
    }
    log4cpp::Category & L = log4cpp::Category::getRoot();

    //                                                            _____________
    // _________________________________________________________/ Calibrations
    // Instantiate calibrations, steered from YAML file
    na64dp::calib::Manager calibMgr;
    // - files index object -- calibration updates steered by YAML
    na64dp::calib::RangeOverrideRunIndex fsi;
    calibMgr.add_run_index( &fsi );
    // - generic calibration data loader object
    na64dp::calib::GenericLoader genericCalibLoader;
    calibMgr.add_loader( "generic", &genericCalibLoader );
    // - add standard data indexes
    std::unordered_map<std::string, na64dp::calib::YAMLCalibInfoCtr> calibDataIdxCtrs;
    na64dp::util::setup_default_calibration_indexes( calibDataIdxCtrs
                                                   , genericCalibLoader
                                                   , calibMgr );
    // ^^^ TODO: delete instances at the end of the main() function
    // - configure from YAML config file
    try {
        YAML::Node root = YAML::LoadFile( appCfg.calibCfgURI );
        configure_from_YAML( root, &fsi
                           , &genericCalibLoader, "generic"
                           , calibDataIdxCtrs );
    } catch( std::exception & e ) {
        L.fatal( "Failed to configure calibration manager from document \"%s\"."
               , appCfg.calibCfgURI.c_str() );
        throw;
    }
    
    //                                                              ___________
    // ___________________________________________________________/ Allocators
    // Pre-allocate hits storage
    // TODO: treat the parameters more explicitly
    na64dp::HitBanks banks( cfgRoot["hitBanks"] ? cfgRoot["hitBanks"]["SADC"].as<int>() : 100
                          , cfgRoot["hitBanks"] ? cfgRoot["hitBanks"]["APV"].as<int>() : 100
                          , cfgRoot["hitBanks"] ? cfgRoot["hitBanks"]["StwTDC"].as<int>() : 100
                          , cfgRoot["hitBanks"] ? cfgRoot["hitBanks"]["SADC"].as<int>() : 100
                          , cfgRoot["hitBanks"] ? cfgRoot["hitBanks"]["APV"].as<int>() : 100
                          , cfgRoot["hitBanks"] ? cfgRoot["hitBanks"]["CaloEDep"].as<int>() : 100
                          , cfgRoot["hitBanks"] ? cfgRoot["hitBanks"]["trackPoints"].as<int>() : 100
                          , cfgRoot["hitBanks"] ? cfgRoot["hitBanks"]["tracks"].as<int>() : 100
                          // ...
                          );
    //                                                     ____________________
    // __________________________________________________/ Monitoring & output
    #ifdef ROOT_FOUND
    // Open file for root histograms; must be called before handlers creation
    // since handler constructors rely on file already being opened
    TFile * f = new TFile(appCfg.ROOTOutputFile.c_str(), "RECREATE");
    #endif
    // Initialize event processing observer instance, if need
    na64dp::EvProcInfoDispatcher * epi = nullptr;
    int evDispFD = -1;  // keeps file descriptor for ASCII display, if any
    if( ! appCfg.display.empty() ) {
        if( '@' == appCfg.display[0] ) {
            # ifdef ZMQ_FOUND
            epi = new na64dp::NwPubEventProcessingInfo(
                      atoi( appCfg.display.substr(1).c_str() )
                    , appCfg.nMaxEvs
                    , banks
                    );
            # else
            throw std::runtime_error( "Network monitoring is not supported"
                    " by this build since it is configured without ZeroMQ." );
            # endif
        } else {
            int fd;
            if( "-" == appCfg.display ) {
                evDispFD = fd = open( appCfg.display.c_str()
                               , O_CREAT | O_WRONLY | O_TRUNC
                               , S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH
                               );
                
            } else {
                fd = STDIN_FILENO;
            }
            epi = new na64dp::PrintEventProcessingInfo( fd
                        , banks
                        , appCfg.nMaxEvs
                        );
        }
    }
    //                                                         ________________
    // ______________________________________________________/ Data processing
    {
        // Create the pipeline object
        na64dp::Pipeline handlers( cfgRoot["pipeline"]
                                 , banks
                                 , calibMgr
                                 , epi );
        if( handlers.empty() ) {
            L.warn( "Empty pipeline has been built." );
        }

        na64dp::AbstractEventSource * evSrc = nullptr;
        YAML::Node inputCfgNode;
        try {
            inputCfgNode = YAML::LoadFile( appCfg.inputConfigFile );
        } catch( std::exception & e ) {
            std::cerr << "Error occurred while accessing, reading or parsing file \""
                      << appCfg.inputConfigFile
                      << "\": " << e.what() << std::endl;
            throw;
        }
        try {
            evSrc = na64dp::VCtr::self().make<na64dp::AbstractEventSource>(
                        inputCfgNode["type"].as<std::string>(),
                        banks, calibMgr, inputCfgNode, appCfg.inputs );
        } catch(std::exception & e) {
            L.fatal( "During instantiation of the data source object an"
                    " exception has occurred: \"%s\".", e.what() );
            throw;
        }
        // Re-entrant instance of the event structure buffering output being read
        na64dp::Event event(banks);
        if(epi) epi->start();
        size_t evsCount = 0;
        // Process the data
        assert(evSrc);
        while( evSrc->read(event) ) {
            handlers.process(event);
            event.clear();
            banks.clear();
            ++evsCount;
            if( appCfg.nMaxEvs && evsCount >= appCfg.nMaxEvs ) break;  // max counter exceeded
        }  // End event processing done
        if( evSrc ) {
            delete evSrc;
        }
    }
    //                                                                _________
    // _____________________________________________________________/ Clean up
    if(epi) {
        epi->finalize();
        delete epi;
        epi = nullptr;
    }
    #ifdef ROOT_FOUND
    // Close file for root histograms
    f->Write();
    delete f;
    #endif
    if( evDispFD > 0 && !isatty(evDispFD) ) {
        close(evDispFD);
    }
    // That's all, folks
    L.info("All done -- exiting normally.");
    log4cpp::Category::shutdown();

    return EXIT_SUCCESS;
}





