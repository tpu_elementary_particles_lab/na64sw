# Selectors Domin Specific Language

This directory contains BISON/FLEX assets for small domain-specific language
to produce reasonably efficient predicates at the runtime (from string,
config files and so on).

## Note on the Project Location

This project is too tiny (so far) to become a subject for a dedicated repo, so
we keep it within the larger repository. Despite it was initially created for
a single purpose, the effort seems to be promising overall for various areas
where runtime-composed selecton expressions would be a nice feature.

## Expression Types

Each expression is a boolean statement that might be evaluated to either
`true` or `false`. Each expression may be deduced to _value comparison
expressions_ over integer variables with boolean operation over them. At the
most trivial case the expression consists only of comparison operation.

Examples of _value comparison expressions_ which is valid as trivial language's
boolean statement:

* `2 < 3` (evaluates to `true`)
* `2 >= 3` (evaluates to `false`)

Examples of complex boolean statements on comparison expressions:

* `2 < 3 && 3 > 2` (evaluates to `false`)
* `! 3 < 2` (evaluates to `true`).

For the comparison expression the following binary operations are supported:

* EQALS `==`
* DOES NOT EQUAL `!=`
* GREATER `>`
* GREATER OR EQUAL `>=`
* LESSER `<`
* LESS OR EQUAL `<=`

For the logic statements the following operators are supported (by the order
of priority):

* Unary NOT (`!`) operator
* Binary AND (`&&`) operator
* Binary OR (`||`) operator
* Binary XOR (`^^`) operator

Additionally, the round brackets (`(`, `)`) may change the priority.

Examples of valid expressions:

* `(!1 == 2) ^^ (3 <= 42)`
* `(1 == 2 || 3 == 3) && 5 == 5`

Note, that contrary to C, unary operator will not implicitly cast the integral
value to boolean, so expression `!0` is NOT valid.

The operands at the comparison expressions may be defined by string identifiers
that have to be resolved (substituted) by particular numerical values defined
by getters.

* `a == 2`
* `chip == SADC && kin != ECAL`

String identifiers (names) are the alphanumeric (`[a-zA-Z\_0-9]`) sequences of
arbitrary length started from non-digits, e.g. `one`, `_a123`.

## Comparison Values

We define following set of values for comparison expressions:

1. Expression literals
2. Principal value
3. Unconditionally computed from principal value ("static")
4. Conditionally computed from principal value ("dynamic")
5. Externally-resolved string identifiers

### Expression Literals

Only positive integral values from zero up to C's `UINT_MAX` values are
supported, provided in decimal, octal or hexidecimal forms.

### Principal Value

Principal value is a value of type `na64dp_dsul_PrincipalValue_t` that
represents a value (or data sample of complex structure) the ready predicate
has to evaluate against (what to test). Static and dynamic _getters_ shall
compute their values based on principal value albeit the principal value may
not be the direct subject of comparison expressions.

### Uncondtionally Computed Values

Properties extracted or computed from principal value that may be defined
independently on the any particular state of principal value. These values
will be computed only when `na64dp_dsul_update_static_symbols()` function
is called.

### Uncondtionally Computed Values

Properties extracted or computed from principal value that may be defined
only when principal value obeys to some criteria or in certain state. This
values will be computed each time and only when expression evaluation reaches
this node.

### Externally-resolved String Identifiers

API defines that at the translation time user code may submit an external
resolver that will define matching between some string alphanumeric identifier
and value that has to obey the same validity range as literals (from zero
to `UINT_MAX`). This external resolver symbol is provided via `void *` pointer
to a special _getter function_ (see below). This values updated only when
`na64dp_dsul_update_extern_symbols()` function is called. But corresponding
getter may be invoked during lexical analysis of the expression to acknowledge
the identifier as an extarn symbol.

## Getter Functions

The _getter function_ is a user function of special signature provided within
getters table argument to translating function. We have dedicated types for
ordinary (static and dynamic) and extern values.

The getters table is the plain array of `na64dpsu_SymDefinition` structure
terminated with instance of `NULL` name pointer that has to be provided to
`na64dp_dsul_compile_detector_selection()` function in order to get the
_evaluation buffer_ (see below). All entries in the getters table array except
for terminative one have to obey the following rule:

1. Its `name` field has to start from one of the following symbols: `~` for
dynamic value getters, `$` for static values getters, `+` for extern resolution
getter(s).
2. For static and dynamic getters the rest (except for the first) characters
in name has to correspond to the identifiers this values has to correspond
within the expression. Empty names are not allowed for static and dynamic
getters. For external getters name does not play a role.
3. Valid static and dynamic value getters have to initialize the `get` field
of `callback` union in the entry with function pointer of corresponding type.
The extern resolution getter has to be provided at `resolve_name` field of
the same union.

Once evaluation buffer is ready (translation is finished) the getters table
may be safely deleted.

## Evaluation Buffer

To decrease stack messing during frequetn evaluation, all the translation
result resides in a compact contiguous memory chunk called further "evaluation
buffer". This buffer has the following structure:

1. Header block
    1. Root node pointer
    2. Extern functions begin pointer
    3. Static functions begin pointer
    4. Data block begin pointer (literals start)
    5. Extern values data block start (0 offset for values)
    6. Static values data block start
    7. Buffer end pointer
2. Nodes
3. Functions
    1. Extern getter functions
    2. Static getter functions (NULL-terminated)
4. Principal value
5. Data
    1. Literals
    2. Extern values
    3. Static values
5. Extern values names

Notes:
1. To iterate over extern getters, rely on the static end to get the end pointer.
2. To gain access to principal value, use data block begin pointer minus
`sizeof(Principal_t)`.

User code has to submit pre-allocated chunk of enough size to hold the
evaluation buffer. Typically, for the expression of 3-4 comparison expressions
with 2-3 boolean operations over it the evaluation buffer will occupy <500
bytes.

Evaluation buffer is not copyable as it stores absolute pointers to its
sections.

## Usage

1. Having a memory chunk for evaluation buffer and getters table defined, one may
translate a valid string expression into evaluation buffer with
`na64dp_dsul_compile_detector_selection()` function.
2. To initialize or reset values of extern symbols one has to call the
`na64dp_dsul_update_extern_symbols()` function.
3. Having a principal value to test one has to invoke the
`na64dp_dsul_update_static_symbols()` function to initialize or set static
values.
4. To evaluate the expression and obtain the result one has to invoke
`na64dp_dsul_eval()` function.

Typically, one would frequently change the principal value affecting only
static values, so functions `na64dp_dsul_update_static_symbols()` and
`na64dp_dsul_eval()` shall be called each time new sample is tested.

Extern context containing some external definitions is typically updated
less frequently than principal value.

## Translation Function Behaviour and Retrurn Codes

During translation process the `na64dp_dsul_compile_detector_selection()`
function will involve some heap-allocation that is expected to be completely
freed after evaluation buffer is ready.

The `na64dp_dsul_compile_detector_selection()` function shall return following
values as an indication of the translation status:

* `0` -- ok, buffer ready
* `-1` -- string expression is NULL or seems not to be a C-string
* `-2` -- given memory chunk is not sufficient for the evaluation buffer
* `-3` -- failed to allocate interim data on heap (dynamic memory error)
* `-4` -- syntax error or any other error with parser/lexer
* `-5` -- bad getters table entry met (bad prefix character, null function pointer)

The `na64dp_dsul_compile_detector_selection()` that generates the evaluation
buffer will pick up only getters involved in the expression, so if getter
`$static` is not a subject of given expression it won't be called during
principal update.

## Notes on Debug

The `na64dp_dsul_compile_detector_selection()` function accepts pointer to
C's `FILE` struct that, if not `NULL` will be used for detailed report on the
translation process.

To control the evaluation buffer state, the function `na64dp_dsul_dump()` might
be used.

## Limitations

* Up to `4095` in total values supported (literals, external, static,
dynamic). Particular number is regulated be macro
`NA64DP_DETID_MAX_DICT_OFFSET` -- to save some space we exploit the zero page's
address space, which might be considered a nasty hack by some purists.
* Supports only positive integer as comparison value types
* No arithmetic or bitwise operations supported

## TODO

* Sometimes, if some lexer/parser error occured during one error being written
in a buffer, the substituted message is being printed to `stderr`. We have to
either foresee immediate return, or somehow stack error messages.
* Clean naming getting rid of `na64dp_` prefix and move to standalone repos as
this project may be used aside from main NA64DP project.

