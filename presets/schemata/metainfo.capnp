@0xee468ecb40208b66;

struct HandlerStats {
    eventsDiscriminated @0 :UInt64;
    elapsedTime @1 :Float32;
    name @2 :Text;
    num @3 :UInt16;

    hitsPassed @4: UInt64;
    hitsDiscriminated @5: UInt64;
}

struct EventProcessingState {
    handlers @0 :List(HandlerStats);
    elapsedTime @1 :Float32;
    eventsPassed @2 :UInt64;
    nEventsExpected @3 :UInt64;
    banks :group {
        nSADCHits @4 :UInt64;
        nAPVHits @5 :UInt64;
        nSADCFittingEntries @6 :UInt64;
        nAPVClusters @7 :UInt64;
        nTrackPoints @8 :UInt64;
        nTracks @9 :UInt64;
    }
}

