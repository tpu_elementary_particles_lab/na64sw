FROM crankone/hepfarm-amd64-dbg:latest

RUN sudo emerge root capnproto yaml-cpp zeromq cppzmq pyzmq
RUN sudo /bin/sh -c "echo 'dev-python/pycapnp ~amd64' > /etc/portage/package.accept_keywords/pycapnp" \
 && sudo emerge dev-python/pycapnp
RUN sudo emerge cmake dev-libs/uriparser dev-cpp/gtest sys-devel/gdb dev-libs/log4cpp
RUN sudo /usr/bin/ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N "" \
 && sudo /usr/bin/ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key -N ""
RUN sudo emerge sci-physics/geant
RUN sudo USE="rave" emerge genfit
RUN sudo emerge dev-libs/msgpack
RUN sudo emerge dev-cpp/eigen
