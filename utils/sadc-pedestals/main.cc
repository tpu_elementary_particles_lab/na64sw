#include "na64util/namedFileIterator.hh"
#include "na64util/numerical/langaus.hh"

#include <iostream>
#include <TH2F.h>
#include <TF1.h>
#include <TObjString.h>
#include <TFitResult.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TStyle.h>

#include <yaml-cpp/yaml.h>

#include <iostream>
#include <fstream>

/* This is a draft for util that probably shall become more elaborated in the
 * future. Possible features:
 *  - Various fitting functions (Gaus, Landau); Now it uses Gauss+Landau
 *  convolution ("langaus")
 *  - Tuning of the fitting algorithm (now uses default ROOT's migrad on
 *  loglikehood minimization)
 *  - More output formats (consider .root files with histograms). Currently,
 *  only CSV is supported
 *  - Elaborating accept/reject strategy (currently, criterion is not
 *  meaningful)
 *  and way more...
 */

static void
print_usage( const std::string & appName
           , std::ostream & os ) {
    os << "Usage:" << std::endl
       << "    $ " << appName << "<processed-file.root> <config.yaml>" << std::endl
       << "Applications reads histograms data from <processed-file.root>"
          " and performs pedestals extraction by fitting it with according to"
          " <config.yaml>."
       << std::endl;
}

int 
main(int argc, char * argv[]) {
    if( 3 != argc ) {
        std::cerr << "Bad number of arguments (expected 3 or 4)." << std::endl;
        print_usage( argv[0], std::cerr );
        exit(EXIT_FAILURE);
    }

    // Load to the YAML::Node object describing pipeline
    YAML::Node cfg;
    try {
        cfg = YAML::LoadFile( argv[2] );
    } catch( std::exception & e ) {
        std::cerr << "Error occured while accessing, reading or parsing file \""
                  << argv[2]
                  << "\": "
                  << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }

    TFile * f = TFile::Open( argv[1], "READ" );
    if( !f ) {
        std::cerr << "Failed to instantiate ROOT file \"" << argv[1]
                  << "\"." << std::endl;
        exit(EXIT_FAILURE);
    }

    const std::string nameRxs = cfg["lookupRegex"].as<std::string>()
                    , descRxs = cfg["descriptionRegex"].as<std::string>()
                    ;

    std::cout << "Name lookup regex in use: " << nameRxs << std::endl
              << "Description regex in use: " << descRxs << std::endl
              ;

    na64dp::util::NamedFileIterator it( f, "TH1F",
            nameRxs.c_str() );

    TPRegexp descRx( descRxs );

    TObject * objPtr;
    size_t objCount = 0;

    // set (open) output stream
    std::ostream * osPtr;
    {
        const std::string outFilePath
                = cfg["output"]["filePath"].as<std::string>();
        if( "-" == outFilePath ) {
            osPtr = &std::cout;
        } else {
            osPtr = new std::ofstream( outFilePath
                                     , std::ios::out );
        }
    }
    std::ostream & os = *osPtr;
    bool useExtFmt = "extended" == cfg["output"]["format"].as<std::string>();

    const std::string goodPathPat = cfg["saveHists"]["good"].as<std::string>()
                    , badPathPat = cfg["saveHists"]["bad"].as<std::string>()
                    ;

    os << "#TBName,pedType,";
    if( useExtFmt ) {
        // use extended format for studies
        os << "mean,rms,skewness,kurtosis,"
           << "width,widthErr,"
           << "mpv,mpvE,"
           << "area,areaErr,"
           << "sigma,sigmaErr,"
           << "chi2,ndf"
           ;
    } else {
        // use laconic form for pedestals
        os << "pedVal,pedErr,nEntries";
    }
    os << std::endl;
    while((objPtr = it.get_match())) {
        ++objCount;
        TH1F * hstPtr = dynamic_cast<TH1F*>(objPtr);
        assert( hstPtr );
        
        TObjArray * subs = descRx.MatchS( hstPtr->GetTitle() );
        TString pedType, tbname;
        if (subs->GetLast()+1 > 2) {
            pedType = dynamic_cast<TObjString *>(subs->At(1))->GetString();
            tbname  = dynamic_cast<TObjString *>(subs->At(2))->GetString();
        } else {
            std::cerr << "Description string \"" << hstPtr->GetTitle()
                      << "\" of histogram \"" << hstPtr->GetName()
                      << "\" does not match regex. Ignored." << std::endl;
            continue;
        }

        if( hstPtr->GetSumOfWeights() == 0 ) {
            std::cerr << "Histogram \"" << hstPtr->GetName()
                      << "\" has zero sum of weights, ignored." << std::endl;
            continue;
        }

        std::ios_base::fmtflags commonFlags( os.flags() );

        os << tbname << "," << pedType << ","
           << std::scientific;  // <== NOTE this!

        if( useExtFmt ) {
           os << hstPtr->GetMean() << ","
              << hstPtr->GetRMS() << ","
              << hstPtr->GetSkewness() << ","
              << hstPtr->GetKurtosis() << ","
              ;
        }
    
        {
            TCanvas *c = new TCanvas();
            c->SetLogy();
            gStyle->SetOptStat(111111111);  // Set meaningful stats TPaveStat
            gStyle->SetOptFit(1111);

            Double_t 
                     #if 0
                     fitRange[2] = { hstPtr->GetMean() - (hstPtr->GetRMS() > 3 ?   hstPtr->GetRMS()/2 : 30 )
                                   , hstPtr->GetMean() - (hstPtr->GetRMS() > 3 ? 3*hstPtr->GetRMS()   : 100) }
                     #else
                     fitRange[2] = { 10
                                   , 600 }
                     #endif
                   , startValues[4] = { 1  // scaled width (Landau distribution)
                                      , hstPtr->GetMean()  // most prob value
                                      , hstPtr->GetSumOfWeights()  // area
                                      , hstPtr->GetRMS()  // sigma
                                      }
                   , lowParLims[4] = { 1e-6
                                     , 10
                                     , hstPtr->GetEntries()/10.
                                     , .05
                                     }
                   , upParLims[4] = { 1.
                                    , 600
                                    , hstPtr->GetEntries()*100  // for large number of event may be wrong
                                    , 10
                                    }
                   ;

            //std::cout << "Scaled width: " << lowParLims[0] << ", " << startValues[0] << ", " << upParLims[0] << std::endl
            //          << "         MPV: " << lowParLims[1] << ", " << startValues[1] << ", " << upParLims[1] << std::endl
            //          << "        area: " << lowParLims[2] << ", " << startValues[2] << ", " << upParLims[2] << std::endl
            //          << "       sigma: " << lowParLims[3] << ", " << startValues[3] << ", " << upParLims[3] << std::endl
            //          ;

            bool isGood = true;
            Double_t pars[4], errors[4], chiSq;
            Int_t ndf;
            TF1 * ff = na64dp::util::langausfit( hstPtr
                                               , fitRange
                                               , startValues
                                               , lowParLims, upParLims
                                               , NULL
                                               , pars
                                               , errors
                                               , chiSq, ndf
                                               );
            if( chiSq/ndf > 1e2
             || fabs(hstPtr->GetMean() - pars[1]) > pars[3]*3 ) {
                isGood = false;
            }

            if( useExtFmt ) {
                if( ff && ((!cfg["nanRejected"].as<bool>()) || isGood)) {
                    for(int i = 0; i < 4; ++i) {
                        os << pars[i] << "," << errors[i] << ",";
                    }
                } else {
                    for(int i = 0; i < 4; ++i) {
                        os << "nan,nan,";
                    }
                }
                os << chiSq;
            } else {
                if( ff && ((!cfg["nanRejected"].as<bool>()) || isGood)) {
                    // use MVP of langaus fit as the pedestal value and
                    // sigma + Error_MVP as validity interval
                    os << std::scientific << pars[1] << ","
                       << errors[1] + pars[3] << ",";
                    os.flags( commonFlags );
                    os << hstPtr->GetEntries()
                       ;
                } else {
                    for(int i = 0; i < 4; ++i) {
                        os << "nan,nan,0,";
                    }
                }
            }

            if(ff) {
                hstPtr->Draw();
                ff->Draw("SAME");
                char bf[256];
                const char * pat = goodPathPat.c_str();
                if( !isGood ) {
                    pat = badPathPat.c_str();
                }
                snprintf( bf, sizeof(bf), pat
                        , hstPtr->GetName() );
                c->Print( bf );
            } else {
                std::cerr << "No fit function returned for \""
                          << tbname
                          << "\"." << std::endl;
            }
            delete c;
        }
        os << std::endl;
    }
    if( osPtr != &std::cout ) {
        static_cast<std::ofstream&>(os).close();
        delete osPtr;
    }
    std::cout << objCount << " entries have been processed."
              << std::endl;
    return EXIT_SUCCESS;
}

