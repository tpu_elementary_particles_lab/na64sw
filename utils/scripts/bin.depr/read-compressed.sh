#!/bin/bash

# This shell script example demonstrates a reading of data previously generated
# with `ECALEventWrite' handler and compressed with `bzip2' util. Usage:
#   $ read-compressed.sh <.bz2 filename> <pipe-args...>

INPUT_FILE=$1

# Drop the first command line argument and run the data processing util with
# the rest arguments and provide the input data to stdin after decompression
shift
bzcat $INPUT_FILE | ./pipe $@

