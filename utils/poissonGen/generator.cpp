#include <iostream>
#include <cmath>
#include <fstream>
#include <cstdlib>
#include <ctime>


int main()  {
    //srand (time(NULL)); // Setting another Seed
    double step = 450;
    double e = M_E; // The e number
    double lambda = 1000000;
    double lambdaL; // The collateral variable
    double par = pow(e,step);
    double k;  // The returnable number
    double p;
    double time;    

    std::ofstream ofs;
    ofs.open("data.txt");
    
     
    for (int i = 0; i < 100; i++) {
        lambdaL = lambda;
        k = 0;
        p = 1;
        
        // Generating a poisson's random number with average lambda
        // (k-1) is the distributed number
        do {
            srand(time(NULL)); // Setting another Seed
            k = k + 1;
            double u = (double)(rand())/RAND_MAX; // Generate random (double)number in range from 0 to 1
            p=p*u;
            
            while ((p < 1) && (lambdaL > 0)) {
                
                if (lambdaL > step) {
                p = p*par;
                lambdaL = lambdaL - step;
                }
                
                else {
                    p = p*pow(e,lambdaL);
                    lambdaL = 0;
                }
            }
        }
        while (p > 1.);
        
        ofs << time << std::endl;
        }
    ofs.close();
    return 0;
}
 
