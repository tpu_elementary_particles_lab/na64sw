set xrange [-80:40]
p0 = 400
p1 = 12
p2 = 1
f(x) = (p0)*exp(-((x-p1)+exp(-(x-p1)))/(2*p2))

p01 = 3200
p11 = 12
p21 = 1
g(x) = (p01)*exp(-((x-p11+((9.98381/(1.25*10**(-8)))*10**(-7)))+exp(-(x-p11+((9.98381/(1.25*10**(-8)))*10**(-7)))))/(2*p21))
plot f(x) title 'Moyal' with lines linestyle 1, \
     g(x) notitle with lines linestyle 2
