/*
 * 20th of February, 2020
 *  There are two methods of subtracting
 *  pedestals in our reconstruction:
 *  dynamic (with setting as pedestal value 
 *  mean value of first 3 ADS samples) and
 *  statistical (with subtracting Mean Value
 *  from pedestal's distribution hist).
 *  This program realizing subtracting two 
 *  histograms (for each cell), filled by
 *  those methods and writes a difference in new 
 *  TFile as new histogram  
 */


//  Generate name of TDirectory
void dir_name_gen( char* tDirName
                 , char* tSubDirName
                 , char* dirName1) {
    sprintf( dirName1, "%s/%s", tDirName, tSubDirName );
}

// Generate name of TH1F
void name_gen( char * histName
             , int x
             , int y
             , int flag) {
    // %s_%s - to compose string("ECAL_hist") and int variables
    // and to put "_" between them
    sprintf( histName, "%s_%i_%i_%i", "ECAL_hist", x, y, flag);
}

// Get TDirectory pointer
TDirectory * get_directory (TFile * readFile
                          , char * dirName) {
    TDirectory * dir = readFile->GetDirectory(dirName);
    if( !dir ) {
        std::cerr << "Error: unable to retrieve TDirectory instance \""
                  << dirName << "\" from file " << readFile << "." << std::endl;
        return NULL;
    }
    return dir;
}

// Create an instance of TH2F*
TH2F* hist_instance( TFile * readFile
                   , char * histName
                   , char * dirName
                   , TDirectory * dir) {
    
    TH2F * hist = (TH2F*) dir->Get(histName);
    if (!hist) {
        std::cerr << "Error: unable to retrieve histogram \"" << histName << "\""
                  << " from directory \"" << dirName << "\" from file "
                  << readFile << "." << std::endl;
    }
    return hist;
}

void dif_Peds () {
    //  File for reading
    char tFileName1 [] = "../Ecal-Diff-Hists.root";
    //  File for writing
    char tFileName2 [] = "../Subtracted-Hists.root";
    //  Directories in readfile with dynamically
    //  and statistically subtracting peds hists
    char tDir1Name [] = "Hist-Dyn";
    char tDir2Name [] = "Hist-Mean";
    //  Subdirectories in TDirectories
    char TSubDir1Name [] = "main";
    char TSubDir2Name [] = "preshower";
    
    // Generated in cycle TH2F names
    char histName1 [15];
    char histName2 [15];
    
    // Generated TDirectory names
    char dirName1 [20];  //  Name of ECAL preshower hist
    char dirName2 [20];  //  Name of ECAL main hist
    
    int x, y; // The cell position parameters
    int flag; // = 0 if preshower, = 1 if main part
    
    //  Instances of existing histograms
    //  , subtracted dynamically and by mean value from hists 
    TH2F * dynHistPreshower;
    TH2F * meanHistPreshower;
    TH2F * dynHistMain;
    TH2F * meanHistMain;
    
    //  Open TFile with existing histograms
    TFile * readFile = TFile::Open(tFileName1);
    if (!readFile) {
    std::cerr << "Cannot open file " << tFileName1 << endl;
    }
    
    //  Create TFile for writing future historams
    //  Option "recreate" is using for writing functional
    TFile * writeFile = new TFile(tFileName2, "recreate");
    if (!writeFile) {
    std::cerr << "Cannot open file " << tFileName2 << endl;
    }
    
    //  Create two TDirectories inside of writeFile
    TDirectory * wf1 = writeFile->mkdir("preshower");
    TDirectory * wf2 = writeFile->mkdir("main"); 
    
    //  Histogram process with preshower part  
    
    flag = 0;
    
    //  Getting pointers on TDirectories of writeFile
    dir_name_gen(tDir1Name, TSubDir2Name, dirName1);
    TDirectory * dynDir1 = get_directory(readFile, dirName1);
    dir_name_gen(tDir2Name, TSubDir2Name, dirName2);
    TDirectory * meanDir1 = get_directory(readFile, dirName2);

    //  Go to directory in writeFile with preshower histograms
    wf1 -> cd ();
    
    //  Process of passing through all of preshower histograms
    for (x = 0; x <= 5; x++) {
        for (y = 0; y <= 5; y++) {
            
            //  Generating name of current reading histogram 
            //  with dynamic subtraction
            name_gen( histName1, x, y, flag);
            
            //  Creating an instance of Histogram with dynamic subtraction
            dynHistPreshower = hist_instance(readFile, histName1, dirName1, dynDir1);
            
            //  Generating name of current reading histogram 
            //  with statistical subtraction
            name_gen( histName1, x, y, flag);
            
            //  Creating an instance of Histogram with dynamic subtraction
            meanHistPreshower = hist_instance(readFile, histName1, dirName2, meanDir1);
            
            //  Creating a pointer for new hist with histogram's difference
            TH2F * SubtrHistPoint = new TH2F ( (char*)histName1, "hist", 32, 0, 32, meanHistPreshower->GetNbinsY(), 0, 2400);
            
            //  Fill new histogram with difference of histograms
            SubtrHistPoint->Add( dynHistPreshower, meanHistPreshower, 1, -1);
        }
    }
    
    // Histogram process with main part
    // All steps are same as for preshower part
    
    flag = 1;
    dir_name_gen(tDir1Name, TSubDir1Name, dirName1);
    TDirectory * dynDir2 = get_directory(readFile, dirName1);
    dir_name_gen(tDir2Name, TSubDir1Name, dirName2);
    TDirectory * meanDir2 = get_directory(readFile, dirName2);
    wf2 -> cd ();
    for (x = 0; x <= 5; x++) {
        for (y = 0; y <= 5; y++) {
            name_gen( histName2, x, y, flag);
            std::cout << histName2 << std::endl;
            dynHistMain = hist_instance(readFile, histName2, dirName1, dynDir2);
            name_gen( histName2, x, y, flag);
            meanHistMain = hist_instance(readFile, histName2, dirName2, meanDir2);
            TH2F * SubtrHistPoint = new TH2F ( (char*)histName2, "hist", 32, 0, 32, meanHistPreshower->GetNbinsY(), 0, 2400);
            SubtrHistPoint->Add( dynHistMain, meanHistMain, 1, -1);
        }
    }
    writeFile->Write();
}
