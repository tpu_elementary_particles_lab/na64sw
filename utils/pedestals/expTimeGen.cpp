/* Build snippet:
 *  $ g++ -L/usr/lib/root/6.22/lib -lCore -lImt -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lROOTVecOps -lTree -lTreePlayer -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc -lROOTDataFrame -pthread -lm -ldl -rdynamic ../ecal-edu/utils/pedestals/expTimeGen.cpp -I /usr/lib/root/6.22/include/
 */

#include <boost/math/distributions/exponential.hpp> 
#include <boost/random.hpp>
#include <map>

/// Parameters defining a Moyal function
struct MoyalPars {
    double mu, sigma, S;
};

/// A Moyal function
double
moyal_f( double t, const MoyalPars & p) {
    return (p.S/(sqrt(2*M_PI)*p.sigma))
        * exp( - ( (t-p.mu)/p.sigma + exp(- (t-p.mu)/p.sigma ) )/2 );
}

/// A wrapper class generating time w.r.t. given frequncy
class TimeGenerator {
private:
    boost::exponential_distribution<> _expDst;
    boost::mt19937 _rng;
    boost::variate_generator< boost::mt19937 &
                            , boost::exponential_distribution<> > _vGen;
public:
    /// Ctr parameterised with frequency and random seed
    TimeGenerator(double freq, int seed) : _expDst(freq)
                                         , _vGen(_rng, _expDst) {
        _rng.seed(seed);
    }
    /// Returns random, exponentially-distributed time interval
    double call() { return _vGen(); }
};

/// A parameter getter
class MoyalParamSource {
public:
    /// Shall return weight of an event and set references parameters
    virtual double get( MoyalPars & p ) const {
        p.mu = 15;
        p.sigma = .6;
        p.S = 10;
        return 1;
    }
};

int
main(int argc, char * argv[])  {
    if( argc != 4 ) {
        std::cerr << "usage: <freq> <nSamples> <seed>" << std::endl;
        return EXIT_FAILURE;
    }

    double lambda = std::stod(argv[1]);  // mean intensity of the beam
    size_t nEvents = std::stoi(argv[2]);  // number of samples (precision vs performance)
    int seed = std::stoi(argv[3]);  // random seed for reproducible results

    // Time generator state
    TimeGenerator t( lambda, seed );
    // Event parameters source issuing the random values for Moyal functions
    MoyalParamSource mps;
    // Whether to consider pile-ups pf higher order
    bool multipileup = true;
 
    // Mean pedestal value for current frequency
    double meanVal = 0;

    std::map< double
            , std::pair<double, MoyalPars> > pileup;

    // Defines integration/averaging boundaries
    int wp0 = 0, wp = 8, wpInc = 1;
    const int window = 32;
    // Simulate events
    for (int i = 0; i < nEvents; i ++) {
        // Loop below produces at least one pile-up event that shall occur
        // before the "current" event. We do not consider "current" event,
        // however may consider one or more pile-up events that affects
        // digitization window of the current event. To be inclusive, we shall
        // get the max position of "current" event as it will become 
        double pileUpEventTime = 0;
        do {
            MoyalPars mp;
            // time distance till the pile-up event
            double dt = t.call();
            pileUpEventTime += dt;
            // parameters of the pile-up event
            double weight = mps.get(mp);
            // add the pile-up event to consideration
            pileup.emplace( pileUpEventTime
                          , std::pair<double, MoyalPars>(weight, mp));
            if( ! multipileup ) break;
        } while(pileUpEventTime < window);

        // Get the averaged picture
        double pVal = 0;

        // get the pedestal value (from 0 + dt to 0 + dt + wp)
        for( int wpi = wp0; wpi < wp; wpi += wpInc ) {
            // get amplitude sum at point wpi
            for( auto pueEntry : pileup ) {
                const double evTime = pueEntry.first;
                const double evWeight = pueEntry.second.first;
                const MoyalPars & mp = pueEntry.second.second;
                pVal += moyal_f(evTime + wpi, mp);
            }
        }
        pVal /= (wp - wp0)/wpInc;
        meanVal += pVal;
        pileup.clear();
    }

    std::cout << meanVal / nEvents << std::endl;;

    return 0;
}
