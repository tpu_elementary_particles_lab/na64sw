// Read data from "Peds.txt" and send it to console

#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>

void read_file (std::ifstream &ifs)
{
    std::string s;
    char x1, y1, flag1;
    int x, y, flag, isEven;
    float Mean, StdDev;
    do
    {
    // Assuming, that data parted by backspaes, read it into variables
    ifs >> isEven >> x >> y >> flag >> Mean >> StdDev;

    std::cout << isEven << "   " <<  x << "   " << y << "   " << flag 
              << "   " << Mean << "   " << StdDev << std::endl;
    }
    while (getline(ifs, s));
}

int main () 
{
    std::ifstream ifs;
    ifs.open("Peds.txt");
    
    read_file(ifs);
    
    ifs.close();
    
 return 0;   
}


