/// Stores rect array of pairs of values
class Grid : public std::vector< std::pair<double, double> > {
public:
    typedef std::vector< std::pair<double, double> > Parent;
private:
    size_t _nRho, _nPhi, _nZ;
public:
    Grid( Int_t nRho, Int_t nPhi, Int_t nZ )
        : Parent( nRho*nPhi*nZ, std::pair<double, double>(0, 0) )
        , _nRho(nRho), _nPhi(nPhi), _nZ(nZ) {}
    
    size_t n_cell( Int_t rhoI, Int_t phiI, Int_t zI ) const {
        assert(rhoI < _nRho);
        assert(phiI < _nPhi);
        assert(zI < _nZ);
        return _nRho*_nPhi*zI + _nRho*phiI + rhoI;
    }

    std::pair<double, double> & at(Int_t rhoI, Int_t phiI, Int_t zI) {
        return Parent::at( n_cell(rhoI, phiI, zI) );
    }

    const std::pair<double, double> & at(Int_t rhoI, Int_t phiI, Int_t zI) const {
        return Parent::at( n_cell(rhoI, phiI, zI) );
    }
};

/// Returns mean value for cell
double
mean(const std::pair<double, double> & p, size_t N) {
    return p.first/N;
}

/// Returns unbiased \sigma^2 for cell
double
variance(const std::pair<double, double> & p, size_t N) {
    const double s = p.first
               , sqs = p.second;
    return (sqs - s*s/N)/(N-1);
}

void
emShMassProfile_wcal_ascii() {
    // Input files
    const char * inputFiles[] = { "wcal-test.dat"
                                , "" };
    // Input parameters, that has to be taken from GDML (in principle):
    int radialN = 3  // radial segmentation
      , angularN = 2  // angular segmentation
      , nLayersPrsh = 3   // number of layers (of single snadwiches) in preshower
      , nLayersMain = 15  // number of layers in main part
      ;
    double length = 125  // length in abs units (mm)
         , cylRadius = 35  // radius in abs units (mm)
         ;
    int nLayersPerZCell = 1;  // number of sandiches per single histogram cell

    // Define value grids
    Grid gridPrshAds(radialN, angularN, nLayersPrsh)
       , gridPrshScn(radialN, angularN, nLayersPrsh)
       , gridMainAds(radialN, angularN, nLayersMain)
       , gridMainScn(radialN, angularN, nLayersMain)
       ;
    const char gridDescriptions[][64] = {
            "Presh.scintillator",
            "Presh.converter",
            "Main scintillator",
            "Main converter"
        };
    Grid * gridPtrs[] = { &gridPrshScn, &gridPrshAds
                        , &gridMainScn, &gridMainAds
                        };

    size_t evTotal = 0;
    {  // Read the files into arrays
        int rhoIdx  // rho-index (0 center)
          , lyrIdx  // 1 -- prs-scn, 2 -- prs-cnv, 3 -- main-scn, 4 -- main-cnv
          , phiIdx  // phi-index
          , zIdx  // z-index (sandwich number)
          , isMain  // isd main part?
          ;
        double edepS, edepSSq;
        char matName[128];

        // Read files
        for( const char ** filename = inputFiles
           ; '\0' != **filename
           ; ++filename ) {
            int nEvents = -1;
            FILE * f = fopen(*filename, "r");
            assert(f);
            fscanf(f, "# %d", &nEvents);
            // Read entries from file
            while(EOF != fscanf( f, "%d %d %d %d %d %lf %lf %s"
                  , &rhoIdx, &lyrIdx, &phiIdx, &zIdx, &isMain
                  , &edepS, &edepSSq
                  , matName
                  )) {
                assert(lyrIdx < 5);
                Grid & g = *(gridPtrs[lyrIdx-1]);
                auto & p = g.at(rhoIdx, phiIdx, zIdx);
                p.first += edepS;
                p.second += edepSSq;
            }
            fclose(f);
            evTotal += nEvents;
        }
    }

    //
    // Calc full variance and mean: preshower, main
    double means[4] = {0, 0, 0, 0}
         , varns[4] = {0, 0, 0, 0}
         ;
    // preshower
    for( char i = 0; i < 4; ++i ) {
        const Grid & g = *(gridPtrs[i]);
        for( const auto & p : g ) {
            double m = mean(p, evTotal)
                 , sigma = variance(p, evTotal)
                 ;
            means[i] += m;
            varns[i] += sigma;
        }
    }

    for( char i = 0; i < 4; ++i ) {
        std::cout << std::setw(24) << gridDescriptions[i]
            << ": " << std::setw(10) << means[i] << " +/- "
            << sqrt(varns[i])
            << " MeV" << std::endl
            ;
    }
    std::cout << "  preshower: " << means[0] + means[1]
              << " +/- " << sqrt(varns[0] + varns[1])
              << " MeV" << std::endl;
    std::cout << "       main: " << means[2] + means[3]
              << " +/- " << sqrt(varns[2] + varns[3])
              << " MeV" << std::endl;

    std::cout << "total: " << means[0] + means[1] + means[2] + means[3]
              << " +/- " << sqrt(varns[0] + varns[1] + varns[2] + varns[3])
              << " MeV" << std::endl;
}


