/*
 * This script reads the binary file generated with `SegmentHistograms` SD
 * of our Geant4 application and ...
 * */

// Stores rectangular array of pairs of values providing means of iterating and
// addressing over the cells.
class Grid : public std::vector< std::pair<double, double> > {
public:
    typedef std::vector< std::pair<double, double> > Parent;
private:
    // Indexes
    size_t _nRho, _nPhi, _nZ;
public:
    // Ctr creating the grid of certain dimensions
    Grid( Int_t nRho, Int_t nPhi, Int_t nZ )
        : Parent( nRho*nPhi*nZ, std::pair<double, double>(0, 0) )
        , _nRho(nRho), _nPhi(nPhi), _nZ(nZ) {}
    
    // Returns offset in plain array from indexes triplet
    size_t n_cell( Int_t rhoI, Int_t phiI, Int_t zI ) const {
        assert(rhoI < _nRho);
        assert(phiI < _nPhi);
        assert(zI < _nZ);
        return _nRho*_nPhi*zI + _nRho*phiI + rhoI;
    }

    // Returns pair of values by indexes triplet
    std::pair<double, double> & at(Int_t rhoI, Int_t phiI, Int_t zI) {
        return Parent::at( n_cell(rhoI, phiI, zI) );
    }

    // Returns immutable pair of values by indexes triplet
    const std::pair<double, double> & at(Int_t rhoI, Int_t phiI, Int_t zI) const {
        return Parent::at( n_cell(rhoI, phiI, zI) );
    }
};

// Returns mean value for particular cell
double
mean(const std::pair<double, double> & p, size_t N) {
    return p.first/N;
}

/// Returns unbiased \sigma^2 for particular cell
double
variance(const std::pair<double, double> & p, size_t N) {
    const double s = p.first
               , sqs = p.second;
    return (sqs - s*s/N)/(N-1);
}

#if 0
struct BinFileReadingProxy {
    FILE * f;
    BinFileReadingProxy(FILE * f_) : f(f) {}
    friend template<typename T> BinFileReadingProxy & operator>>(T &);
};

template<typename T> BinFileReadingProxy &
operator>>(BinFileReadingProxy & p, T & dest) {
    fread( &dest, sizeof(dest), 1, p.f);
}
#endif

void
emShMassProfile_wcal() {
    // Input files
    const char * inputFiles[] = {
                //"wcal-test-150GeV-r35mmxW9x10.bin",
                //"wcal-test-150GeV-r35mmxW9x10-01.bin",
                //"wcal-test-150GeV-r35mmxW9x10-02.bin",
                //
                "wcal-test-150GeV-r50mmxW6x15-01.bin",
                "" };
    //                                                        _________________
    // _____________________________________________________/ Input Parameters
    // Input parameters, that has to be taken from GDML (in principle):
    int radialN = 3  // radial segmentation
      , angularN = 2  // angular segmentation
      , nLayersPrsh = 3   // number of layers (of single snadwiches) in preshower
      , nLayersMain = 15  // number of layers in main part
      ;
    //double length = 125  // length in abs units (mm)
    //     , cylRadius = 35  // radius in abs units (mm)
    //     ;
    int nLayersPerZCell = 1;  // number of sandiches per single histogram cell

    //                                                      ___________________
    //____________________________________________________/ Reading Structures
    // Define value grids
    Grid gridPrshAds(radialN, angularN, nLayersPrsh)
       , gridPrshScn(radialN, angularN, nLayersPrsh)
       , gridMainAds(radialN, angularN, nLayersMain)
       , gridMainScn(radialN, angularN, nLayersMain)
       ;
    const char gridDescriptions[][64] = {
            "Total",
            "Presh.scintillator",
            "Presh.converter",
            "Main scintillator",
            "Main converter"
        };
    Grid * gridPtrs[] = { &gridPrshScn, &gridPrshAds
                        , &gridMainScn, &gridMainAds
                        };

    size_t evTotal = 0;
    uint8_t nDims;
    // This is for 4-hst scheme
    TProfile * profs[5] = { nullptr, nullptr, nullptr, nullptr, nullptr };
    // Read files
    for( const char ** filename = inputFiles
       ; '\0' != **filename
       ; ++filename ) {
        int nEvents = -1;
        FILE * f = fopen(*filename, "rb");
        assert(f);
        {
            uint32_t ui32b;
            uint16_t ui16b, I[5];  // rho, layerID, phi, z, isMain
            uint8_t ui8b;
            double s, sqs;
            // 0: nEvents:uint32_t -- number of events on the record
            fread( &ui32b, sizeof(ui32b), 1, f );
            nEvents = ui32b;
            std::cout << "Expected " << nEvents << " events in file \""
                << nEvents << "\"." << std::endl;
            // 1: nCells:uint32_t -- number of cell scorers on the record
            fread( &ui32b, sizeof(ui32b), 1, f );
            const int nCells = ui32b;
            // 2: nDims:uint8_t -- number of dimensions
            fread( &ui8b, sizeof(ui8b), 1, f );
            nDims = ui8b;
            assert(nDims == 5);
            // 3: ((index:uint16_t[nDims] + sum:double + sqSum:double)[nCells])
            for( int i = 0; i < nCells; ++i ) {
                fread(I, sizeof(I), 1, f);
                fread(&s, sizeof(s), 1, f);
                fread(&sqs, sizeof(sqs), 1, f);
                // 1 -- prs-scn, 2 -- prs-cnv, 3 -- main-scn, 4 -- main-cnv
                assert(I[1] > 0 && I[1] < 5);
                Grid & g = *(gridPtrs[I[1]-1]);
                auto & p = g.at( I[0], I[2], I[3] );
                p.first += s;
                p.second += sqs;
            }
            // 4: nHistogramsSets:uint16_t
            fread(&ui16b, sizeof(ui16b), 1, f);
            const int nHstSets = ui16b;
            // 5: ( nBins:uint16_t
            //    + upper:double
            //    + nHistograms:uint32_t
            //    + (index:uint16_t[nDims] + bins:(uint32_t + double + double)[nBins+1])[nHistograms]
            //    )[nHistogramsSets]
            for( int i = 0; i < nHstSets; ++i ) {
                fread(&ui16b, sizeof(ui16b), 1, f);
                const int nBins = ui16b;
                double lowerBound, upperBound;
                fread(&lowerBound, sizeof(upperBound), 1, f);
                fread(&upperBound, sizeof(upperBound), 1, f);
                fread(&ui32b, sizeof(ui32b), 1, f);
                const int nHsts = ui32b;
                for(int j = 0; j < nHsts; ++j) {
                    fread(I, sizeof(I), 1, f);

                    // Pick up a profile to fill
                    TProfile * profPtr;
                    if( I[1] > 0 && I[1] < 5 ) {
                        if( ! profs[I[1]] ) {
                            char namebf[64];
                            snprintf(namebf, sizeof(namebf), "edep%d", I[1]);
                            profs[I[1]] = new TProfile(namebf, gridDescriptions[I[1]]
                                    , nBins, lowerBound, upperBound );
                        }
                        profPtr = profs[I[1]];
                    } else {
                        if( ! profs[0] ) {
                            profs[0] = new TProfile("edepTotal", "Total edep"
                                    , nBins, lowerBound, upperBound );
                        }
                        profPtr = profs[0];
                    }
                    #if 0
                    {  // ... TODO: do smth w hst
                        std::cout << "  histogram #" << j << " "
                            << I[0] << "x"
                            << I[1] << "x"
                            << I[2] << "x"
                            << I[3] << "x"
                            << I[4] << " read." << std::endl;
                    }
                    #endif
                    uint32_t nEntries;
                    double s, sqs;
                    for( int nBin = 0; nBin < nBins + 2; ++nBin ) {
                        fread(&nEntries, sizeof(nEntries), 1, f);  // bin height
                        fread(&s, sizeof(s), 1, f);  // sum
                        fread(&sqs, sizeof(sqs), 1, f);  // sum of squares

                        profPtr->AddBinContent( nBin, nEntries );
                        profPtr->SetBinEntries( nBin, nEvents );
                        profPtr->SetBinError( nBin, sqrt(sqs/nEntries - s*s/(nEntries*nEntries))/nEvents );
                        profPtr->SetEntries( nEvents );
                        // Print histogram in file:
                        // - mean = s/N
                        //s /= nEntries;
                        // - stddev = sqrt( n_i^2/N - s*s/N^2 )
                        //sqs = sqrt( sqs/nEntries,  );
                        //profs[I[1]-1]->Fill();
                    }
                }
            }
            // 6: end-of-file marker for debug
            fread(&ui32b, sizeof(ui32b), 1, f);
            assert(0xE7D0F11E == ui32b);
            std::cout << "  ...file \"" << filename << "\" read." << std::endl;
        }
        fclose(f);
        evTotal += nEvents;
    }

    //
    // Calc full variance and mean: preshower, main
    double means[4] = {0, 0, 0, 0}
         , varns[4] = {0, 0, 0, 0}
         ;
    // preshower
    for( char i = 0; i < 4; ++i ) {
        const Grid & g = *(gridPtrs[i]);
        for( const auto & p : g ) {
            double m = mean(p, evTotal)
                 , sigma = variance(p, evTotal)
                 ;
            means[i] += m;
            varns[i] += sigma;
        }
    }

    for( char i = 0; i < 4; ++i ) {
        std::cout << std::setw(24) << gridDescriptions[1+i]
            << ": " << std::setw(10) << means[i] << " +/- "
            << sqrt(varns[i])
            << " MeV" << std::endl
            ;
    }
    std::cout << "  preshower: " << means[0] + means[1]
              << " +/- " << sqrt(varns[0] + varns[1])
              << " MeV" << std::endl;
    std::cout << "       main: " << means[2] + means[3]
              << " +/- " << sqrt(varns[2] + varns[3])
              << " MeV" << std::endl;

    std::cout << "total: " << means[0] + means[1] + means[2] + means[3]
              << " +/- " << sqrt(varns[0] + varns[1] + varns[2] + varns[3])
              << " MeV" << std::endl;

    char canvasNameBf[32];
    for( int i = 0; i < 5; ++i ) {
        if(!profs[i]) continue;
        snprintf(canvasNameBf, sizeof(canvasNameBf), "cnv%d", i);
        TCanvas * cnv = new TCanvas(canvasNameBf);
        cnv->cd();
        profs[i]->Draw();
        std::cout << "xxx" << std::endl;  // XXX
    }
}


