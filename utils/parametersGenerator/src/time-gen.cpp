#include <cmath> 
#include <iostream>
#include <fstream>
#include <cstdlib> // Included for rand() 
#include <ctime>
#include <iomanip> // Included for .precision()
#include <boost/math/distributions/poisson.hpp>
#include "boost/random.hpp"
  
 int main(int argc, char * argv[])  {
    using namespace boost::math;
    double lambda = std::stod(argv[1]);  // Mean intensity of the beam
    
    typedef boost::poisson_distribution<> PoissonDistribution;
    typedef boost::mt19937 RandomNumberGenerator;    // pick the random number generator method,
    typedef boost::variate_generator< RandomNumberGenerator&, PoissonDistribution > Generator;  // link the generator to the distribution
    RandomNumberGenerator generator;
    PoissonDistribution myPoisson(lambda);
    Generator numberGenerator(generator, myPoisson);
    generator.seed(static_cast<unsigned int>(time(0))); // seed with some initial value
 
    for (int i = 0; i < 10000000; i ++) {
        std::cout << 1./numberGenerator() << std::endl;    }
        /*
    // Variables for Knuth's algorithm    
    double k = 0.;  // The returnable value
    double p = exp(-lambda);
    double s = p;
    
    std::cout.precision(10);    
    
    // Generating a poisson's random number with average lambda
    // (k-1) is the distributed value
    for (int i = 0; i < 1000; i ++) { 
    double k = 0.;  // The returnable value
    double p = exp(-lambda);
    double s = p;
    srand(static_cast<unsigned int>(time(0)));
    double u = (double)(rand())/RAND_MAX; // Generate random (double)number in range from 0 to 1
       while (u > s) {
            k = k + 1.;
            p=p*lambda/k;
            s = s + p;
        } 
    // Calculating time
    std::cout << std::fixed << k << std::endl;
    }
    */
    return 0;
} 
