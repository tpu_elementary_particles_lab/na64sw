/*
 * This program reads walues of 
 * generated pile-up waveforms
 * (not clear waveforms) from file 
 * ../data/waveforms.txt and calculates
 * values for Odd pedestal and Even pedestal.
 * Those values writes in the output
 * files (../data/pedestalsEven.txt and 
 * ../data/pedestalsOdd.txt
 */

#include <iostream>
#include <fstream> 

int main (int argc, char * argv []) {
    std::string waveFileName = argv[1];
    std::string oddPedFileName = argv[2];
    std::string evenPedFileName = argv[3];
    std::string buffer;
    double sampleEven[2];
    double sampleOdd[2];
    double pedestalOdd;
    double pedestalEven;
    double check[2]; // Variables for check the end of file
    
    // Create output and an input flows
    std::ifstream ifs;
    ifs.open(waveFileName);
    std::ofstream ofs1;
    ofs1.open(oddPedFileName);
    std::ofstream ofs2;
    ofs2.open(evenPedFileName);
    
    
    while (getline(ifs, buffer)) {
        getline(ifs, buffer);
        // Fill values odd and even first samples of clear wave
        // for pedestals calculation
        ifs >> sampleOdd[0];
        ifs >> sampleEven[0];
        ifs >> sampleOdd[1];
        ifs >> sampleEven[1];
        
        // Calculate pedestal values
        pedestalOdd = (sampleOdd[0] + sampleOdd[1]) / 2;
        //if (check[0] != pedestalOdd) {
        //    check[0] = pedestalOdd;
        //} else check[0] = 10000;
        pedestalEven = (sampleEven[0] + sampleEven[1]) / 2;
        //if (check[1] != pedestalEven) {
        //    check[1] = pedestalEven;
        //} else check[1] = 10000;
        
        //if ((check [0] != 10000) && check[1] != 10000) {
            // Output pedestals values
            ofs1 << pedestalOdd << std::endl;
            ofs2 << pedestalEven << std::endl;
        //}
    }
    ifs.close();
    ofs1.close();
    ofs2.close();
    return 0;
}
        
        
