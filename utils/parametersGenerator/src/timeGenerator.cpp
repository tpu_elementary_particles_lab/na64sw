/*
 * This is the program, genering a value
 * of puasson-distributed time interval
 * between two events in experiment. 
 * Generator uses value of the mean intensity 
 * of the beam, wich passes to the program
 * as a command line arguement. (lambda = 1000000).
 */
#include <cmath>
#include <iostream>
#include <fstream>
#include <cstdlib> // Included for rand() 
#include <ctime>
#include <iomanip> // Included for .precision()
  
 int main(int argc, char * argv[])  {
    double lambda = std::stod(argv[1]);  // Mean intensity of the beam
    
    // Variables for Knuth's algorithm
    double lambdaL = lambda;  
    double step = 450;  
    double par = exp(step);  
    double k = 0.;  // The returnable value
    double p = 1.;
    
    std::cout.precision(10);

    srand(static_cast<unsigned int>(time(0)));
    
    // Generating a poisson's random number with average lambda
    // (k-1) is the distributed value
    for (int i = 0; i < 1000; i ++) { 
        k = 0.;
        p = 1.;
        lambdaL = lambda;
        do {
            k = k + 1;
            
            double u = (double)(rand())/RAND_MAX; // Generate random (double)number in range from 0 to 1
            p=p*u;
            while ((p < 1) && (lambdaL > 0)) {
                if (lambdaL > step) {
                    p = p*par;
                    lambdaL = lambdaL - step;
                } else {
                p = p * exp(lambdaL);
                lambdaL = 0;
                }
            }
        } while (p > 1.);
    
    // Calculating time
    std::cout << std::fixed << 1./(k-1) << std::endl;
    }
    return 0;
} 
