/*
 * The program reading values of moyal-function
 * parameters (main, and pile-up tail
 * from input file (1st parameter) (../data.parameters.txt).
 * Calculates waveform values depending on
 * time and print them in output file (2nd parameter)
 * (../data/waveforms.txt). Value of time between 2 
 * events getting from the file (4th parameter)
 * (../data/timeIntervals.txt)
 */

#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip> // Included for using ofs.precision()

// get value of waveform (based on Moyal 
// function parameters) in accurate SADC interval t 
float get_waveform( double t
                  , float p0  // Area
                  , float p1  // MaxPosition
                  , float p2  // Width
                  ) {
    // Calculate the value of moyal-function 
    float value = (p0)*exp(-((t-p1)+exp(-(t-p1)))/(2*p2));
    return value;
}

int main(int argc, char * argv[]) {
    std::string paramFileName = argv[1];  // Name of the input file with parameters
    std::string outputFileName = argv[2];  // Name of the output file
    double intervalSADC = 12.5e-9;  // SADC interval in seconds
    std::string timesFileName = argv[3];  // Name of the file with poisson intervals
    std::string buffer;  // String for useless information of input file
    double waveform1[32];  // Array, containing all waveform values
    double waveform2[32];  // Array with pile-up waveform's tail
    
    // Create an input flows, bonding with file
    std::ifstream ifs1;
    ifs1.open(paramFileName);
    std::ifstream ifs2;
    ifs2.open(timesFileName);
    
    // Create an output flow, bonding with file
    std::ofstream ofs;
    ofs.open(outputFileName);
    
    // Read rubbish to the buffer
    for (int i = 0; i < 11; i++) {
        getline(ifs1,buffer);
    }
    
    // Moyal function parameters
    double Area;
    double Width;
    double Max;
    
    double time;  // Delta t between events
    double deltaT;  // Delta t in values of SADS interval 
    
    while(getline(ifs1, buffer)) {  // Read hollow strings in buffer
        // Fill parameters from files
        ifs1 >> Area;
        ifs1 >> Width;
        ifs1 >> Max;
        ifs2 >> time;
        //std::cout << Area << "   " << Width << "   " << Max << std::endl;
      
        deltaT = time / intervalSADC;
        
        ofs << std::endl;
    
        // Fill waveform and print waveform values in output file
        for (int t = 0; t < 32; t++) {
            waveform1[t] = get_waveform(t, Area, Max, Width);
            ofs << waveform1[t] << "    ";
        }
        ofs << std::endl;
        
        ifs1 >> Area;
        ifs1 >> Width;
        ifs1 >> Max;
        
        //std::cout << Area << "   " << Width << "   " << Max << std::endl;
            
        // Fill pile-up waveform's end
        for (int t = 0; t < 32; t++) {
            waveform2[t] = get_waveform(t+deltaT, Area, Max, Width);
            ofs << waveform2[t] << "    ";
        }
    }
    // Close input and an output flows
    ifs1.close();
    ifs2.close();
    ofs.close();
}
