#pragma once
#include <unistd.h>
#include <iostream>

struct RootParameter {
        // part to directory in root file   
        std::string TFileName;
        // part to directory with histogram
        std::string TDirName;
        // name histogram
        std::string TNameHist;
        // part to directory with preshower or main part
        std::string TSumWFDirPartECAL;
};

struct FlagProcess {    
    // if true to set log scale
    bool flaglog;
    // whether to perform amplitude calibrations relying on the
    // Jenks natural breaks classification results in the input file
    bool doCalib;
    // 
    bool doFitGaus;
};

class ApplicationConfig 
{
    private:
    public:
   
        // cell position
        int x, y;
        // Parameters of information about directory root histogram
        RootParameter RootParam;
        // logical flags responsible for data processing
        FlagProcess FlagProc;
        // name output file pdf
        std::string NameFileOut;

        //
        ApplicationConfig();

        // displays information on the command line interface
        void usage_info( const char * appName
                       , std::ostream & os);

        // command line interface
        int set_app_config( int argc, char * argv[]);      
};
