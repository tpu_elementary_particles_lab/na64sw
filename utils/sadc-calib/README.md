# SADC Calibration Scripts

This dir contains scenarios assets for calibration routines of various SADC
detectors.

## Obtaining Pedestals Values

* `retrieve_pedestals.C` ROOT scripts is intended to retrieve pedestals from
`.root` files containing the `pedestals-*` histograms subclassed from `TH1`.
For each entry of that type it will generate a line in the output file
containing columns "name", "number of hits" and few controlled by flags, in
order:
    - `0x1` -- enables "mean" column
    - `0x2` -- enables "std.dev" column
    - `0x4` -- enables "parabolic" (polynomial of 2) interpolation center
    - `0x8` -- enables "gaussian" fit center
Thus, the full table may be obtained with `0xf` bitflag configuration (`15` in
decimal form).



