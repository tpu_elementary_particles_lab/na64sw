from math import floor, ceil
import re

rxsCellID=r'\s*(?P<x2d>-?\d{2}),(?P<y2d>(?:-?\d{2})|(?:yy)),(?P<runs>(?:#\d{4},?)+)\s*'
rxCellID = re.compile(rxsCellID)

rxsDstr=r'\s*(?P<isPreshowerMean>p?)m(?P<mean>\d{1,4}\.?\d{0,2}),(?P<isPreshowerSigma>p?)s(?P<sigma>\d{1,4}\.?\d{0,2})\s*'
rxDstrID = re.compile(rxsDstr)

csLine = [-9550, -5730, -1910, 1910, 5730, 9550]

def concat_2digits( ref, val ):
    ref = ref/100.
    if ref < 0:
        ref = int( ceil(ref) )
    else:
        ref = int( floor(ref) )
    ref *= 100
    ref += int(val)
    return ref

class CellData(object):
    def __init__(self, xIdx, yIdx, yCoord=None):
        self.idxs = (xIdx, yIdx)
        self.r = None
        self.c = [None, yCoord]
        self.p = [None, None]
        self.s = [None, None]

    def set_cs( self, m, xIdx, yIdx ):
        self.r = list(map(lambda s: int(s[1:]), filter(lambda x: x, m.group('runs').split(','))))
        if self.c[0] is not None:
            raise RuntimeError('Repeatative setting of coordinates.')
        self.c[0] = concat_2digits(csLine[xIdx], m.group('x2d') )
        y = m.group('y2d')
        if 'yy' != y:
            self.c[1] = concat_2digits(csLine[5-yIdx], m.group('y2d') )
            return self.c[1]
        return None

    def set_dst( self, m ):
        isOnPreshower = bool(m.group('isPreshowerMean'))
        if isOnPreshower:
            if not bool(m.group('isPreshowerSigma')):
                return False
        else:
            if bool(m.group('isPreshowerSigma')):
                return False
        mean = float( m.group('mean') )
        sigma = float( m.group('sigma') )
        if isOnPreshower:
            self.p = [mean, sigma]
        else:
            self.s = [mean, sigma]
        return True

    def set_y_if_need(self, y):
        if self.c[1] is None:
            self.c[1] = y

    def __str__(self):
        xc, yc = None, None
        xs, ys = '--', '--'
        if self.c[0] is not None:
            xc = self.c[0]/10
            xs = '%.1f'%xc
        if self.c[1] is not None:
            yc = self.c[1]/10
            ys = '%.1f'%yc
        #sc = "%d×%d / %6s×%6s"%(self.idxs[0], self.idxs[1], xs, ys)
        sc = 'Run(s): %s, beam position: x=%d mm, y=%d mm\n'%(self.r, round(self.c[0]/10), round(self.c[1]/10))
        sc += "%d-%d-0 : mean = %d / sigma = %.1f\n"%(self.idxs[0], self.idxs[1], self.p[0], self.p[1])
        sc += "%d-%d-1 : mean = %d / sigma = %.1f\n"%(self.idxs[0], self.idxs[1], self.s[0], self.s[1])
        return sc
        #return "%s, %d/%d, %d/%d"%(sc,
        #                self.p[0], self.p[1],
        #                self.s[0], self.s[1]
        #            )

def parse_calib( fName ):
    d = [[ None for x in range(0,6) ] for y in range(0, 6)]
    with open(fName) as f:
        linNum = -2
        for nLine, line in enumerate(f.readlines()):
            if line.startswith('--'):
                linNum += 1
                continue
            yLine = None
            for colNum, col in enumerate(line.split('|')[1:]):
                m = rxCellID.match(col)
                #print( '%d×%d'%(colNum, linNum) )
                if m:
                    if d[colNum][linNum] is None:
                        d[colNum][linNum] = CellData( colNum, 5-linNum, yCoord=yLine )
                    yVal = d[colNum][linNum].set_cs(m, colNum, linNum)
                    if yVal is not None:
                        yLine = yVal
                        for n in range(0,5) :
                            if d[n][linNum] is not None:
                                d[n][linNum].set_y_if_need( yVal )
                    continue
                m = rxDstrID.match(col)
                if m:
                    if d[colNum][linNum] is None:
                        d[colNum][linNum] = CellData( colNum, 5-linNum )
                    r = d[colNum][linNum].set_dst(m)
                    if r:
                        continue
                #raise RuntimeError('Unable to parse')
                print( 'Unable to parse expression in'
                        ' line %d, at %d×%d cell: "%s".'%(nLine+1, colNum, linNum, col) )
    return d

#d = parse_calib( 'ecal_coord_calibration-09.09.017.txt' )

#for cellCol in d:
#    for cell in cellCol:
#        print( cell, end='\t' )
#    print()

#for line in d:
#    for cell in line:
#        print(cell)
#    print()

