#!/usr/bin/python3
#
# This script performs parallel (multiprocessed) invokation over the ECAL
# calibration data, accepting the description of the calibration session in a
# special form (the only time when ECAL calibration session was documented in
# such a way by now is 09.09.2017).
# Note that this script does not claim any particular function of the pipeline
# it is invoking with.

import sys, re, os
import subprocess
import multiprocessing
import argparse

import parse_ECAL as pe
from pathlib import Path
from threading import Thread
from functools import partial

def get_chunks_list_at( chunksDirPath
                      , globPattern='cdr*.dat'
                      , chunkRegex=r"^.*/cdr(\d+)-(\d+)\.dat$" ):
    """
    Recursively searches for the chunk files of format
    """
    d = {}
    rxChunkPath=re.compile(chunkRegex)
    for path in Path(chunksDirPath).rglob(globPattern):
        strPath = str(path)
        m = rxChunkPath.match( strPath )
        if not m:
            sys.stderr.write( 'File "%s" matches the glob'
                    ' pattern, but does not match regex.'%path )
            continue
        chunkID, runID = int(m.group(1)), int(m.group(2))
        if runID in d:
            if chunkID in d[runID]:
                sys.stderr('Ignoring duplicating chunk "%s"; '
                        'previously accounted one is "%s"'%( path, d[runID][chunkID] ))
                continue
            d[runID][chunkID] = path
        else:
            d[runID] = { chunkID : path }
    return d

def calib_measurements( dataDirPath, calibDescription ):
    chunks = get_chunks_list_at(dataDirPath)
    description = pe.parse_calib(calibDescription)

    for cellsLine in description:
        for cell in cellsLine:
            calibChunks = []
            for runID in cell.r:
                calibChunks += list((str(path), runID, chunkID) for chunkID, path in chunks[runID].items())
            argDict = {
                    'cellIndex' : ( cell.idxs[0], cell.idxs[1] ),
                    'beamPosition' : ( cell.c[0]/100, cell.c[1]/100 ),
                    'measuredDistributions' : {
                        'preshower' : (cell.p[0], cell.p[1]),
                        'main' : (cell.s[0], cell.s[1])
                    },
                    'calibChunks' : calibChunks
                }
            yield argDict

def run_pipe( md, outDir='.', pipeExec='na64dp-pipe', opts=[] ):
    outFilePath = 'calibration-output-%dx%d.root'%(md['cellIndex'])
    outFilePath = os.path.join( outDir, outFilePath )
    cmd = [pipeExec] \
        + opts \
        + ['--root-file=%s'%outFilePath ] \
        + [ e[0] for e in md['calibChunks'] ]
    #+ ['-D@%d'%(5723 + )]  # requires container to open multiple ports
    #print(cmd)
    return subprocess.call(cmd, shell=False)

#
# Entry point
############

# Run snippet (from build dir):
#   $ NA64DP_CFG_ROOT=../ecal-edu/ ../ecal-edu/utils/ecal-calib/preview_calib_data.py \
#           --data-dir /data/ \
#           --desc ../ecal-edu/presets/ecal_coord_calibration-09.09.017.txt \
#           -- -M ../p348-daq/maps/ -r ../ecal-edu/presets/pipe-configs/util/ecal-calibration.yaml
if '__main__' == __name__:
    p = argparse.ArgumentParser( description='Make preview of ECAL calibration data' )
    p.add_argument( '-c', '--description', help='Calibration runs description file' )
    p.add_argument( '-d', '--data-dir', help='Directory with chunks (will be scanned'
            ' recursively)' )
    p.add_argument( '-e', '--pipe-exec', help='Location of na64dp-pipe util'
            , default='./na64dp-pipe')
    p.add_argument( '-j', '--jobs', help='Max number of parallel subprocess'
            ' call to operate', default=multiprocessing.cpu_count(), type=int )
    p.add_argument( '-o', '--out-dir', help='Output dir where the .root files'
            ' will be written' )
    p.add_argument( 'pipeopts', nargs='*' )
    args = p.parse_args()

    pool = multiprocessing.Pool(args.jobs)

    pool.map( #lambda m : run_pipe(m, pipeExec=args.pipe_exec, opts=args.pipeopts)
            partial( run_pipe
                   , pipeExec=args.pipe_exec
                   , opts=args.pipeopts
                   , outDir=args.out_dir )
            , calib_measurements( args.data_dir, args.description )
            )
    #for m in calib_measurements( args.data_dir, args.description ):
    #    worker = workers.get_vacant()
    #    worker.run( run_pipe, m, pipeExec=args.pipe_exec, opts=args.pipeopts )
