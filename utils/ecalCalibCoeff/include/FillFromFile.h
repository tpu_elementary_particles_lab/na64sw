#pragma once
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_matrix_double.h>
#include <gsl/gsl_linalg.h>

// counts the number of lines in a file
int number_line_in_file (const char* NameFile);

//
void fill_gsl_matrix_from_file(const char* NameFile
                              , gsl_matrix* OriginalMatrix);
