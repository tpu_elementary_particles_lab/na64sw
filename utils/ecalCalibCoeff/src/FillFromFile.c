#include "FillFromFile.h"

// counts the number of lines in a file
// TODO: reading file char by char is higly inefficient
int number_line_in_file( const char* fileName ){
    // open file for read
    FILE* file = fopen( fileName, "r" );
    // check if the file is open  
    if ( !file) {
        fprintf( stderr
               , "Unable to open file \"%s\": %s.\n"
               , fileName
               , strerror(errno) );
        return EXIT_FAILURE;
    }
    // counter for the number of rows
    int counter = 0;
    char ch;  
    // calculation of the number of lines
    while( (ch = fgetc(file)) != EOF ){
        if ( ch == '\n' ){ ++counter; }
    }
    // closing the file and returning the number of lines
    fclose(file);
    return counter;
}

//
void fill_gsl_matrix_from_file( const char* fileName
                              , gsl_matrix* OriginalMatrix){
    // opening a file for reading values and writing to a matrix
    FILE* file = fopen( fileName, "r" );
    // temporary variable for writing a value from a file
    float temp = 0;  
    for( int i = 0; i < OriginalMatrix->size1; i++ ){
        for( int j = 0; j < OriginalMatrix->size2; j++ ){
            // writing to a temporary variable the values
            fscanf( file, "%f ", &temp );
            // write to the matrix element with position
            // i and j values from a temporary variable
            if(temp >= 0 ){
                gsl_matrix_set(OriginalMatrix, i, j, temp);
            } else { 
                gsl_matrix_set(OriginalMatrix, i, j, 0);
            }
        }
    }
    // close file
    fclose(file); 
}

