#include "ecalCalibCoef.h"
#include "FillFromFile.h"
#include "ApplicationConfig.h"
#include "MatrixProcess.h"

int main(int argc, char * argv[]){
    // creating a structure with command line interface parameters
    ApplicationConfig AppConf;
    // setting parameters to the structure from the command line
    int rs = set_app_config(&AppConf, argc, argv);
    // if was used help (-h) opt
    if( rs == 1 ) { return EXIT_SUCCESS;}
    
    //heck if cell arguments were entered
    if(AppConf.FlagProc.PrintCell){
        if ( AppConf.position.x == -1 
          || AppConf.position.y == -1 
          || AppConf.position.z == -1){
            fprintf(stderr, "At least one cell argument is not set.\n");
            return EXIT_FAILURE;;
        }
    }
    
    // processor cycle number recording to calculate runtime
    unsigned int start_time =  clock();

    // number of columns and rows in the original matrix
    int col = 72
      , row = number_line_in_file( AppConf.DirParam.InFileName );

    // creating a matrix GSL of size row by col
    gsl_matrix* OriginalMatrix = gsl_matrix_alloc (row, col);
    // filling matrix from file
    fill_gsl_matrix_from_file(AppConf.DirParam.InFileName, OriginalMatrix);

    // Print runtime and original matrix
    if( AppConf.FlagProc.PrintInfoProcess ){
        printf("Original matrix (%5.2f s): \n", print_time(start_time));
    }
      
    // space initialization for pseudoinverse matrix Moore–Penrose
    gsl_matrix* PseudoInverseMatrix = gsl_matrix_alloc (col, row);
    // Get pseudoinverse matrix from original matrix   
    float determ = get_pseudoinv_matrix(OriginalMatrix, PseudoInverseMatrix);
    //
    if( determ == 0 ){
        printf("Determ %f\n", determ);
        fprintf(stderr, "Determ %f\n", determ);
        exit(1);
    }

    // Print runtime and pseudoinverse matrix
    if (AppConf.FlagProc.PrintInfoProcess) {
        printf("pseudoinverse matrix (%5.2f s): \n", print_time(start_time));
    }  

    // space initialization for check pseudoinverse matrix
    gsl_matrix* CheckMatrix = gsl_matrix_alloc (col, col);
    // matrix by pseudoinverse matrix multiplication to check
    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans
                   , 1.0, PseudoInverseMatrix, OriginalMatrix 
                   , 0.0, CheckMatrix);
    // Print runtime and pseudoinverse matrix
    if (AppConf.FlagProc.PrintInfoProcess) {
        printf( "matrix and pseudoinverse matrix multiplication (%5.2f s):\n"
              , print_time(start_time));
    }
   
    // prints the sum of matrix elements
    if (AppConf.FlagProc.PrintInfoProcess) {
        printf( "Sum element matrix divided by min(row, col):  %e \n" 
              , sum_gsl_matrix_element(CheckMatrix) );
    }

    // initialization of the vector under the full energy in the event
    gsl_vector* EnergTotl = gsl_vector_alloc (row);
    // full energy vector filling
    fill_vector_energ (EnergTotl);
    // initialization of a vector for an array of coefficients
    gsl_vector* Coeff  = gsl_vector_alloc (col);
    // multiplication of the pseudoinverse matrix and 
    // the vector of free elements (column of total energies)
    gsl_blas_dgemv( CblasNoTrans, 1, PseudoInverseMatrix
                  , EnergTotl, 0, Coeff);
    
    // print table coefficients
    if (AppConf.FlagProc.PrintTableCoeff){
        print_gsl_vector(Coeff);
        printf("\n");
    }
    
    // print energy information 
    if(AppConf.FlagProc.PrintInfoEnerge){
        //
        float ChiOndeg = chi_sq_gsl_matrix_vector(EnergTotl, Coeff, OriginalMatrix);
        //
        printf("chi_sq_on_deg %f\n", ChiOndeg);
        // multiplication of the matrix of sums signals
        // in the cell and calibration coefficients
        gsl_blas_dgemv( CblasNoTrans, 1, OriginalMatrix 
                      , Coeff, 0, EnergTotl );
        // Max energy in vector with energy 
        printf("Max energy: %7.3f \n", gsl_vector_max(EnergTotl));
        // Mix energy in vector with energy
        printf("Min energy: %7.3f \n", gsl_vector_min(EnergTotl));
        // Mean energy in vector with energy
        printf("Mean energy: %7.3f \n",  mean_gsl_vector(EnergTotl));
    }
    
    //print cell number and its coefficient
    if(AppConf.FlagProc.PrintCell){
        int i = AppConf.position.z * 36 + AppConf.position.y * 6 + AppConf.position.x;
        printf( "%d_%d_%d %10.4e%s\n", AppConf.position.x, AppConf.position.y
                                 , AppConf.position.z,  gsl_vector_get(Coeff, i ), " GeV/S" );
    }

    // delete GSL object
    gsl_matrix_free(OriginalMatrix);
    gsl_matrix_free(PseudoInverseMatrix);
    gsl_matrix_free(CheckMatrix);
    gsl_vector_free(EnergTotl);
    gsl_vector_free(Coeff); 
    return 0;
}

