Run(s): [3159], beam position: x=-955 mm, y=955 mm
0-5-0 : mean = 1486 / sigma = 547.2
0-5-1 : mean = 2493 / sigma = 136.7

Run(s): [3148], beam position: x=-955 mm, y=573 mm
0-4-0 : mean = 1593 / sigma = 540.7
0-4-1 : mean = 2269 / sigma = 130.7

Run(s): [3144], beam position: x=-955 mm, y=191 mm
0-3-0 : mean = 1494 / sigma = 493.0
0-3-1 : mean = 2187 / sigma = 110.0

Run(s): [3132], beam position: x=-955 mm, y=-191 mm
0-2-0 : mean = 1329 / sigma = 505.0
0-2-1 : mean = 2337 / sigma = 118.8

Run(s): [3131], beam position: x=-955 mm, y=-573 mm
0-1-0 : mean = 1438 / sigma = 479.7
0-1-1 : mean = 2357 / sigma = 127.8

Run(s): [3119], beam position: x=-955 mm, y=-955 mm
0-0-0 : mean = 1132 / sigma = 453.8
0-0-1 : mean = 2272 / sigma = 123.9

Run(s): [3158], beam position: x=-573 mm, y=955 mm
1-5-0 : mean = 1695 / sigma = 581.1
1-5-1 : mean = 2269 / sigma = 124.1

Run(s): [3149], beam position: x=-573 mm, y=573 mm
1-4-0 : mean = 1390 / sigma = 501.0
1-4-1 : mean = 2368 / sigma = 124.2

Run(s): [3142], beam position: x=-573 mm, y=191 mm
1-3-0 : mean = 1324 / sigma = 471.0
1-3-1 : mean = 2042 / sigma = 114.3

Run(s): [3133], beam position: x=-573 mm, y=-191 mm
1-2-0 : mean = 1489 / sigma = 521.8
1-2-1 : mean = 2152 / sigma = 119.7

Run(s): [3130], beam position: x=-573 mm, y=-573 mm
1-1-0 : mean = 1404 / sigma = 524.6
1-1-1 : mean = 2314 / sigma = 166.6

Run(s): [3120], beam position: x=-573 mm, y=-955 mm
1-0-0 : mean = 1373 / sigma = 497.0
1-0-1 : mean = 2283 / sigma = 121.5

Run(s): [3157], beam position: x=-191 mm, y=955 mm
2-5-0 : mean = 1430 / sigma = 525.0
2-5-1 : mean = 2228 / sigma = 124.4

Run(s): [3150], beam position: x=-191 mm, y=573 mm
2-4-0 : mean = 1440 / sigma = 516.0
2-4-1 : mean = 2316 / sigma = 115.2

Run(s): [3141], beam position: x=-191 mm, y=191 mm
2-3-0 : mean = 1353 / sigma = 495.0
2-3-1 : mean = 2356 / sigma = 114.4

Run(s): [3134], beam position: x=-191 mm, y=-191 mm
2-2-0 : mean = 1300 / sigma = 511.0
2-2-1 : mean = 2341 / sigma = 101.6

Run(s): [3129], beam position: x=-191 mm, y=-573 mm
2-1-0 : mean = 1169 / sigma = 449.6
2-1-1 : mean = 2365 / sigma = 94.7

Run(s): [3121], beam position: x=-191 mm, y=-955 mm
2-0-0 : mean = 1357 / sigma = 514.9
2-0-1 : mean = 2353 / sigma = 104.5

Run(s): [3156], beam position: x=191 mm, y=955 mm
3-5-0 : mean = 1139 / sigma = 435.7
3-5-1 : mean = 2198 / sigma = 119.0

Run(s): [3151], beam position: x=191 mm, y=573 mm
3-4-0 : mean = 1470 / sigma = 551.6
3-4-1 : mean = 2424 / sigma = 124.9

Run(s): [3140], beam position: x=191 mm, y=191 mm
3-3-0 : mean = 1480 / sigma = 683.6
3-3-1 : mean = 2297 / sigma = 112.4

Run(s): [3135], beam position: x=191 mm, y=-191 mm
3-2-0 : mean = 1432 / sigma = 644.2
3-2-1 : mean = 2302 / sigma = 111.3

Run(s): [3128], beam position: x=191 mm, y=-573 mm
3-1-0 : mean = 1435 / sigma = 557.6
3-1-1 : mean = 2200 / sigma = 110.5

Run(s): [3122], beam position: x=191 mm, y=-955 mm
3-0-0 : mean = 1412 / sigma = 586.4
3-0-1 : mean = 2097 / sigma = 106.6

Run(s): [3155], beam position: x=573 mm, y=955 mm
4-5-0 : mean = 1549 / sigma = 551.8
4-5-1 : mean = 2036 / sigma = 123.0

Run(s): [3152], beam position: x=573 mm, y=573 mm
4-4-0 : mean = 1416 / sigma = 500.5
4-4-1 : mean = 2350 / sigma = 140.1

Run(s): [3139], beam position: x=573 mm, y=191 mm
4-3-0 : mean = 1570 / sigma = 539.7
4-3-1 : mean = 2274 / sigma = 126.5

Run(s): [3136], beam position: x=573 mm, y=-191 mm
4-2-0 : mean = 1298 / sigma = 491.0
4-2-1 : mean = 2179 / sigma = 112.8

Run(s): [3126, 3127], beam position: x=573 mm, y=-573 mm
4-1-0 : mean = 1074 / sigma = 413.7
4-1-1 : mean = 2625 / sigma = 133.9

Run(s): [3123], beam position: x=573 mm, y=-955 mm
4-0-0 : mean = 1435 / sigma = 547.3
4-0-1 : mean = 2337 / sigma = 127.8

Run(s): [3154], beam position: x=955 mm, y=955 mm
5-5-0 : mean = 1290 / sigma = 489.7
5-5-1 : mean = 2233 / sigma = 120.6

Run(s): [3153], beam position: x=955 mm, y=573 mm
5-4-0 : mean = 780 / sigma = 314.9
5-4-1 : mean = 2263 / sigma = 122.4

Run(s): [3138], beam position: x=955 mm, y=191 mm
5-3-0 : mean = 1420 / sigma = 486.3
5-3-1 : mean = 2291 / sigma = 118.0

Run(s): [3137], beam position: x=955 mm, y=-191 mm
5-2-0 : mean = 1618 / sigma = 554.0
5-2-1 : mean = 2322 / sigma = 125.1

Run(s): [3125], beam position: x=955 mm, y=-573 mm
5-1-0 : mean = 1323 / sigma = 500.3
5-1-1 : mean = 2009 / sigma = 138.4

Run(s): [3124], beam position: x=955 mm, y=-955 mm
5-0-0 : mean = 1343 / sigma = 535.0
5-0-1 : mean = 2577 / sigma = 172.4