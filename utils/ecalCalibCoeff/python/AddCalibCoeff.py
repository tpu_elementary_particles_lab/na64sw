import re
import subprocess, os

# search for files by the given file name <name> 
# and fixed directory <path>. 
# Returns the path to the file if it finds it
def find_file(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)

# Application Launch. Accepts the arguments: 
# [<"path to application">, <"key 1">, <"key value 1"> ...]. 
# Returns application output
def StartApp(PartNameApp):
    process = subprocess.Popen( PartNameApp #['env'] 
                              , stdout=subprocess.PIPE
                              , stderr=subprocess.PIPE
                              , encoding='utf-8'
                              )
    out, err = process.communicate()
    return out

# Template to search for number runs
# and cell position in a text document
pattern = 'Run\D+\[(\d{4})\D?.?(\d{4})?\]\D+x=([\s+-]?\d+\.?\d*)\D+y=([\s+-]?\d+\.?\d*)\D+(\d\D\d\D\d)\D+mean\D+(\d+\.?\d*)\D+sigma\D+(\d+\.?\d*)\D+(\d\D\d\D\d)\D+mean\D+(\d+\.?\d*)\D+sigma\D+(\d+\.?\d*)'
# Writing text from a file to a temporary 
# variable for further work
InFile = open('utils/ecalCalibCoeff/data.txt', 'r')
string = InFile.read()
InFile.close()
# Search in the document for 
# the runs name and cell position
RunsName = re.search(pattern, string)
# Temporary variable for storing cell position
tempCell = ''
# Path to the folder with calibration runs
#pathData = "/home/ilyavoronchikhin/project/na64-soft/data"
pathData = "../data/" 
# Path to pipe
pathPipe = "./bin/pipe"
# Path to ecc
pathecc = "./bin/ecc"
# Dictionary for coefficients
dictCoeff = {}

# The cycle in which iteration occurs on the found calibration
# wounds. At each iteration, the file is searched with the name
# found, then the pipeline starts, then ecc starts. The pipeline
# starts with the transferred location of the calibration run,
# forming a matrix of areas in a text file. ecc based on 
# this matrix receives coefficients. After receiving the 
# coefficients, they are recorded
if RunsName:
    for RunsName in re.finditer(pattern, string):
        # Temporary variable for file name
        tempNameFile = ''
        # Temporary variable for runs name and cell position
        tempCell = RunsName[5]
        tempNameRun = RunsName[1]
        # Checking the existence of a calibration file
        if find_file('cdr01001-00' + tempNameRun + '.dat', pathData):
            # File name generation
            tempNameFile += 'cdr01001-00' + tempNameRun + '.dat'
            # Output file name and cell position
            OutCommandCons = "Find run: " + find_file(tempNameFile, pathData)
            OutCommandCons += " -x " + tempCell[0] + " -y " + tempCell[2]
            print(OutCommandCons)
            # Arguments for starting the pipeline
            argumentPipe = [ pathPipe
                           , find_file(tempNameFile, pathData)
                           , "-c", "config.yaml"
                           , "-M", "../p348-daq/maps/"

                           ]
            # Launch pipeline
            print("Start pipeline:", " ", pathPipe, "with ", find_file(tempNameFile, pathData) )
            StartApp(argumentPipe)
            # Filling the coefficients in the dictionary
            for i in (0, 1):
                # Set cell position
                xPosition = tempCell[0]
                yPosition = tempCell[2]
                zPosition = i
                # Arguments to run ecc 
                argumentCoff = [ pathecc
                               , "-f", "SumForCalib.txt"
                               , "-x", str(xPosition), "-y", str(yPosition), "-z", str(zPosition), "-C"]
                # Launch ecc
                print("Start ecc", pathecc, "with ", "SumForCalib.txt" )
                tempNameCoeff = StartApp(argumentCoff)
                print( "calculated for cell coefficient:", tempNameCoeff)
                # If the coefficient was not calculated
                if tempNameCoeff:
                    # Separation of output by cell position and 
                    # coefficient values for this cell
                    CellCoff = tempNameCoeff.split(" ")
                    # Creating a key to write to the dictionary
                    tempName = (CellCoff[0][0], CellCoff[0][2], str(zPosition) )
                    # Writing the coefficient value for the received key
                    #print(CellCoff)
                    if tempName in dictCoeff:
                        temp = str(dictCoeff[tempName])
                        temp += " " + str(CellCoff[1])  
                        dictCoeff[tempName] = temp
                    else:
                        dictCoeff[tempName] = CellCoff[1] 
                else:
                    # If the coefficient was not calculated
                    print("not coeff")
        else:
            print("Not find data file ", 'cdr01001-00' + tempNameRun + '.dat')
else:
    print("Нет совпадений!")

# Open file for writing
OutFile = open('CalibCoeff.txt', 'w+')
for dictIt in dictCoeff.keys():
    print("%s_%s_%s %s\n"%(dictIt[0], dictIt[1], dictIt[2], dictCoeff[dictIt]))
    OutFile.write("%s_%s_%s %s\n"%(dictIt[0], dictIt[1], dictIt[2], dictCoeff[dictIt]))
OutFile.close()

