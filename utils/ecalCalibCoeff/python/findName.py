import re
import subprocess, os

# search for files by the given file name <name> 
# and fixed directory <path>. 
# Returns the path to the file if it finds it
def find_file(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)

# Application Launch. Accepts the arguments: 
# [<"path to application">, <"key 1">, <"key value 1"> ...]. 
# Returns application output
def StartApp(PartNameApp):
    process = subprocess.Popen( PartNameApp #['env'] 
                              , stdout=subprocess.PIPE
                              , stderr=subprocess.PIPE
                              , encoding='utf-8'
                              )
    out, err = process.communicate()
    return out

# Template to search for number runs
# and cell position in a text document
pattern = 'Run\D+\[(\d{4})\D?.?(\d{4})?\]\D+x=([\s+-]?\d+\.?\d*)\D+y=([\s+-]?\d+\.?\d*)\D+(\d\D\d\D\d)\D+mean\D+(\d+\.?\d*)\D+sigma\D+(\d+\.?\d*)\D+(\d\D\d\D\d)\D+mean\D+(\d+\.?\d*)\D+sigma\D+(\d+\.?\d*)'
# Writing text from a file to a temporary 
# variable for further work
InFile = open('utils/ecalCalibCoeff/data.txt', 'r')
string = InFile.read()
InFile.close()
# Search in the document for 
# the runs name and cell position
RunsName = re.search(pattern, string)
# Temporary variable for storing cell position
tempCell = ''
# Path to the folder with calibration runs
#pathData = "/home/ilyavoronchikhin/project/na64-soft/data"
pathData = "/run/user/1000/gvfs/smb-share:server=109.123.135.76,share=share/na64-data/ecal-calib" 
# Path to pipe
pathPipe = "./bin/pipe"
# Path to ecc
pathecc = "./bin/ecc"
# Dictionary for coefficients
dictCoeff = {}

# The cycle in which iteration occurs on the found calibration
# wounds. At each iteration, the file is searched with the name
# found, then the pipeline starts, then ecc starts. The pipeline
# starts with the transferred location of the calibration run,
# forming a matrix of areas in a text file. ecc based on 
# this matrix receives coefficients. After receiving the 
# coefficients, they are recorded
if RunsName:
    for RunsName in re.finditer(pattern, string):
        # Temporary variable for file name
        tempNameFile = ''
        # Temporary variable for runs name and cell position
        tempCell = RunsName[5]
        tempNameRun = RunsName[1]
        #
        if find_file('cdr01001-00' + tempNameRun + '.dat', pathData):
            print()    
        else:
            print("Not find data file ", 'cdr01001-00' + tempNameRun + '.dat' + " -x " + tempCell[0] + " -y " + tempCell[2])
else:
    print("Нет совпадений!")

# Open file for writing
OutFile = open('CalibCoeff.txt', 'w+')
for dictIt in dictCoeff.keys():
    print("%s_%s_%s %s\n"%(dictIt[0], dictIt[1], dictIt[2], dictCoeff[dictIt]))
    OutFile.write("%s_%s_%s %s\n"%(dictIt[0], dictIt[1], dictIt[2], dictCoeff[dictIt]))
OutFile.close()
