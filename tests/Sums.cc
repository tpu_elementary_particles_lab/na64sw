#include "na64util/numerical/online.hh"

#include <cmath>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_vector.h>
#define delete delete_sample
#include <gsl/gsl_movstat.h>
#undef delete

#include <fstream>
#include <gtest/gtest.h>

namespace na64dp {
namespace test {

using namespace numerical;

// Checks basic validity of Klein scorer implementation; uses series with known
// sum to validate summation convergence.
// TODO: currently this UT takes 10ms and is one of the slowest UTs. Consider
// using series with faster convergence and attenuation instead
// of Basel's problem/Apéry's constant calculus
TEST(Sums, KleinScorer) {
    // Basel's problem is used to approximate the convergence limit of the sum;
    // unfortunately, convergence is too slow to be used in unit tests
    #if 0
    const double sumLimit = M_PI*M_PI/6.;
    KleinScorer kleinS;
    double directS = 0;
    for( long int i = 1; i < 1e7; ++i ) {
        const double v = 1./(i*i);
        directS += v;
        kleinS.add(v);
    }
    const double directError = fabs( sumLimit - directS )
               , kleinError = fabs( sumLimit - kleinS.result() )
               ;
    std::cout << "xxx direct=" << directS << " (" << directError << ")"
              << ", klein=" << kleinS.result() << " (" << kleinError << ")"
              << ", real=" << sumLimit << std::endl;
    #else
    // Apéry's constant as cubic series converges faster
    const double sumLimit = 1.202056903159594285399738161511449990764986292;
    KleinScorer kleinS;
    double directS = 0;
    // Note: number of iterations needed to reveal the error may vary depending
    // on hardware/platform/double type precision, etc.
    for( long int i = 1; i < 3e5; ++i ) {
        const double v = 1./(i*i*i);
        directS += v;
        kleinS += v;
    }
    const double directError = fabs( sumLimit - directS )
               , kleinError = fabs( sumLimit - kleinS )
               ;
    #endif
    // In case of errors, check this output:
    //std::cout << "xxx direct=" << directS << " (" << directError << ")"
    //          << ", klein=" << kleinS.result() << " (" << kleinError << ")"
    //          << ", real=" << sumLimit << std::endl;
    EXPECT_GT( directError, kleinError );
}

// Uses GSL routines to validate the online covariance calculus on each step
TEST(Sums, Correlation) {
    double testSamples[][2] = {
        {3.45, 1.18},
        {7.62, 2.33},
        {5.56, 1.76},
        {.44,  0.12},
        {7.89, 5.45},
        {-1.34, 0.1},
        {123.37, 80},
        {std::nan("0"), 0.}
    };
    double dataX[sizeof(testSamples)/(2*sizeof(double))]
         , dataY[sizeof(testSamples)/(2*sizeof(double))]
         ;
    CorrelationScorer C;
    size_t n = 0;
    for( double (* sample) [2] = testSamples
       ; !std::isnan((*sample)[0])
       ; ++sample ) {
        //std::cout << "xxx accounting " << (*sample)[0]
        //          << ", " << (*sample)[1] << std::endl;
        C.account( (*sample)[0], (*sample)[1] );
        dataX[n] = (*sample)[0];
        dataY[n] = (*sample)[1];

        ++n;
        double rMeanX = gsl_stats_mean( dataX, 1, n )
             , rMeanY = gsl_stats_mean( dataY, 1, n )
             ;
        ASSERT_NEAR( rMeanX, C.mean_x(), 1e-12 );
        ASSERT_NEAR( rMeanY, C.mean_y(), 1e-12 );
        if( n > 1 ) {
            const double rVarX = gsl_stats_variance_m( dataX, 1, n, rMeanX )
                       , rVarY = gsl_stats_variance_m( dataY, 1, n, rMeanY )
                       , rCov  = gsl_stats_covariance_m( dataX, 1
                                                       , dataY, 1
                                                       , n, rMeanX, rMeanY )
                       , rCorr = gsl_stats_correlation( dataX, 1
                                                      , dataY, 1
                                                      , n  );
                       ;
            ASSERT_NEAR( rVarX, C.variance_x(), 1e-12 );
            ASSERT_NEAR( rVarY, C.variance_y(), 1e-12 );
            ASSERT_NEAR( rCov,  C.covariance(), 1e-12 );
            ASSERT_NEAR( rCorr, C.pearson_correlation(), 1e-12 );
        }
    }
}

// Uses GSL routines to validate the online covariance matrix calculus on
// each step
TEST(Sums, CorrelationMatrix) {
    double testSamples[][5] = {
        {3.45,  1.18,  -3, 0, 0.11},
        {7.62,  2.33,  -7, 1, 0.87},
        {5.56,  1.76, -11, 0, 0.53},
        {.44,   0.12,  -1, 0, 0.21},
        {7.89,  5.45, 786, 0, 0.44},
        {-1.34,  0.1, -99, 0, 0.42},
        {123.37,  80,  11, 1, 0.  },
        {std::nan("0"), 0, 0, 0, 0}
    };
    CovarianceMatrix m(5);
    double means[5], rVar[5], rCov[5][5], rCorr[5][5];
    size_t n = 0;
    for( double (* sample) [5] = testSamples
       ; !std::isnan((*sample)[0])
       ; ++sample ) {
        m.account(*sample);
        ++n;
        for( int i = 0; i < 5; ++i ) {
            means[i] = gsl_stats_mean( (*testSamples) + i, 5, n );
            EXPECT_NEAR( means[i], m.mean(i), 1e-10 );
        }
        if( n < 2 ) continue;
        for( int i = 0; i < 5; ++i ) {
            rVar[i] = gsl_stats_variance_m( (*testSamples) + i, 5, n, means[i] );
            EXPECT_NEAR( rVar[i], m.variance(i), 1e-10 );
            for( int j = i; j < 5; ++j ) {
                rCov[i][j] = gsl_stats_covariance_m( (*testSamples) + i, 5
                                                   , (*testSamples) + j, 5
                                                   , n, means[i], means[j] );
                rCorr[i][j] = gsl_stats_correlation( (*testSamples) + i, 5
                                                   , (*testSamples) + j, 5
                                                   , n );
                //std::cout << "xxx #" << n << " "
                //          << i << ", " << j
                //          << ", vars=" << rVar[i] << "/" << rVar[j]
                //          << ", real cov=" << rCov[i][j]
                //          << ", m cov=" << m.covariance(i, j)
                //          << ", real cor=" << rCorr[i][j]
                //          << ", m corr=" << m.pearson_correlation(i, j)
                //          << std::endl;  // XXX
                EXPECT_NEAR( rCov[i][j],  m.covariance(i, j), 1e-10 );
                ASSERT_NEAR( rCorr[i][j], m.pearson_correlation(i, j), 1e-10 );
            }
        }
    }
}

// uncomment to write the output file
//#define WRITE_SUM_COMPARISON "./sums.2move/sliding-sum-10000x200-norecacheO.klein.dat"

TEST(Sums, MovingStats) {
    #ifdef WRITE_SUM_COMPARISON
    std::ofstream ofs(WRITE_SUM_COMPARISON);
    #endif
    const size_t nSamples = 10000
               , winSize = 200
               ;
    double args[nSamples];
    gsl_vector * inVec = gsl_vector_alloc(nSamples);
    MovingStats<KleinScorer> ms(winSize, false, true, 0.);
    // Generate samples -- a tangent curve with logarithmic sampling
    for( size_t i = 0; i < nSamples; ++i ) {
        double arg_ = pow((i+1)/double(nSamples), 8);
        args[i] = arg_*2*M_PI;
        gsl_vector_set( inVec, i, tan( args[i] ) );
    }

    gsl_movstat_workspace * wsPtr = gsl_movstat_alloc2(winSize - 1, 0);
    gsl_vector * meanVec = gsl_vector_alloc(nSamples)
             , * varVec = gsl_vector_alloc(nSamples)
             , * stdDevVec = gsl_vector_alloc(nSamples)
             ;

    // Compute window mean and standard deviation over all the input values
    auto endType = GSL_MOVSTAT_END_TRUNCATE;
    gsl_movstat_mean( endType, inVec, meanVec, wsPtr );
    gsl_movstat_variance( endType, inVec, varVec, wsPtr );
    gsl_movstat_sd( endType, inVec, stdDevVec, wsPtr );

    #ifdef WRITE_SUM_COMPARISON
    ofs << "#"
        << std::setw(14) << "arg "          // 1
        << std::setw(15) << "sample "       // 2
        << std::setw(15) << "m.direct "     // 3
        << std::setw(15) << "m.mov "        // 4
        << std::setw(15) << "m.gsl "        // 5
        << std::setw(15) << "m.d.vs.gsl "   // 6
        << std::setw(15) << "m.d.vs.mov "   // 7
        //<< std::setw(14) << ""
        << std::endl;
    #endif
    for( ssize_t i = 0; i < (ssize_t) nSamples; ++i ) {
        ms.account( gsl_vector_get(inVec, i) );
        //if( (ms.mean() - gsl_vector_get( meanVec, i ))/ms.mean() < 1e-2 ) continue;
        ssize_t k = 0;
        double directMean = 0.;
        for( k = 0; k < (ssize_t) winSize && i - k >= 0; ++k ) {
            double sample = gsl_vector_get( inVec, i - k );
            directMean += sample;
        }
        directMean /= k;
        #ifdef WRITE_SUM_COMPARISON
        ofs << std::scientific
            << std::setw(14) << args[i] << " "
            << std::setw(14) << gsl_vector_get( inVec, i ) << " "
            << std::setw(14) << directMean << " "
            << std::setw(14) << ms.mean() << " "
            << std::setw(14) << gsl_vector_get( meanVec, i ) << " "
            << std::setw(14) << fabs(directMean - gsl_vector_get( meanVec, i )) << " "
            << std::setw(14) << fabs(directMean - ms.mean()) << " "
            //<< std::setw(14) << gsl_vector_get( varVec, i ) << " "
            //<< std::setw(14) << gsl_vector_get( stdDevVec, i )
            << std::endl;
        #endif
    }

    gsl_vector_free(inVec);
    gsl_vector_free(meanVec);
    gsl_vector_free(varVec);
    gsl_vector_free(stdDevVec);
    gsl_movstat_free(wsPtr);
}

}
}
