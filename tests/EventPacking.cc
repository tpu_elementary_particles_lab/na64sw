#include "na64event/event.hh"

#if defined(CapnProto_FOUND) && CapnProto_FOUND

#include <gtest/gtest.h>
#include <limits>

#include "na64event/serialize.hh"

namespace na64dp {
namespace test {

// We will fill the hits data with these values, alternating, to provide
// (some) variation within the data
const double cMx = std::numeric_limits<double>::max()
           , cMn = std::numeric_limits<double>::min()
           ;

namespace cpn = ::capnp::na64dp;

// Fills an APV hit with some testing data; depending on odd variable may
// alternate values to provide distinct hits within a cluster for subsequent
// check.
static void fill_apv_hit( APVHit & hit, int channelNo ) {
    bool odd = channelNo % 2;
    hit.rawData.wireNo = odd ? std::numeric_limits<APVWireNo_t>::max()
                             : std::numeric_limits<APVWireNo_t>::min()
                             ;
    hit.rawData.samples[0] = odd ? std::numeric_limits<uint32_t>::min()
                                 : std::numeric_limits<uint32_t>::max()
                                 ;
    hit.rawData.samples[1] = odd ? std::numeric_limits<uint32_t>::max()
                                 : std::numeric_limits<uint32_t>::min()
                                 ;
    hit.rawData.samples[2] = odd ? std::numeric_limits<uint32_t>::min()
                                 : std::numeric_limits<uint32_t>::max()
                                 ;
    hit.channelNo = channelNo;
    hit.a02 = odd ? cMn : cMx;
    hit.a12 = odd ? cMx : cMn;
    // ...
}
// Reentrant assertion-control function to compare two APV hits
static void compare_apv_hits( const APVHit & a
                            , const APVHit & b ) {
    EXPECT_EQ( a.rawData.wireNo, b.rawData.wireNo );
    EXPECT_EQ( a.rawData.samples[0], b.rawData.samples[0] );
    EXPECT_EQ( a.rawData.samples[1], b.rawData.samples[1] );
    EXPECT_EQ( a.rawData.samples[2], b.rawData.samples[2] );
    EXPECT_EQ( a.channelNo, b.channelNo );
    EXPECT_EQ( a.a02, b.a12 );
    EXPECT_EQ( a.a12, b.a12 );
    // ...
}


// Fills an APV cluster with some testing data
static void fill_apv_cluster( HitBanks & banks
                            , APVCluster & c
                            , int clusterID
                            , int nHits
                            , std::vector<std::string> & serializedHits
                            , serialization::StatePacking & statePack ) {
    bool odd = clusterID & 1;
    // some hits
    for( int i = 0; i < nHits; ++i ) {
        auto hitBPtr = banks.of<APVHit>().create();
        fill_apv_hit(*hitBPtr, i + clusterID);
        c.emplace( hitBPtr->channelNo, hitBPtr );
        // serialize hit
        serializedHits.push_back( serialization::to_bytes(*hitBPtr, statePack) );
    }
    c.position = odd ? cMx : cMn;
    c.wPosition = odd ? cMn : cMx;
    c.charge = odd ? cMx : cMn;
    c.sparseness = odd ? cMn : cMx;
}
// Reentrant assertion-control function to compare two APV clusters
static void compare_apv_clusters( const APVCluster & a
                                , const APVCluster & b ) {
    EXPECT_EQ( a.position,    b.position );
    EXPECT_EQ( a.wPosition,   b.wPosition );
    EXPECT_EQ( a.charge,      b.charge );
    EXPECT_EQ( a.sparseness,  b.sparseness );
    // ...
    EXPECT_EQ( a.size(), b.size() );
    for( auto hitRef : a ) {
        auto resHitRef = b.find( hitRef.first );
        ASSERT_NE( resHitRef, b.end() );
        compare_apv_hits( *(hitRef.second), *(resHitRef->second));
    }
}
static bool clusters_are_identical( const APVCluster & a
                                  , const APVCluster & b ) {
    if( a.position   != b.position ) return false;
    if( a.wPosition  != b.wPosition ) return false;
    if( a.charge     != b.charge ) return false;
    if( a.sparseness != b.sparseness ) return false;
    //if( a.size()     != b.size() ) return false;
    for( auto hitRef : a ) {
        auto resHitRef = b.find( hitRef.first );
        if( resHitRef == b.end() ) { return false; }
        //compare_apv_hits( *(hitRef.second), *(resHitRef->second));
    }
    // ...

    return true;
}


static void compare_track_points( const TrackPoint & a
                                , const TrackPoint & b ) {
    EXPECT_EQ( a.lR[0], b.lR[0] );
    EXPECT_EQ( a.lR[1], b.lR[1] );
    EXPECT_EQ( a.lR[2], b.lR[2] );
    EXPECT_EQ( a.gR[0], b.gR[0] );
    EXPECT_EQ( a.gR[1], b.gR[1] );
    EXPECT_EQ( a.gR[2], b.gR[2] );
    EXPECT_EQ( a.station, b.station );

    EXPECT_EQ( a.clusterRefs.size(), b.clusterRefs.size() );
    // Comparison of the clusters composing a track point is somehat complex
    // since it have distinct instances of the clusters that are
    // physically the same, so we have to rely on the data identity.
    // To accomplish this, first compose the std::multimap from what is found
    // in cluster references sets
    std::multimap<DetID_t, PoolRef<APVCluster> > aClusters( a.clusterRefs.begin(), a.clusterRefs.end() )
                                                   , bClusters( b.clusterRefs.begin(), b.clusterRefs.end() )
                                                   ;
    #if 0
    std::cout << "a ptr = " << &a << std::endl;
    for( auto aP : aClusters ) {
        std::cout << "a, " << aP.first << " <-> " << aP.second
                  << ", nHits=" << aP.second->size()
                  << std::endl;
    }
    std::cout << "b ptr = " << &b << std::endl;
    for( auto bP : bClusters ) {
        std::cout << "b, " << bP.first << " <-> " << bP.second
                  << ", nHits=" << bP.second->size()
                  << std::endl;
    }
    #endif
    ASSERT_EQ( aClusters.size(), bClusters.size() );
    // Now, compare clusters within each equal range
    for( auto aIt = aClusters.begin()
       ; aClusters.end() != aIt
       ; aIt = aClusters.upper_bound(aIt->first) ) {
        auto aERng = aClusters.equal_range( aIt->first )
           , bERng = bClusters.equal_range( aIt->first )
           ;
        // Assure both equal ranges have same number of elements
        ASSERT_EQ( std::distance( aERng.first, aERng.second )
                 , std::distance( bERng.first, bERng.second ) );
        // Find matches between both cluster sets, assuring that every cluster
        // from within range A has exactly one corresponding match in B
        for( auto it = aERng.first; it != aERng.second; ++it ) {
            bool found = false;
            for( auto iit = bERng.first; iit != bERng.second; ++iit ) {
                const APVCluster * aCPtr =  &(*it->second)
                               , * bCPtr = &(*iit->second)
                               ;
                ASSERT_GT( aCPtr->size(), 0 );
                ASSERT_GT( bCPtr->size(), 0 );
                // Assure none of the pairs are of the same ptr; fail
                // would mean that A/B sets were mixed
                ASSERT_NE( aCPtr, bCPtr );
                bool aMatchB = clusters_are_identical(*aCPtr, *bCPtr);
                if( aMatchB ) {
                    #if 0  // XXX
                    std::cout << "Match: cluster's detID=" << aIt->first
                              << ", nHits(a)=" << aCPtr->size()
                              << ", nHits(b)=" << bCPtr->size()
                              << std::endl;
                    #endif
                    // assure clusters really match (rather a check for 
                    // clusters_are_identical() as it does not really checks
                    // for identity).
                    compare_apv_clusters( *aCPtr, *bCPtr );
                    if( found ) {  // XXX
                        std::cerr << "False cluster association discovered!"
                                  << std::endl;
                    }
                    // assure that match has not been previously met
                    ASSERT_FALSE( found );
                    found = true;
                }
            }
            EXPECT_TRUE( found );
        }
    }
    // ...
}

TEST( EventSerialization, SADCHitSerialize ) {
    HitBanks banks;
    serialization::StatePacking statePack( banks );
    serialization::StateUnpacking stateUnpack( banks );
    SADCHit origHit, resHit;
    std::string serialized;
    {   // write serialized hit
        SADCHit & hit = origHit;

        // Fill hit with some data
        for( int i = 0; i < 32; ++i ) {
            hit.wave[i] = i%2 ? cMx : cMn;
        }
        hit.sum = cMx;
        hit.eDep = cMn;
        hit.maxSample = cMx;
        hit.maxValue = cMn;
        hit.pedestals[0] = cMx;
        hit.pedestals[1] = cMn;
        // Impose some fitting data in "fit"
        hit.fittingData = banks.of<WfFittingData>().create();
        hit.fittingData->chisq = cMx;
        hit.fittingData->rms = cMn;
        hit.fittingData->fitType = WfFittingData::moyal;
        auto & mpRef = hit.fittingData->fitPars.moyalPars;
        mpRef.p[0] = mpRef.p[2] = cMx;
        mpRef.p[1] = cMn;
        mpRef.err[0] = mpRef.err[1] = cMn;
        mpRef.err[1] = cMx;
        mpRef.chisq_dof = cMx;
        // ...

        // serialize hit using traits function
        serialized = serialization::to_bytes(hit, statePack);
    }
    {   // read serialized hit
        serialization::from_bytes( serialized, resHit, stateUnpack );
    }
    {   // check equality
        for( int i = 0; i < 32; ++i ) {
            EXPECT_EQ( origHit.wave[i], resHit.wave[i] );
        }
        EXPECT_EQ( origHit.sum, resHit.sum );
        EXPECT_EQ( origHit.eDep, resHit.eDep );
        EXPECT_EQ( origHit.maxSample, resHit.maxSample );
        EXPECT_EQ( origHit.maxValue, resHit.maxValue );
        EXPECT_EQ( origHit.pedestals[0], resHit.pedestals[0] );
        EXPECT_EQ( origHit.pedestals[1], resHit.pedestals[1] );
        // Check fitting equality
        EXPECT_EQ( origHit.fittingData->chisq
                 ,  resHit.fittingData->chisq );
        EXPECT_EQ( origHit.fittingData->rms
                 ,  resHit.fittingData->rms );
        EXPECT_EQ( origHit.fittingData->fitType
                 ,  resHit.fittingData->fitType );
        auto & oMpRef = origHit.fittingData->fitPars.moyalPars
           , & rMpRef =  resHit.fittingData->fitPars.moyalPars
           ;
        EXPECT_EQ( oMpRef.p[0], rMpRef.p[0] );
        EXPECT_EQ( oMpRef.p[1], rMpRef.p[1] );
        EXPECT_EQ( oMpRef.p[2], rMpRef.p[2] );
        EXPECT_EQ( oMpRef.err[0], rMpRef.err[0] );
        EXPECT_EQ( oMpRef.err[1], rMpRef.err[1] );
        EXPECT_EQ( oMpRef.err[2], rMpRef.err[2] );
        EXPECT_EQ( oMpRef.chisq_dof, rMpRef.chisq_dof );
        // ...
    }
}

TEST( EventSerialization, APVHitSerialize ) {
    HitBanks banks;
    serialization::StatePacking statePack( banks );
    serialization::StateUnpacking stateUnpack( banks );
    PoolRef<APVHit> origHitPtr = banks.of<APVHit>().create()
                      , resHitPtr = banks.of<APVHit>().create()
                      ;
    std::string serialized;

    {   // fill hit with some data
        APVHit & hit = *origHitPtr;
        fill_apv_hit(hit, 42);

        // serialize hit using traits function
        serialized = serialization::to_bytes(hit, statePack);
    }
    {   // deserialize
        serialization::from_bytes( serialized, *resHitPtr, stateUnpack);
    }
    {   // check
        compare_apv_hits( *origHitPtr, *resHitPtr );
    }
}


TEST( EventSerialization, APVClusterSerialize ) {
    HitBanks banks;
    serialization::StatePacking statePack = { banks };
    serialization::StateUnpacking stateUnpack = { banks };
    APVCluster & origCluster = *banks.of<APVCluster>().create()
             , & resCluster = *banks.of<APVCluster>().create();
    std::string serialized;
    std::vector<std::string> serializedHits;

    {   // fill cluster with some data
        APVCluster & c = origCluster;
        fill_apv_cluster( banks, c, 42, 14, serializedHits, statePack );

        // serialize hit using traits function
        serialized = serialization::to_bytes(c, statePack);
    }
    {   // deserialize
        for( auto hitSeries : serializedHits ) {
            auto hitBPtr = banks.of<APVHit>().create();
            serialization::from_bytes( hitSeries, *hitBPtr, stateUnpack );
        }
        serialization::from_bytes( serialized, resCluster, stateUnpack );
    }
    {   // check
        compare_apv_clusters( origCluster, resCluster );
    }
}

TEST( EventSerialization, TrackPointSerialize ) {
    HitBanks banks;
    serialization::StatePacking statePack( banks );
    serialization::StateUnpacking stateUnpack( banks );

    TrackPoint & origTrackPoint = *banks.of<TrackPoint>().create()
             , & resTrackPoint = *banks.of<TrackPoint>().create();
    std::string serialized;
    std::vector<std::string> serializedHits;
    std::vector<std::string> serializedClusters;

    {   // fill track point with data
        TrackPoint & tp = origTrackPoint;
        tp.lR[0] = tp.lR[2] = cMx;
        tp.lR[1] = cMn;
        tp.lR[0] = tp.lR[2] = cMn;
        tp.lR[1] = cMx;
        tp.station = 42;
        for( int i = 0 ; i < 10; ++i ) {
            // create track cluster
            auto clusterPtr = banks.of<APVCluster>().create();
            fill_apv_cluster( banks, *clusterPtr, i*21, 2*i + 1
                            , serializedHits, statePack );
            ASSERT_GT( clusterPtr->size(), 0 );
            serializedClusters.push_back( serialization::to_bytes(*clusterPtr, statePack) );
            // associated cluster with track point
            auto ir = tp.clusterRefs.insert(
                    std::pair<DetID_t, PoolRef<APVCluster>>( 42 + i / 3, clusterPtr) );
            ASSERT_TRUE( ir.second );
        }
        // serialize track point to string
        serialized = serialization::to_bytes( tp, statePack );
    }
    {   // deserialize
        for( auto hitSeries : serializedHits ) {
            auto hitBPtr = banks.of<APVHit>().create();
            serialization::from_bytes( hitSeries, *hitBPtr, stateUnpack );
        }
        for( auto clusterSeries : serializedClusters ) {
            auto clusPtr = banks.of<APVCluster>().create();
            serialization::from_bytes( clusterSeries, *clusPtr, stateUnpack );
            ASSERT_GT( clusPtr->size(), 0 );
        }
        serialization::from_bytes( serialized, resTrackPoint, stateUnpack);
    }
    {   // check track point identity
        compare_track_points(origTrackPoint, resTrackPoint);
    }
}

}
}

#endif
