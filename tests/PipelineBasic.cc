#include "na64dp/pipeline.hh"
#include "na64dp/abstractHandler.hh"
#include "na64dp/abstractEventSource.hh"

#include <gtest/gtest.h>

/**\file tests/PipelineBasic.cc
 *
 * Testing unit processing basic pipeline's discrimination logic as well as
 * construction of basic structures.
 * Creates two handlers within pipeline over a mocking source. The source
 * sets only the event identifier and yields fixed number of events. The
 * handlers discriminates each 2nd and 3rd event and records event ids passed
 * by.
 * Then the unit verifies that all the events supposed to be passed were passed
 * indeed.
 * */

namespace na64dp {
namespace test {

// Mock handler used for testing purposes; discriminates events by their IDs
class MockHandler : public AbstractHandler {
public:
    std::vector<EventID> idsPassed;
private:
    int _evIDDiv;
public:
    MockHandler( int evIDDiv )
                : _evIDDiv(evIDDiv) {}
    virtual ProcRes process_event(Event * event) override {
        if( event->id % _evIDDiv ) {
            idsPassed.push_back( event->id );
            return kOk;
        }
        return kDiscriminateEvent;
    }
};

// Mock events source
class MockSource : public AbstractEventSource {
private:
    size_t _evCounter
         , _evMaxCount;
public:
    MockSource( HitBanks & banks, size_t max )
            : AbstractEventSource(banks)
            , _evCounter(0)
            , _evMaxCount(max) {}
    virtual bool read(Event & e) override {
        ++_evCounter;
        e.id = _evCounter;
        return _evCounter < _evMaxCount;
    }
};

// Builds pipeline of two mock handlers over mock source, checks the basic
// discrimination logic
TEST(Pipeline, EventDiscriminationLogic) {
    HitBanks banks;
    MockHandler * h1 = new MockHandler(2)  // discriminate each 2nd event
              , * h2 = new MockHandler(3)  // discriminate each 3rd event
              ;
    Pipeline p;
    p.push_back(h1);
    p.push_back(h2);
    MockSource src(banks, 19);

    Event event(banks);
    while( src.read(event) ) {
        p.process( event );
    }
    // h1 has contain increasing odd numbers: 1, 3, 5 ... 17
    int nC = -1;
    EXPECT_EQ( h1->idsPassed.size(), 9 );
    for( auto n : h1->idsPassed ) {
        EXPECT_TRUE( n%2 );
        EXPECT_EQ( nC += 2, n );
    }
    // h1 has contain 1, 5, 7, 11, 13, 17
    const int checkArr[] = { 1, 5, 7, 11, 13, 17 };
    const int * nCPtr = checkArr;
    EXPECT_EQ( h2->idsPassed.size(), 6 );
    for(auto n : h2->idsPassed) {
        EXPECT_TRUE( n%3 );
        EXPECT_EQ( *(nCPtr++), n );
    }
}

}  // namespace na64dp::test
}  // namespace na64dp

