#include "na64sw-config.h"

#include "log4cpp/Category.hh"
#include "log4cpp/Appender.hh"
#include "log4cpp/FileAppender.hh"
#include "log4cpp/OstreamAppender.hh"
#include "log4cpp/Layout.hh"
#include "log4cpp/BasicLayout.hh"
#include "log4cpp/Priority.hh"

#include <gtest/gtest.h>

#ifdef ROOT_FOUND
#include "na64util/ROOT-sighandlers.hh"
#endif

/**\file main.cc
 * \brief Unit tests launcher
 *
 * Besides of usual GTest options (printed with -h option given to executable),
 * this program check for two environment variables that are useful during
 * development and/or CI/CD:
 *  * LOGLEVEL -- is provided to logging subsystem global NA64DP severity. It
 * effectively accepts only "DEBUG" option making output extremely loquations
 * (though, desirable during debugging).
 *  * LOGFILE -- should refer to file where log must be printed. Useful for
 * generating automated reports on remote deployment or testing environment.
 * */

namespace na64dp {
namespace test {

// Testing environment sets upp the logger
class Environment : public ::testing::Environment {
private:
    log4cpp::Priority::PriorityLevel _rootLevel;
    const std::string _file;
public:
    Environment( log4cpp::Priority::PriorityLevel lvl=log4cpp::Priority::DEBUG // TODO: FATAL
               , const std::string & logFile="" ) : _rootLevel(lvl)
                                                  , _file(logFile)
                                                  {}

    ~Environment() override {}

    // Override this to define how to set up the environment.
    void SetUp() override {
        log4cpp::Category & root = log4cpp::Category::getRoot();

        log4cpp::Appender * consoleAppender
            = new log4cpp::OstreamAppender("console", &std::cout);
        consoleAppender->setLayout(new log4cpp::BasicLayout());

	    log4cpp::Appender * fileAppender = nullptr;
        if( ! _file.empty() ) {
            fileAppender = new log4cpp::FileAppender("default", _file.c_str());
            fileAppender->setLayout(new log4cpp::BasicLayout());
            root.addAppender( fileAppender );
        }

	    root.setPriority( _rootLevel );
	    root.addAppender( consoleAppender );
    }

    // Override this to define how to tear down the environment.
    void TearDown() override {
        log4cpp::Category::shutdown();
    }
};

}  // namespace na64dp::test
}  // namespace na64dp

int
main(int argc, char* argv[]) {
    #ifdef ROOT_FOUND
    // disable ROOT signal handlers
    {
        const char * v = getenv("KEEP_ROOT_SIGHANDLERS");
        if( !v || !(v && ('1' == v[0] && '\0' == v[1] )) ) {
            disable_ROOT_sighandlers();
        } else {
            std::cerr << "ROOT signal handlers are kept." << std::endl;
        }
    }
    #endif

    ::testing::InitGoogleTest(&argc, argv);

    const char * pLvlStr = getenv("LOGLEVEL");
    log4cpp::Priority::PriorityLevel pLvl = log4cpp::Priority::FATAL;
    if( NULL == pLvlStr || !strcmp("FATAL", pLvlStr) ) {
        pLvl = log4cpp::Priority::FATAL;
    } else if( !strcmp("DEBUG", pLvlStr) ) {
        pLvl = log4cpp::Priority::DEBUG;
    } else {
        std::cerr << "Ignoring $LOGLEVEL=\"" << pLvlStr
                  << "\" -- only DEBUG and FATAL are allowed. Log level kept"
                     " as FATAL." << std::endl;
    }

    const char * oFile = getenv("LOGFILE");

    // gtest takes ownership of the TestEnvironment ptr - we don't delete it.
    ::testing::AddGlobalTestEnvironment( new na64dp::test::Environment( pLvl,
                oFile ? oFile : "") );
    return RUN_ALL_TESTS();
}

