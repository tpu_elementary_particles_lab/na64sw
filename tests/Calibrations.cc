#include "na64calib/manager.hh"
#include "na64detID/TBName.hh"

#include <gtest/gtest.h>

/**\file Calibrations.cc
 *
 * Testing unit setting up mocking calibration facility. Checks that all the
 * calibration subscribers are notified on calibration changes, check the
 * validity of runs range look-up procedure and so on.
 * */

namespace na64dp {
namespace test {

# if 0
/// A mock calibration handle class
class MockCalibHandleBasic : public iCalibHandle {
private:
    utils::TBNameMappings _nameMappings;
    int _calibVer;
protected:
    virtual bool _new_run_no( na64ee::runNo_t rNo ) override {
        bool doNotify = false;
        if( rNo > 0 && rNo <= 10 ) {
            doNotify = ( _calibVer != 1 );
            _calibVer = 1;
            return doNotify;
        }
        doNotify = ( _calibVer != 2 );
        _calibVer = 2;
        return doNotify;
    }
    virtual bool _has_calib_info( const std::string &, DetID_t ) const override { return false; }
    virtual const CalibBuffer & _get_calib_info( const std::string &, DetID_t ) override { throw std::runtime_error("forbidden call"); }
    virtual void _set_calib_info( const std::string &, DetID_t, const CalibBuffer & ) override {}
public:
    MockCalibHandleBasic() {}
    virtual const utils::TBNameMappings & naming() override {
        return _nameMappings;
    }
    int calib_ver() const { return _calibVer; }
};


/// A mock calibration subscriber class
class MockSubscriber : public iCalibSubscriber {
private:
    int _calibVer;
    na64ee::runNo_t _recachedOn;
protected:
    virtual void _recache_calibration() override {
        MockCalibHandleBasic * chPtr = dynamic_cast<MockCalibHandleBasic*>(calib_handle_ptr());
        ASSERT_TRUE(chPtr);
        _calibVer = chPtr->calib_ver();
        _recachedOn = chPtr->get_run_no();
    }
public:
    MockSubscriber(iCalibHandle * ch) : iCalibSubscriber(ch)
                                      , _calibVer(-1)
                                      , _recachedOn(0) {}
    MockSubscriber(const MockSubscriber & o) : iCalibSubscriber(o)
                                             , _calibVer(-1)
                                             , _recachedOn(0) {}
    int calib_ver() const { return _calibVer; }
    na64ee::runNo_t recached_on() const { return _recachedOn; }
};


TEST(CalibrationObjects, SubscribersAreNotified) {
    MockCalibHandleBasic mch;
    MockSubscriber subs1(&mch);
    // Expect that calibration is not set before specific run number was
    // provided to calibration handle
    EXPECT_EQ( -1, subs1.calib_ver() );
    EXPECT_EQ(  0, subs1.recached_on() );
    // Set some run number in first range (1-10) causing recache on that number
    mch.set_run_no( 5 );
    EXPECT_EQ(  1, subs1.calib_ver() );
    EXPECT_EQ(  5, subs1.recached_on() );
    // Create copy of subscriber
    // MockSubscriber subs2(subs1);
    // ... TODO: desired behaviour is unclear
    // Set other run number from same range. Handle must not cause recaching
    mch.set_run_no( 9 );
    EXPECT_EQ(  1, subs1.calib_ver() );
    EXPECT_EQ(  5, subs1.recached_on() );
    EXPECT_EQ(  9, mch.get_run_no() );
    // Set run number from second range. Handle must cause recaching
    mch.set_run_no( 99 );
    EXPECT_EQ(  2, subs1.calib_ver() );
    EXPECT_EQ( 99, subs1.recached_on() );
}

// Test stateful validity ranges index lazy caching
TEST(CalibrationObjects, StatefulRunsValidityRanges) {
    // |--a--|--b--...
    // ^33   ^72
    na64dp::StatefulRunsRangeIndex<char> i;
    i.insert({33, 'a'});
    i.insert({72, 'b'});
    ASSERT_THROW( i.current(), errors::UninitIndexState );
    ASSERT_THROW( i.set_run_no(10), errors::NoValidForRange );
    EXPECT_TRUE(i.set_run_no(34));
    EXPECT_EQ( i.current(), 'a' );
    EXPECT_FALSE(i.set_run_no(33));
    EXPECT_EQ( i.current(), 'a' );
    EXPECT_TRUE(i.set_run_no(90));
    EXPECT_EQ( i.current(), 'b' );
}

// Test stateful validity ranges index lazy caching
TEST(CalibrationObjects, StatefulSingularRunsValidityRanges) {
    // |--a--...
    // ^1
    na64dp::StatefulRunsRangeIndex<char> i;
    i.insert({1, 'a'});
    ASSERT_THROW( i.current(), errors::UninitIndexState );
    ASSERT_THROW( i.set_run_no(0), errors::NoValidForRange );
    EXPECT_TRUE(i.set_run_no(1));
    EXPECT_EQ( i.current(), 'a' );
    EXPECT_FALSE(i.set_run_no(33));
    EXPECT_EQ( i.current(), 'a' );
}

TEST( CalibrationObjects, newStyle ) {
    Calibrations C;

    std::unordered_map<DetID_t, int> fourOrig;
    fourOrig[0x45] = 42;

    C.set<int>( "some", std::unordered_map<DetID_t, int>() );
    C.set<int>( "four", fourOrig );
    
    auto four = C.get<int>("four");
    ASSERT_EQ( four[0x45], 42 );
}
#endif

}  // namespace na64dp::test
}  // namespace na64dp


