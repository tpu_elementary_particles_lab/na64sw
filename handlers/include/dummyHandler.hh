#pragma once
#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/// A trivial handler that does nothing
class DummyHandler : public AbstractHandler {
public:
    /// Does nothing but allows event to be propagated through the handler
    virtual AbstractHandler::ProcRes process_event(Event * event) override;
};

}
}
