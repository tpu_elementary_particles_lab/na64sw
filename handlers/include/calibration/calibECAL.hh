#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Handler for proccessing SADC hits of ECAL.
 *
 * Proccesses information from SADC chips of ecal and applies calibrations
 *
 * */
class CalibECAL : public AbstractHitHandler<SADCHit> {
	
public:
    struct ECALCalib {
        const char * planeName;
        double factor;
        double energy;
        double time;
        double timeError;
    };
    
    struct NamingCache {
        DetChip_t sadcChipCode;
        DetKin_t ecalKinCode;
    };
	
private:
	
	/// Time cut from config
	double _timeCut;
	
    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, ECALCalib *> _calibs;
    
protected:

	NamingCache _namingCache;
    
    const nameutils::DetectorNaming * _names;

    virtual void handle_update( const nameutils::DetectorNaming & ) override;

public:
    CalibECAL( calib::Dispatcher & dsp
             , const std::string & only
             , double timeCut );
                                              
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit &) override;
};

}
}

