#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Handler for proccessing SADC hits of beam counters.
 *
 * Proccesses information from SADC chips of beam counters
 * and applies calibrations
 *
 * */
class CalibCount : public AbstractHitHandler<SADCHit> {
	
public:
    struct CountCalib {
        const char * planeName;
        double energy;
        double time;
        double timeError;
    };
    
    struct NamingCache {
        DetChip_t sadcChipCode;
        DetKin_t sKinCode;
        DetKin_t vKinCode;
        DetKin_t tKinCode;
    };
	
private:
    /// Placements dictionary; TODO: subst with calib handle
    std::map<DetID_t, CountCalib *> _calibs;
    
protected:
	
	NamingCache _namingCache;
    
    const nameutils::DetectorNaming * _names;

    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    
public:
    CalibCount( calib::Dispatcher & dsp
              , const std::string & only );
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit &) override;
};

}
}
