#pragma once

#include "na64dp/abstractHandler.hh"

namespace na64dp {

class PickEventsRange : public AbstractHandler {
public:
    typedef unsigned long EventNumber_t;
private:
    EventNumber_t _from, _to;
    bool _negate;
    EventNumber_t _counter;
public:
    PickEventsRange( EventNumber_t from_
                   , EventNumber_t to_
                   , bool negate_);
    // Returns starting number of events to be left for further processing.
    EventNumber_t from() const { return _from; }
    // Returns final number of events to read.
    EventNumber_t to() const { return _to; }
    // Returns true if current event is allowed for read.
    virtual ProcRes process_event(Event * event) override;
};

}
