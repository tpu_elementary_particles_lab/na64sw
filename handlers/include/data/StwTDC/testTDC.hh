#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

#include <TH1D.h>

namespace na64dp {
namespace handlers {

/**\brief Handler for proccessing SADC hits of ECAL.
 *
 * Proccesses information from SADC chips of ecal and applies calibrations
 *
 * */
class TestTDC : public AbstractHitHandler<StwTDCHit> {

private:
    /// Hit position
    TH1D *hitPosST4X;
    TH1D *hitPosST4Y;
    TH1D *hitPosST5X;
    TH1D *hitPosST5Y;
	TH1D *hitPosST6X;
    TH1D *hitPosST6Y;
    
public:
    TestTDC( calib::Dispatcher & dsp
           , const std::string & only );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , StwTDCHit &) override;
};

}
}
