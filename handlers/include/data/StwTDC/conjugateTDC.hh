#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief Handler for proccessing NA64TDC hits of straw detectors.
 *
 * Proccesses information from NA64TDC chips of straw and creates
 * space points (TrackPoint)
 *
 * */
class StwTDCConjugate : public AbstractHitHandler<StwTDCHit> {

public:
    struct DetPlacementEntry {
        const char * planeName;
        float x, y, z;
        float rotAngleX, rotAngleY, rotAngleZ;
        float sizeX, sizeY;
        float resolution;
        float numOfWires;
    };
    
    typedef uint16_t StwWireNo;
    
    typedef std::multimap<DetID_t, PoolRef<StwTDCHit>> StwHits;
       
    /// Pair of conjugated TrackPoint candidates
    typedef std::pair<PoolRef<StwTDCHit>, PoolRef<StwTDCHit>> StwPair;

private:
	
	std::map<DetID_t, DetPlacementEntry* > _placements;
	
	/// Routine to insert track point
	HitsInserter<TrackPoint> _tpInserter;
	
	///< HODs kin ID cache
	DetKin_t _strawKinID;
	
	std::map<DetID_t, StwHits> _stwHits;
		
	std::multimap<DetID_t, StwPair> _accountedHits;
	
	const float _timeWindow;

protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    
    void _conjugate_hits( DetID_t did, StwHits & hits );
    
    void _create_track_point(Event &);
    
public:
    StwTDCConjugate( calib::Dispatcher & dsp
                   , const std::string & only
                   , ObjPool<TrackPoint> & bank
                   , float timeWindow = 70. );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , StwTDCHit &) override;
};

}
}
