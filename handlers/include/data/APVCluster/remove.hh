#pragma once

#include "na64dp/abstractHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief APV Cluster in-place remove handler
 *
 * Sorts clusters wrt some value obtained by getter function and removes from
 * event all except for some leading ones per plane.
 *
 * Depending on `ascending` value, clusters with either the highest
 * (`ascending=true`) or lowest (`ascending=false`) values will avoid removal.
 */
class APVRemoveClusters : public AbstractHandler {
private:
    /// Value-retrieval function obtaining sorting value
    EvFieldTraits<APVCluster>::ValueGetter _getter;
    /// Number of leading clusters to left, per plane
    size_t _nLeave;
    /// Wether to leave max values
    bool _ascending;
public:
    /**\brief Main ctr for cluster-removing handler
     *
     * \param getter A getter function obtaining value from cluster instance
     * \param nLeave how many leading cluster have to remain
     * \param ascending Defines direction of sort
     * */
    APVRemoveClusters( EvFieldTraits<APVCluster>::ValueGetter getter
                     , int nLeave=1
                     , bool ascending=true );

    /// Removes clusters from event that does not match the configured criteria
    virtual ProcRes process_event(Event *);
};

}
}

