#pragma once

#include "na64dp/abstractHandler.hh"
#include "na64calib/dispatcher.hh"

#include <fstream>

namespace na64dp {
namespace handlers {

/**\brief Generates CSV dump of clusters collected for APV detector stations
 *
 * Generated dump is useful for external analysis. Accepts single parameter
 * defining, whether to dump hits defining a cluster.
 *
 * Output format's common part:
 *  - event ID - numerical event identifier
 *  - track ID - numerical track identifier
 *  - cluster number - unique number of cluster within a track
 *  - station name - name of station (e.g. `MM4`, `GM1`)
 *  - projection letter - letter defining projection
 *  - mean wire number - $\sum\limits_i^{N_c} N_{w,i}/N_c$
 *  - mean weighted by charge wire number $(\sum\limits_i^{N_c} q_i N_{w,i})/N_c$
 *  - cluster sparseness value
 * If `writeHits=true`, output of each 1D cluster will be appended with:
 *  - nuber of hits - number of hits composing a cluster
 *  - (repeated) phys wire number
 *  - (repeated) mapped wire number
 *  - (repeated) charge measurement 1st sample
 *  - (repeated) charge measurement 2nd sample
 *  - (repeated) charge measurement 3rd sample
 *
 * \todo "applyTo" ctr argument is currently being ignored
 * */
class APVClusterDump : public AbstractHandler {
private:
    calib::Handle<nameutils::DetectorNaming> _naming;
    std::ofstream _os;
    bool _writeHits;
public:
    APVClusterDump( calib::Dispatcher & dsp
                  , const std::string & oFilePath
                  , const std::string & only
                  , bool writeHits=false);
    virtual ProcRes process_event( Event * e ) override;
    virtual void finalize() override;
};

}
}

