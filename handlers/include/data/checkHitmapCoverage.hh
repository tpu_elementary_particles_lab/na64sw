#pragma once

#include "na64dp/abstractHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief A diagnostic handler checking the hits map coverage
 *
 * This handler compares hits indexed in main event's map versus the entries
 * found in a pool allocator to find out usage of hits, i.e. how many of the
 * hits allocated in pool survived within the current event. This information
 * is put in logs then.
 *
 * \note Situation of <100% usage of pool allocators is normal for certain
 *       circumustances, i.e. intentional deletion of hit instance will not
 *       cause re-allocating the entire pool.
 * */
template<typename HitT>
class CheckHitmapCoverage : public AbstractHandler {
private:
    std::set<const HitT *> _hitsInEvent;
    HitBanks & _banks;
public:
    CheckHitmapCoverage( HitBanks & banks
                       ) : _banks(banks)
                         {}
    /// After parent's `AbstractHitHandler<HitT>::process_event()` checks the
    /// bank coverage
    virtual ProcRes process_event(Event *) override;
};

template<typename HitT> AbstractHandler::ProcRes
CheckHitmapCoverage<HitT>::process_event(Event *e) {
    typedef EvFieldTraits<HitT> Traits;
    // Get the main map for hits of certain type within an event
    const typename Traits::MapType & map = Traits::map(*e);
    // Get the allocator reference
    const ObjPool<HitT> & bank = Traits::bank(_banks);
    // Iterate over the map collecting unique hit pointers for comparison
    for( auto it = map.cbegin()
       ; map.cend() != it
       ; ++it ) {
        auto prPair = *it;
        // Check that reference is within a range range
        if( prPair.second.offset_id() >= bank.get_storage().size() ) {
            std::cerr << "Map produces wrong pool reference for a hit!"
                      << std::endl;  // TODO: log
        } else {
            const HitT * hitPtr = prPair.second.operator->();
            _hitsInEvent.insert(hitPtr);
        }
    }
    std::set<size_t> missedHits;
    // Check coverage of hits within a certain bank
    size_t nHit = 0;
    for( const HitT & bankEntry : bank.get_storage() ) {
        auto it = _hitsInEvent.find( &bankEntry );
        if( _hitsInEvent.end() == it ) {
            missedHits.insert( nHit );
        }
        ++nHit;
    }
    if( ! missedHits.empty() ) {
        // TODO: more explainatory message here
        //log().warning( "Found %zu missed hits in current event." );
        std::cerr << "Found " << missedHits.size() << " hits in bank that are"
            " not belong to main hits map" << std::endl;
    }
    return kOk;
}

}
}

