#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

#include "na64event/hitInserter.hh"

namespace na64dp {
namespace handlers {

/**\brief Handler for test of CaloEDep.
 *
 * Proccesses information from SADC chips of calorimeters
 *
 * */
class TestCaloEDep : public AbstractHitHandler<CaloEDep> {

protected:
	/// CaloEDep allocator reference
    HitsInserter<CaloEDep> _caloInserter;

public:
    TestCaloEDep( calib::Dispatcher & dsp
                , const std::string & only
                , ObjPool<CaloEDep> & bank );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , CaloEDep &) override;
};

}
}
