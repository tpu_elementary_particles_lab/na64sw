#pragma once
#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <vector>
#include <fstream>

namespace na64dp {
namespace handlers {

/**\brief Depicts value changes over time
 *
 * Common use case of this handler is restricted rather by raw data as it is
 * expected to track value changes over contigious series of events.
 *
 * Using given value getter, extracts value from hits of certain type, puts
 * them at temporary storage of fixed capacity ("window") once window is full,
 * computes key statistical features like mean, standard deviation, median,
 * skewness, etc stores them by events range key and flushes the storage.
 *
 * Differs from "moving statistics" (sliding window) algorithm family in that
 * it does not perform statistical extractions for each sample \f$x_i\f$, but
 * instead once per \f$N\f$ samples.
 *
 * Helps to observe some time- or event-dependant systematics like in-spill
 * drifts, periodical fluctuations and so on.
 *
 * \todo unit tests on various working modes
 * */
class TimeSeriesTracker {
public:
    /// Extracted statistics entry.
    struct DistributionParameters {
        EventID evStart  ///< First event number
              , evEnd   ///< Last event number
              ;
        std::pair<time_t, uint32_t> timeStart, timeEnd;
        size_t nSamples;  ///< Number of samples being considered for this entry
        double mean  ///< Mean value
             , stddev  ///< Standard deviation value
             , skewness  ///< Skewness (3-rd central moment)
             , kurtosis  ///< Kurtosis (4-th central moment)
             , median  ///< Median value of distributon
             , autocorr ///< Autocorrelation
             , lowQ  ///< Lower quartile
             , upQ  ///< Upper quartile
             ;
        /// List of outliers (out of 2nd quantile)
        std::vector<double> outliers;
    };
    /// Defines the way of events breakdown
    enum BreakType {
        /// Makes new entry each N of events
        kFixedWindowLength,
        /// Makes new entry each N of events, dropping only the first half
        kHalfOverlap,
        /// Makes new entry each on spill number change (high memory consumption)
        kBySpill,
        /// Makes new entry based on time gap (or by the end of the spill)
        kPeriodic,
    };
private:
    const BreakType _bt;
    const bool _accountOutliers;
protected:
    /// List of current samples list
    std::vector<double> _samples;
    /// First event this samples set is valid for
    EventID _firstEvent;
    /// First event time
    std::pair<time_t, uint32_t> _firstEventTime;
    /// Last event this samples set is valid for
    EventID _lastEvent;
    /// Time of the last event being considered
    std::pair<time_t, uint32_t> _lastEventTime;

    /// Index of considered entries
    std::vector<DistributionParameters> _entries;

    /// Number of samples to consider
    const size_t _winSize;
    /// Invoked at the end of entry accumulation
    virtual void _finalize_entry( bool removeOutliers=false );
    /// Re-sets first/last event identifiers
    void _reset_events_ids();
    /// Has to be set for periodic
    std::pair<time_t, uint32_t> _period;
public:
    static BreakType break_type_by_str( const std::string & );
    static void extract_distribution_parameters( std::vector<double> & samples
                                               , bool removeOutliers
                                               , DistributionParameters & );

    TimeSeriesTracker( BreakType bt
                     , size_t winSize
                     , std::pair<time_t, uint32_t> timeWindow
                     , bool accountOutliers=true) : _bt(bt)
                                                  , _accountOutliers(accountOutliers)
                                                  , _winSize(winSize)
                                                  , _period(timeWindow)
                                                  {
        _reset_events_ids();
    }
    virtual ~TimeSeriesTracker() {}

    /// Returns break type this instance was initialized
    BreakType break_type() const { return _bt; }
    /// Shall be called on new event processing starts, prior to sample
    /// consideration; may invoke _entry_finalize()
    void account_event_no(EventID evID, const std::pair<time_t, uint32_t> & );
    /// Adds the given sample to statistics; may invoke _entry_finalize()
    void account_sample(double);
    /// Forces dropping the stats
    void finalize_current_entry(bool removeOutliers)
        { _finalize_entry(removeOutliers); }

    const std::vector<DistributionParameters> & entries() const { return _entries; }
};

std::ostream & operator<<(std::ostream &, const TimeSeriesTracker::DistributionParameters &);

/**\brief Template implementation for time series analyzing handler
 *
 * \todo currently supports only the hardcoded CSV output; foresee file name
 * parameterisation, ROOT output or whatsoever
 * */
template<typename HitT>
class TimeSeries : public AbstractHitHandler<HitT>
                 , public TDirAdapter {
public:
    struct TimeSeriesCtrArgs {
        TimeSeriesTracker::BreakType bt;
        size_t winSize;
        std::pair<time_t, uint32_t> timeWindow;
        bool accountOutliers;
    };
protected:
    /// Common time series tracker constructor arguments
    TimeSeriesCtrArgs _ctrArgs;
    /// Current event time
    std::pair<time_t, uint32_t> _cEvTime;
    /// Index of time series, individual to detector
    std::map<DetID_t, TimeSeriesTracker *> _series;
    /// A value getter callback
    typename EvFieldTraits<HitT>::ValueGetter _getter;
    /// Common name suffix
    const std::string _hstName;
public:
    TimeSeries( calib::Dispatcher & cdsp
              , const std::string & selection
              , typename EvFieldTraits<HitT>::ValueGetter getter
              , const std::string & hstName
              , TimeSeriesTracker::BreakType bt
              , size_t winSize
              , std::pair<time_t, uint32_t> timeWindow
              , bool accountOutliers
              ) : AbstractHitHandler<SADCHit>(cdsp, selection)
                , TDirAdapter(cdsp)
                , _ctrArgs{bt, winSize, timeWindow, accountOutliers}
                , _getter(getter)
                , _hstName(hstName)
                {}

    /// Overriden to get the event time
    virtual AbstractHandler::ProcRes process_event( Event * event ) override {
        _cEvTime = event->time;
        return AbstractHitHandler<HitT>::process_event(event);
    }

    /// Takes into account value
    virtual bool process_hit( EventID eid
                            , DetID_t did_
                            , SADCHit & currentHit) override {
        assert(_getter);
        DetID_t did = EvFieldTraits<HitT>::uniq_detector_id(did_);
        auto it = _series.find(did);
        if( _series.end() == it ) {
            // No series instance exists for current detector entity -- create
            // and insert one
            TimeSeriesTracker * trackerPtr = new TimeSeriesTracker(
                        _ctrArgs.bt, _ctrArgs.winSize, _ctrArgs.timeWindow
                        , _ctrArgs.accountOutliers
                    );
            it = _series.emplace(did, trackerPtr).first;  // `first' is an iterator
        }
        // fill bin content in the histogram
        it->second->account_sample( _getter(currentHit) );
        it->second->account_event_no( eid, _cEvTime );
        return true;
    }

    /// Writes saved statistics
    virtual void finalize() override {
        std::ofstream ofs("/tmp/time-series.csv");  // TODO: parameterization
        for( auto p : _series ) {
            #if 0
            std::string detName = nm.detector_ddd_name_by_id(p.first);
            #else
            std::map<std::string, std::string> ctx;
            ctx["hist"] = _hstName;
            this->TDirAdapter::naming().append_subst_dict_for(p.first, ctx);
            std::string path = util::str_subst( this->TDirAdapter::naming().path_template(p.first), ctx );
            std::string detName = path.substr( path.rfind('/') + 1, std::string::npos );
            #endif
            p.second->finalize_current_entry(_ctrArgs.accountOutliers);
            for( auto entry : p.second->entries() ) {
                ofs << detName << "," << entry << std::endl;
            }
            delete p.second;
        }
    }
};


}
}

