# pragma once

# include "na64dp/abstractHandler.h"

namespace na64dp {
namespace handlers {

/**\brief Writes event to stream.
 *
 * This handler writes events into a given stream.
 *
 * Handler may be configured to write events into a file or UNIX stream (say,
 * named FIFO socket) or use network connection. It exploits internal buffering
 * mechanism to speed up the processing.
 *
 * For UNIX file descriptor, use ..., for network connection provide a port
 * number with 
 *
 * */
class EventWrite : public AbstractHandler {
public:
    /// Every single event has to fit into buffer of this length.
    static const size_t singleEventBufferSize;
protected:
    char * _singleEventBuffer
       , * _eventsBuffer
       , * _eventsBufferPosition;
    size_t _bufferSize;
private:
    int _file, _portNo;
    std::vector<char> _buffer;
protected:
    /// Writes buffer to file and sets current pointer to beginning
    void _dump_events_buffer();
public:
    /// Configures handler to use file descriptor
    EventWrite( int fd
              , size_t bufferSize );
    EventWrite( int portNo );
    ~EventWrite();
    virtual ProcRes process_event(Event * event) override;
    virtual void finalize() override;

    // Function, complementary to the _dump_events_buffer() -- used for reading
    // events from serialized stream. First, reads the chunk length description
    // number. If this length is greater than size of the supplied buffer,
    // returns negative number corresponding to the difference of the buffer
    // size and chunk size (user code is then supposed to provide the greater
    // buffer). Otherwise, reads the data and returns size of the chunk being
    // read.
    //static ssize_t read_events_buffer( utils::io::IOD<utils::io::in> fd, char * buffer, size_t bufferSize );
};

}
}

