#pragma once

#include "data/SADC/getPedestalsByFront.hh"
#include "data/SADC/subtractPedestals.hh"
#include "na64util/numerical/online.hh"
#include "na64calib/SADC/pedestals.hh"

namespace na64dp {
namespace handlers {

/**\brief Moving statistics pedestals calculus
 *
 * This handler implements more robust pedestals derivation than
 * straightforward algorithm `SADCGetPedestalsByFront` by involving windowed
 * (rolling, sliding) statistical state that takes into account some fixed
 * amount of pedestals being previously calculated. Particular
 * implementation of the sliding statistics is choosen by `iMovStats`
 * implementation supplied by ctr.
 *
 * \code{.yaml}
 *      - name: SADCMovStatPeds
 *        applyTo: "kin == HCAL"
 *        nSamples: 4
 *        dropOutlierEvent: true
 *        scorer:
 *          type: klein
 *          window: 500
 *          excludeOutliers: true
 *          recacheOutliers: true
 *          recachingFrac: 0.
 * \endcode
 * 
 * YAML parameters:
 *  * `name` -- see `AbstractHitHandler`
 *  * `applyTo` -- see `AbstractHitHandler`
 *  * `nSamples` -- number of samples within the waveform to use (see
 *  `SADCGetPedestalsByFront`)
 *  * `dropOutlierEvent` -- whether to discard the hit and stop event
 *  propagation if hit is identified as an outlier.
 *  * `scorer` -- type of scorer to use. Possible values: `double` for plain
 *  scorer, fast but prone to catastrophic cancellation error or `klein` for
 *  more precise but slower one.
 *    * `window` -- number of hits stored for sliding stats calculus
 *    * `excludeOutliers` -- whether to exclude outliers
 *    (\f$x_i - \mu > 3 \sigma\f$) from statistics calculus
 *    * `recacheOutliers` -- whether to re-cache sums when accounted outliers
 *    leave the window of moving statistics
 *    * `recachingFrac` -- part (in the units of window) of stored events that
 *    makes scorer to recalculate moving statistics. E.g. for
 *    `recachingFrac=1.5` of `window=100`, every 150 events will cause
 *    re-caching of sums. Used as a side measure to circumvent the catastrophic
 *    cancellation. Set to 0 to prevent from this type of movstat recaching.
 *
 * \todo Support for `savedScorers` parameter -- a template path string
 * denoting the place where the scorer state has to be saved. Useful for
 * reentrant usage to stabilize sliding statistics during the initial read-out.
 * */
class SADCMovStatPeds : public SADCGetPedestalsByFront
                      , public calib::Handle<calib::Pedestals> {
public:
    struct MovStatArgs {
        size_t winSize;
        double excludeOutliers, recacheOutliers;
        double recachingFrac;
    };
    struct MovStatEntry {
        numerical::iMovStats * stat[2];
    };
    typedef numerical::iMovStats * (*MovStatCtr)( const MovStatArgs & );
private:
    /// Sets threshold (in units of \f$\sigma\f$) for pedestals to be
    /// considered as pedestal; otherwise, scorer's mean value will be used
    double _dropOutlierEventThreshold;
    /// Constructor proxy for moving statistics insertion
    MovStatCtr _movStatEntryCtr;
    /// Common arguments
    const MovStatArgs _movStatArgs;
    /// Windowed (sliding) pedestal statistics, one entry per detector
    std::map<DetID_t, MovStatEntry> _stats;
    /// True if handler has to use average values before initial sum is
    /// accumulated.
    bool _useAvrgVals;
protected:
    /// Saves scorers by given template path argument, does not set `scorersAreSaved`.
    //void _save_scorers( const MovStatEntry & );  // XXX
public:
    /// Default ctr 
    SADCMovStatPeds( calib::Dispatcher & cdsp
                   , const std::string & only
                   , MovStatArgs & msArgs
                   , MovStatCtr msCtr
                   , unsigned char nfsa=4
                   , double dropOutlierEvent=0.
                   , bool avrgInit=false
                   ) : SADCGetPedestalsByFront(cdsp, only, nfsa)
                     , calib::Handle<calib::Pedestals>("pedestals", cdsp)
                     , _dropOutlierEventThreshold(dropOutlierEvent)
                     , _movStatEntryCtr(msCtr)
                     , _movStatArgs(msArgs)
                     , _useAvrgVals(avrgInit)
                     {}
    /// Computes average values and modifies the hit accordingly
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , SADCHit & currentHit ) override;
};

}
}

