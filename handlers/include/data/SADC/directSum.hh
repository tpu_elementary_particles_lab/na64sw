#pragma once

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Computes linear sum of SADC waveform
 *
 * This handler calculates the sum on the SADC hit waveform by summing up all
 * the 32 samples.
 *
 * Accepts single parameter "zeroNegativeValues:bool" that, when set to `true'
 * makes the handler to set negative samples to 0. Can be omitted, default
 * value is `false'.
 *
 * \see SADCLinearSum
 * */
class SADCDirectSum : public AbstractHitHandler<SADCHit> {
private:
    bool _doZeroNegative;
public:
    SADCDirectSum( calib::Dispatcher & ch
                 , const std::string & only
                 , bool doZeroNegative=false ) : AbstractHitHandler<SADCHit>(ch, only)
                                               , _doZeroNegative(doZeroNegative)
                                               {}
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit & currentHit);
};

}
}

