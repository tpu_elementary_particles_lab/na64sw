#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64calib/SADC/pedestals.hh"
#include "na64event/fields/SADC.hh"

namespace na64dp {
namespace handlers {

/**\brief Assigns pedestals values from calibration data to event's
 *
 * This simple handler obtains pre-computed pedestals values from calibrations
 * entry and asigns corresponding fields within a SADC hits for subsequent
 * usage.
 *
 * If entry does not exist in calibration data, the pedestals value is kept
 * intact or set to `std::nan(0)` depending on boolean option
 * `setNonExistingToNan`.
 * */
class AssignPedestals : public AbstractHitHandler<SADCHit> {
protected:
    bool _setNonExistingToNan;
    calib::Handle<calib::Pedestals> _pedestals;
public:
    AssignPedestals( calib::Dispatcher & cdsp
                   , const std::string & detSelection
                   , bool setNonExistingToNan=false );

    virtual bool process_hit( EventID, DetID_t, SADCHit & ) override;
};

}
}

