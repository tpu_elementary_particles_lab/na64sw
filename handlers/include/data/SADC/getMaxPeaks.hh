#pragma once

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Finds all local maxima in SADC waveform
 *
 * This is simple finder of maxima. Iterates over 32 samples finding
 * samples with changing signs "-+" in two neighbouring samples and 
 * writes this value and corresponding sample number to `maxima` 
 * field of SADC hit.
 *
 * YAML config only accepts standard detector-by-name selection argument.
 * */
class SADCGetMaxPeaks : public AbstractHitHandler<SADCHit> {
private:

	/// threshold for minimum amplitude of local maximum
	double _threshold;

protected:

	bool _check_peak( double wave[32]
					, int position );

public:
	SADCGetMaxPeaks( calib::Dispatcher & cdsp
				   , const std::string & only
                   , double threshold = 0.);

    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , SADCHit & hit ) override;
};

}
}

