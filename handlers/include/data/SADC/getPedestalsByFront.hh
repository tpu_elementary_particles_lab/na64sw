#pragma once

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Takes into account first N samples assuming averaged as pedestals
 * values
 *
 * Parameterized by number of samples "nSamples" taken for _each_ SADC channel.
 * E.g. for `nSamples=4`, 4x2=8 starting values (from 32) will be taken. Two
 * mean values for `SADCHit::pedestals[0,1]` will be then written in
 * corresponding hit.
 *
 * Note, that no subtraction performed by this handler. Use dedicated handler
 * to apply pedestal subtraction afterwards (e.g. `SADCSubtractPedestals`).
 *
 * Result of applying the subtraction with pedestals derived with this handler
 * can be demonstrated visually by means of `SADCAmpHists` handler. Before
 * subtracting pedestals:
 *
 * \image html handlers/sadc-amp-before.png
 *
 * After subtraction:
 *
 * \image html handlers/sadc-amp-after.png
 *
 * YAML config node examples:
 *
 * \code{.yaml}
 *      - name: SADCGetPedestalsByFront
 * \endcode
 * or
 * \code{.yaml}
 *      - name: SADCGetPedestalsByFront
 *        nSamples: 4
 * \endcode
 *
 * \note Albeit this kind of pedestals estimation is known as relatively good,
 * it suffers from pile-up events. As an alternative one might be interested in
 * more advanced techniques. */
class SADCGetPedestalsByFront : public AbstractHitHandler<SADCHit> {
private:
    /// Number of samples to take from each channel
    unsigned char _nFirstSamples;
public:
    /// Default ctr 
    SADCGetPedestalsByFront( calib::Dispatcher &  ch
                           , const std::string & only
                           , unsigned char nfsa=4
                           ) : AbstractHitHandler<SADCHit>(ch, only)
                             , _nFirstSamples(nfsa) {}
    /// Computes average values
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , SADCHit & currentHit ) override;
};

}
}

