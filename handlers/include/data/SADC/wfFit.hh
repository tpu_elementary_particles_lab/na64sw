# pragma once

# include "na64util/numerical/SADCWaveFormFit.h"
# include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief SADC waveform function-fitting handler
 *
 * This handler performs numerical fitting of the 32 data points provided
 * within the SADC hit "waveform" information with certain function. It wraps
 * the underlying GSL procedures providing configurable way to selectively fit
 * measurements from certain detector entities.
 *
 * Fitting procedure begins with some values of fitting parameters being
 * pre-set. They have to be provided with `p` array, and their particular
 * meaning is defined by fitting function.
 *
 * Fitting procedure may fail. In this case, no fitting information will be
 * added into hit data (if it was not written by any fitting handler before,
 * it will be empty, otherwise values are kept intact).
 *
 * If logging file descriptor is provided (not NULL) it will be used for
 * writing ASCII logs of fitting procedure. If it is not
 *
 *      SADCWfFit:
 *          onlyFor: [ECAL]
 *          function: moyal
 *          writeLogTo: /tmp/na64dp-SADC-fitting-log.txt
 *          initial: [nan, nan, nan]
 *
 * \todo describe YAML syntax in more details in the doc
 * \todo provide more fitting functions
 * \todo add config examples for various fitting function cases
 * */
class SADCWfFit : public AbstractHitHandler<SADCHit> {
public:
    static constexpr int nParameters = 5;
protected:
    /// Internal logging stream written by fitting procedure.
    FILE * _logfile;
    /// Indicates that handler must close the logging file itself.
    bool _doCloseLogFile;
    /// Fitting data allocator
    ObjPool<WfFittingData> & _fitDataBank;
private:
    /// Fitting function callback
    SADCWF_FittingFunction _fit_f;
    /// Values to start with.
    double _p[nParameters];
    /// Reentrant array of samples to fit
    double _moyalWave[32];
    /// Input fitting structure bearing the data to fit (reentrant instance)
    SADCWF_FittingInput _input;
    /// Fitting function parameters (reentrant instance)
    SADCWF_FittingFunctionParameters _pars;
public:
    /// Constructs new fitting handler instance
    SADCWfFit( calib::Dispatcher &  ch
             , ObjPool<WfFittingData> & fitDataBank
             , SADCWF_FittingFunction fit_f
             , const double * p
             , const std::string & onlyDetectors
             , FILE * logFile
             , bool closeFile=false );

    # if 0
    /// Digitalize moyal function
    void digit_moyal(double area, double max, double width);
    void calc_rmsDev( double * rmsDev
                    , double * waveMoyal
                    , double * waveData );
    # endif
    
    /// Frees some internal buffers of the handler.
    ~SADCWfFit();

    /// Fits SADC waveform and writes fitting parameters.
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit & currentEcalHit ) override;
}; 

}
}

