#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64event/event.hh"

namespace na64dp {
namespace handlers {

class SADCAppendPeds : public AbstractHitHandler<SADCHit> {
public:
    SADCAppendPeds( calib::Dispatcher & cdsp
                  , const std::string & selection )
                            : AbstractHitHandler<SADCHit>(cdsp, selection) {}
    ~SADCAppendPeds(){}
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , SADCHit & hit ) override;
};

}
}

