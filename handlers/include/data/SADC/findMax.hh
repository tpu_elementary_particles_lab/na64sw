#pragma once

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Somewhat advanced waveform maximum search algorithm
 *
 * 
 * */
class SADCFindMax : public AbstractHitHandler<SADCHit> {
private:
    /// When set causes hit removal when finder is unable to locate peak
    bool _rmHitOnFailure;
    /// Amplitude lookup range
    double _ampRange[2];
    /// Timing lookup range
    double _timeRange[2];
public:
    SADCFindMax( calib::Dispatcher & ch
               , const std::string & only
               , bool removeHitOnFailure
               , double ampMin, double ampMax
               , double timeMin, double timeMax )
                            : AbstractHitHandler<SADCHit>(ch, only)
                            , _rmHitOnFailure(removeHitOnFailure)
                            , _ampRange{ ampMin, ampMax }
                            , _timeRange{ timeMin, timeMax } {}

    ~SADCFindMax(){}

    /// Locates maximum sample -- value and closes sample number, filling the
    /// hit's `maxValue` and `maxSample` fields.
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , SADCHit & hit ) override;

    /// Shall return whether the maximum value was found and set amplitude/time
    // /values
    virtual bool find_maximum( const double * wf
                             , double & value
                             , double & time );

    /// Fills peaks indexes in `nSamples` considering left and right neighbours.
    static int * collect_peaks( const double * wf, int * nSamples );

    /// Picks up maximum sample value index within the given amplitude range.
    /// Returns -1 if there is no value in the range.
    static int max_peak_idx( const double * wf
                           , const double ampMin, const double ampMax
                           , const double timeMin, const double timeMax );
};

}
}


