#pragma once

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief 

 * 
 * */
class SmoothWave : public AbstractHitHandler<SADCHit> {
private:

public:
    SmoothWave( calib::Dispatcher & ch
               , const std::string & only
               ) : AbstractHitHandler<SADCHit>(ch, only)
               {}
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit & currentHit);
};

}
}
