#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64util/functions.hh"

namespace na64dp {
namespace handlers {

/**\brief Leave/remove APV hits belong to certain cluster
 *
 * This is a very specific handler that removes hits from an event by taking
 * into account values of their clusters.
 *
 * With respect to comparator, the cluster's value is the first argument while
 * the threshold is second.
 * */
class APVClusteredHitsSelect : public AbstractHitHandler<APVCluster> {
protected:
    /// Value getter function
    EvFieldTraits<APVCluster>::ValueGetter _getter;
    /// Value to compare with
    const double _threshold;
    /// Comparison function
    functions::Comparator _compare;
public:
    APVClusteredHitsSelect( calib::Dispatcher & ch
                          , const std::string & only
                          , EvFieldTraits<APVCluster>::ValueGetter getter
                          , functions::Comparator comparator
                          , double threshold );

    virtual bool process_hit( EventID
                            , DetID_t
                            , APVCluster & ) override;
};

}
}

