#pragma once

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief Calculates ratios r_{1,2} and r_{1,3} for hits on APV detector.
 *
 * A-ratios are the useful coefficients for APV hits data quality assurance.
 *
 * r_{1,2} = a_1/a_2, r_{1,3} = a_1/a_3
 *
 * \image html handlers/apv-a-ratio.png
 *
 * where $a_i, i = 1, 2, 3$ is the amplitude samples usually brought by APV
 * chip.
 *
 * This handler has no parameters.
 */
class APVCalcARatios : public AbstractHitHandler<APVHit> {
public:
    /// Default ctr
    APVCalcARatios( calib::Dispatcher & ch, const std::string & select );
    /// Writes ratios to `APVHit::a12` and `APVHit::a13`
    virtual bool process_hit( EventID
                            , DetID_t
                            , APVHit & cHit );
};

}
}

