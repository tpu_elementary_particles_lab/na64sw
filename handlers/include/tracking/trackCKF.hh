#pragma once

#include "na64sw-config.h"

#ifdef GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TH1D.h>
#include <TH2D.h>
#include <TVector3.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
 
class TrackCKF : public AbstractHitHandler<TrackPoint>
			   , public TDirAdapter {

public:
	typedef double DPlace;
	
	typedef std::multimap<DetID_t, PoolRef<TrackPoint>> Hits;
	
	// Track points sorted by their planes
	typedef std::vector<std::vector<PoolRef<TrackPoint>>> TrPoints;
	
	// create seeds
	typedef std::vector<TVector3> Seed;

private:

	ObjPool<Track> & _tracksBank;
	
	TH1D *hMom;
    TH2D *hMomVsECal;
    TH1D *hMomVsChi;
    
    int _nBins, _min, _max;
    
    std::string _overridenPath;
    
    std::map<DetID_t, TH1F *> _histograms;

    std::map<DPlace, Hits> _tpMap;
    
    typedef std::vector<std::vector<genfit::TrackPoint>> GenfitTP;
    
    GenfitTP _genfitTPs;
    
    std::vector<GenfitTP> _allReps;
    
    // track candidate class
	void _sort_hits_by_planes( TrPoints & trackPoints );
	
    void _create_seed( TrPoints & trackPoints
					 , Seed & seeds );
					 
	bool _process_seed( TVector3 & seed
	   			      , const TrPoints & trackPoints
	   			      , int PDGcode
	   			      , double eDep );
		
		
public:
    TrackCKF( calib::Dispatcher & dsp
            , const std::string & only
            , ObjPool<Track> & bank
            , const std::string & geoFilePat );
    
    ~TrackCKF();
                  
    virtual ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , TrackPoint & hit ) override;
                            
	virtual void finalize() override;
};


}
}

#endif  //  defined(GenFit_FOUND)


