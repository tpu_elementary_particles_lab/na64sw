#pragma once

#include "na64sw-config.h"

#ifdef GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class TrackSeed : public AbstractHitHandler<TrackPoint> {

public:

	struct Cell {
		PoolRef<TrackPoint> trackPoint;
		DetID_t planeName;
		std::string name;
		double x, y, z;
		int state;
		int layer;
		double charge;
		bool update;
	};
	
	typedef std::multimap<DetID_t, Cell> CellMap;

private:
	
	/// Map of cells sorted by station
    std::map<double, CellMap> _cells;

protected:

	double _magPosition;
	
	int _missingHit;
	
	void _cellular_automaton( std::vector<std::vector<Cell>> & cell  );
	
	void _create_space_points( CellMap & map 
							 , std::vector<std::vector<Cell>> & spDown
							 , std::vector<std::vector<Cell>> & spUp);
							 
	void _calculate_cell_state( std::vector<Cell> & curCell
		                      , std::vector<Cell> & prevCell );

	bool _update_cell_state( std::vector<std::vector<Cell>> & cell );
	
	void _track_seeding( std::vector<std::vector<Cell>> & cell );	          

public:
    TrackSeed( calib::Dispatcher & ch
             , const std::string & only
             , ObjPool<TrackPoint> & obTP
             , ObjPool<Track> & bank
             , double magPosition
             , int missingHit=0. );
                  
    virtual AbstractHandler::ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID
                            , DetID_t
                            , TrackPoint & ) override;
             
};

}
}

#endif  //  defined(GenFit_FOUND)


