#pragma once

#include "na64sw-config.h"

#ifdef GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

namespace genfit { 
class EventDisplay;
}

namespace na64dp {
namespace handlers {

/**\brief Entry for track proccessing routine
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class TrackInit : public AbstractHandler {

private:
    /// Event display on
    bool _eventDisplay;
    bool _matInteraction;
    double _magFieldUp, _magFieldDown;
    double _magValue;

public:
    TrackInit( calib::Dispatcher & dsp
             , const std::string & only
             , bool eventDisplay
             , bool matInteraction
             , const std::string & geoFilePath 
             , double magFieldUp
             , double magFieldDown
             , double magValue
             );
                  
    virtual ProcRes process_event(Event * ) override;

};

}
}

#endif  //  defined(GenFit_FOUND)


