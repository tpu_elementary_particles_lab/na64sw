#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

#ifdef GenFit_FOUND

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class TrackFitting : public AbstractHandler {
private:
	int _minIter, _maxIter;
	double _minMomCut, _maxMomCut;
	int _verbose;

public:
    TrackFitting( calib::Dispatcher & dsp
                , const std::string & only
                , int minIter
                , int maxIter 
                , double minMomCut
                , double maxMomCut
                , int verbose);
                  
    virtual ProcRes process_event(Event * ) override;

};

}
}

#endif  //  defined(GenFit_FOUND)


