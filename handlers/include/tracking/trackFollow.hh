#pragma once

#include "na64sw-config.h"

#ifdef GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class TrackFollow : public AbstractHitHandler<TrackPoint> {
private:
    /// Track allocator reference
    ObjPool<Track> & _tracksBank;

	std::map<DetID_t, PoolRef<TrackPoint>> _tps;

public:
    TrackFollow( calib::Dispatcher & dsp
               , const std::string & only
               , ObjPool<Track> & bank );
                  
    virtual ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , TrackPoint & hit ) override;
};

}
}

#endif  //  defined(GenFit_FOUND)


