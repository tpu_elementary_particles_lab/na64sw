#pragma once

#include "na64sw-config.h"
#include <fstream>

#ifdef GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TH2F.h>

namespace genfit {  // fwd decls
class AbsTrackRep;
class AbsKalmanFitter;
}

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class TrackPlotRes : public AbstractHitHandler<TrackPoint>
                   , public TDirAdapter {

private:
    /// Track allocator reference
    ObjPool<Track> & _tracksBank;
    
    int _nBins, _min, _max;
    
    std::string _overridenPath;
    
    std::map<DetID_t, TH2F *> _histograms;
    
    std::ofstream resFile;
    
public:
    TrackPlotRes( calib::Dispatcher & dsp
                  , const std::string & only
                  , ObjPool<Track> & bank
                  );
    ~TrackPlotRes();
                  
    virtual ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , TrackPoint & hit ) override {assert(false);}
                            
    virtual void finalize() override;
};

}
}

#endif  //  defined(GenFit_FOUND)


