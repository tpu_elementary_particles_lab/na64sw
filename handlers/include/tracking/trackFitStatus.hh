#pragma once

#include "na64sw-config.h"

#ifdef GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class TrackFitStatus : public AbstractHitHandler<TrackPoint>
                      , public TDirAdapter {

private:
    /// Track allocator reference
    ObjPool<Track> & _tracksBank;
    /// Verbose for tracking information output
    int _verbose;
    
public:
    TrackFitStatus( calib::Dispatcher & dsp
                  , const std::string & only
                  , ObjPool<Track> & bank
                  , int verbose );
                  
    virtual ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , TrackPoint & hit ) override {assert(false);}
};

}
}

#endif  //  defined(GenFit_FOUND)


