#pragma once

#include "na64sw-config.h"

#ifdef GenFit_FOUND

#include "na64dp/abstractHitHandler.hh"
#include "na64event/hitInserter.hh"

#include <TVector3.h>

namespace na64dp {
namespace handlers {

/**\brief Simple handler to find track corresponding to energy depositon
 *
 * 
 *
 * */
 
class TrackFindSimple : public AbstractHitHandler<TrackPoint> {

protected:
	struct TrackCand {
			TVector3 seed;
			TVector3 mom;
			int pdg;
			std::vector<PoolRef<TrackPoint>> points;
	};
	
public:
	typedef double DPlacement;
	
	typedef std::multimap<DetID_t, PoolRef<TrackPoint>> Hits;
	
	// Track points sorted by their planes
	typedef std::vector<std::vector<PoolRef<TrackPoint>>> TrPoints;
	
	// Seeds for initial angle hypothesis
	typedef std::vector<TVector3> Seed;
	
	typedef std::vector<TrackCand> TrackCands;
	
private:

	/// Track allocator reference
    ObjPool<Track> & _tracksBank;
	    
    std::map<DPlacement, Hits> _trackPointsMap;
    
    TrackCands _trackCands;
    
    double _minResidual;
    int _minCandSize;
    double _momentum;
        
    // track candidate class
	void _sort_hits_by_planes( TrPoints & );
	
    void _create_seed( TrPoints &
					 , Seed & );
					 
	bool _process_seed( TVector3 &
	   			      , const TrPoints &
	   			      , int PDGcode
	   			      , double eDep );
		
		
public:
    TrackFindSimple( calib::Dispatcher & dsp
				   , const std::string & only
				   , ObjPool<Track> & bank
                   , double minResidual
			       , int minCandSize
			       , double momentum );
                  
    virtual ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , TrackPoint & hit ) override;

};


}
}

#endif  //  defined(GenFit_FOUND)


