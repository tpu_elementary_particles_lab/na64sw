#pragma once

#include "na64sw-config.h"

#ifdef ROOT_FOUND

#include "plotting/cluster1DHistogram.hh"

namespace na64dp {
namespace handlers {

/**\brief 1D histogram depicting cluster positions distribution
 *
 * Uses `Cluster1DHistogram` ancestor class to create histograms and implements
 * it value-filling method in order to populate histogram with cluster centers
 * in certain projection defined by detector plane.
 *
 * YAML config snippet:
 *
 * \code{.yaml}
 *     - name: ClusterCenterPlot
 *       histName: "clusterCenter"
 *       histDescr: "APV hits clusters width distribution"
 *       nBins: 100
 *       range: [0, 400] 
 * \endcode
 *
 * Resulting histogram reflects distribution of clusters of certain width.
 * \image html handlers/apv-cluster-width.png
 * */
class ClusterCenterPlot : public Cluster1DHistogram {
protected:
    void _fill_cluster_value(const APVCluster &, TH1F *) override;
public:
    ClusterCenterPlot( calib::Dispatcher &
                     , const std::string & hstName
                     , const std::string & hstDescr
                     , Int_t nBins, Float_t hstMin, Float_t hstMax );
};

}
}
#endif


