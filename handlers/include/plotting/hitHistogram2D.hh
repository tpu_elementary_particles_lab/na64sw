#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TH2.h>

namespace na64dp {
namespace handlers {

/**\brief A per-hit histogramming template handler.
 *
 * A template handler class building 2D histograms for each detector entity of
 * certain hit. Depicts values acquired with [value getters](\ref HitValueGetters).
 * Represents a 2D generalization of 1D version -- see `HitHistogram1D` template for more
 * detailed description.
 *
 * \image html handlers/amp-ratios.png
 *
 * YAML config snippet (for APV version, for SADC use `SADCHistogram2D` with
 * appropriate getters names in `valueX` and `valueY`):
 *
 * \code{.yaml}
 *    - name: APVHistogram2D
 *      histName: aRatios
 *      histDescr: "APV hit amplitude ratios plot"
 *      valueX: aRatio12
 *      nBinsX: 30
 *      rangeX: [-2, 2]
 *      valueY: aRatio13
 *      nBinsY: 30
 *      rangeY: [-2, 2]
 * \endcode
 *
 * \note For 1D version, see `HitHistogram2D` template.
 * \see HitHistogram1D
 * \see TDirAdapter
 * */
template<typename HitT>
class HitHistogram2D : public AbstractHitHandler<HitT>
                     , public TDirAdapter {
private:
    /// Getters for horizontan and vertical histogram axes
    typename EvFieldTraits<HitT>::ValueGetter _getters[2];
    /// Number of bins by horizontal and vertical axes
    Int_t _nBins[2];
    /// Min/max boundaries of histogram, by horizontal and vertical axes
    Double_t _mins[2], _maxs[2];
    const std::string _hstBaseName  ///< Histogram base name suffix
                    , _hstDescription;  ///< Histogram common description
    /// Index of histograms by unique detector ID
    std::map<DetID_t, TH2F *> _histograms;
public:
    /**\brief Generic ctr
     *
     * \param ch Calibration handle instance
     * \param getter1 A value getter #1 (horizontal axis), retrieving certain value from the hit
     * \param getter2 A value getter #2 (vertical axis), retrieving certain value from the hit
     * \param hstBaseName A string used to build unique histogram name
     * \param hstDescription A common description for histograms created by this handler
     * \param nBins1 Number of bins within the histogram horizontal axis
     * \param min1 Lower bound of histogram's range over horizontal axis
     * \param max1 Upper bound of histogram's range over horizontal axis
     * \param nBins2 Number of bins within the histogram vertical axis
     * \param min2 Lower bound of histogram's range over vertical axis
     * \param max2 Upper bound of histogram's range over vertical axis
     * */
    HitHistogram2D( calib::Dispatcher & cdsp
                  , const std::string & only
                  , typename EvFieldTraits<HitT>::ValueGetter getter1
                  , typename EvFieldTraits<HitT>::ValueGetter getter2
                  , const std::string & hstBaseName
                  , const std::string & hstDescription
                  , Int_t nBins1, Double_t min1, Double_t max1
                  , Int_t nBins2, Double_t min2, Double_t max2
                  ) : AbstractHitHandler<HitT>(cdsp, only)
                    , TDirAdapter(cdsp)
                    , _getters{getter1, getter2}
                    , _nBins{nBins1, nBins2}, _mins{min1, min2}, _maxs{max1, max2}
                    , _hstBaseName(hstBaseName), _hstDescription(hstDescription)
                    {
        assert(getter1);
        assert(getter2);
    }
    
    /// Explicitly deletes created histograms
    ~HitHistogram2D() {
        for(auto p : _histograms) {
            delete p.second;
        }
    }

    /// For certain detector entity, fills histogram with certain hit value
    virtual bool process_hit( EventID
                            , DetID_t did_
                            , HitT & currentHit ) override {
        DetID_t did = EvFieldTraits<HitT>::uniq_detector_id(did_);
        auto it = _histograms.find(did);
        if( _histograms.end() == it ) {
            // No histogram entry exists for current detector entity -- create
            // and insert one
            std::string hstName = _hstBaseName;
            dir_for(did, hstName)->cd();
            TH2F * newHst = new TH2F( hstName.c_str()
                                    , _hstDescription.c_str()
                                    , _nBins[0], _mins[0], _maxs[0]
                                    , _nBins[1], _mins[1], _maxs[1] );
            it = _histograms.emplace(did, newHst).first;  // `first' is an iterator
        }
        // fill bin content in the histogram
        it->second->Fill( _getters[0](currentHit), _getters[1](currentHit) );
        return true;
    }

    /// Writes histogram into current TFile
    virtual void finalize() {
        for( auto idHstPair : _histograms ) {
            tdirectories()[idHstPair.first]->cd();
            idHstPair.second->Write();
        }
    }
};

}
}

