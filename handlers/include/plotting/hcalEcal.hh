#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

#include <TH2F.h>

namespace na64dp {
namespace handlers {

/**\brief A Genfit-based test track fitting handler
 *
 * Enables Genfit procedures to perform station-based track fitting routine.
 *
 * */
class HcalEcal : public AbstractHitHandler<TrackPoint> {

private:

    /// Momentum resolution
    TH2F *eDep;
    
public:
    HcalEcal( calib::Dispatcher & dsp
            , const std::string & only );
                  
    virtual ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , TrackPoint & hit ) override {assert(false);}
                            
    virtual void finalize() override;
};

}
}
