#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64event/event.hh"

class TH2F;

namespace na64dp {
namespace handlers {

/** A correlation "preshower-vs-rest" energy deposition plot
 *
 * Depicts sums of energy deposition in preshower vs rest part of ECAL.
 * Use selection to restrict cells for summation
 * */
class PreshowerVsRestECAL : public AbstractHitHandler<SADCHit> {
private:
    size_t _nBinsPreshower;
    double _preshMin, _preshMax;
    size_t _nBinsRest;
    double _restMin, _restMax;
    double _preshowerSum, _restSum;
    const std::string _hstName, _hstTitle;

    TH2F * _histogram;
    DetID _ecalDID;
protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
public:
    PreshowerVsRestECAL( calib::Dispatcher & cdsp
                       , const std::string & selection
                       , const std::string & hstName
                       , const std::string & hstTitle
                       , size_t nBinsPreshower, double preshMin, double preshMax
                       , size_t nBinsRest, double restMin, double restMax
                       );
    ~PreshowerVsRestECAL();
    /// Fills the histogram
    virtual ProcRes process_event( Event * ) override;
    /// Increases the sums
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , SADCHit & hit ) override;
    /// Assures the writinh
    virtual void finalize() override;
};

}
}



