#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TH2F.h>

#include <yaml-cpp/node/node.h>
#include <vector>

namespace na64dp {
namespace handlers {

/**\brief Combined plot of the sampled amplitude from SADC detectors
 *
 * This handler depicts a somewhat combined image of multiple waveforms from
 * SADC detector in a single histogram. It might be useful for various
 * qualitative checks and assurance considerations.
 *
 * \code{.yaml}
 *    - name: SADCAmpHists
 *      baseName: "amps-pedCorrected"
 *      ampNBins: 200
 *      ampRange: [0, 4000]
 * \endcode
 *
 * SADC typically provides a time-descrete measurement of the voltage/current
 * waveform produced by PMT or SiPM detector as an array of 32 measurements.
 * Each time detector entity was triggered this handler puts all 32 digits onto
 * the 2D histogram. After number of events processed, a somwhat of "averaged"
 * picture can be observed for qualitative checks. For example, the picture
 * below indicates that previous handlers did not apply any pedestals
 * subtraction to the waveform leading to what whan may call "saw-shape".
 *
 * \image html handlers/sadc-amp-before.png
 *
 * Cf. with the same picture (produced by the same handler) after dynamic
 * pedestals subtraction handler (`DynSubtractPeds`) applied:
 *
 * \image html handlers/sadc-amp-after.png
 *
 * Since SADC detectors in NA64 always produce 32 samples per hit, this value
 * is hardcoded within this handler implementation.
 *
 * This handler uses `TDirAdapter` class to put the particular histogram
 * into approprite `TDirectory` instance within ROOT's `TFile`. For details,
 * see the corresponding [section of related page](\ref TDirAdapterDetails).
 *
 * Other types of histograms considering some scalar hit-related properties may
 * be produced with templated version of the 1D/2D histogram, -- see
 * `HitHistogram1D` and `HitHistogram2D`, but for this type of plot it required
 * a standalone handler.
 *
 * \see TDirAdapter
 * */
class SADCAmpHists : public TDirAdapter
                   , public AbstractHitHandler<SADCHit> {
private:
    /// Number of bins by vertical axis
    size_t _ampNBins;
    /// Amplitude range bounds
    double _ampRange[2];
    /// Histogram base name suffix
    const std::string _hstBaseName;
    /// Histogram index by detector ID
    std::map<DetID_t, TH2F *> _hists;
public:
    /// Main ctr of the handler
    SADCAmpHists( calib::Dispatcher & ch
                , size_t nAmpBins
                , double ampMin, double ampMax
                , const std::string & hstBaseName="amp"
                );
    /// Dtr deletes all dynamically-allocated histogram instances
    ~SADCAmpHists();
    /// Puts SADc samples onto a histogram
    virtual bool process_hit( EventID
                            , DetID_t
                            , SADCHit & currentEcalHit) override;
    /// Writes histograms to `TFile` instance by appropriate paths
    virtual void finalize() override;
};

}
}

