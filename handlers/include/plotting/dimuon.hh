#pragma once

#include "na64sw-config.h"

#include "na64dp/abstractHitHandler.hh"

#include <TH1F.h>

namespace na64dp {
namespace handlers {

/**\brief A simple handler to plot energy deposition
 *
 * Handler to plot energy deposition in VETO with a certain
 * threshold in HCAL. For dimuon production analysis
 *
 * */
class DiMuon : public AbstractHitHandler<SADCHit> {

private:

    /// Momentum resolution
    TH1F *eDep;
    
    const int _minEnergyHCAL, _maxEnergyHCAL;
    const int _minEnergyECAL, _maxEnergyECAL;
    
public:
    DiMuon( calib::Dispatcher & dsp
          , const std::string & only
          , int minEnergyHCAL = 0.
          , int maxEnergyHCAL = 200. 
          , int minEnergyECAL = 0.
          , int maxEnergyECAL = 200.);
                  
    virtual ProcRes process_event(Event * ) override;
                            
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , SADCHit & hit ) override {assert(false);}
                            
    virtual void finalize() override;
};

}
}
