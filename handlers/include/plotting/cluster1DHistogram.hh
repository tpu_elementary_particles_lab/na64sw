#pragma once

#include "na64dp/abstractHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TDirectory.h>
#include <TH1F.h>

namespace na64dp {
namespace handlers {

/**\brief Base class 1D distribution plots related to clusters on APV detectors
 *
 * This handler creates one histogram per APV detector plane (thus one for
 * each projection of each GEM and MM station).
 *
 * \note This handler is an important ancestor for subclassing handlers that
 * may retrieve other types of 1D cluster information, as implements most of
 * the relevant features: `TDirectory` routing (via `TDirAdapter`, cluster
 * retrieval, histogram's create, save and delete lifecycle).
 *
 * \todo Re-write it using cluster getter function.
 * */
class Cluster1DHistogram : public AbstractHandler
                         , public TDirAdapter {
private:
    const std::string _hstBaseName  ///< base histogram name suffix
                    , _hstDescription;  /// common histogram description string
    const Int_t _nBins;  ///< number of bins in histogram
    const Float_t _hstMin  ///< histogram range min
                , _hstMax  ///< histogram range max
                ;
    /// Index of histograms by detector ID
    std::map<DetID_t, TH1F *> _histograms;
protected:
    /// Shoult get value from a cluster and fill the histogram
    virtual void _fill_cluster_value( const APVCluster &, TH1F * ) = 0;
public:
    /// Main handler ctr
    Cluster1DHistogram( calib::Dispatcher &
                      , const std::string & hstName
                      , const std::string & hstDescr
                      , Int_t nBins, Float_t hstMin, Float_t hstMax );
    /// Deletes histograms created on heap
    ~Cluster1DHistogram();
    /// Performs histogram fill with obtained clusters
    virtual AbstractHandler::ProcRes process_event( Event * ) override;
    /// Saves filled histograms
    virtual void finalize();
};

}
}

