#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

#include <TH1.h>
#include <TMap.h>

namespace na64dp {
namespace handlers {

/**\brief A per-hit histogramming template handler.
 *
 * A template handler class producing 1D histograms for each detector entity of
 * certain hit. Depicts values acquired with _value getter_. The value getter
 * callbacks are described in details in corresponding
 * [section of related page](\ref HitValueGetters).
 *
 * \image html handlers/Histogram1D-example-1.png
 * \image html handlers/Histogram1D-example-2.png
 *
 * Histograms then will be stored in common TFile within corresponding
 * directories (`TDirectory`) as described in
 * [related docs of TDirAdapter](\ref TDirAdapterDetails).
 *
 * \code{.yaml}
 * - name: SADCHitHistogram1D
 *       value: sum
 *       histName: "linear-sum"
 *       histDescr: "Linear waveform sum approximation"
 *       nBins: 500
 *       range: [0, 5000]
 * \endcode
 *
 * This is the template class (as its mating two-dimensional version, the
 * `HitHistogram2D`), so its implementation provided in header while
 * template code instantiation is performed in
 * [virtual constructor](\ref VirtualCtr), in conjugated `.cc` file.
 *
 * \note For 2D generalization, see `HitHistogram2D` template.
 * \see HitHistogram2D
 * \see TDirAdapter
 * */
template<typename HitT>
class HitHistogram1D : public AbstractHitHandler<HitT>
                     , public TDirAdapter {
private:
    /// A value getter callback
    typename EvFieldTraits<HitT>::ValueGetter _getter;
    /// Number of histogram bins
    Int_t _nBins;
    /// Min/max range of histogram
    Double_t _min, _max;
    const std::string _hstBaseName  ///< Histogram base name suffix
                    , _hstDescription;  ///< Histogram common description
    /// Index of histograms by unique detector ID
    std::map<DetID_t, TH1F *> _histograms;
    /// Histogram path template. Leave empty to not override default one
    std::string _overridenPath;
public:
    /**\brief Generic ctr for getter-based 
     *
     * \param cdsp Calibration dispatcher instance
     * \param getter A value getter, retrieving certain value from the hit
     * \param hstBaseName A string used to build unique histogram name
     * \param hstDescription A common description for histograms created by this handler
     * \param nBins Number of bins within the histogram
     * \param min_ Lower bound of histogram's range
     * \param max_ Upper bound of histogram's range
     * \param saveDict Makes the handler to save TCollection of DetID-to-histName object
     * */
    HitHistogram1D( calib::Dispatcher & cdsp
                  , const std::string & only
                  , typename EvFieldTraits<HitT>::ValueGetter getter
                  , const std::string & hstBaseName
                  , const std::string & hstDescription
                  , Int_t nBins
                  , Double_t min_, Double_t max_
                  , const std::string & overridePath=""
                  ) : AbstractHitHandler<HitT>(cdsp, only)
                    , TDirAdapter(cdsp)
                    , _getter(getter)
                    , _nBins(nBins), _min(min_), _max(max_)
                    , _hstBaseName(hstBaseName), _hstDescription(hstDescription)
                    , _overridenPath(overridePath)
                    {
        assert(getter);
        //if(saveDict) {
        //    _dict = new TMap();
        //}
    }
    
    /// Deletes histogram instances allocated on heap
    ~HitHistogram1D() {
        for(auto p : _histograms) {
            delete p.second;
        }
    }

    /// For certain detector entity, fills histogram with certain hit value
    virtual bool process_hit( EventID
                            , DetID_t did_
                            , HitT & currentHit ) override {
        DetID_t did = EvFieldTraits<HitT>::uniq_detector_id(did_);
        auto it = _histograms.find(did);
        if( _histograms.end() == it ) {
            // No histogram entry exists for current detector entity -- create
            // and insert one
            std::string hstName = _hstBaseName;
            auto substCtx = subst_dict_for(did, hstName);
            std::string description = util::str_subst( _hstDescription, substCtx );
            auto p = dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            TH1F * newHst = new TH1F( p.first.c_str()
                                    , description.c_str()
                                    , _nBins, _min, _max );
            it = _histograms.emplace(did, newHst).first;  // `first' is an iterator
            //TString detNameTStr( this->naming()(did).c_str() )
            //      , hstNameTStr( hstName.c_str() );
            //_dict->Add( &detNameTStr, &hstNameTStr );
        }
        // fill bin content in the histogram
        it->second->Fill( _getter(currentHit) );
        return true;
    }

    /// Writes histogram into current TFile
    virtual void finalize() override {
        for( auto idHstPair : _histograms ) {
            tdirectories()[idHstPair.first]->cd();
            idHstPair.second->Write();
        }
    }
};

}
}

