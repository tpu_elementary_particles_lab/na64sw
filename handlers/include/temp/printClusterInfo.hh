#pragma once

#include "na64dp/abstractHitHandler.hh"
#include "na64util/TDirAdapter.hh"

namespace na64dp {
namespace handlers {

class APVPrintClusterInfo : public AbstractHitHandler<APVHit>, public TDirAdapter {

public:
    APVPrintClusterInfo( calib::Dispatcher & dsp, const std::string & select );
    
    /// An interface function, performs clusterization on given event.
    virtual AbstractHandler::ProcRes process_event( Event * e ) override;
    /// Fills hits cache
    virtual bool process_hit( EventID
                            , DetID_t
                            , APVHit & ) override;
};

}
}

