#include "plotting/dimuon.hh"

#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TH1D.h>

namespace na64dp {
namespace handlers {

DiMuon::DiMuon( calib::Dispatcher & cdsp
              , const std::string & only
              , int minEnergyHCAL
              , int maxEnergyHCAL
              , int minEnergyECAL
              , int maxEnergyECAL )
        : AbstractHitHandler<SADCHit>(cdsp, only)
        , _minEnergyHCAL(minEnergyHCAL)
        , _maxEnergyHCAL(maxEnergyHCAL)
        , _minEnergyECAL(minEnergyECAL)
        , _maxEnergyECAL(maxEnergyECAL) {
    
    eDep = new TH1F("VETO energy deposition" ,"eDep"
                   , 100, 0, 600 );
    
}

DiMuon::ProcRes
DiMuon::process_event(Event * evPtr) {
    
    double & hcal = evPtr->hcalEdep;
    double & ecal = evPtr->ecalEdep;
    double & veto = evPtr->vetoEdep;
		
	if ( ( hcal > _minEnergyHCAL && hcal < _maxEnergyHCAL ) &&
	     ( ecal > _minEnergyECAL && ecal < _maxEnergyECAL ) ) {
			eDep->Fill( veto );
	} else {
		return kDiscriminateEvent;
	}
   
    return kOk;
}

void
DiMuon::finalize(){
    
    TCanvas* c1 = new TCanvas(); 
    c1->cd(1);
    eDep->Draw();   
    
}

REGISTER_HANDLER( DiMuon, banks, ch, cfg
                , "Handler to plot VETO for dimuon production" 
                " energy deposition" ) {
    
    return new DiMuon( ch
                     , aux::retrieve_det_selection(cfg)
                     , cfg["EnergyRangeHCAL"][0].as<double>()
                     , cfg["EnergyRangeHCAL"][1].as<double>()
                     , cfg["EnergyRangeECAL"][0].as<double>()
                     , cfg["EnergyRangeECAL"][1].as<double>()
                     );
}
}
}
