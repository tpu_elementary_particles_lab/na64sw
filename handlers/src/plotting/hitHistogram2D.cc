#include "plotting/hitHistogram2D.hh"

namespace na64dp {

REGISTER_HANDLER( SADCHitHistogram2D, banks, ch, cfg
                , "Plots certain values of a SADC detector hits as 2D histogram" ) {
    EvFieldTraits<SADCHit>::ValueGetter getters[2];
    for(int i = 0; i < 2; ++i) {
        char keyBf[32];
        snprintf(keyBf, sizeof(keyBf), "value%c", i ? 'Y' : 'X');
        const std::string getterStr = cfg[keyBf].as<std::string>();
        getters[i] = utils::value_getter<SADCHit>( getterStr );
    }

    return new handlers::HitHistogram2D<SADCHit>( ch
                    , aux::retrieve_det_selection(cfg)
                    , getters[0], getters[1]
                    , cfg["histName"].as<std::string>()
                    , cfg["histDescr"].as<std::string>()
                    , cfg["nBinsX"].as<int>()
                    , cfg["rangeX"][0].as<double>()
                    , cfg["rangeX"][1].as<double>()
                    , cfg["nBinsY"].as<int>()
                    , cfg["rangeY"][0].as<double>()
                    , cfg["rangeY"][1].as<double>()
                    );
}

REGISTER_HANDLER( APVHitHistogram2D, banks, ch, cfg
                , "Plots certain values of a APV detector hits as 2D histogram" ) {
    EvFieldTraits<APVHit>::ValueGetter getters[2];
    for(int i = 0; i < 2; ++i) {
        char keyBf[32];
        snprintf(keyBf, sizeof(keyBf), "value%c", i ? 'Y' : 'X');
        const std::string getterStr = cfg[keyBf].as<std::string>();
        getters[i] = utils::value_getter<APVHit>( getterStr );
    }

    return new handlers::HitHistogram2D<APVHit>( ch
                    , aux::retrieve_det_selection(cfg)
                    , getters[0], getters[1]
                    , cfg["histName"].as<std::string>()
                    , cfg["histDescr"].as<std::string>()
                    , cfg["nBinsX"].as<int>()
                    , cfg["rangeX"][0].as<double>()
                    , cfg["rangeX"][1].as<double>()
                    , cfg["nBinsY"].as<int>()
                    , cfg["rangeY"][0].as<double>()
                    , cfg["rangeY"][1].as<double>()
                    );
}

REGISTER_HANDLER( APVClusterHistogram2D, banks, ch, cfg
                , "Plots certain values of a APV detector clusters as 2D histogram" ) {
    EvFieldTraits<APVCluster>::ValueGetter getters[2];
    for(int i = 0; i < 2; ++i) {
        char keyBf[32];
        snprintf(keyBf, sizeof(keyBf), "value%c", i ? 'Y' : 'X');
        const std::string getterStr = cfg[keyBf].as<std::string>();
        getters[i] = utils::value_getter<APVCluster>( getterStr );
    }

    return new handlers::HitHistogram2D<APVCluster>( ch
                    , aux::retrieve_det_selection(cfg)
                    , getters[0], getters[1]
                    , cfg["histName"].as<std::string>()
                    , cfg["histDescr"].as<std::string>()
                    , cfg["nBinsX"].as<int>()
                    , cfg["rangeX"][0].as<double>()
                    , cfg["rangeX"][1].as<double>()
                    , cfg["nBinsY"].as<int>()
                    , cfg["rangeY"][0].as<double>()
                    , cfg["rangeY"][1].as<double>()
                    );
}

REGISTER_HANDLER( CaloEDepHistogram2D, banks, ch, cfg
                , "Plots certain values of a CaloEDep as 2D histogram" ) {
    EvFieldTraits<CaloEDep>::ValueGetter getters[2];
    for(int i = 0; i < 2; ++i) {
        char keyBf[32];
        snprintf(keyBf, sizeof(keyBf), "value%c", i ? 'Y' : 'X');
        const std::string getterStr = cfg[keyBf].as<std::string>();
        getters[i] = utils::value_getter<CaloEDep>( getterStr );
    }

    return new handlers::HitHistogram2D<CaloEDep>( ch
                    , aux::retrieve_det_selection(cfg)
                    , getters[0], getters[1]
                    , cfg["histName"].as<std::string>()
                    , cfg["histDescr"].as<std::string>()
                    , cfg["nBinsX"].as<int>()
                    , cfg["rangeX"][0].as<double>()
                    , cfg["rangeX"][1].as<double>()
                    , cfg["nBinsY"].as<int>()
                    , cfg["rangeY"][0].as<double>()
                    , cfg["rangeY"][1].as<double>()
                    );
}

}
