 #include "plotting/hitHistogram3D.hh"

namespace na64dp {

REGISTER_HANDLER( SADCHitHistogram3D, banks, ch, cfg
                , "Plots certain values of a SADC detector hits as 3D histogram" ) {
    EvFieldTraits<SADCHit>::ValueGetter getters[3];
    char axes[] = "XYZ";
    for(int i = 0; i < 3; ++i) {
        char keyBf[32];
        snprintf(keyBf, sizeof(keyBf), "value%c", axes[i]);
        const std::string getterStr = cfg[keyBf].as<std::string>();
        getters[i] = utils::value_getter<SADCHit>( getterStr );
    }

    return new handlers::HitHistogram3D<SADCHit>( ch
                    , aux::retrieve_det_selection(cfg)
                    , getters[0], getters[1], getters[2]
                    , cfg["histName"].as<std::string>()
                    , cfg["histDescr"].as<std::string>()
                    , cfg["nBinsX"].as<int>()
                    , cfg["rangeX"][0].as<double>()
                    , cfg["rangeX"][1].as<double>()
                    , cfg["nBinsY"].as<int>()
                    , cfg["rangeY"][0].as<double>()
                    , cfg["rangeY"][1].as<double>()
                    , cfg["nBinsZ"].as<int>()
                    , cfg["rangeZ"][0].as<double>()
                    , cfg["rangeZ"][1].as<double>()
                    );
}

REGISTER_HANDLER( APVHitHistogram3D, banks, ch, cfg
                , "Plots certain values of a APV detector hits as 3D histogram" ) {
    EvFieldTraits<APVHit>::ValueGetter getters[3];
    char axes[] = "XYZ";
    for(int i = 0; i < 3; ++i) {
        char keyBf[32];
        snprintf(keyBf, sizeof(keyBf), "value%c", axes[i]);
        const std::string getterStr = cfg[keyBf].as<std::string>();
        getters[i] = utils::value_getter<APVHit>( getterStr );
    }

    return new handlers::HitHistogram3D<APVHit>( ch
                    , aux::retrieve_det_selection(cfg)
                    , getters[0], getters[1], getters[2]
                    , cfg["histName"].as<std::string>()
                    , cfg["histDescr"].as<std::string>()
                    , cfg["nBinsX"].as<int>()
                    , cfg["rangeX"][0].as<double>()
                    , cfg["rangeX"][1].as<double>()
                    , cfg["nBinsY"].as<int>()
                    , cfg["rangeY"][0].as<double>()
                    , cfg["rangeY"][1].as<double>()
                    , cfg["nBinsZ"].as<int>()
                    , cfg["rangeZ"][0].as<double>()
                    , cfg["rangeZ"][1].as<double>()
                    );
}

REGISTER_HANDLER( CaloEDepHistogram3D, banks, ch, cfg
                , "Plots certain values of a CaloEDep hits as 3D histogram" ) {
    EvFieldTraits<CaloEDep>::ValueGetter getters[3];
    char axes[] = "XYZ";
    for(int i = 0; i < 3; ++i) {
        char keyBf[32];
        snprintf(keyBf, sizeof(keyBf), "value%c", axes[i]);
        const std::string getterStr = cfg[keyBf].as<std::string>();
        getters[i] = utils::value_getter<CaloEDep>( getterStr );
    }

    return new handlers::HitHistogram3D<CaloEDep>( ch
                    , aux::retrieve_det_selection(cfg)
                    , getters[0], getters[1], getters[2]
                    , cfg["histName"].as<std::string>()
                    , cfg["histDescr"].as<std::string>()
                    , cfg["nBinsX"].as<int>()
                    , cfg["rangeX"][0].as<double>()
                    , cfg["rangeX"][1].as<double>()
                    , cfg["nBinsY"].as<int>()
                    , cfg["rangeY"][0].as<double>()
                    , cfg["rangeY"][1].as<double>()
                    , cfg["nBinsZ"].as<int>()
                    , cfg["rangeZ"][0].as<double>()
                    , cfg["rangeZ"][1].as<double>()
                    );
}

}
