#include "plotting/cluster1DHistogram.hh"

namespace na64dp {
namespace handlers {

/** Created instance relies on `TDirAdapter` TDirectory routing.
 *
 * \param ch Calibration handle
 * \param hstName Base string name for histogram
 * \param hstDescr Common description string for histogram
 * \param nBins Number of bins
 * \param hstMin Histogram range min
 * \param hstMax Histogram range max
 * */
Cluster1DHistogram::Cluster1DHistogram( calib::Dispatcher & cdsp
        , const std::string & hstName
        , const std::string & hstDescr
        , Int_t nBins, Float_t hstMin, Float_t hstMax ) : TDirAdapter(cdsp)
                                                        , _hstBaseName(hstName)
                                                        , _hstDescription(hstDescr)
                                                        , _nBins(nBins)
                                                        , _hstMin(hstMin), _hstMax(hstMax) {
}

AbstractHandler::ProcRes
Cluster1DHistogram::process_event( Event * ePtr ) {
    assert(ePtr);
    for( auto chp : ePtr->apvClusters ) {
        DetID did = chp.first;
        APVCluster & cluster = *chp.second;

        DetID_t uDid = EvFieldTraits<APVHit>::uniq_detector_id(did);
        auto it = _histograms.find(uDid);
        if( _histograms.end() == it ) {
            // No histogram entry exists for current detector entity -- create
            // and insert one
            std::string hstName = _hstBaseName;
            dir_for(uDid, hstName)->cd();
            TH1F * newHst = new TH1F( hstName.c_str()
                                    , _hstDescription.c_str()
                                    , _nBins, _hstMin, _hstMax );
            it = _histograms.emplace(uDid, newHst).first;  // `first' is an iterator
        }
        _fill_cluster_value( cluster, it->second );
    }
    return kOk;
}

/**Writes histograms at the end of processing. */
void
Cluster1DHistogram::finalize() {
    for( auto idHstPair : _histograms ) {
        tdirectories()[idHstPair.first]->cd();
        idHstPair.second->Write();
    }
}

/**It is important to explicitly delete created histograms in order to prevent
 * ROOT from saving them into TFile's root. */
Cluster1DHistogram::~Cluster1DHistogram() {
    for( auto hstP : _histograms ) {
        delete hstP.second;
    }
}

}

// no REGISTER_HANDLER for abstract class

}

