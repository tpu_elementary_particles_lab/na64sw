#include "plotting/clusterCenterPlot.hh"

#ifdef ROOT_FOUND

namespace na64dp {
namespace handlers {

void
ClusterCenterPlot::_fill_cluster_value(const APVCluster & cluster, TH1F * hst) {
    hst->Fill( cluster.position );
}

ClusterCenterPlot::ClusterCenterPlot( calib::Dispatcher & cdsp
                                  , const std::string & hstName
                                  , const std::string & hstDescr
                                  , Int_t nBins, Float_t hstMin, Float_t hstMax )
        : Cluster1DHistogram( cdsp, hstName, hstDescr, nBins, hstMin, hstMax ) {
}

}


REGISTER_HANDLER( ClusterCenterPlot, banks, ch, yamlNode
                , "Produces a 1D histogram of APV hits cluster center distribution" ) {
    return new handlers::ClusterCenterPlot( ch
                                 , yamlNode["histName"].as<std::string>()
                                 , yamlNode["histDescr"].as<std::string>()
                                 , yamlNode["nBins"].as<Int_t>()
                                 , yamlNode["range"][0].as<Float_t>()
                                 , yamlNode["range"][1].as<Float_t>() );
}

}

#endif


