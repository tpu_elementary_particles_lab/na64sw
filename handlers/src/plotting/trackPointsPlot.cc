#include "plotting/trackPointsPlot.hh"

#include <TH2F.h>

namespace na64dp {
namespace handlers {

TrackPointsPlot::TrackPointsPlot( calib::Dispatcher & ch
                                , const std::string & hstBaseName
                                , const std::string & hstDescription
                                , Int_t nBinsX, Float_t rangeXMin, Float_t rangeXMax
                                , Int_t nBinsY, Float_t rangeYMin, Float_t rangeYMax
                                , const std::string & hstPathTemplate
                                , const std::string & selection
                                ) : TDirAdapter(ch)
                                  , SelectiveHitHandler(ch, selection)
                                  , _hstBaseName(hstBaseName)
                                  , _hstDescription(hstDescription)
                                  , _nBins{nBinsX, nBinsY}
                                  , _ranges{ {rangeXMin, rangeXMax}
                                           , {rangeYMin, rangeYMax} }
                                  , _hstPathTemplate(hstPathTemplate)
                                  {
}

TrackPointsPlot::~TrackPointsPlot() {
    for( auto p : _hists ) {
        delete p.second;
    }
}

bool
TrackPointsPlot::_process_track_point( TrackPoint & tp ) {
    if( ! tp.station ) {
        // track point does not belong to any station and probably is some
        // virtual one that must be a subject of another handler
        return true;
    }
    auto it = _hists.find( tp.station );
    if( _hists.end() == it ) {
        // no histogram exists for this detector entity -- create and insert
        std::string histName = _hstBaseName;
        dir_for(tp.station, histName, _hstPathTemplate)->cd();
        TH2F * newHst = new TH2F( histName.c_str()
                                , _hstDescription.c_str()
                                , _nBins[0], _ranges[0][0], _ranges[0][1]
                                , _nBins[1], _ranges[1][0], _ranges[1][1] );
        it = _hists.emplace( tp.station, newHst ).first;
    }
    // fill the histogram
    it->second->Fill( tp.lR[0], tp.lR[1] );
    return true;
}

AbstractHandler::ProcRes
TrackPointsPlot::process_event( Event * evPtr ) {
	#if 0
    for( auto & trackPair : e->tracks ) {
        //TrackID trID(trackPair.first);
        for( auto & trackPoint : *(trackPair.second) ) {
            if( is_selective() && !matches(trackPoint->station) ) {
                continue;
            }
            _process_track_point( *trackPoint );
        }
    }
    #endif
    
    for (auto p : evPtr->trackPoints) {
		if( is_selective() && !matches((*p.second).station) ) {
			continue;
		}
	    _process_track_point( *p.second );
	    
	}
		   
    return kOk;
}

void
TrackPointsPlot::finalize() {
    for( auto idHstPair : _hists ) {
        tdirectories()[idHstPair.first]->cd();
        idHstPair.second->Write();
    }
}

}

REGISTER_HANDLER( TrackPointsPlot, banks, ch, cfg
                , "Plots 2D histogram of per-station tracks points distribution" ) {
    return new handlers::TrackPointsPlot( ch
                                        , cfg["histName"].as<std::string>()
                                        , cfg["histDescr"].as<std::string>()
                                        , cfg["nBinsX"].as<Int_t>()
                                        , cfg["rangeX"][0].as<Float_t>()
                                        , cfg["rangeX"][1].as<Float_t>()
                                        , cfg["nBinsY"].as<Int_t>()
                                        , cfg["rangeY"][0].as<Float_t>()
                                        , cfg["rangeY"][1].as<Float_t>()
                                        , cfg["histsTPath"].as<std::string>()
                                        , aux::retrieve_det_selection(cfg) );
}

}

