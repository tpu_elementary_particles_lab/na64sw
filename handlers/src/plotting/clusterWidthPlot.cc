#include "plotting/clusterWidthPlot.hh"

#ifdef ROOT_FOUND

namespace na64dp {
namespace handlers {

void
ClusterWidthPlot::_fill_cluster_value(const APVCluster & cluster, TH1F * hst) {
    hst->Fill( cluster.size() );
}

ClusterWidthPlot::ClusterWidthPlot( calib::Dispatcher & cdsp
                                  , const std::string & hstName
                                  , const std::string & hstDescr
                                  , Int_t nBins, Float_t hstMin, Float_t hstMax )
        : Cluster1DHistogram( cdsp, hstName, hstDescr, nBins, hstMin, hstMax ) {
}

}


REGISTER_HANDLER( ClusterWidthPlot, banks, ch, yamlNode
                , "Produces a histogram of APV hits cluster width distribution" ) {
    return new handlers::ClusterWidthPlot( ch
                                 , yamlNode["histName"].as<std::string>()
                                 , yamlNode["histDescr"].as<std::string>()
                                 , yamlNode["nBins"].as<Int_t>()
                                 , yamlNode["range"][0].as<Float_t>()
                                 , yamlNode["range"][1].as<Float_t>() );
}

}

#endif

