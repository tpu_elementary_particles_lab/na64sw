#include "calibration/calibVETO.hh"

#include "na64detID/cellID.hh"

namespace na64dp {
namespace handlers {

static CalibVETO::VETOCalib gVETOCalibs[] = {
	
	#if 1
	// Energy deposition, tmean, tsigma
	// No time calibrations for VETO in 2017???
	{"VETO:0-0--", 9.8 , 590 , 1 , 1 },
	{"VETO:1-0--", 9.8 , 501 , 1 , 1 },
	{"VETO:2-0--", 9.8 , 590 , 1 , 1 },
	{"VETO:3-0--", 9.8 , 637 , 1 , 1 },
	{"VETO:4-0--", 9.8 , 610 , 1 , 1 },
	{"VETO:5-0--", 9.8 , 566 , 1 , 1 },

	#else
	// 2018
	{"VETO:0-0--", 9.8 , 790 , 1 , 1 },
	{"VETO:1-0--", 9.8 , 772 , 1 , 1 },
	{"VETO:2-0--", 9.8 , 820 , 1 , 1 },
	{"VETO:3-0--", 9.8 , 400 , 1 , 1 },
	{"VETO:4-0--", 9.8 , 600 , 1 , 1 },
	{"VETO:5-0--", 9.8 , 800 , 1 , 1 },	
	

	#endif
};

CalibVETO::CalibVETO( calib::Dispatcher & ch
                    , const std::string & only
                    , double timeCut) 
                    : AbstractHitHandler<SADCHit>(ch, only)
				    , _timeCut(timeCut) {
						
	ch.subscribe<nameutils::DetectorNaming>(*this, "default");
}

void
CalibVETO::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<SADCHit>::handle_update(nm);

    _names = &nm;
    _namingCache.sadcChipCode = nm.chip_id("SADC");
    _namingCache.vetoKinCode = nm.kin_id("VETO").second;
    
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0
       ; i < sizeof(gVETOCalibs)/sizeof(VETOCalib)
       ; ++i ) {
		
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gVETOCalibs[i].planeName);
        // impose placement entry into `_placements' map
        _calibs.emplace(did, gVETOCalibs + i);
    }
}

bool
CalibVETO::process_hit( EventID
					  , DetID_t did
                      , SADCHit & hit ) {
	
	#if 0
	std::cout << naming()[did] << std::endl;
	std::cout << hit.maxValue << std::endl;
	std::cout << hit.maxSample << std::endl;
	#endif
	
	// Process ECAL hits and calibrate energy deposition
	
	DetID stationID(did);
	
	if ( _namingCache.vetoKinCode != stationID.kin() ) {
		return true;
	}

	double bin(12.5); // bin time in ns
				
	const VETOCalib * cCalibs = _calibs[did];
	
	double & masterTime = _current_event().masterTime;
	
	double bestValue(std::numeric_limits<double>::min());
	double bestSample(std::numeric_limits<int>::min());
	double smDiff(std::numeric_limits<int>::max());
		
	for ( auto & it : hit.maxima ) {
		double timeDiff = ((it.first * bin) - cCalibs->time) - masterTime;
		
		if ( std::fabs(timeDiff) < smDiff ) {
			smDiff = std::fabs(timeDiff);
			bestSample = it.first;
			bestValue = it.second;
		}
	}
	
	if ( std::fabs(smDiff) < _timeCut ) { 
	
		hit.maxValue = bestValue;
		hit.maxSample = bestSample;
		
		hit.eDep = bestValue * (cCalibs->factor / cCalibs->energy);
	} else {
		hit.eDep = 0;
	}
							   
	return true;
}

}

REGISTER_HANDLER( CalibVETO, banks, ch, yamlNode
                , "Handler for VETO time and energy calibration" ) {
    
    return new handlers::CalibVETO( ch
								  , aux::retrieve_det_selection(yamlNode)
								  , yamlNode["timeCut"].as<double>() );
};
}
