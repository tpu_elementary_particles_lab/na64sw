#include "calibration/calibHCAL.hh"

namespace na64dp {
namespace handlers {

static CalibHCAL::HCALCalib gHCALCalibs[] = {
	
	#if 1
	// Energy deposition, tmean, tsigma
	{"HCAL0:0-0--", 65.5 , 1.27 , 2529 , -60.6 , 8 } ,
	{"HCAL0:0-1--", 65.5 , 1.27 , 2519 , -68.7 , 8 } ,
	{"HCAL0:0-2--", 65.5 , 1.27 , 2531 , -58.4 , 8 } ,
	{"HCAL0:1-0--", 65.5 , 1.27 , 2484 , -58.1 , 8 } ,
	{"HCAL0:1-1--", 65.5 , 1.27 , 2524 , -61.5 , 8 } ,
	{"HCAL0:1-2--", 65.5 , 1.27 , 2497 , -59.8 , 8 } ,
	{"HCAL0:2-0--", 65.5 , 1.27 , 2506 , -65.1 , 8 } ,
	{"HCAL0:2-1--", 65.5 , 1.27 , 2468 , -56.5 , 8 } ,
	{"HCAL0:2-2--", 65.5 , 1.27 , 2497 , -59.5 , 8 } ,
	{"HCAL1:0-0--", 65.5 , 1.48 , 2453 , -62.9 , 14 } ,
	{"HCAL1:0-1--", 65.5 , 1.48 , 2467 , -66.0 , 14 } ,
	{"HCAL1:0-2--", 65.5 , 1.48 , 2591 , -70.7 , 14 } ,
	{"HCAL1:1-0--", 65.5 , 1.48 , 2520 , -67.1 , 14 } ,
	{"HCAL1:1-1--", 65.5 , 1.48 , 2536 , -56.6 , 14 } ,
	{"HCAL1:1-2--", 65.5 , 1.48 , 2450 , -58.7 , 14 } ,
	{"HCAL1:2-0--", 65.5 , 1.48 , 2499 , -62.3 , 14 } ,
	{"HCAL1:2-1--", 65.5 , 1.48 , 2558 , -68.0 , 14 } ,
	{"HCAL1:2-2--", 65.5 , 1.48 , 2566 , -72.9 , 14 } ,
	{"HCAL2:0-0--", 65.5 , 1.21 , 2423 , -68.5 , 8 } ,
	{"HCAL2:0-1--", 65.5 , 1.21 , 2433 , -54.3 , 8 } ,
	{"HCAL2:0-2--", 65.5 , 1.21 , 2480 , -57.8 , 8 } ,
	{"HCAL2:1-0--", 65.5 , 1.21 , 2568 , -72.5 , 8 } ,
	{"HCAL2:1-1--", 65.5 , 1.21 , 2521 , -61.2 , 8 } ,
	{"HCAL2:1-2--", 65.5 , 1.21 , 2559 , -58.4 , 8 } ,
	{"HCAL2:2-0--", 65.5 , 1.21 , 2459 , -61.4 , 8 } ,
	{"HCAL2:2-1--", 65.5 , 1.21 , 2570 , -57.4 , 8 } ,
	{"HCAL2:2-2--", 65.5 , 1.21 , 2546 , -59.2 , 8 } ,
	{"HCAL3:0-0--", 65.5 , 1 , 2528 , -65.7 , 8 } ,
	{"HCAL3:0-1--", 65.5 , 1 , 2425 , -64.0 , 8 } ,
	{"HCAL3:0-2--", 65.5 , 1 , 2445 , -66.8 , 8 } ,
	{"HCAL3:1-0--", 65.5 , 1 , 2501 , -63.7 , 8 } ,
	{"HCAL3:1-1--", 65.5 , 1 , 2423 , -64.3 , 8 } ,
	{"HCAL3:1-2--", 65.5 , 1 , 2470 , -68.6 , 8 } ,
	{"HCAL3:2-0--", 65.5 , 1 , 2400 , -67.2 , 8 } ,
	{"HCAL3:2-1--", 65.5 , 1 , 2550 , -64.5 , 8 } ,
	{"HCAL3:2-2--", 65.5 , 1 , 2380 , -57.7 , 8 } ,
	#else
	
	
	// 2018
	{"HCAL0:0-0--", 100 , 1 , 2102 , -60.6 , 8 } ,
	{"HCAL0:0-1--", 100 , 1 , 2281 , -68.7 , 8 } ,
	{"HCAL0:0-2--", 100 , 1 , 2302 , -58.4 , 8 } ,
	{"HCAL0:1-0--", 100 , 1 , 2318 , -58.1 , 8 } ,
	{"HCAL0:1-1--", 100 , 1 , 2344 , -61.5 , 8 } ,
	{"HCAL0:1-2--", 100 , 1 , 2303 , -59.8 , 8 } ,
	{"HCAL0:2-0--", 100 , 1 , 2407 , -65.1 , 8 } ,
	{"HCAL0:2-1--", 100 , 1 , 2114 , -56.5 , 8 } ,
	{"HCAL0:2-2--", 100 , 1 , 2069 , -59.5 , 8 } ,
	{"HCAL1:0-0--", 100 , 1 , 2363 , -62.9 , 14 } ,
	{"HCAL1:0-1--", 100 , 1 , 2408 , -66.0 , 14 } ,
	{"HCAL1:0-2--", 100 , 1 , 2283 , -70.7 , 14 } ,
	{"HCAL1:1-0--", 100 , 1 , 2352 , -67.1 , 14 } ,
	{"HCAL1:1-1--", 100 , 1 , 2374 , -56.6 , 14 } ,
	{"HCAL1:1-2--", 100 , 1 , 2411 , -58.7 , 14 } ,
	{"HCAL1:2-0--", 100 , 1 , 2486 , -62.3 , 14 } ,
	{"HCAL1:2-1--", 100 , 1 , 2342 , -68.0 , 14 } ,
	{"HCAL1:2-2--", 100 , 1 , 2262 , -72.9 , 14 } ,
	{"HCAL2:0-0--", 100 , 1 , 1880 , -68.5 , 8 } ,
	{"HCAL2:0-1--", 100 , 1 , 1994 , -54.3 , 8 } ,
	{"HCAL2:0-2--", 100 , 1 , 1551 , -57.8 , 8 } ,
	{"HCAL2:1-0--", 100 , 1 , 2252 , -72.5 , 8 } ,
	{"HCAL2:1-1--", 100 , 1 , 2000 , -61.2 , 8 } ,
	{"HCAL2:1-2--", 100 , 1 , 1863 , -58.4 , 8 } ,
	{"HCAL2:2-0--", 100 , 1 , 2273 , -61.4 , 8 } ,
	{"HCAL2:2-1--", 100 , 1 , 2203 , -57.4 , 8 } ,
	{"HCAL2:2-2--", 100 , 1 , 2184 , -59.2 , 8 } ,
	{"HCAL3:0-0--", 100 , 1 , 2245 , -65.7 , 8 } ,
	{"HCAL3:0-1--", 100 , 1 , 1917 , -64.0 , 8 } ,
	{"HCAL3:0-2--", 100 , 1 , 1829 , -66.8 , 8 } ,
	{"HCAL3:1-0--", 100 , 1 , 2208 , -63.7 , 8 } ,
	{"HCAL3:1-1--", 100 , 1 , 2850 , -64.3 , 8 } ,
	{"HCAL3:1-2--", 100 , 1 , 2454 , -68.6 , 8 } ,
	{"HCAL3:2-0--", 100 , 1 , 1920 , -67.2 , 8 } ,
	{"HCAL3:2-1--", 100 , 1 , 1850 , -64.5 , 8 } ,
	{"HCAL3:2-2--", 100 , 1 , 2679 , -57.7 , 8 } ,
	#endif
};

CalibHCAL::CalibHCAL( calib::Dispatcher & ch
                    , const std::string & only
                    , double timeCut ) 
                    : AbstractHitHandler<SADCHit>(ch, only)
                    , _timeCut(timeCut) {
						
	ch.subscribe<nameutils::DetectorNaming>(*this, "default");
}

void
CalibHCAL::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<SADCHit>::handle_update(nm);

    _names = &nm;
    _namingCache.sadcChipCode = nm.chip_id("SADC");
    _namingCache.hcalKinCode = nm.kin_id("HCAL").second;
    
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0
       ; i < sizeof(gHCALCalibs)/sizeof(HCALCalib)
       ; ++i ) {
		
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gHCALCalibs[i].planeName);
        // impose placement entry into `_placements' map
        _calibs.emplace(did, gHCALCalibs + i);
    }
}

bool
CalibHCAL::process_hit( EventID
					  , DetID_t did
                      , SADCHit & hit ) {
	
	#if 0
	std::cout << naming()[did] << std::endl;
	std::cout << hit.maxValue << std::endl;
	std::cout << hit.maxSample << std::endl;
	#endif
	
	// Process ECAL hits and calibrate energy deposition
	
	DetID stationID(did);
	
	if ( _namingCache.hcalKinCode != stationID.kin() ) {
		return true;
	}
				
	const HCALCalib * cCalibs = _calibs[did];
		
	double bin(12.5); // bin time in ns
	
	double & masterTime = _current_event().masterTime;
	
	double bestValue(std::numeric_limits<double>::min());
	double bestSample(std::numeric_limits<int>::min());
	double smDiff(std::numeric_limits<int>::max());
		
	for ( auto & it : hit.maxima ) {
		double timeDiff = ((it.first * bin) - cCalibs->time) - masterTime;
		
		if ( std::fabs(timeDiff) < smDiff ) {
			smDiff = std::fabs(timeDiff);
			bestSample = it.first;
			bestValue = it.second;
		}
	}
	
	if ( std::fabs(smDiff) < _timeCut ) { 
	
		hit.maxValue = bestValue;
		hit.maxSample = bestSample;
		
		hit.eDep = bestValue * (cCalibs->factor / cCalibs->energy);
	} else {
		hit.eDep = 0;
	}
							   
	return true;
}

}

REGISTER_HANDLER( CalibHCAL, banks, ch, yamlNode
                , "Handler for HCAL time and energy calibration" ) {
    
    return new handlers::CalibHCAL( ch
								  , aux::retrieve_det_selection(yamlNode)
								  , yamlNode["timeCut"].as<double>() );
};
}
