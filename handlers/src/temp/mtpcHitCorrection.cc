#include "new_data/mtpcHitCorrection.hh"

#include "math.h"

namespace na64dp {
namespace handlers {

APVCorrHitTime::APVCorrHitTime( calib::Dispatcher & cdsp
                              , const std::string & only
                              , ObjPool<APVCluster> & obCluster
                              ) : AbstractHitHandler<APVCluster>(cdsp, only) {}

bool
APVCorrHitTime::process_hit( EventID
                           , DetID_t did
                           , APVCluster & clus ) {
	
	//std::cout << "New cluster in " << naming()[did] << " of size " << clus.size() << " is here!" << std::endl;

	double x(0), y(0), xy(0), x2y(0), x2(0), x3(0), x4(0);
	double a1(0), b1(0), a2(0), b2(0), c2(0);
	double detA2, detB2, detC2, det2(0);
	double n = clus.size();

	for ( auto hit : clus ) {
		
		x   += 1;
		y   += (*hit.second).time;
		xy  += hit.first * (*hit.second).time;
		x2y += pow( hit.first, 2) * (*hit.second).time;
		x2  += pow( hit.first, 2);
		x3  += pow( hit.first, 3);
		x4  += pow( hit.first, 4);
		
		#if 1		
		std::cout << "Hit on wire: " << hit.first << " * "
				  << "with charge: " << (*hit.second).maxCharge << " * "
				  << "and time: " << (*hit.second).time << std::endl;
		#endif
	}
	
	a1 = ( x * y - xy * n ) / ( pow( x2, 2 ) - n * x2 );
		
	b1 = ( y - a1 * x ) / n;
	
	std::cout << "Event in detector " << naming()[did]
	          << " with angle: " << atan(a1) << std::endl;
	
	
	#if 0
	
	det2 = x2*(x2*x2 - x*x3) - x*(x3*x2 - x*x4) + n*(x3*x3 - x4*x2);
	
	detA2 = x * (x*x2y - xy*x2) - n * (x2*x2y -xy*x3) + y * (x2*x2 - x*x3);
	
	detB2 = x2 * ( xy*x2 - x*x2y ) - y * ( x3*x2 - x*x4 ) + n * ( x3*x2y - x4*xy );
	
	detC2 = x2 * ( x2*x2y - xy*x3 ) - x * ( x3*x2y - xy*x4 ) + y * ( x3*x3 - x2*x4 );
	
	a2 = detA2 / det2;
	b2 = detB2 / det2;
	c2 = detC2 / det2;
	
	if ( n > 10 ) {
		std::cout << "a2: " << a2 << std::endl;
		std::cout << "b2: " << b2 << std::endl;
		std::cout << "c2: " << c2 << std::endl;
	}
	#endif
	
    return true;
}   

}

REGISTER_HANDLER( APVCorrHitTime, banks, ch, cfg
                , "Calculates time incline and mean value for APV clusters" ) {
    return new handlers::APVCorrHitTime( ch 
									   , aux::retrieve_det_selection(cfg)
									   , banks.of<APVCluster>() );
}

}

