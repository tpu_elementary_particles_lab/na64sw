#include "new_data/printClusterInfo.hh"

namespace na64dp {
namespace handlers {

APVPrintClusterInfo::APVPrintClusterInfo( calib::Dispatcher & cdsp
                              , const std::string & only
                              ) : AbstractHitHandler<APVHit>(cdsp, only)
                                , TDirAdapter(cdsp) {}
                                
bool
APVPrintClusterInfo::process_hit( EventID
                           , DetID_t did
                           , APVHit & cHit ) {
    return true;
}

AbstractHandler::ProcRes
APVPrintClusterInfo::process_event(Event * evPtr ) {
	
	for (auto p : evPtr->apvClusters) {
		
		std::string stationName = TDirAdapter::naming()[DetID(p.first)];
		
		std::cout << stationName << std::endl;
		
		std::cout << "Cluster charge is " << p.second->charge << std::endl;
		std::cout << "Cluster time is " << p.second->time << std::endl;
		std::cout << "Cluster time error is " << p.second->timeError << std::endl;
		
	}
	
	return kOk;
}

}


REGISTER_HANDLER( APVPrintClusterInfo, banks, ch, cfg
                , "Prints info about APV detector clusters" ) {
    return new handlers::APVPrintClusterInfo(ch, aux::retrieve_det_selection(cfg));
}

}

