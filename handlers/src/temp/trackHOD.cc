#include "tracking/trackHOD.hh"

#include "na64detID/cellID.hh"

#ifdef GenFit_FOUND

// Tracking engine and track representatives
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <AbsMeasurement.h>
#include <DetPlane.h>
#include <PlanarMeasurement.h>
#include <SharedPlanePtr.h>

namespace na64dp {
namespace handlers {

static TrackHOD::DetPlacementEntry gPlacements[] = {
	
	// Placement for 2017. Very undisarable solution, itterator
	// might be preferable.
	// StationName, X, Y, Z, 
    // rotAngleX, rotAngleY, rotAngleZ, 
    // resolution
	
	#if 0
    {"HOD0X0-0-29", -32.9,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-1-29", -33.1,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-2-29", -33.3,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-3-29", -33.5,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-4-29", -33.7,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-5-29", -33.9,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-6-29", -34.1,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-7-29", -34.3,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-8-29", -34.5,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-9-29", -34.7,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-10-29", -34.9,   0,  -189.8,  0,  0,   0,  0.3},
    {"HOD0X0-11-29", -35.1,   0,  -189.8,  0,  0,   0,  0.3},
    {"HOD0X0-12-29", -35.3,   0,  -189.8,  0,  0,   0,  0.3},
    {"HOD0X0-13-29", -35.5,   0,  -189.8,  0,  0,   0,  0.3},
    {"HOD0X0-14-29", -35.7,   0,  -189.8,  0,  0,   0,  0.3},
    
    {"HOD0Y0-0-30", -34.3,    1.4,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-1-30", -34.3,    1.2,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-2-30", -34.3,    1.0,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-3-30", -34.3,    0.8,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-4-30", -34.3,    0.6,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-5-30", -34.3,    0.4,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-6-30", -34.3,    0.2,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-7-30", -34.3,    0.0,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-8-30", -34.3,   -0.2,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-9-30", -34.3,   -0.4,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-10-30", -34.3,  -0.6,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-11-30", -34.3,  -0.8,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-12-30", -34.3,  -1.0,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-13-30", -34.3,  -1.2,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-14-30", -34.3,  -1.4,  -189.81,  0,  0,   0,   0.3},
    
    {"HOD1X0-0-29", -35.6, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-1-29", -35.8, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-2-29", -36.0, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-3-29", -36.2, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-4-29", -36.4, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-5-29", -36.6, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-6-29", -36.8, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-7-29", -37.0, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-8-29", -37.2, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-9-29", -37.4, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-10-29", -37.6, 0,   -67.5,  0,  0,   0,  0.3},
    {"HOD1X0-11-29", -37.8, 0,   -67.5,  0,  0,   0,  0.3},
    {"HOD1X0-12-29", -38.0, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-13-29", -38.2, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-14-29", -38.4, 0,   -67.5,  0,  0,   0,   0.3},
    
    {"HOD1Y0-0-30", -37.0, -1.4,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-1-30", -37.0, -1.2,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-2-30", -37.0, -1.0,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-3-30", -37.0, -0.8,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-4-30", -37.0, -0.6,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-5-30", -37.0, -0.4,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-6-30", -37.0, -0.2,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-7-30", -37.0,    0,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-8-30", -37.0,  0.2,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-9-30", -37.0,  0.4,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-10-30", -37.0, 0.6,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-11-30", -37.0, 0.8,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-12-30", -37.0, 1.0,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-13-30", -37.0, 1.2,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-14-30", -37.0, 1.4,   -67.51,  0,  0,   0,   0.3},
    #endif
    
    {"HOD0X0-0-29", -34.3, 0, -189.8, 0, 0, 0, 2.8, 2.8, 0.3, 15},
    {"HOD0Y0-0-30", -34.3, 0, -189.8, 0, 0, 0, 2.8, 2.8, 0.3, 15},
    {"HOD1X0-0-29", -37.0, 0,  -67.5, 0, 0, 0, 2.8, 2.8, 0.3, 15},
    {"HOD1Y0-0-30", -37.0, 0,  -67.5, 0, 0, 0, 2.8, 2.8, 0.3, 15},

};

TrackHOD::TrackHOD( calib::Dispatcher & ch
                  , const std::string & only
                  , ObjPool<TrackPoint> & obTP
                  , int threshold )
        : AbstractHitHandler<SADCHit>(ch, only)
        , _tpInserter(obTP)
        , _threshold(threshold) {
}

void
TrackHOD::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<SADCHit>::handle_update(nm);
    
    try {
        _hodKinID = nm.kin_id("HOD").second;
    } catch( std::exception & e ) {
        _log.error( "Couldn't find HOD kin ID in current name mappings (error: %s)."
                  , e.what() );
    }
    
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0
       ; i < sizeof(gPlacements)/sizeof(DetPlacementEntry)
       ; ++i ) {
		
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gPlacements[i].planeName);
        // impose placement entry into `_placements' map
        _placements.emplace(did, gPlacements + i);
    }
}

bool
TrackHOD::process_hit( EventID
					 , DetID_t did
                     , SADCHit & hit ) {
	
	// Process hodoscope hits
	
	DetID stationID(did);
	
	if ( _hodKinID == stationID.kin() ) {
		
		// Create multimap with unique detector (plane) identifier
		// and fill it with SADC hodoscope hits and corresponding number
		// of hits in detector plane
		
		// unset payload to get unique identifier for hodoscope
		// with multiply SADC chips attached
		stationID = _create_unique_det(stationID);

		auto idxIt = _hodoHits.find( stationID );
		if( _hodoHits.end() == idxIt ) {
			auto ir = _hodoHits.emplace( stationID, Hits() );
			assert( ir.second );
			idxIt = ir.first;
		}
		
		Hits & wireSADC = idxIt->second;
		
		if ( hit.maxValue >= _threshold ) {
			wireSADC.emplace( did
				, _current_event().sadcHits.pool().get_ref_of(hit));
		}
		
	}
							   
	return true;
}

TrackHOD::ProcRes
TrackHOD::process_event(Event * evPtr) {
	
	// Get reference track
    //Track & track = *(evPtr->tracks.begin()->second);
    
    auto rc = AbstractHitHandler<SADCHit>::process_event( evPtr );
    
    for( auto detEntry : _hodoHits ) {
		_insert_hod_hits( *evPtr
						, detEntry.first
						, detEntry.second );
    }
    
    _hodoHits.clear();
    
    return rc;
}

DetID
TrackHOD::_create_unique_det( DetID did ) {
	
	CellID cid(did.payload());
	cid.set_x(0);
	cid.set_y(0);
	did.payload(cid.cellID);
	
	return did;
}		

void
TrackHOD::_insert_hod_hits( Event & e
						  , DetID did
						  , Hits & rawHits ) {
	
	HODCluster c;
	std::vector<HODCluster> validClusters;
	
	for ( auto hit : rawHits) {
		
		// Decode DetID_t and get "strip number"
		DetID stationID(hit.first);
		CellID cID(stationID.payload());
		HODStripNo wireNo = cID.get_y();
	
		if ( _possibly_belongs_to_cluster(c, wireNo, (*hit.second) ) ) {
			
			c.emplace( wireNo , hit.second );
			
			continue;
		
		} else {
			if ( c.size() >= 2 ) {
				validClusters.push_back(c);
			}
			c.clear();
		}
	}
	if ( !c.empty() ) {
		if ( c.size() >= 2 ) {
			validClusters.push_back(c);
		}
		c.clear();
	}
	
	// Insert found clusters as genfit::TrackPoint into genfit::Track
	for ( auto clus : validClusters ) {
		
		TrackPoint tp;
	
		genfit::TrackPoint * genfitTP = new genfit::TrackPoint();
		
		int xProj(29), yProj(30);
		
		int planeId(0);
		int hitId(0);  
		
		TVectorD hitCoords(1);
		TMatrixDSym hitCov(1);	
		
		DetID_t stationDiD(did);
		
		const DetPlacementEntry * cPlacement = _placements[stationDiD];
		
		CellID cID(did.payload());
		HODProj proj = cID.get_z();
		
		if ( proj == xProj ) {
			hitCoords(0) = ((cPlacement->sizeX * _get_cluster_center(clus) / cPlacement->numOfWires) 
						 - (( cPlacement->sizeX )/2) );
			tp.lR[0] = hitCoords(0);
			tp.gR[0] = ( hitCoords(0) * cos( cPlacement->rotAngleZ ) ) + cPlacement->x;
			
		} else if ( proj == yProj ) {
			hitCoords(0) = ((cPlacement->sizeY * _get_cluster_center(clus) / cPlacement->numOfWires) 
						 - (( cPlacement->sizeY )/2) );
			tp.lR[1] = hitCoords(0);
			tp.gR[1] = ( hitCoords(0) * sin( cPlacement->rotAngleZ ) ) + cPlacement->y;
		}
		tp.lR[2] = 0;

		// Get detector resolution
		double resolution(cPlacement->resolution);
		hitCov(0, 0) = resolution*resolution;

		// Set strip detector planes
		TVector3 o_plane( cPlacement->x
						, cPlacement->y
						, cPlacement->z );
		TVector3 u_plane( cPlacement->sizeX, 0, 0 );
		TVector3 v_plane( 0, cPlacement->sizeY, 0 );

		// Now we have options that we likely have to discuss.
		// Here we see slightly bulky solution that allows to rotate
		// detector plane in all direction based on info from geometry.dat.
		// However, in most cases we have simple rotation around normal,
		// so we might prefer to use genfit::DetPlane::rotate(double angle)?

		if (cPlacement->rotAngleX != 0) {
			u_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
			v_plane.RotateX( TMath::Pi() * (cPlacement->rotAngleX) / 180 );
		}

		if (cPlacement->rotAngleY != 0) {
			u_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
			v_plane.RotateY( TMath::Pi() * (cPlacement->rotAngleY) / 180 );
		}
				
		if (cPlacement->rotAngleZ != 0) {
			u_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
			v_plane.RotateZ( TMath::Pi() * (cPlacement->rotAngleZ) / 180 );
		}

		genfit::SharedPlanePtr plane(new genfit::DetPlane( o_plane
														 , u_plane
														 , v_plane));
																 
		genfit::AbsMeasurement * measurement 
							   = new genfit::PlanarMeasurement( hitCoords
															  , hitCov
															  , did
															  , hitId
															  , nullptr );
				 
		static_cast<genfit::PlanarMeasurement*>(measurement)->setPlane( plane
																	  , planeId );
																	  
		if ( proj == yProj ) {
			static_cast<genfit::PlanarMeasurement*>(measurement)->setStripV();
		}
					
		genfitTP->addRawMeasurement( measurement );
		
		tp.genfitTrackPoint.reset( genfitTP );
		
		// TrackPoint coordinates 
		tp.gR[2] = cPlacement->z;
		
		for ( auto & it : clus ) {
			tp.charge += (*it.second).maxValue;
		}

		tp.station = did;
		
		*_tpInserter( e, did, naming() ) = tp;	
	}
	
	c.clear();
	validClusters.clear();
	
}

bool
TrackHOD::_possibly_belongs_to_cluster( const HODCluster & clus
									  , HODStripNo wNo
									  , const SADCHit & cHit ) const {
	if( clus.empty() ) {
        // empty cluster -- let the first hit to be affiliated to it
        return true;
    }
    assert( !clus.empty() );
    
    if ( clus.size() > 3 ) {
		return false;
	}
    
    const std::pair<HODStripNo, PoolRef<SADCHit> > & le = *(clus.rbegin());
    
    return 1 == wNo - le.first;
}

double
TrackHOD::_get_cluster_center( const HODCluster & cluster ) const {
    assert( !cluster.empty() );
    double clusterCenter = 0;
    double charge = 0;
    
    for( const auto & cHitPair : cluster ) {
        clusterCenter += cHitPair.first * (*cHitPair.second).maxValue;
        charge += (*cHitPair.second).maxValue;
    }
    clusterCenter /= charge;
    
    return clusterCenter;
}
}

REGISTER_HANDLER( TrackHOD, banks, ch, cfg
                , "Handler for HODoscope hits processing" ) {
    
    return new handlers::TrackHOD( ch
								 , aux::retrieve_det_selection(cfg)
								 , banks.of<TrackPoint>()
								 , cfg["threshold"].as<int>() );
}
}
#endif  // defined(GenFit_FOUND)
