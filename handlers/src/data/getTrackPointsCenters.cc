#include "data/getTrackPointsCenters.hh"
#include "na64detID/wireID.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {
namespace handlers {

void
GetTrackPointsCenters::_process_track_point( TrackPoint & tp ) {
    if( 2 == tp.clusterRefs.size() ) {
        // Note: we rely on count of clusters rather then on some internal
        // knwoledge of particular detector assembly. This assumption works
        // only for NA64 and only until the times when stations with >2 planes
        // will appear.

        // Write third local coordinate to be NaN
        tp.lR[2] = std::nan("0");
        // Obtain cluster references
        const std::pair<DetID_t, PoolRef<APVCluster> > & pA = *tp.clusterRefs.begin()
                                                     , & pB = *tp.clusterRefs.rbegin();
        assert( &pA != &pB );
        // Retain the wire ID signatures
        DetID didA(pA.first)
            , didB(pB.first);
        WireID wA(didA.payload())
             , wB(didB.payload());
        msg_debug( _log, "Retrieved A %#x, pos: %f, size: %zu"
                  , didA.id, pA.second->position, pA.second->size() );
        msg_debug( _log, "Retrieved B %#x, pos: %f, size: %zu"
                  , didB.id, pB.second->position, pB.second->size() );
        assert( !std::isnan(pA.second->position) );
        assert( !std::isnan(pB.second->position) );
        // Use wire ID signature to re-order the positions wrt the
        // rules: "X prcedes Y" and "U precedes V", copy the `position` value
        // from cluster reference to local coordinates (`lR`)
        if( WireID::kX == wA.proj() || WireID::kU == wA.proj() ) {
            tp.lR[0] = pA.second->position;
            tp.lR[1] = pB.second->position;
        } else {  // pA is kY or kV
            assert( ( WireID::kY == wA.proj() || WireID::kV == wA.proj() ) );
            tp.lR[0] = pB.second->position;
            tp.lR[1] = pA.second->position;
        }
    } else {
        // This might be a point for future elaboration in case NA64
        // will introduce redundancy within a single station
        NA64DP_RUNTIME_ERROR( "Can not derive local coordinates from"
                " track point constisting of number of clusters != 2." );
    }
}

AbstractHandler::ProcRes
GetTrackPointsCenters::process_event( Event * e ) {
    #if 0
    for( auto & trackPair : e->tracks ) {
        //TrackID_t tID = trackPair.first;
        Track & track = *(trackPair.second);
        for( PoolRef<TrackPoint> & trackPoint : track ) {
            _process_track_point(*trackPoint);
        }
    }
    #endif
    
    for (auto p : e->trackPoints) {
		_process_track_point( *p.second );
	}
    return kOk;
}

}

REGISTER_HANDLER( GetTrackPointsCenters
                , banks, ch, cfg
                , "Obtains clusters ceters information" ) {
    return new handlers::GetTrackPointsCenters();
}

}

