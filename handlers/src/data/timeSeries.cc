#include "data/timeSeries.hh"
#include <gsl/gsl_statistics.h>

namespace na64dp {
namespace handlers {

void
TimeSeriesTracker::_finalize_entry( bool removeOutliers ) {
    if( _samples.empty() ) return;
    DistributionParameters en;
    en.evStart = _firstEvent;
    en.evEnd = _lastEvent;
    en.timeStart = _firstEventTime;
    en.timeEnd = _lastEventTime;
    extract_distribution_parameters( _samples, removeOutliers, en );
    _entries.push_back(en);
    // For half dropping mode, we have to clear only half
    if( kHalfOverlap != break_type() ) {
        _samples.clear();
    } else {
        _samples.erase( _samples.begin(), _samples.begin() + _samples.size()/2 );
    }
    _reset_events_ids();
}

void
TimeSeriesTracker::_reset_events_ids() {
    // todo: rewrite once na64ee will make it easier...
    _firstEvent = 0;
    _lastEvent = 0;
}

void
TimeSeriesTracker::extract_distribution_parameters( std::vector<double> & samples
                                                  , bool removeOutliers
                                                  , TimeSeriesTracker::DistributionParameters & en ) {
    en.outliers.clear();
    en.nSamples = samples.size();
    en.mean = gsl_stats_mean( samples.data(), 1, samples.size() );
    en.stddev = gsl_stats_sd_m( samples.data(), 1, samples.size(), en.mean );
    en.autocorr = gsl_stats_lag1_autocorrelation_m( samples.data(), 1, samples.size()
                                                  , en.mean );
    // Fill outliers / remove outliers from samples
    std::vector<double> * regularSamples = nullptr;
    if( removeOutliers ) {
        regularSamples = new std::vector<double>();
        regularSamples->reserve( samples.size() );
    }
    for( auto sample : samples ) {
        if( fabs(en.mean - sample) > 3*en.stddev ) {
            en.outliers.push_back(sample);
            continue;
        }
        if( removeOutliers ) {
            assert(regularSamples);
            regularSamples->push_back(sample);
        }
    }
    std::vector<double> & s = removeOutliers ? *regularSamples : samples;
    std::sort( s.begin(), s.end() );
    if( removeOutliers ) {
        en.mean = gsl_stats_mean( s.data(), 1, s.size() );
        en.stddev = gsl_stats_variance_m( s.data(), 1, s.size(), en.mean );
    }
    en.skewness = gsl_stats_skew_m_sd( s.data(), 1, s.size()
                                     , en.mean, en.stddev );
    en.kurtosis = gsl_stats_kurtosis_m_sd( s.data(), 1, s.size()
                                         , en.mean, en.stddev );
    en.median = gsl_stats_median_from_sorted_data( s.data(), 1, s.size() );
    en.lowQ = gsl_stats_quantile_from_sorted_data( s.data(), 1, s.size()
                                                 , .25 );
    en.upQ  = gsl_stats_quantile_from_sorted_data( s.data(), 1, s.size()
                                                 , .75 );
    if( regularSamples ) {
        delete regularSamples;
    }
}

void
TimeSeriesTracker::account_event_no( EventID eid
                                   , const std::pair<time_t, uint32_t> & evTime ) {
    _lastEvent = eid;
    _lastEventTime = evTime;
    if( 0 == _firstEvent.run_no() && 0 == _firstEvent.spill_no() ) {
        _firstEvent = eid;
        _firstEventTime = evTime;
        return;
    }
    if((eid.run_no() != _lastEvent.run_no()
     || eid.spill_no() != _lastEvent.spill_no()) ) {
        // Finalize by spill number condition
        _finalize_entry(!_accountOutliers);
        return;
    }
    if( kPeriodic == break_type() ) {
        // Find interval length and compare with first event time
        if( evTime.first - _firstEventTime.first >= _period.first
         && evTime.second - _firstEventTime.second >= _period.second ) {
            _finalize_entry(!_accountOutliers);
            return;
        }
    }
}

void
TimeSeriesTracker::account_sample(double sample) {
    _samples.push_back(sample);
    if( ( kFixedWindowLength == break_type()
       || kHalfOverlap == break_type() ) && _winSize == _samples.size() ) {
        // Finalize by fixed width conditions
        _finalize_entry(!_accountOutliers);
    }
}

TimeSeriesTracker::BreakType
TimeSeriesTracker::break_type_by_str( const std::string & name ) {
    if( name == "fixed" ) {
        return kFixedWindowLength;
    } else if( name == "half" ) {
        return kHalfOverlap;
    } else if( name == "spill" ) {
        return kBySpill;
    } else if( name == "periodic" ) {
        return kPeriodic;
    }
    NA64DP_RUNTIME_ERROR( "Unknown break type provided: \"%s\"."
                        , name.c_str() );
}

std::ostream &
operator<<(std::ostream & os, const TimeSeriesTracker::DistributionParameters & ps) {
    // column 1, 2: event ID for start and end
    os << ps.evStart << ",";
    os << ps.evEnd << ",";
    // column 3, 4 -- sec, usec of first event
    os << ps.timeStart.first << "," << ps.timeStart.second << ",";
    // column 5, 6 -- sec, usec of last event
    os << ps.timeEnd.first << "," << ps.timeEnd.second << ",";
    os << ps.nSamples  // column 7 -- number of samples
       << "," << ps.mean  // column 8 -- mean
       << "," << ps.stddev  // column 9 -- standard deviation
       << "," << ps.skewness  // column 10 -- skewness
       << "," << ps.kurtosis  // column 11 -- curtosis
       << "," << ps.median  // column 12 -- median (50% percentile)
       << "," << ps.autocorr  // column 13 -- autocorrelation
       << "," << ps.lowQ  // column 14 -- low quartile (25% percentile)
       << "," << ps.upQ   // column 15 -- upper quartile (75% percentile)
       ;
    // Rest columns are for outliers
    for( auto outl : ps.outliers ) {
        os << "," << outl;
    }
    return os;
}

}  // namespace ::na64dp::handlers

REGISTER_HANDLER( SADCTimeSeries
                , banks, ch, cfg
                , "Collects value distributions over time series for SADC hits" ) {
    std::pair<time_t, uint32_t> timeWindow = {0, 0};  // TODO
    return new handlers::TimeSeries<SADCHit>( ch
            , aux::retrieve_det_selection(cfg)
            , utils::value_getter<SADCHit>(cfg["value"].as<std::string>())
            , cfg["histName"].as<std::string>()
            , handlers::TimeSeriesTracker::break_type_by_str(cfg["breakBy"].as<std::string>())
            , cfg["nSamples"] ? cfg["nSamples"].as<size_t>() : 0
            , timeWindow
            , cfg["trimOutliers"] ? (! cfg["trimOutliers"].as<bool>()) : false
            );
}

#if 0
REGISTER_HANDLER( APVTimeSeries
                , banks, ch, cfg
                , "Collects value distributions over time series for SADC hits" ) {
    return new handlers::TimeSeries<APVHit>( ch
                                           , aux::retrieve_det_selection(cfg)
                                           , utils::value_getter<APVHit>(cfg["value"].as<std::string>())
                                           , cfg["histName"].as<std::string>()
                                           , cfg["histDescr"].as<std::string>()
                                           , cfg["nBins"].as<int>()
                                           , cfg["range"][0].as<double>()
                                           , cfg["range"][1].as<double>()
                                           );
}

REGISTER_HANDLER( APVClusterTimeSeries
                , banks, ch, cfg
                , "Collects value distributions over time series for APV clusters" ) {
    return new handlers::TimeSeries<APVCluster>( ch
                                               , aux::retrieve_det_selection(cfg)
                                               , utils::value_getter<APVCluster>(cfg["value"].as<std::string>())
                                               , cfg["histName"].as<std::string>()
                                               , cfg["histDescr"].as<std::string>()
                                               , cfg["nBins"].as<int>()
                                               , cfg["range"][0].as<double>()
                                               , cfg["range"][1].as<double>()
    );
}
#endif

}

