#include "data/checkHitmapCoverage.hh"

namespace na64dp {

REGISTER_HANDLER( CheckHitsCoverageSADC
                , banks, ch, cfg
                , "Check SADC hits coverage: main map vs bank" ) {
    return new handlers::CheckHitmapCoverage<SADCHit>( banks );
}

}

