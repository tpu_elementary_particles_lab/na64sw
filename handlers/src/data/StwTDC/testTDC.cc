#include "data/StwTDC/testTDC.hh"

#include "na64detID/wireID.hh"

namespace na64dp {
namespace handlers {

TestTDC::TestTDC( calib::Dispatcher & ch
                , const std::string & only
				) : AbstractHitHandler<StwTDCHit>(ch, only) {}

bool
TestTDC::process_hit( EventID
					, DetID_t did
                    , StwTDCHit & hit ) {
	
	DetID stationID(did);
	stationID.unset_payload();
	
	std::map<std::string, std::string> substDict;
	naming().append_subst_dict_for( stationID, substDict );
	std::string station = util::str_subst( "{kin}{statNum}", substDict, true );
	
	#if 1
	std::cout << naming()[did] << ", "
			  << "wire number: " << hit.rawData.wireNo << " , "
			  << "hit time: "    << hit.rawData.time << std::endl;
	#endif
			   
	return true;
}

TestTDC::ProcRes
TestTDC::process_event(Event * evPtr) {
        
    AbstractHitHandler<StwTDCHit>::process_event( evPtr );
    
    return kOk;
}

}

REGISTER_HANDLER( TestTDC, banks, ch, yamlNode
                , "Handler for testing Straw detectors" ) {
    
    return new handlers::TestTDC( ch
								, aux::retrieve_det_selection(yamlNode) );
};
}
