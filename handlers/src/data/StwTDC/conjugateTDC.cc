#include "data/StwTDC/conjugateTDC.hh"

#include "na64detID/wireID.hh"

namespace na64dp {
namespace handlers {

StwTDCConjugate::StwTDCConjugate( calib::Dispatcher & ch
                                , const std::string & only
                                , ObjPool<TrackPoint> & obTP
                                , float timeWindow ) 
                                : AbstractHitHandler<StwTDCHit>(ch, only)
                                , _tpInserter(obTP)
                                , _timeWindow(timeWindow) {}

static StwTDCConjugate::DetPlacementEntry gPlacements[] = {
	
	// Placement for 2018.
	// StationName, X, Y, Z, 
    // rotAngleX, rotAngleY, rotAngleZ, 
    // resolution

    {"ST6", -32.7,   0,  -334.4,  0,  0,   0,   20,   20,   0.1,   64},
    {"ST4", -35,   0,  -186.6,  0,  0,   0,   20,   20,   0.1,   64},
    {"ST5", -38,   -2.5,  - 73.6,  0,  0,   0,   20,   20,   0.1,   64},

    
};

void
StwTDCConjugate::handle_update( const nameutils::DetectorNaming & nm ) {
	AbstractHitHandler<StwTDCHit>::handle_update(nm);
    
    try {
		_strawKinID = nm.kin_id("ST").second;
    } catch( std::exception & e ) {
        _log.error( "Couldn't find ST kin ID in current name mappings (error: %s)."
                  , e.what() );
    }
    
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0
       ; i < sizeof(gPlacements)/sizeof(DetPlacementEntry)
       ; ++i ) {
		
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gPlacements[i].planeName);
        // impose placement entry into `_placements' map
        _placements.emplace(did, gPlacements + i);
    }
}

bool
StwTDCConjugate::process_hit( EventID
	         				, DetID_t did
                            , StwTDCHit & hit ) {
	
	// unset payload but we want to keep original did
	DetID stationID(did);
		
	if ( _strawKinID == stationID.kin() ) {
		
		// Create multimap with unique detector (plane) identifier
		// and fill it with NA64TDC straw hits and corresponding number
		// of hits in detector plane
		
		// unset payload to get unique identifier for straw	
		stationID.unset_payload();
		
		auto idxIt = _stwHits.find( stationID );
		if( _stwHits.end() == idxIt ) {
			auto ir = _stwHits.emplace( stationID, StwHits() );
			assert( ir.second );
			idxIt = ir.first;
		}
		
		StwHits & wireStw = idxIt->second;
		
		wireStw.emplace( did
			, _current_event().stwtdcHits.pool().get_ref_of(hit));
	}
	
	return true;
}


StwTDCConjugate::ProcRes
StwTDCConjugate::process_event(Event * evPtr) {
        
    AbstractHitHandler<StwTDCHit>::process_event( evPtr );

    for ( auto & hit : _stwHits ) {
		_conjugate_hits( hit.first, hit.second );
	}
	
	_create_track_point(*evPtr);
	
	#if 0
	for ( auto & hit : _accountedHits ) {
		
		std::map<std::string, std::string> substDict;
		naming().append_subst_dict_for( hit.first, substDict );
		std::string station = utils::str_subst( "{kin}{statNum}", substDict, true );
		
		std::cout << station << std::endl;
		std::cout << (*hit.second.first).rawData.wireNo << std::endl;
		std::cout << (*hit.second.second).rawData.wireNo << std::endl;
	}
	#endif
	
    _accountedHits.clear();
    _stwHits.clear();
    
    return kOk;
}

void
StwTDCConjugate::_conjugate_hits( DetID_t did, StwHits & hits ) {
	
	std::vector<PoolRef<StwTDCHit>> xHits, yHits;
	
	for ( auto & hit : hits ) {
		
		DetID station(hit.first);
		
		// Iterate over hits in station and sort it relative to planes
		if ( WireID::proj_label( WireID(station.payload()).proj() ) == 'X' ) {
			xHits.push_back(hit.second);
		} else if ( WireID::proj_label( WireID(station.payload()).proj() ) == 'Y' ) {
			yHits.push_back(hit.second);
		} else {
			NA64DP_RUNTIME_ERROR( "Straw hit doesn't belong to X or Y plane" );
		}
    }
	
	for ( auto & x : xHits ) {
		for ( auto & y : yHits ) {
			// Ignore hits that are not in time window
			if ( std::fabs((*x).rawData.time - (*y).rawData.time) > _timeWindow ) {
				continue;
			} else {
				_accountedHits.emplace( did, std::make_pair(x, y) );
			}
		}
	}
		
	xHits.clear();
	yHits.clear();
						
}

void
StwTDCConjugate::_create_track_point(Event & evPtr) {

	for ( auto & cPair : _accountedHits ) {
		
		TrackPoint tp;
		
		const DetPlacementEntry * cPlacement = _placements[cPair.first];
		
		const double angle = cPlacement->rotAngleZ;
		
		const StwWireNo wireX = (*cPair.second.first).rawData.wireNo;
		const StwWireNo wireY = (*cPair.second.second).rawData.wireNo;
			
		tp.lR[0] =((cPlacement->sizeX * wireX / (cPlacement->numOfWires / 2)) - (( cPlacement->sizeX )) ) + 1.2;
					
		tp.lR[1] = ((cPlacement->sizeY * wireY / (cPlacement->numOfWires / 2)) - (( cPlacement->sizeY )) ) + 1.2;
				 
		tp.gR[0] = ( tp.lR[0] * cos( angle * M_PI / 180.0 ) - tp.lR[1] * sin ( angle * M_PI / 180.0 ) ) + cPlacement->x;
					 
		tp.gR[1] = ( tp.lR[1] * cos( angle * M_PI / 180.0 ) + tp.lR[0] * sin ( angle * M_PI / 180.0 ) ) + cPlacement->y;
		
		if ( (*cPair.second.first).rawData.wireNo % 2 ) {
			tp.lR[2] = 0;		
			tp.gR[2] = cPlacement->z;
		} else {
			tp.lR[2] = 6;		
			tp.gR[2] = (cPlacement->z + 6);
		}
	
		tp.charge += 1;
		
		tp.station = cPair.first;
		
		*_tpInserter( evPtr, tp.station, naming() ) = tp;
	}

}

}

REGISTER_HANDLER( StwTDCConjugate, banks, ch, yamlNode
                , "Handler for testing Straw detectors" ) {
    
    return new handlers::StwTDCConjugate( ch
								, aux::retrieve_det_selection(yamlNode)
								, banks.of<TrackPoint>()
								, yamlNode["timeWindow"].as<float>() );
};
}
