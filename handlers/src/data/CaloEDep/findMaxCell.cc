#include "data/CaloEDep/findMaxCell.hh"

namespace na64dp {
namespace handlers {

CaloEDepMaxCell::CaloEDepMaxCell( calib::Dispatcher & ch
                                , const std::string & only ) 
		: AbstractHitHandler<CaloEDep>(ch, only) {
	
	ch.subscribe<nameutils::DetectorNaming>(*this, "default");			

}

void
CaloEDepMaxCell::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<CaloEDep>::handle_update(nm);

    _names = &nm;
    _namingCache.sadcChipCode = nm.chip_id("SADC");
    _namingCache.ecalKinCode = nm.kin_id("ECAL").second;
    _namingCache.hcalKinCode = nm.kin_id("HCAL").second;
}

bool
CaloEDepMaxCell::process_hit( EventID
			     		    , DetID_t did
                            , CaloEDep & hit ) {
	
	// TODO: Incomplete handler
	
	DetID stationID(did);
	
	if ( _namingCache.ecalKinCode == stationID.kin() ) {
		_current_event().ecalEdep += hit.eDepMeV;
	}

	if ( _namingCache.hcalKinCode == stationID.kin() ) {
		_current_event().hcalEdep += hit.eDepMeV;
	}
	
	return true;
}



}

REGISTER_HANDLER( CaloEDepMaxCell, banks, ch, yamlNode
                , "Handler to find sample with highest energy deposition" ) {
    
    return new handlers::CaloEDepMaxCell( ch
								        , aux::retrieve_det_selection(yamlNode) );
};
}
