#include "data/write.hh"
#include "na64util/str-fmt.hh"

# include <fcntl.h>

namespace na64dp {
namespace handlers {

const size_t EventWrite::singleEventBufferSize = 1024*1024;

EventWrite::EventWrite( int fd
                      , size_t bufferSize ) : _bufferSize(bufferSize)
                                                    , _file(fd) {
    // Check that file descriptor is opened
    if( fcntl( _file, F_GETFD ) == -1 || errno == EBADF ) {
        NA64DP_RUNTIME_ERROR( "Bad file descriptor provided to"
                " serialization handler." );
    }
    assert(singleEventBufferSize > 0);
    _singleEventBuffer = new char [singleEventBufferSize];
    assert(_bufferSize > 0);
    _eventsBuffer = _eventsBufferPosition = new char [_bufferSize + sizeof(size_t)];
    assert(_bufferSize > singleEventBufferSize);
    _file = fd;
}

EventWrite::~EventWrite() {
    delete [] _singleEventBuffer;
    delete [] _eventsBuffer;
    utils::io::release_fd(_file);
}

void
EventWrite::_dump_events_buffer() {
    size_t chunkSize = _eventsBufferPosition - _eventsBuffer;
    ssize_t wr;
    wr = write( _file, &chunkSize, sizeof(chunkSize) );
    if( sizeof(chunkSize) != wr ) {
        NA64DP_RUNTIME_ERROR(
                std::string("Unable to write chunk descriptor: ") + strerror(errno) );
    }
    wr = write( _file, _eventsBuffer, chunkSize );
    if( (ssize_t) chunkSize != wr ) {
        NA64DP_RUNTIME_ERROR(
                std::string("Unable to write chunk: ") + strerror(errno) );
    }
    _eventsBufferPosition = _eventsBuffer;
}

bool
EventWrite::process_event( Event * event ) {
    // Serialize current event to single events buffer
    size_t evSize = event_serialize( _singleEventBuffer
                                   , event );
    // If current event will exceed the overall buffer size, dump it
    if( _bufferSize < (_eventsBufferPosition - _eventsBuffer) + evSize ) {
        _dump_events_buffer();
    }
    // Write event from single event buffer to buffer by current position
    memcpy( _eventsBufferPosition, _singleEventBuffer, evSize );
    _eventsBufferPosition += evSize;
    return true;
}

void
EventWrite::finalize() {
    _dump_events_buffer();
    close( _file );
}

}

REGISTER_HANDLER( EventWrite, banks, ch, cfg
                , "Serializes event and write output to the given stream." ) {
    int fd = utils::io::acquire_fd_by_str(cfg["file"].as<std::string>().c_str());
    return new EventWrite( fd, cfg["bufferSize"] ? cfg["bufferSize"].as<size_t>()
                                                          : 16*1024*1024 );
}

}
