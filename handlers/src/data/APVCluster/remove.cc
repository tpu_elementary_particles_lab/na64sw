#include "data/APVCluster/remove.hh"

#include <algorithm>

namespace na64dp {
namespace handlers {

APVRemoveClusters::APVRemoveClusters( EvFieldTraits<APVCluster>::ValueGetter getter
                                    , int nLeave
                                    , bool ascending ) : _getter(getter)
                                                       , _nLeave(nLeave)
                                                       , _ascending(ascending) {
}

AbstractHandler::ProcRes
APVRemoveClusters::process_event(Event * e) {
    if(e->apvClusters.empty())
        return kOk;  // no clusters, nothing to process within an event

    // cluster object indexed by detector ID, will contain iterators sorted by
    // some value
    std::map< DetID_t
            , std::multimap<double, APVClustersIndex::Parent::iterator> > sortedClusters;
    // iterate over all the clusters, filling sorted clusters index
    for( auto it = e->apvClusters.begin()
       ; e->apvClusters.end() != it
       ; ++it ) {
        DetID_t did = it.offsetIt->first;
        const APVCluster & c = *(*it).second;
        // get the sorted clusters entry for certain plane
        auto entryIt = sortedClusters.find(did);
        if( sortedClusters.end() != entryIt ) {
            continue;  // plane is already been accounted
        }
        // plane has nto been accounted -- use equal range to get all the
        // clusters for certain plane
        auto planeClusters = e->apvClusters.container().equal_range(did);
        std::multimap<double, APVClustersIndex::Parent::iterator> sortedByValue;
        //sortedByValue.reserve( std::distance( planeClusters.first
        //                                    , planeClusters.second ) );
        for( auto iit = planeClusters.first
           ; iit != planeClusters.second
           ; ++ iit ) {
            const double v = _getter( c );
            sortedByValue.emplace(v, iit);
        }
        sortedClusters.insert( std::make_pair(did, sortedByValue) );
    }
    // remove all clusters except last few for each plane
    // note that std guarantees all the remaining iterators remain valid on
    // element erase
    if( _ascending ) {
        for( auto entryIt = sortedClusters.begin()
           ; sortedClusters.end() != entryIt
           ; ++entryIt ) {
            // determine whether there are clusters to be removed actually; if
            // number of sorted clusters is le number to leave, we can not actually
            // remove anything
            if( entryIt->second.size() <= _nLeave ) continue;
            auto it = entryIt->second.rbegin();
            // advance reverse iterator to retain some leading clusters (by
            // previous checks it is guaranteed that there are enough elements
            // to afvance)
            std::advance( it, _nLeave );
            for( ; entryIt->second.rend() != it; ++it ) {
                e->apvClusters.container().erase( it->second );
            }
        }
    } else {
        for( auto entryIt = sortedClusters.begin()
           ; sortedClusters.end() != entryIt
           ; ++entryIt ) {
            if( entryIt->second.size() <= _nLeave ) continue;
            auto it = entryIt->second.begin();
            std::advance( it, _nLeave );
            for( ; entryIt->second.end() != it; ++it ) {
                e->apvClusters.container().erase( it->second );
            }
        }
    }
    
    return kOk;
}

}

REGISTER_HANDLER( APVRemoveClusters, banks, ch, cfg
                , "Retains 1D clusters from APV detector wrt some criteria" ) {
    return new handlers::APVRemoveClusters(
                utils::value_getter<APVCluster>(cfg["sortBy"].as<std::string>()),
                cfg["leaveN"].as<int>(),
                cfg["ascending"] ? cfg["ascending"].as<bool>() : true
            );
}

}

