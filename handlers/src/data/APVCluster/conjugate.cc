#include "data/APVCluster/conjugate.hh"
#include "na64detID/TBName.hh"
#include "na64detID/wireID.hh"
#include "na64dp/abstractHitHandler.hh"
#include "na64util/str-fmt.hh"

#include <map>
#include <utility>

namespace na64dp {
namespace handlers {

APVConjugateClusters::APVConjugateClusters( calib::Dispatcher & dsp
                                          , const std::string & only
                                          , ObjPool<TrackPoint> & obTP
                                          ) : _tpInserter(obTP) {
    dsp.subscribe<nameutils::DetectorNaming>(*this, "default");
    #if 1
    if( ! only.empty() ) NA64DP_RUNTIME_ERROR( "Not yet implemented" );
    #endif
}

void
APVConjugateClusters::handle_update( const nameutils::DetectorNaming & nm ) {
    _names = &nm;
    _namingCache.apvChipCode = nm.chip_id("APV");
    _namingCache.gmKinCode = nm.kin_id("GM").second;
    _namingCache.mmKinCode = nm.kin_id("MM").second;
    assert( _namingCache.apvChipCode == nm.kin_id("GM").first );
    assert( _namingCache.apvChipCode == nm.kin_id("MM").first );
}

AbstractHandler::ProcRes
APVConjugateClusters::process_event( Event * ePtr ) {
    if( ePtr->apvClusters.empty() ) {
        // event carries no clusters
        return kOk;
    }

    typedef std::pair< size_t, size_t > ClusterPair;
    // Define a custom comparison, that ignores ordering
    // It is achieved by forcefully making the first element to be >=
    // than second within a pair during comparison (ptr that actually doesn't
    // matter)
    auto cmp = [](ClusterPair a, ClusterPair b) {
        if( a.first < a.second ) {
            std::swap(a.first, a.second);
        }
        if( b.first < b.second ) {
            std::swap(b.first, b.second);
        }
        return a < b;
    };

    // A set of cluster pairs that have been already taken into account
    std::set<ClusterPair, decltype(cmp)> accountedClusters(cmp);

    size_t nMissed = 0;

    for( auto thisIt = ePtr->apvClusters.container().begin()
       ; ePtr->apvClusters.container().end() != thisIt
       ; ++thisIt ) {
        // Refers to tuple (DetID_t, APVCluster), where DetID_t value is
        // plane identifier
        auto cPair = *thisIt;
        // Clusters joint to current
        std::set< std::pair<DetID_t, PoolRef<APVCluster>> > adjointClusters;

        // This section (till #endif) is apparently to be modified if more than
        // two conjugated planes will be present at the experiment
        # if 1
        // Get projection code from detector identifier payload
        DetID did(cPair.first);
        WireID wID(did.payload());
        DetID conjDid(did);
        // Get conjugated projection code and build conjugated detector ID.
        WireID::Projection conjProjCode
            = WireID::conjugated_projection_code(wID.proj());
        conjDid.payload(conjProjCode);
        assert( did != conjDid );

        // Note that find result in general does not correspond to a single
        // cluster. It may refer to none, one or to multiple clusters, so we
        // have to apply std::multimap::equal_range() here.
        auto conjER = ePtr->apvClusters.container().equal_range( conjDid );

        if( conjER.first == conjER.second ) {  // XXX
            assert(_names);
            const nameutils::DetectorNaming & nm = *_names;
            msg_debug( _log, "No conjugated clusters for detector %s, %c, %x"
                        " (conjugated %s, %c, %x)"
                    , nm[did].c_str()
                    , WireID::proj_label( WireID(did.payload()).proj() )
                    , did.payload()
                    , nm[conjDid].c_str()
                    , WireID::proj_label( WireID(conjDid.payload()).proj() )
                    , conjDid.payload()
                    );
            continue;
        }

        size_t thisClusterOID = cPair.second
             , conjClusterOID
             ;

        // For each cluster on conjugated plane, produce a corresponding track
        // point by adjoining it with current cluster
        for( auto conjIt = conjER.first
           ; conjER.second != conjIt
           ; ++ conjIt ) {
			   
			/* QuickFix. Allows us to have just a pair of X and Y hits
			 * with their corresponding planes in each TrackPoint. However,
			 * initial structure of TrackPoint contains 
			 * std::set < std::pair < DetID_t, Poolref < APVCluster > > >
			 * so, we use it here and clear adjointClusters each step, but it
			 * is desirable to redesign TrackPoint field to contain
			 * std::pair < std::pair < DetID_t, Poolref < APVCluster > > >
			 *///////////////////////////////////////////////////////////////
			    
			adjointClusters.clear();
			   
            conjClusterOID = conjIt->second;
            assert( conjClusterOID != thisClusterOID );
            assert(conjIt != ePtr->apvClusters.container().end());
            std::pair< size_t, size_t > clusPair( thisClusterOID, conjClusterOID );
            if( accountedClusters.end() != accountedClusters.find(clusPair) ) {
                // cluster had been already accounted (on previous iterations)
                continue;
            }
            PoolRef<APVCluster> thisCluster( ePtr->apvClusters.pool(), thisClusterOID )
                              , conjCluster( ePtr->apvClusters.pool(), conjClusterOID )
                              ;
            // Well, this assertions are questionable, but this is what we
            // expect in current implementation -- all 1D clusters have to
            // have their `position` field initialized
            assert( !std::isnan(thisCluster->position) );
            assert( !std::isnan(conjCluster->position) );

            //adjointClusters.emplace( thisIt->first, thisIt );
            //adjointClusters.emplace( conjIt->first, conjIt );
            adjointClusters.emplace( thisIt->first, thisCluster );
            adjointClusters.emplace( conjIt->first, conjCluster );

            // create and insert new track point
            did.unset_payload();  // drop information about projection plane entirely
            assert(did);
            
            {
                assert(_names);
                auto tpRef = _tpInserter( *ePtr, did, *_names );
                #if 1
                *tpRef = TrackPoint{ { std::nan("0"), std::nan("0"), std::nan("0")}  // lR
                                   , { std::nan("0"), std::nan("0"), std::nan("0")}  // gR
                                  , adjointClusters  // clusterRefs
                                  , did.id
                                  , std::nan("0")
                                  };
                #else
                *tpRef = TrackPoint();
                tpRef->clusterRefs = adjointClusters;
                tpRef->station = did.id;
                #endif                
            }
            
            // Add clusters to set of accounted clusters to prevent
            // repeatitive consideration
            accountedClusters.emplace( std::make_pair( thisClusterOID
                                                     , conjClusterOID ) );
        }
        if( adjointClusters.empty() ) {
            ++nMissed;
            continue;
        }
        # endif
    }

    return kOk;
}

}

REGISTER_HANDLER( APVConjugateClusters, banks, cdsp, cfg
                , "Emplaces 2D clusters entries using knowledge about"
                    " conjugated tracking detector planes" ) {
    return new handlers::APVConjugateClusters( cdsp
                                             , aux::retrieve_det_selection(cfg)
                                             , banks.of<TrackPoint>() );
}

}

