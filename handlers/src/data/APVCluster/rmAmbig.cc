#include "data/APVCluster/rmAmbig.hh"

namespace na64dp {
namespace handlers {

APVRmAmbigClusters::APVRmAmbigClusters( calib::Dispatcher & ch
                                      , const std::string & detSelection )
        : AbstractHitHandler<APVCluster>(ch, detSelection), TDirAdapter(ch) {}

bool
APVRmAmbigClusters::process_hit( EventID
                               , DetID_t did
                               , APVCluster & cluster ) {

    // Retrieve the index by detector ID
    auto idxIt = _idx.find( did );

    if( _idx.end() == idxIt ) {
		auto ir = _idx.emplace( did, ClustersByValue() );
        assert( ir.second );
        idxIt = ir.first;
    }
    ClustersByValue & clustersByChannel = idxIt->second;

    std::string stationName = TDirAdapter::naming()[did];
  
	//std::cout << stationName << std::endl;

	//std::cout << "Cluster time :" << cluster.time << std::endl;

	//std::cout << "Cluster timeError :" << cluster.timeError << std::endl;

   

    // Add cluster reference by DAQ channel

    for( auto hitPair : cluster ) {
        clustersByChannel.emplace( hitPair.second->rawData.wireNo
                 , _current_event().apvClusters.pool().get_ref_of(cluster) );
    }

    return true;
}

AbstractHandler::ProcRes
APVRmAmbigClusters::process_event( Event * ePtr ) {

    // Accumulate sorted clusters map
    auto rc = AbstractHitHandler<APVCluster>::process_event( ePtr );
    // For each detector record, discriminate ambiguous clusters
    for( auto detEntry : _idx ) {
        _collect_ambiguous_clusters( detEntry.first, detEntry.second );
    }

    // Build a reverse map to speed up cluster removal: cluster iterator by its
    // offset ID

    std::map<size_t, APVClustersIndex::Iterator> cIts;
    for( auto it = ePtr->apvClusters.begin()
       ; it != ePtr->apvClusters.end()
       ; ++it ) {
		cIts.emplace( (*it).second.offset_id(), it );

    }

    #if 1

    //std::cout << "xxx #best: " << _bestClusters.size()

	//		  << " vs " << ePtr->apvClusters.size()

	//		  << std::endl;  // XXX

    // Remove all except for "best" (on certain wire) clusters

    for( auto cPair : cIts ) {
		// curret cluster pool referece
		const size_t cOffset = cPair.first;
		//const PoolRef<APVCluster> & cPoolRef = (*cPair.second).second;
		// do not remove if in "best" clusters

        if( _bestClusters.end() != _bestClusters.find(cOffset)) continue;
        // resolve reverse and remove
        
		EvFieldTraits<APVCluster>::remove_from_event(
				  *ePtr
				, cPair.second
                , TDirAdapter::naming() );
    }

    #else

    for( auto cluster2remove : _falseClusters ) {
        auto it2rm = cIts.find( cluster2remove.offset_id() );
        assert( cIts.end() != it2rm );
        EvFieldTraits<APVCluster>::remove_from_event( *ePtr, it2rm->second
                , TDirAdapter::naming() );
    }

    #endif

    //std::cout << "xxx #left: " << ePtr->apvClusters.size()
	//		  << std::endl;  // XXX

    // Drop caches being valid only for current event
    _idx.clear();
    _falseClusters.clear();
    _bestClusters.clear();
    return rc;

}

void
APVRmAmbigClusters::_collect_ambiguous_clusters( DetID_t
                                              , ClustersByValue & clusters ) {

    for( auto it = clusters.begin(), next = clusters.upper_bound(it->first)    
       ; it != clusters.end()
       ; it = next ) {
			
        next = clusters.upper_bound(it->first);
        // channelNo = it->first
        if( 1 == std::distance(it, next) ) continue;  // single cluster
        // create index of clusters sorted by criteria
        std::map<double, PoolRef<APVCluster>> byValue;
        std::transform( it, next
                      , std::inserter(byValue, byValue.end())
                      , []( const std::pair<DetID_t, PoolRef<APVCluster>> & ce ) {

            return std::make_pair( ce.second->charge  // TODO: use configurable getter here
                                 , ce.second);
        } );

        #if 1

        // Remove all clusters except for largest ones
        //auto cluster2rm = byValue.rbegin();
        _bestClusters.insert( byValue.rbegin()->second.offset_id() );
        //while( ++cluster2rm != byValue.rend() ) {

		//	if( _bestClusters.end() == _bestClusters.find(cluster2rm->second) )
		//		_falseClusters.insert( cluster2rm->second );
        //}
        # else
        // ...
        # endif
    }
}

}

REGISTER_HANDLER( APVRmAmbigClusters, banks, ch, yamlNode
                , "Removes ambigiuos APV clusters from an event" ) {
    return new handlers::APVRmAmbigClusters( ch
                    , aux::retrieve_det_selection(yamlNode) );

}

}
