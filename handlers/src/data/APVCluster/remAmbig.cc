#include "data/APVCluster/remAmbig.hh"

namespace na64dp {
namespace handlers {


APVRemAmbigClusters::APVRemAmbigClusters( calib::Dispatcher & ch
                                        , const std::string & detSelection )
		: AbstractHitHandler<APVCluster>(ch, detSelection) {}

bool
APVRemAmbigClusters::process_hit( EventID
                                , DetID_t did
                                , APVCluster & cluster ) {

    // Retrieve the index by detector ID
    auto idxIt = _idx.find( did );
    if( _idx.end() == idxIt ) {
        auto ir = _idx.emplace( did, ClustersByWire() );
        assert( ir.second );
        idxIt = ir.first;
    }
    ClustersByWire & clustersByChannel = idxIt->second;
    
    // Add cluster reference by DAQ channel
    for( auto hitPair : cluster ) {
        clustersByChannel.emplace( hitPair.second->rawData.wireNo
                , _current_event().apvClusters.pool().get_ref_of(cluster) );
    }
    return true;
}

const APVRemAmbigClusters::BestClusters &
APVRemAmbigClusters::best_for(DetID_t did) const {
	auto it = _bestClusterIDs.find(did);
	assert( _bestClusterIDs.end() != it );
	return it->second;
}

APVRemAmbigClusters::BestClusters &
APVRemAmbigClusters::best_for(DetID_t did, bool create) {
	auto it = _bestClusterIDs.find(did);
	if( _bestClusterIDs.end() == it ) {
		if(create) {
			auto ir = _bestClusterIDs.emplace(did, BestClusters());
			assert( ir.second );
			it = ir.first;
		} else {
			NA64DP_RUNTIME_ERROR( "No bests for plane \"%s\"."
				, naming()[did].c_str() );
		}
	}
	return it->second;
}

AbstractHandler::ProcRes
APVRemAmbigClusters::process_event( Event * ePtr ) {

    // Accumulate sorted clusters map
    auto rc = AbstractHitHandler<APVCluster>::process_event( ePtr );
    // For each detector record, discriminate ambiguous clusters
    for( auto detEntry : _idx ) {
		_collect_ambiguous_clusters( detEntry.first, detEntry.second );
    }
	
    // Build a reverse map to speed up cluster removal: cluster iterator by its
    // offset ID
    std::map<size_t, APVClustersIndex::Iterator> cIts;
    for( auto it = ePtr->apvClusters.begin()
       ; it != ePtr->apvClusters.end()
       ; ++it ) {
           if( is_selective() && !matches((*it).first) ) continue;
        cIts.emplace( (*it).second.offset_id(), it );
    }

    // Remove all except for "best" (on certain wire) clusters
    for( auto cPair : cIts ) {
		// curret cluster pool referece
		const size_t cOffset = cPair.first;
		DetID_t planeID = (*cPair.second).first;
		//const PoolRef<APVCluster> & cPoolRef = (*cPair.second).second;
		// do not remove if in "best" clusters
		const auto & bests = best_for(planeID);
        if( bests.end() != bests.find(cOffset) ) continue;
       // std::cout << "  ..." << cOffset << " not in best" << std::endl;
        // resolve reverse and remove
		EvFieldTraits<APVCluster>::remove_from_event(
				  *ePtr
				, cPair.second
                , naming() );
    }  

    _idx.clear();
    _bestClusterIDs.clear();
    
    return rc;
 
}

void
APVRemAmbigClusters::_collect_ambiguous_clusters( DetID_t planeID
												, ClustersByWire & clusters ) {
	
	BestClusters & bests = best_for(planeID, true);
    
    for( auto it = clusters.begin(), next = clusters.upper_bound(it->first)    
       ; it != clusters.end()
       ; it = next ) {
				
        next = clusters.upper_bound(it->first);
        
        // channelNo = it->first
        //if( 1 == std::distance(it, next) ) continue;  // single cluster
        // create index of clusters sorted by criteria
        std::map<double, PoolRef<APVCluster>> byValue;
        # if 1
        std::transform( it, next
                      , std::inserter(byValue, byValue.end())
                      , []( const std::pair<DetID_t, PoolRef<APVCluster>> & ce ) {
            return std::make_pair( ce.second->charge  // TODO: use configurable getter here
                                 , ce.second);
        } );
        # else
        for( auto iit = it; iit != next; ++iit ) {
			auto ir = byValue.emplace( iit->second->charge, iit->second );
			if( ! ir.second && iit->second.offset_id() != ir.first->second.offset_id() ) {
				std::cout << "collision, charge =" << iit->second->charge
						  << ", detID=" << iit->first
						  << ", new cluster offset=" << iit->second.offset_id()
						  << ", exist. cluster offset=" << ir.first->second.offset_id()
						  << std::endl;
			}  // XXX
		}
        # endif
        
        bests.emplace( byValue.rbegin()->second.offset_id() );
    }	
}

}

REGISTER_HANDLER( APVRemAmbigClusters, banks, ch, yamlNode
                , "Removes ambigiuos APV clusters from an event" ) {
    return new handlers::APVRemAmbigClusters( ch
						, aux::retrieve_det_selection(yamlNode) );
}

}

