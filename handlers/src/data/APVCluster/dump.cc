#include "data/APVCluster/dump.hh"
#include "na64detID/wireID.hh"
#include "na64detID/TBName.hh"
#include "na64dp/abstractHitHandler.hh"

namespace na64dp {
namespace handlers {

APVClusterDump::APVClusterDump( calib::Dispatcher & cdsp
                              , const std::string & oFilePath
                              , const std::string &
                              , bool writeHits ) : _naming("default", cdsp)
                                                 , _os(oFilePath)
                                                 , _writeHits(writeHits)
                                                 {}

AbstractHandler::ProcRes
APVClusterDump::process_event( Event * e ) {
    for( auto trackPair : e->tracks ) {  // for each track within an event
        size_t nTrackPoint = 0;
        Track & track = *(trackPair.second);
        for( PoolRef<TrackPoint> tp : track ) {  // for each track point within a track
            ++nTrackPoint;
            DetID stationID(tp->station);
            for( const auto & clusterPair : tp->clusterRefs ) {  // for each cluster within a track point
                WireID wid( DetID(clusterPair.first).payload() );
                const APVCluster & c = *clusterPair.second;
                _os << e->id << ","  // event ID
                    << trackPair.first << ","  // track ID
                    //<< nCluster << "," // cluster number
                    << nTrackPoint << "," // track point number
                    << (*_naming)[clusterPair.first] << ","  // detector name
                    << WireID::proj_label( wid.proj() )  << "," // projection letter (X, U, etc)
                    << c.position << ","
                    << c.wPosition << ","
                    << c.charge << ","
                    << c.sparseness
                    ;
                if( _writeHits ) {
                    _os << "," << c.size();
                    for( auto hitPair : c ) {
                        _os << ","
                            << hitPair.first << ","  // phys wire ID
                            << hitPair.second->rawData.wireNo << ","  // DAQ wire ID
                            << hitPair.second->rawData.samples[0] << ","  // charge 1
                            << hitPair.second->rawData.samples[1] << ","  // charge 1
                            << hitPair.second->rawData.samples[2]        // charge 1
                            ;
                    }
                }
                _os << std::endl;
            }
        }
    }
    return kOk;
}

void
APVClusterDump::finalize() {
    _os.close();
}

}

REGISTER_HANDLER( APVClusterDump
                , banks, ch, cfg
                , "Writes CSV dump of cluster composing track points" ) {
    return new handlers::APVClusterDump( ch
                                       , cfg["outputFile"].as<std::string>()
                                       , aux::retrieve_det_selection(cfg)
                                       , cfg["writeHits"] ? cfg["writeHits"].as<bool>() : false
                                       );
}

}

