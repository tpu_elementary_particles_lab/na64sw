#include "data/SADC/directSum.hh"

namespace na64dp {
namespace handlers {

bool
SADCDirectSum::process_hit( EventID
                          , DetID_t
                          , SADCHit & cHit ){
    cHit.sum = 0;
    for( int i = 0; i < 32; ++i ){
        auto a = cHit.wave[i];
        if( _doZeroNegative ) {
            if( a < 0 ) a = 0;
        }
        cHit.sum += a / 2; 
    }
    return true;
}

}

REGISTER_HANDLER( SADCDirectSum, banks, ch, yamlNode
                , "Computes direct sum of SADC waveform" ){
    bool zeroNegatives = yamlNode["zeroNegativeValues"] ? yamlNode["zeroNegativeValues"].as<bool>()
                                                        : false;
    return new handlers::SADCDirectSum( ch
            , aux::retrieve_det_selection(yamlNode)
            , zeroNegatives );
}

}

