#include "data/SADC/findMaxSample.hh"

namespace na64dp {
namespace handlers {

bool
SADCFindMaxSample::process_hit( EventID
                              , DetID_t did
                              , SADCHit & hit ) {
    int maxSampleNo = 0;
    for( int i = 1; i < 32; ++i ) {
        if( hit.wave[i] > hit.wave[maxSampleNo] ) {
            maxSampleNo = i;
        }
    }
    hit.maxValue = hit.wave[maxSampleNo];
    hit.maxSample = maxSampleNo;
    return true;
}

}  // namespace ::na64dp::handlers


REGISTER_HANDLER( SADCFindMaxSample, banks, ch, cfg
                , "Locates maximum sample in SADC waveform by direct lookup"
                  " of each sample's amplitude" ) {
    return new handlers::SADCFindMaxSample( ch
                                          , aux::retrieve_det_selection(cfg)
                                          );
}

}



