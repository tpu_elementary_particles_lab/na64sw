#include "data/SADC/movStatPeds.hh"

#include <unistd.h>

namespace na64dp {
namespace handlers {

bool
SADCMovStatPeds::process_hit( EventID eventID
                            , DetID_t detID
                            , SADCHit & hit ) {
    // Call usual dynamic pedestals calculus by first N samples.
    SADCGetPedestalsByFront::process_hit( eventID, detID, hit );
    auto pedEntryIt = _stats.find(detID);
    if( _stats.end() == pedEntryIt ) {
        // No moving statistics instance exists for current detector ID --
        // create one using the constructor function
        auto ir =
            _stats.emplace( detID, MovStatEntry{
                    { _movStatEntryCtr( _movStatArgs )
                    , _movStatEntryCtr( _movStatArgs )
                    }
                } );
        assert(ir.second);
        pedEntryIt = ir.first;
        // Try to retrieve existing pedestals values to mitigate initial
        // shiver
        if( _useAvrgVals ) {
            calib::Handle<calib::Pedestals> & pedsHandle = *this;
            if( pedsHandle->find(detID) != pedsHandle->end() ) {
                #if 0
                const calib::MovStatPedestals & p
                                = pedEntryIt->second.bgnState
                                = calibs()[detID].get<calib::MovStatPedestals>();
                msg_debug( _log, "Setting initial sliding state mock for \"%s\":"
                        " mean=%e, sigma=%e, N=%d"
                        , calibs().naming()(detID).c_str()
                        , p.mean, p.stdDev, (int) p.n );
                #else
                NA64DP_RUNTIME_ERROR( "TODO" );
                #endif
            }
        }
    }
    auto stat = pedEntryIt->second.stat;
    // Compare calculated pedestals with mean +/- stddev to figure out
    // outliers. If this hit is an outlier, depending on setting, either
    // discriminate an entire event, or use windowed mean as pedestal instead
    // of calculated one.
    bool useMean = false;
    for( int i = 0; i < 2; ++i ) {
        if( _dropOutlierEventThreshold
         && stat[i]->is_outlier(hit.pedestals[i], _dropOutlierEventThreshold) ) {
            // otherwise, use mean
            useMean = true;
        }
    }
    if( useMean ) {
        hit.pedestals[0] = stat[0]->mean();
        hit.pedestals[1] = stat[1]->mean();
    } else {
        stat[0]->account(hit.pedestals[0]);
        stat[1]->account(hit.pedestals[1]);
    }
    return true;
}

static numerical::iMovStats *
plain_ctr( const SADCMovStatPeds::MovStatArgs & a ) {
    return new numerical::MovingStats<double>( a.winSize
                , a.excludeOutliers
                , a.recacheOutliers
                , a.recachingFrac
                );
}

static numerical::iMovStats *
klein_ctr( const SADCMovStatPeds::MovStatArgs & a ) {
    return new numerical::MovingStats<numerical::KleinScorer>( a.winSize
                , a.excludeOutliers
                , a.recacheOutliers
                , a.recachingFrac
                );
}

}  // namespace ::na64dp::handlers

REGISTER_HANDLER( SADCMovStatPeds, banks, ch, cfg
                , "calculate SADC pedestals by first N values (default is 4)"
                  " and compare it with windowed mean/stddev values to"
                  " drop outliers" ) {
    handlers::SADCMovStatPeds::MovStatArgs a;
    a.winSize = cfg["scorer"]["window"].as<size_t>();
    a.excludeOutliers = cfg["scorer"]["excludeOutliers"].as<double>();
    a.recacheOutliers = cfg["scorer"]["recacheOutliers"].as<double>();
    a.recachingFrac = cfg["scorer"]["recachingFrac"].as<double>();
    handlers::SADCMovStatPeds::MovStatCtr msctr;
    if( cfg["scorer"]["type"].as<std::string>() == "klein" ) {
        msctr = handlers::klein_ctr;
    } else if( cfg["scorer"]["type"].as<std::string>() == "plain" ) {
        msctr = handlers::plain_ctr;
    } else {
        NA64DP_RUNTIME_ERROR( "Wrong mov. stat. scorer type"
                " specification: \"%s\"."
                , cfg["scorer"]["type"].as<std::string>().c_str() );
    }

    return new handlers::SADCMovStatPeds( ch
                , aux::retrieve_det_selection(cfg)
                , a
                , msctr
                , cfg["nSamples"] ? cfg["nSamples"].as<int>() : 4
                , cfg["dropOutlierEvent"] ? cfg["dropOutlierEvent"].as<double>() : 0.
                , cfg["initWithAverages"] ? cfg["initWithAverages"].as<bool>() : false
                );
}

}

