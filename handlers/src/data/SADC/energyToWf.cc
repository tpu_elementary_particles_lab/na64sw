#include "data/SADC/energyToWf.hh"

namespace na64dp {
namespace handlers {

bool
EnergyToWf::process_hit( EventID eid
                       , DetID_t did_
                       , SADCHit & currentHit ) {
    throw std::runtime_error("TODO: energy to wf SADC hits handler");
}

}

REGISTER_HANDLER( EnergyToWf, banks, ch, yamlNode
                , "Generates a waveform based on their energy (reverse transform, MC)" ) {
    return new handlers::EnergyToWf(ch, aux::retrieve_det_selection(yamlNode));
}

}

