#include "data/SADC/subtractPedestals.hh"

namespace na64dp {
namespace handlers {

bool
SADCSubtractPedestals::process_hit( EventID
                                  , DetID_t detID
                                  , SADCHit & hit ) {
    if( std::isnan(hit.pedestals[0]) || std::isnan(hit.pedestals[1]) ) {
        if( _errorOnMissed )
            NA64DP_RUNTIME_ERROR( "Missed pedestal value for \"%s\""
                                , naming()[detID].c_str() );
        return true;
    }
    for(int i = 0; i < 32; ++i){
       hit.wave[i] -= hit.pedestals[i%2];
       if( _nullifyNegative && hit.wave[i] < 0 ) {
           hit.wave[i] = 0;
       }
    }
    return true;
}

}

REGISTER_HANDLER( SADCSubtractPedestals, banks, ch, yamlNode
                , "Subtract pedestal values from SADC waveforms." ) {
    return new handlers::SADCSubtractPedestals( ch
                , aux::retrieve_det_selection(yamlNode)
                , yamlNode["nullifyNegative"] ? yamlNode["nullifyNegative"].as<bool>() : false
                , yamlNode["errorOnMissed"] ? yamlNode["errorOnMissed"].as<bool>() : true
                );
}

}

