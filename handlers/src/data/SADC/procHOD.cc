#include "data/SADC/procHOD.hh"

#ifdef GenFit_FOUND

#include "na64detID/cellID.hh"

namespace na64dp {
namespace handlers {

static HODHitProcess::DetPlacementEntry gPlacements[] = {
	
	// Placement for 2017. Very undisarable solution, itterator
	// might be preferable.
	// StationName, X, Y, Z, 
    // rotAngleX, rotAngleY, rotAngleZ, 
    // resolution
	
	#if 0
    {"HOD0X0-0-29", -32.9,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-1-29", -33.1,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-2-29", -33.3,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-3-29", -33.5,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-4-29", -33.7,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-5-29", -33.9,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-6-29", -34.1,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-7-29", -34.3,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-8-29", -34.5,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-9-29", -34.7,   0,  -189.8,  0,  0,   0,   0.3},
    {"HOD0X0-10-29", -34.9,   0,  -189.8,  0,  0,   0,  0.3},
    {"HOD0X0-11-29", -35.1,   0,  -189.8,  0,  0,   0,  0.3},
    {"HOD0X0-12-29", -35.3,   0,  -189.8,  0,  0,   0,  0.3},
    {"HOD0X0-13-29", -35.5,   0,  -189.8,  0,  0,   0,  0.3},
    {"HOD0X0-14-29", -35.7,   0,  -189.8,  0,  0,   0,  0.3},
    
    {"HOD0Y0-0-30", -34.3,    1.4,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-1-30", -34.3,    1.2,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-2-30", -34.3,    1.0,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-3-30", -34.3,    0.8,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-4-30", -34.3,    0.6,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-5-30", -34.3,    0.4,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-6-30", -34.3,    0.2,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-7-30", -34.3,    0.0,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-8-30", -34.3,   -0.2,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-9-30", -34.3,   -0.4,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-10-30", -34.3,  -0.6,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-11-30", -34.3,  -0.8,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-12-30", -34.3,  -1.0,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-13-30", -34.3,  -1.2,  -189.81,  0,  0,   0,   0.3},
    {"HOD0Y0-14-30", -34.3,  -1.4,  -189.81,  0,  0,   0,   0.3},
    
    {"HOD1X0-0-29", -35.6, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-1-29", -35.8, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-2-29", -36.0, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-3-29", -36.2, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-4-29", -36.4, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-5-29", -36.6, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-6-29", -36.8, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-7-29", -37.0, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-8-29", -37.2, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-9-29", -37.4, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-10-29", -37.6, 0,   -67.5,  0,  0,   0,  0.3},
    {"HOD1X0-11-29", -37.8, 0,   -67.5,  0,  0,   0,  0.3},
    {"HOD1X0-12-29", -38.0, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-13-29", -38.2, 0,   -67.5,  0,  0,   0,   0.3},
    {"HOD1X0-14-29", -38.4, 0,   -67.5,  0,  0,   0,   0.3},
    
    {"HOD1Y0-0-30", -37.0, -1.4,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-1-30", -37.0, -1.2,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-2-30", -37.0, -1.0,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-3-30", -37.0, -0.8,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-4-30", -37.0, -0.6,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-5-30", -37.0, -0.4,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-6-30", -37.0, -0.2,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-7-30", -37.0,    0,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-8-30", -37.0,  0.2,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-9-30", -37.0,  0.4,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-10-30", -37.0, 0.6,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-11-30", -37.0, 0.8,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-12-30", -37.0, 1.0,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-13-30", -37.0, 1.2,   -67.51,  0,  0,   0,   0.3},
    {"HOD1Y0-14-30", -37.0, 1.4,   -67.51,  0,  0,   0,   0.3},
    #endif
    
    {"HOD0X0-0-0", -34.3, 0, -189.8, 0, 0, 0, 2.8, 2.8, 0.3, 15},
    {"HOD1X0-0-0", -37.0, 0,  -67.5, 0, 0, 0, 2.8, 2.8, 0.3, 15},
};

HODHitProcess::HODHitProcess( calib::Dispatcher & cdsp
                  , const std::string & only
                  , ObjPool<TrackPoint> & obTP
                  , int threshold )
        : AbstractHitHandler<SADCHit>(cdsp, only)
        , _tpInserter(obTP)
        , _threshold(threshold) {
}

void
HODHitProcess::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<SADCHit>::handle_update(nm);
    
    try {
        _hodKinID = nm.kin_id("HOD").second;
    } catch( std::exception & e ) {
        _log.error( "Couldn't find HOD kin ID in current name mappings (error: %s)."
                  , e.what() );
    }
    
    // iterate over `gPlacements' list, filling _placements map
    for( long unsigned int i = 0
       ; i < sizeof(gPlacements)/sizeof(DetPlacementEntry)
       ; ++i ) {
		
        // retrieve numerical detector id by its string name
        DetID_t did = nm.id(gPlacements[i].planeName);
        // impose placement entry into `_placements' map
        _placements.emplace(did, gPlacements + i);
    }
}

bool
HODHitProcess::process_hit( EventID
					      , DetID_t did
                          , SADCHit & hit ) {
	
	DetID stationID(did);
	
	// Process hodoscope hits	
	if ( _hodKinID == stationID.kin() ) {
		
		// Create multimap with unique detector (plane) identifier (wire)
		// and fill it with SADC hodoscope hits and corresponding number
		// of hits in detector plane
		
		// unset payload to get unique identifier for hodoscope
		// with multiply SADC chips attached
				
		stationID = _create_unique_det(stationID);

		auto idxIt = _hodoHits.find( stationID );
		if( _hodoHits.end() == idxIt ) {
			auto ir = _hodoHits.emplace( stationID, Hits() );
			assert( ir.second );
			idxIt = ir.first;
		}
		
		Hits & wireSADC = idxIt->second;
		
		// Get wire number and projection from DetID payload
		DetID wireID(did);
		CellID cID(wireID.payload());
		HODStripNo wireNo = cID.get_y();
		HODProj projection = cID.get_z();
		
		HODHit cHit;
		
		cHit.did = did;
		cHit.wireNo = wireNo;
		cHit.ref = _current_event().sadcHits.pool().get_ref_of(hit);
		cHit.time = hit.maxSample;
		
		if ( projection == 29 ) {
			cHit.xproj = true;
			cHit.yproj = false;
		} else {
			cHit.xproj = false;
			cHit.yproj = true;
		}
						
		if ( hit.maxValue >= _threshold ) {
			wireSADC.emplace( wireNo
				, cHit);
		}
		
	}
							   
	return true;
}

HODHitProcess::ProcRes
HODHitProcess::process_event(Event * evPtr) {

    // Vector of conjugated hodoscope hits
    std::vector<hitPair> conHits;
    
    AbstractHitHandler<SADCHit>::process_event( evPtr );
    
    for( auto detEntry : _hodoHits ) {
		_create_clusters( detEntry.first
		                , detEntry.second 
		                , conHits);
		                
		_create_track_points( *evPtr
							, detEntry.first
		                    , conHits );
		conHits.clear();
    }

    _hodoHits.clear();
    
    return kOk;
}

DetID
HODHitProcess::_create_unique_det( DetID did ) {
	
	CellID cid(did.payload());
	cid.set_x(0);
	cid.set_y(0);
	cid.set_z(0);
	did.payload(cid.cellID);
	
	return did;
}		

void
HODHitProcess::_create_clusters( DetID did
						       , Hits & rawHits
						       , std::vector<hitPair> & conHits) {
	
	std::vector<HODHit> clusterX;
	std::vector<HODHit> clusterY;
			
	for ( auto hit : rawHits) {
		if ( hit.second.xproj ) {
			// assign first Hit to cluster candidate
			clusterX.push_back( hit.second );
		} else if ( hit.second.yproj ) {
			clusterY.push_back( hit.second );
		}
	}
	
	#if 0
	std::cout << "Start of clusters in X plane" << std::endl;
		for ( auto ir : clusterX ) {
			std::cout << "Hit wire ID: " << ir.wireNo << std::endl;
		}
	std::cout << "Start of clusters in Y plane" << std::endl;
		for ( auto ir : clusterY ) {
			std::cout << "Hit wire ID: " << ir.wireNo << std::endl;
		}
	#endif
	
	if ( !clusterX.empty() && !clusterY.empty() ) {
	for ( auto & hitX : clusterX ) {
		for ( auto & hitY : clusterY ) {
				
		// Check if there is no time difference between hits
			if ( std::fabs(hitX.time - hitY.time) > 1 ) {
				continue;
			}
			conHits.push_back(std::make_pair(hitX, hitY));
		}
	}}
	
}

void
HODHitProcess::_create_track_points( Event & e
                                   , DetID_t did
				                   , std::vector<hitPair> & conHits) {
	
	for ( auto & cPair : conHits ) {
		TrackPoint tp;
		//tp.genfitTrackPoint = nullptr;
		EvFieldTraits<TrackPoint>::reset_hit(tp);
		
		const DetPlacementEntry * cPlacement = _placements[did];
		
		tp.lR[0] = ((cPlacement->sizeX * cPair.first.wireNo / cPlacement->numOfWires) 
				 - (( cPlacement->sizeX )/2) );		
		tp.lR[1] = ((cPlacement->sizeY * cPair.second.wireNo / cPlacement->numOfWires) 
				 - (( cPlacement->sizeY )/2) );		 
		tp.lR[2] = 0;
					
		const double angle = cPlacement->rotAngleZ;

		tp.gR[0] = ( tp.lR[0] * cos( angle * M_PI / 180.0 ) - tp.lR[1] * sin ( angle * M_PI / 180.0 ) ) + cPlacement->x;   
		tp.gR[1] = ( tp.lR[1] * cos( angle * M_PI / 180.0 ) + tp.lR[0] * sin ( angle * M_PI / 180.0 ) ) + cPlacement->y;
		tp.gR[2] = cPlacement->z;
		
		tp.station = did;
		
		tp.charge += (*cPair.first.ref).maxValue + (*cPair.second.ref).maxValue;
		
		*_tpInserter( e, did, naming() ) = tp;
	}

}

}

REGISTER_HANDLER( HODHitProcess, banks, cdsp, cfg
                , "Handler for HODoscope hits processing" ) {
    
    return new handlers::HODHitProcess( cdsp
								      , aux::retrieve_det_selection(cfg)
								      , banks.of<TrackPoint>()
								      , cfg["threshold"].as<int>() );
}
}

#endif
