#include "data/SADC/wfFit.hh"
#include "na64util/str-fmt.hh"
#include <cmath>

namespace na64dp {
namespace handlers {

SADCWfFit::SADCWfFit( calib::Dispatcher &  ch
                    , ObjPool<WfFittingData> & fitDataBank
                    , SADCWF_FittingFunction fit_f
                    , const double * p
                    , const std::string & detSelection
                    , FILE * logFile
                    , bool closeFile )
            : AbstractHitHandler<SADCHit>(ch, detSelection)
            , _logfile(logFile)
            , _doCloseLogFile(closeFile)
            , _fitDataBank(fitDataBank)
            , _fit_f(fit_f)
            {
    assert(_fit_f);
    assert(p);
    _input.n = 32;  // for SADC detectors in NA64 it is always like that
    _input.samples = static_cast<double*>(malloc( sizeof(double)*32 ));
    _input.sigma   = static_cast<double*>(malloc( sizeof(double)*32 ));
    memcpy( _p, p, sizeof(_p) );
    // TODO: technically, sigma values have to be set outside, but it is not
    // used yet...
    for(int i = 0; i < 32; ++i) {
        _input.sigma[i] = 1.;
    }
}

SADCWfFit::~SADCWfFit() {
    free(_input.sigma);
    free(_input.samples);
    if( _logfile && _doCloseLogFile ) {
        fclose( _logfile );
    }
}

bool
SADCWfFit::process_hit( EventID
                      , DetID_t
                      , SADCHit & cHit ) {
    // set _input (ptr)
    std::copy( cHit.wave, cHit.wave + 32
             , _input.samples );
    double * chiSqPtr = NULL;
    // set _pars
    if( moyal == _fit_f ) {
        _pars.moyalPars.p[0] = _p[0];
        _pars.moyalPars.p[1] = _p[1];
        _pars.moyalPars.p[2] = _p[2];
        chiSqPtr = &(_pars.moyalPars.chisq_dof);
    } else {
        // This probably means, that you forgot to consider other cases in the
        // above if-clause
        NA64DP_RUNTIME_ERROR( "Non-Moyal function is set"
                " for fitting (TODO)" );
    }
    // ... _pars.err[], chisq_dof are to be overwritten

    // Do the fitting
    int rc = fit_SADC_samples( &_input, _fit_f, &_pars, _logfile );
    if( 0 != rc ) {
        // fitting procedure failed, give up
        return true;
    }

    // Check that hit has fitting data associated. If not -- allocate one using
    // pool allocator.

    // Copy the fitting parameters
    if( ! cHit.fittingData.is_valid() ) {
        // Allocate the fitting data using banks
        cHit.fittingData = _fitDataBank.create();
    }
    cHit.fittingData->fitPars.moyalPars = _pars.moyalPars;
    
    // Digitalize the fitting result function and calculate the RMS deviation
    cHit.fittingData->rms = 0.;
    for( int i = 0; i < 32; ++i ) {
        double v = moyal( &_pars, i );
        v -= cHit.wave[i];
        v *= v;
        cHit.fittingData->rms += v;
    }
    cHit.fittingData->rms = sqrt(cHit.fittingData->rms/32);
    // Copy the function-specific fitting parameter to common field for further
    // usage
    cHit.fittingData->chisq = *chiSqPtr;
    
    return true;
}

}

REGISTER_HANDLER( SADCWaveformFit, banks, ch, yamlNode
                , "Fits the SADC waveform with function" ){
    //log4cpp::Category & L = log4cpp::Category::getInstance( "vctr" );
    double iPars[handlers::SADCWfFit::nParameters];
    // Determine the model function to fit
    const std::string & fitFStr = yamlNode["function"].as<std::string>();
    SADCWF_FittingFunction f;
    if( "moyal" == fitFStr ) {
        f = moyal;
        // Acquire initial parameters for Moyal function
        iPars[0] = yamlNode["initial"][0].as<double>();  // area
        iPars[1] = yamlNode["initial"][1].as<double>();  // max
        iPars[2] = yamlNode["initial"][2].as<double>();  // width
        iPars[3] = iPars[4] = 0;
    } else {
        NA64DP_RUNTIME_ERROR( "No model function named \"%s\" defined."
                            , fitFStr.c_str() );
    }
    // Check for the logging file, open if path is provided
    FILE * fitLogF = NULL;
    if( yamlNode["writeLogTo"]
     && !yamlNode["writeLogTo"].as<std::string>().empty() ) {
        const std::string logFileStr = yamlNode["writeLogTo"].as<std::string>();
        fitLogF = fopen( logFileStr.c_str(), "w" );
        if( !fitLogF ) {
            NA64DP_RUNTIME_ERROR( "Couldn't open file \"%s\" for writing: %s"
                                , fitFStr.c_str()
                                , strerror(errno) );
        }
    }
    return new handlers::SADCWfFit( ch
                                  , banks.of<WfFittingData>()
                                  , f
                                  , iPars
                                  , aux::retrieve_det_selection(yamlNode)
                                  , fitLogF, (bool) fitLogF );
}

}
