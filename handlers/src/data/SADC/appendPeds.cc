#include "data/SADC/appendPeds.hh"

namespace na64dp {
namespace handlers {

bool
SADCAppendPeds::process_hit( EventID
                           , DetID_t
                           , SADCHit & currentEcalHit) {
    //  Add Mean pedestal values back to the Event
    for(int i = 0; i < 32; ++i) {
        if ((i % 2) == 0) {
            currentEcalHit.wave[i] = currentEcalHit.wave[i] + currentEcalHit.pedestals[0];
        } else {
            currentEcalHit.wave[i] = currentEcalHit.wave[i] + currentEcalHit.pedestals[1];
        }
    }
    return true;
}

}

REGISTER_HANDLER( SADCAppendPeds, banks, ch, yamlNode
                , "Adds pedestal values back to the waveform" ) {
    return new handlers::SADCAppendPeds(ch, aux::retrieve_det_selection(yamlNode));
}

}

