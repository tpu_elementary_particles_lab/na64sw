#include "data/SADC/SmoothingWave.hh"

namespace na64dp {
namespace handlers {

bool
SmoothWave::process_hit( EventID
                           , DetID_t
                           , SADCHit & cHit ){
    
    int lenMass = 32;
    int k = 5,
        K = 1;
    int rightEl = 1;
    int leftEl = 1;
    for(int count= 0; count < 5; ++count ){
      //
      double SmWave[lenMass] = { 0 };
      //
      for( int i = 0; i < lenMass; ++i ){
        //
        for( int l = - leftEl; l <= rightEl; ++l ){
          // 
          if( i + l <= 0 ){ continue; }
          //
          if( i + l >= lenMass ){ break; }
          //
          if( l == 0 ){K = k;} else {K = 1;}
          SmWave[i] += K * cHit.wave[i + l];
        }
        SmWave[i] /= leftEl + rightEl + k  - 1;
      }    
      //
      for( int i = 0; i < lenMass; ++i ){
        cHit.wave[i] = SmWave[i];
      }
    }
    return true;
}

}

REGISTER_HANDLER( SmoothWave, banks, ch, yamlNode
                , "Smoothing waveform using moving average on 4 points" ){
    return new   handlers::SmoothWave( ch
               , aux::retrieve_det_selection(yamlNode)
               );

}
}

