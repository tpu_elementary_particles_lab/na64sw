#include "data/SADC/getPedestalsByFront.hh"

namespace na64dp {
namespace handlers {

bool
SADCGetPedestalsByFront::process_hit( EventID
                                    , DetID_t
                                    , SADCHit & hit ) {
    int p1 = hit.wave[0]
      , p2 = hit.wave[1];

    for( int i = 1; i < _nFirstSamples; ++i ) {
        p1 += hit.wave[2*i  ];
        p2 += hit.wave[2*i+1];
    }
    p1 /= _nFirstSamples;
    p2 /= _nFirstSamples;
    hit.pedestals[0] = p1;
    hit.pedestals[1] = p2;
    return true;
}

}

REGISTER_HANDLER( SADCGetPedestalsByFront, banks, ch, yamlNode
                , "calculate SADC pedestals by first N values (default is 4)" ) {
    return new handlers::SADCGetPedestalsByFront( ch
                , aux::retrieve_det_selection(yamlNode)
                , yamlNode["nSamples"] ? yamlNode["nSamples"].as<int>() : 4 );
}

}

