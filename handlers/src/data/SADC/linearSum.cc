#include "data/SADC/linearSum.hh"

namespace na64dp {
namespace handlers {

bool
SADCLinearSum::process_hit( EventID
                          , DetID_t
                          , SADCHit & cHit ){
    cHit.sum = 0;
    for( int i = 0; i < 30; ++i ){
        auto a = cHit.wave[i]
           , b = cHit.wave[i + 1]
           ;
        if( _doZeroNegative ) {
            if( a < 0 ) a = 0;
            if( b < 0 ) b = 0;
        }
        cHit.sum += ( a + b ) / 2; 
    }
    return true;
}

}

REGISTER_HANDLER( SADCLinearSum, banks, ch, yamlNode
                , "Computes linear sum of SADC waveform" ){
    bool zeroNegatives = yamlNode["zeroNegativeValues"] ? yamlNode["zeroNegativeValues"].as<bool>()
                                                        : false;
    return new handlers::SADCLinearSum( ch
            , aux::retrieve_det_selection(yamlNode)
            , zeroNegatives );
}

}
