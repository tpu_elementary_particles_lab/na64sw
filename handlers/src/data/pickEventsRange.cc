#include "data/pickEventsRange.hh"

namespace na64dp {

PickEventsRange::PickEventsRange( EventNumber_t from_
                                , EventNumber_t to_
                                , bool negate_=false
                                ) :  _from(from_)
                          , _to(to_)
                          , _negate(negate_)
                          , _counter(0) {
    std::cout << "initialized " << this
              << ", range [" << _from << ", " << _to << "], negate="
              << (_negate ? "true" : "false") << std::endl;
}

AbstractHandler::ProcRes
PickEventsRange::process_event( Event * ) {
    ++_counter;
    return _negate != (_counter > _from && _counter < _to) ? kOk : kStopProcessing;
}

REGISTER_HANDLER( PickEventsRange, banks, ch, yamlNode
                , "Performs event discrimantion by internal count (events range)" ){
    return new PickEventsRange( yamlNode["from"] ? yamlNode["from"].as<unsigned long>()
                                             : 0
                          , yamlNode["to"] ? yamlNode["to"].as<unsigned long>()
                                             : ULONG_MAX
                          , yamlNode["negate"] ? yamlNode["negate"].as<bool>()
                                               : false );
}

}

