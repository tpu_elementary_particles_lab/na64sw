#include "data/hitDiscriminate.hh"
#include "na64util/functions.hh"

namespace na64dp {

template<typename HitT> AbstractHandler *
_new_discrimination_handler( HitBanks &
                           , calib::Dispatcher & calibDsp
                           , const YAML::Node & cfg ) {
    typename EvFieldTraits<HitT>::ValueGetter getter
            = utils::value_getter<HitT>(cfg["value"].as<std::string>());
    auto comparator = na64dp::functions::get_comparator(
            cfg["condition"].as<std::string>().c_str());
    return new handlers::HitDiscriminate<HitT>( calibDsp
                                              , getter
                                              , comparator
                                              , cfg["bound"].as<double>()
                                              , cfg["removeHit"].as<bool>()
                                              , cfg["discriminateEvent"].as<bool>()
                                              , cfg["abruptHitProcessing"].as<bool>()
                                              , aux::retrieve_det_selection(cfg)
                                              );
}

REGISTER_HANDLER( RemoveSADCHits, banks, calibDsp, cfg
        , "Discriminates hits and/or events by certain value within the SADC hit data" ) {
    return _new_discrimination_handler<SADCHit>( banks, calibDsp, cfg );
}

REGISTER_HANDLER( RemoveAPVHits, banks, calibDsp, cfg
        , "Discriminates hits and/or events by certain value within the APV hit data" ) {
    return _new_discrimination_handler<APVHit>( banks, calibDsp, cfg );
}

REGISTER_HANDLER( RemoveAPVClusters, banks, calibDsp, cfg
        , "Discriminates APV hit clusters and/or events by certain value"
                " within the APV cluster data" ) {
    return _new_discrimination_handler<APVCluster>( banks, calibDsp, cfg );
}

// TODO: traits implementation for TrackPoint and Track objects
#if 0
REGISTER_HANDLER( RemoveAPVTrackPoints, banks, ch, cfg
                , "Discriminates track points and/or events by certain value"
                  " within their data" ) {
    return _new_discrimination_handler<TrackPoint>( banks, ch, cfg );
}

REGISTER_HANDLER( RemoveTracks, banks, ch, cfg
                , "Discriminates tracks and/or events by certain value"
                  " within their data" ) {
    return _new_discrimination_handler<Track>( banks, ch, cfg );
}
#endif

}

