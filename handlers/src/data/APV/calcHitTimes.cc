#include "data/APV/calcHitTimes.hh"
#include "na64detID/wireID.hh"
#include "math.h"

#include <stdexcept>

namespace na64dp {
namespace handlers {

static
APVCalcHitTimes::MMCalibData gMMCalibs[] = {
    
    // MM calibrations valid for 2017 runs
    // Structure: {r0, t0, a0}, {r02, t02, a02, r0t0, r0a0, t0a0} for a02 and a12, respectively
    
    {"MM1X", {1.27062,142.427,-33.2835}, {1.86258,6991.31,5598.28,110.198,-95.9439,-5458.81}, 
			 {1.15026,105.008,-35.5102}, {0.336042,2063.89,8577.5,-5.8154,-50.4027,2104.8} },
    {"MM1Y", {1.11978,135.537,-28.1138}, {1.24223,4416.39,5000.11,69.131,-70.4358,-3702.9}, 
		     {1.15096,102.104,-39.3690}, {1.00427,3026.15,22637.4,2.39151,-142.966,1971.33}},
    
    {"MM2X", {1.24329,143.317,-37.5259}, {3.49489,16168.5,10637,233.754,-183.549,-12032.7},
		     {1.16999,104.549,-42.4817}, {0.868475,2455.26,18936.7,11.1406,-123.101,44.7095}},
    {"MM2Y", {1.18926,139.605,-29.9941}, {1.68738,6112.13,5363.87,96.9505,-86.7835,-4804.24},
		     {1.21094,107.743,-41.9657}, {1.36482,3141.26,21955.8,36.9759,-165.631,-2791.92}},
    
    {"MM3X", {1.02052,130.810,-36.3660}, {2.69802,14731.7,20320.8,190.86,-221.41,-14822.3},
		     {1.13799,89.3839,-55.8788}, {4.18576,10354.8,144836,58.8096,-761.729,-3589.2}},
    {"MM3Y", {1.07237,139.053,-33.3187}, {3.03495,15235.8,12587.2,208.919,-181.643,-12181.8},
		     {1.18184,105.753,-46.6008}, {2.44053,6487.61,43302.6,88.3015,-313.361,-8593.57}},
    
    {"MM4X", {1.2061,144.5100,-37.9135}, {4.17452,20989.6,12713.7,291.881,-219.539,-15117.9},
		     {1.16135,104.760,-44.9047}, {1.34475,3481.26,28213.8,31.9155,-187.919,-2443.59}},
    {"MM4Y", {1.18478,140.521,-33.1136}, {2.3714,9854.93,7764.07,148.399,-126.264,-7687.45},
		     {1.18762,106.448,-43.1446}, {1.31737,3192.02,23844.7,33.6313,-170.167,-2547.32}},
    
    {"MM5X", {1.20437,308.222,-30.4724}, {0.643998,2081.09,3570.47,31.4746,-44.1057,-1830.09},
		     {1.15947,268.884,-43.3712}, {0.56849,3869.76,20113.3,-19.0831,-102.753,5490.7}},
    {"MM5Y", {1.21338,314.193,-28.5284}, {1.77169,5583.04,5518.21,93.3147,-88.7577,-4409.83},
		     {1.16234,275.631,-39.0516}, {1.00864,3437.57,23577,-3.96983,-145.636,3201.28}},
    
    
    {"MM7X", {1.25153,317.657,-31.6756}, {1.21648,4272.54,3905.57,68.5415,-64.2775,-3411.74},
		     {1.22831,279.732,-47.8268}, {1.01886,2446.53,20355.6,25.4067,-139.8,-2184.15}},
    {"MM7Y", {1.07888,315.156,-24.2368}, {0.892518,3093.94,2968.9,47.4262,-43.9682,-2178.98},
		     {1.04605,274.510,-32.2398}, {0.341839,4458.32,12351.1,-17.6304,-59.4534,5470.99}},
	
	{"MM6X", {1.25153,317.657,-31.6756}, {1.21648,4272.54,3905.57,68.5415,-64.2775,-3411.74},
		     {1.22831,279.732,-47.8268}, {1.01886,2446.53,20355.6,25.4067,-139.8,-2184.15}},
    {"MM6Y", {1.07888,315.156,-24.2368}, {0.892518,3093.94,2968.9,47.4262,-43.9682,-2178.98},
		     {1.04605,274.510,-32.2398}, {0.341839,4458.32,12351.1,-17.6304,-59.4534,5470.99}},
	

};

static
APVCalcHitTimes::MMWireCalibData gMMWireCalibs[] = {
    // Calibrations for each of micromega DAQ wire (sigmas)
    
    {"MM1X", {9.9354, 10.3841, 9.76253, 9.28448, 9.56489, 9.43789, 9.58244, 8.89798, 
		     9.09545, 9.26835, 8.96771, 8.45196, 8.54463, 9.015, 8.2687, 7.98431, 
		     8.65279, 8.19416, 8.18981, 8.2296, 8.73632, 7.52985, 8.15298, 7.74999, 
		     8.21342, 7.92779, 8.10864, 8.39238, 8.08166, 8.4374, 8.25116, 7.48444, 
		     7.61247, 8.5184, 8.60474, 8.41732, 8.77528, 8.6344, 8.69417, 8.95594, 
		     8.88449, 8.7196, 8.87183, 9.04323, 8.91225, 9.53298, 9.06877, 9.67967, 
		     9.63358, 9.32249, 10.0796, 9.48581, 9.87599, 10.093, 9.85026, 10.1142, 
		     9.95169, 10.0456, 11.2394, 10.7913, 10.7552, 11.3777, 10.6724, 11.9739}},
		     
    {"MM1Y", {10.3914, 9.08516, 9.3797, 8.94983, 8.41703, 8.96401, 9.11767, 8.66433,
		     8.59356, 8.72577, 8.79908, 8.73349, 8.54629, 8.20504, 8.46152, 8.01993, 
		     7.8049, 8.18903, 8.01886, 7.68962, 7.8767, 8.08272, 7.88703, 7.82182,
		     8.00451, 8.09349, 7.47933, 8.01717, 7.43227, 7.70419, 7.83022, 7.73138, 
		     7.86427, 7.59542, 7.94386, 7.73848, 8.2724, 7.70803, 7.9495, 7.92375,
		     8.30823, 8.29469, 8.13466, 8.08506, 8.41151, 8.37522, 8.7086, 8.44433, 
		     8.80605, 8.56326, 8.68402, 8.48067, 9.34918, 9.06239, 9.14818, 9.10258, 
		     9.77954, 10.039, 10.1538, 9.88332, 10.2488, 9.77623, 10.4136, 10.4872}},
    
    {"MM2X", {10.0918, 10.0428, 9.6791, 9.49935, 8.94529, 9.04053, 8.68078, 8.29459,
		     9.44, 8.14218, 7.99698, 8.01984, 8.03498, 7.76072, 7.64061, 7.75418,
		     7.77527, 7.82391, 7.5484, 7.71063, 7.56772, 7.70662, 8.84334, 7.67088,
		     7.51097, 7.51194, 7.42299, 7.55176, 7.4255, 7.43409, 7.38017, 7.51656,
		     7.26128, 7.86541, 7.30651, 7.3143, 7.29799, 7.29708, 7.4265, 7.33568,
		     .35744, 7.31126, 7.3525, 7.50624, 7.49486, 7.59942, 7.51507, 9.85692,
		     7.65889, 7.8245, 7.67151, 7.99752, 7.86467, 8.15335, 7.99431, 8.18595,
		     8.39359, 8.63961, 8.58569, 8.89506, 9.17491, 9.58342, 9.54089, 9.98494}},
		     
    {"MM2Y", {10.6381, 8.97261, 9.32541, 8.40468, 8.39102, 8.01824, 7.94967, 7.88769,
		     7.70745, 9.14579, 7.6096, 8.13398, 7.38974, 7.30424, 7.22057, 7.17356,
		     9.08489, 7.14034, 7.55165, 7.06113, 7.05696, 7.17586, 6.99299, 8.30199,
		     6.99435, 8.82305, 7.47644, 6.82128, 8.77434, 6.8947, 6.88201, 6.83049,
		     6.84987, 6.62077, 6.79079, 9.18794, 6.9396, 6.79054, 6.79659, 6.75762,
		     6.96331, 6.79329, 9.25365, 6.82163, 7.13752, 8.29327, 6.98172, 8.60275,
		     6.92528, 7.07308, 7.896, 7.26338, 7.39359, 7.29641, 9.07836, 7.49242,
		     7.73744, 7.76684, 7.85872, 7.90156, 8.07011, 10.2668, 8.5527, 9.05673}},

    {"MM3X", {9.74986, 9.65, 9.95827, 9.27399, 10.1114, 9.53478, 9.49114, 9.91559,
		     9.66271, 8.67254, 9.27423, 8.9493, 9.5199, 8.70884, 8.46925, 8.90439,
		     8.99264, 8.12038, 8.59339, 8.73957, 8.65023, 8.92117, 8.92058, 8.39414,
		     8.53141, 8.70548, 8.2555, 8.02816, 8.14937, 8.4313, 8.50723, 8.66317,
		     8.26292, 8.52844, 8.71163, 8.35991, 8.95914, 8.55011, 8.93356, 10.5042,
		     9.34826, 8.59917, 9.58243, 9.68234, 8.19721, 8.52033, 8.39849, 8.58338,
		     8.07397, 8.83569, 8.74, 8.80697, 8.42323, 9.53157, 9.59369, 9.75439,
		     9.78946, 10.4339, 10.8714, 10.3666, 11.2912, 11.5684, 12.0158, 12.2893}},
		     
    {"MM3Y", {9.39656, 8.72937, 8.82202, 9.23972, 8.36392, 9.06624, 9.16861, 9.26849,
		     8.9799, 8.61904, 8.08459, 8.19597, 8.10745, 8.31634, 7.741, 8.70479,
		     8.61007, 8.22371, 8.20112, 8.09323, 8.41496, 8.25899, 8.26954, 7.6998,
		     8.16294, 8.12478, 8.01501, 7.80925, 8.60555, 7.84649, 7.89342, 7.71244,
		     7.71583, 8.25869, 7.86248, 7.77738, 8.55154, 8.27034, 8.8107, 9.88694,
		     8.40079, 8.18677, 9.01901, 8.89772, 7.81389, 7.47289, 8.25909, 7.51318,
		     7.94334, 8.40912, 8.24204, 8.33346, 8.14262, 8.40487, 8.40332, 8.78901,
		     9.14863, 9.55814, 9.38875, 9.37368, 9.93599, 10.2306, 10.3883, 10.3629}},
    
    {"MM4X", {15.5016, 12.1189, 12.3799, 12.0549, 11.3665, 10.3824, 10.0708, 10.1429,
		     9.49511, 9.97222, 9.28444, 8.99116, 9.20642, 9.06162, 8.68697, 8.65673,
		     8.36523, 8.41517, 8.38819, 8.46223, 8.32963, 8.26632, 8.23219, 8.48797,
		     8.14904, 8.23685, 8.10541, 8.22938, 8.15229, 8.11376, 8.15025, 8.05622,
		     8.19556, 8.0987, 8.05012, 8.18033, 8.17239, 8.20325, 8.10645, 8.02286,
		     8.21912, 8.25258, 8.18605, 8.28329, 8.25221, 8.48125, 8.23315, 8.57851,
		     8.48077, 8.43163, 8.56499, 8.58786, 8.71733, 8.93913, 8.73092, 9.01345,
		     9.09828, 9.57598, 9.32434, 9.74451, 9.96452, 10.1655, 10.3233, 10.9001}},
		     
    {"MM4Y", {11.7459, 11.0767, 10.8532, 10.7976, 9.57542, 10.2278, 10.1896, 8.98456,
		     9.10254, 9.86967, 8.55641, 8.35991, 8.62055, 8.20363, 8.53301, 8.07396,
		     8.27925, 8.14941, 8.05618, 7.85204, 8.63157, 7.61041, 7.61509, 8.49344,
		     7.48075, 7.52447, 7.72197, 7.46146, 7.79504, 7.37393, 8.02299, 8.23672,
		     7.58121, 7.46922, 8.71549, 7.41504, 7.53836, 8.02362, 7.48694, 7.76998,
		     7.62509, 7.75019, 7.91811, 7.54023, 7.62284, 8.52169, 7.597, 7.69285,
		     8.25523, 7.65482, 7.80905, 7.90051, 7.96271, 8.50559, 7.98484, 8.72622,
		     8.81089, 8.4152, 8.56515, 9.53422, 8.96654, 8.99025, 10.0524, 9.46922}},
    
    {"MM5X", {13.923, 12.8687, 12.1936, 11.7096, 11.4117, 10.599, 11.0178, 10.4627,
		     10.0762, 10.0686, 9.45808, 9.81727, 9.20091, 9.30067, 9.15908, 8.87796,
		     8.97265, 9.10783, 8.77802, 8.64785, 8.47345, 8.5384, 8.57206, 8.57605,
		     8.45847, 8.7575, 8.33237, 8.33444, 8.23275, 8.34742, 8.24166, 8.33038,
		     8.16626, 8.33488, 8.11081, 8.23779, 8.15731, 8.18345, 8.07892, 8.21037,
		     8.26673, 8.28494, 8.13674, 8.30107, 8.24907, 8.33723, 10.374, 10.4433,
		     8.52137, 8.7065, 8.5673, 8.64412, 8.84282, 8.97141, 8.87924, 9.12117,
		     9.4771, 9.70531, 9.89163, 9.96764, 12.5936, 10.5324, 10.8123, 11.2317}},
		     
    {"MM5Y", {11.2444, 11.196, 10.5643, 11.0035, 10.5863, 10.63, 10.4963, 10.3384,
		     9.03816, 10.3194, 9.14642, 9.82307, 9.09128, 8.78221, 8.78039, 8.69119,
		     8.45267, 8.26726, 8.22138, 8.09056, 8.11024, 8.13309, 8.26785, 7.90464,
		     7.84097, 7.92841, 7.95873, 7.70433, 7.86163, 7.7807, 7.7692, 7.77953,
		     7.62098, 7.7635, 7.78537, 7.74137, 7.79141, 7.77211, 7.6706, 7.85226,
		     7.76538, 7.62551, 7.81115, 7.79969, 7.81005, 7.70165, 7.85416, 7.79302,
		     7.94329, 7.85235, 8.02272, 7.95872, 8.14872, 8.17027, 8.1453, 8.17693,
		     8.65182, 8.77642, 8.74362, 8.95801, 9.29885, 9.41429, 9.56462, 9.68592}},
    
   
    {"MM7X", {12.484, 11.4519, 11.3639, 10.622, 10.2765, 9.7871, 10.045, 9.84237,
		     11.3212, 9.81722, 9.51998, 12.5593, 9.37693, 9.48088, 9.10138, 9.20641,
		     8.82261, 9.04945, 8.90837, 8.66081, 10.4361, 10.3052, 8.71768, 8.6756,
		     8.53453, 8.50394, 8.32854, 8.3701, 8.37493, 8.30128, 8.6257, 8.41968,
		     8.11944, 9.94462, 8.18321, 8.31719, 8.13095, 8.2523, 8.20889, 8.29434,
		     8.13951, 8.36342, 8.37252, 8.40691, 11.0418, 8.50762, 8.52151, 8.77926,
		     10.5437, 10.4868, 8.67182, 9.08906, 9.06301, 9.22806, 9.30445, 9.47306,
		     12.1414, 12.4277, 10.0327, 10.2364, 10.5974, 12.3204, 12.4802, 11.3067}},
		     
    {"MM7Y", {11.9772, 10.6683, 10.4206, 10.7063, 10.1786, 10.0306, 9.49869, 9.6415,
		     9.57623, 8.95058, 9.27286, 9.69272, 9.14678, 9.85398, 8.54961, 8.43191,
		     8.45797, 8.40829, 8.08255, 8.10101, 8.19234, 8.22945, 8.42612, 7.95981,
		     7.9091, 7.89324, 8.3131, 7.7373, 7.82431, 7.89458, 7.95108, 7.60007,
		     7.61809, 7.61112, 7.67578, 7.57376, 8.46062, 7.57063, 7.70243, 7.79128,
		     7.69394, 7.67998, 7.99148, 7.71393, 7.72205, 7.7978, 7.81488, 8.10062,
		     8.03859, 8.18649, 8.53273, 8.19212, 8.32302, 8.42943, 8.44302, 9.05094,
		     9.07503, 9.58386, 9.01623, 9.05379, 9.29827, 10.0898, 9.69303, 10.0823}},
   
    {"MM6X", {12.484, 11.4519, 11.3639, 10.622, 10.2765, 9.7871, 10.045, 9.84237,
		     11.3212, 9.81722, 9.51998, 12.5593, 9.37693, 9.48088, 9.10138, 9.20641,
		     8.82261, 9.04945, 8.90837, 8.66081, 10.4361, 10.3052, 8.71768, 8.6756,
		     8.53453, 8.50394, 8.32854, 8.3701, 8.37493, 8.30128, 8.6257, 8.41968,
		     8.11944, 9.94462, 8.18321, 8.31719, 8.13095, 8.2523, 8.20889, 8.29434,
		     8.13951, 8.36342, 8.37252, 8.40691, 11.0418, 8.50762, 8.52151, 8.77926,
		     10.5437, 10.4868, 8.67182, 9.08906, 9.06301, 9.22806, 9.30445, 9.47306,
		     12.1414, 12.4277, 10.0327, 10.2364, 10.5974, 12.3204, 12.4802, 11.3067}},
		     
    {"MM6Y", {11.9772, 10.6683, 10.4206, 10.7063, 10.1786, 10.0306, 9.49869, 9.6415,
		     9.57623, 8.95058, 9.27286, 9.69272, 9.14678, 9.85398, 8.54961, 8.43191,
		     8.45797, 8.40829, 8.08255, 8.10101, 8.19234, 8.22945, 8.42612, 7.95981,
		     7.9091, 7.89324, 8.3131, 7.7373, 7.82431, 7.89458, 7.95108, 7.60007,
		     7.61809, 7.61112, 7.67578, 7.57376, 8.46062, 7.57063, 7.70243, 7.79128,
		     7.69394, 7.67998, 7.99148, 7.71393, 7.72205, 7.7978, 7.81488, 8.10062,
		     8.03859, 8.18649, 8.53273, 8.19212, 8.32302, 8.42943, 8.44302, 9.05094,
		     9.07503, 9.58386, 9.01623, 9.05379, 9.29827, 10.0898, 9.69303, 10.0823}},
};

static
APVCalcHitTimes::GEMCalibData gGEMCalibs[] = {
    // For some reason, NA64 uses old format of GEM's time calibrations (CsGEMTimeCalOld)
    // resulting in a slightly different calibration constant's stucture.
    // The question may arrise here: why not to implement new calibs
    // which will lead to unification and simplification of this code?
    {"GM1X", {0.2,1.1,-40.,22.2,1.65},
             {0.3,1.0,  0.,27. ,1.23}},
             
    {"GM1Y", {0.2,1.1,-40.,22.2,1.65},
             {0.3,1.0,  0.,27. ,1.23}},
             
    {"GM2X", {0.2,1.1,-40.,22.2,1.65},
             {0.3,1.0,  0.,27. ,1.23}},
             
    {"GM2Y", {0.2,1.1,-40.,22.2,1.65},
             {0.3,1.0,  0.,27. ,1.23}},    
               
    {"GM3X", {0.2,1.1,-40.,22.2,1.65},
             {0.3,1.0,  0.,27. ,1.23}},
             
    {"GM3Y", {0.2,1.1,-40.,22.2,1.65},
             {0.3,1.0,  0.,27. ,1.23}},
             
    {"GM4X", {0.2,1.1,-40.,22.2,1.65},
             {0.3,1.0,  0.,27. ,1.23}},
             
    {"GM4Y", {0.2,1.1,-40.,22.2,1.65},
             {0.3,1.0,  0.,27. ,1.23}},  
                 
};

APVCalcHitTimes::APVCalcHitTimes( calib::Dispatcher & cdsp
                                , const std::string & only
                                ) : AbstractHitHandler<APVHit>(cdsp, only) {}

void
APVCalcHitTimes::handle_update( const nameutils::DetectorNaming & nm ) {
    AbstractHitHandler<APVHit>::handle_update(nm);
    
    try {
        _gmKinID = nm.kin_id("GM").second;
    } catch( std::exception & e ) {
        _log.error( "Couldn't find GEM kin ID in current name mappings (error: %s)."
                  , e.what() );
    }
    try {
        _mmKinID = nm.kin_id("MM").second;
    } catch( std::exception & e ) {
        _log.error( "Couldn't find Micromega kin ID in current name mappings (error: %s)."
                  , e.what() );
    }

    for( long unsigned int i = 0; i < sizeof(gMMCalibs)/sizeof(MMCalibData); ++i ) {

        DetID_t did = nm.id(gMMCalibs[i].planeName);

        _mmCalibs.emplace(did, gMMCalibs + i); // TODO: Ask Renat why gMMCalibs + i
    }

    for( long unsigned int i = 0; i < sizeof(gMMWireCalibs)/sizeof(MMWireCalibData); ++i ) {

        DetID_t did = nm.id(gMMWireCalibs[i].planeName);

        _mmWireCalibs.emplace(did, gMMWireCalibs + i); // TODO: Ask Renat why gMMCalibs + i
    }
    
    for( long unsigned int i = 0; i < sizeof(gGEMCalibs)/sizeof(GEMCalibData); ++i ) {

        DetID_t did = nm.id(gGEMCalibs[i].planeName);

        _gemCalibs.emplace(did, gGEMCalibs + i);
    }
}

bool
APVCalcHitTimes::process_hit( EventID
                            , DetID_t did
                            , APVHit & cHit ) {
        
    // Get analog samples from APV chip
    const double & A0 = cHit.rawData.samples[0]
               , & A1 = cHit.rawData.samples[1]
               , & A2 = cHit.rawData.samples[2]
               ;
    
    double maxCharge(0), wCharge(0), charge(0), averageT(0);
    
    // Iterate over analog samples
    
    for (int i = 0; i < 3; ++i) {
		const double q = cHit.rawData.samples[i];
		wCharge += q * (i);
		charge  += q;
		if (maxCharge < q) maxCharge = q;
	}
	
	cHit.maxCharge = maxCharge;
	
	averageT = wCharge / charge;
	
	cHit.averageT = averageT;
    
    // Calculate A ratios
	cHit.a02 = A0/A2;
	cHit.a12 = A1/A2;
    
	// obtain APV detector ID without wire number {{{
    
    DetID stationID(did);
    
    if ( _mmKinID == stationID.kin() ) {
        
        WireID pl(stationID.payload());
        pl.unset_wire_no();
        stationID.payload(pl.id);
        // }}}
        auto entryIt = _mmCalibs.find(stationID);
        assert(entryIt != _mmCalibs.end());
        const MMCalibData * cMMCalibsTemp = entryIt->second;
        
        if (A2 > 0) {
            // Calculate hit time ratios and errors
            if ( cMMCalibsTemp->a02timeParam[0] > cHit.a02 && 
                                                  cHit.a02 != 0 ) {
                                                      
                cHit.t02 = _get_hit_timeMM( did, cHit, cHit.a02, 0 );
                cHit.t02sigma = _get_hit_sigmaMM( did, cHit, cHit.a02, 0 ); }			
            
            if ( cMMCalibsTemp->a12timeParam[0] > cHit.a12 && 
                                                  cHit.a12 != 0 ) {
                                                      
                cHit.t12 = _get_hit_timeMM( did, cHit, cHit.a12, 1 );
                cHit.t12sigma = _get_hit_sigmaMM( did, cHit, cHit.a12, 1 ); }
        }
    } else if ( _gmKinID == stationID.kin() ) {
        
        WireID pl(stationID.payload());
        pl.unset_wire_no();
        stationID.payload(pl.id);
        
        // }}}
        auto entryIt = _gemCalibs.find(stationID);
        assert(entryIt != _gemCalibs.end());
        const GEMCalibData * cGEMCalibsTemp = entryIt->second;

        // Reverse sign and add fixed constant in order to be
        // compatible with new time calculation
        const double tcsT0 = 40.;

        if (A2 > 0) {
                    // Calculate hit time ratios and errors
            if ( cHit.a02 > 0 && ( cGEMCalibsTemp->a02timeParam[4] / cHit.a02  >= 1 ) &&
                       cHit.a02 >= cGEMCalibsTemp->a02timeParam[0] &&
                       cHit.a02 <= cGEMCalibsTemp->a02timeParam[1] ) {
                
                cHit.t02 = cGEMCalibsTemp->a02timeParam[2] +
                           cGEMCalibsTemp->a02timeParam[3] *
                           ::log(cGEMCalibsTemp->a02timeParam[4] / cHit.a02);
                                                 
                cHit.t02sigma = 17.;
                cHit.t02 = -1. * cHit.t02 + tcsT0; }
                
            if ( cHit.a12 > 0 && ( cGEMCalibsTemp->a12timeParam[4] / cHit.a12  >= 1 ) &&
                       cHit.a12 >= cGEMCalibsTemp->a12timeParam[0] &&
                       cHit.a12 <= cGEMCalibsTemp->a12timeParam[1] ) {
                
                cHit.t12 = cGEMCalibsTemp->a12timeParam[2] +
                           cGEMCalibsTemp->a12timeParam[3] *
                           ::log(cGEMCalibsTemp->a12timeParam[4] / cHit.a12);
                                                 
                cHit.t12sigma = 17.;
                cHit.t12 = -1. * cHit.t12 + tcsT0; }
        }
        else { _set_hit_erase_flag(); }
    }
    
    if ( cHit.t02 != 0 && cHit.t12 != 0 ) {
		
		cHit.time = ( cHit.t02 / pow(cHit.t02sigma,2) )
							 + ( cHit.t12 / pow(cHit.t12sigma,2) );
							 
		cHit.timeError = ( 1 / pow(cHit.t02sigma,2) )
							 + ( 1 / pow(cHit.t12sigma,2) );
							 
		cHit.time = cHit.time / cHit.timeError;
		
		cHit.timeError = ( 1 / sqrt( cHit.timeError ) );
		
		if (cHit.timeError > 100 ) { _set_hit_erase_flag(); }
		
	} else { _set_hit_erase_flag(); }
	
	if ( _mmKinID == stationID.kin() ) {
		// TEMPORARY FUNCTION TO CALCULATE HIT TIME RISE 
		cHit.timeRise = _get_hit_timeRise( did, cHit, cHit.a02, cHit.time );
	}
	
    return true;
}

double
APVCalcHitTimes::_get_hit_timeMM( DetID_t did , const APVHit & cHit, double & ratio, int mode ) const {
	
	// obtain APV detector ID without wire number {{{
	DetID stationID(did);
	WireID pl(stationID.payload());
	pl.unset_wire_no();
	stationID.payload(pl.id);
	// }}}
	auto entryIt = _mmCalibs.find(stationID);
	assert(entryIt != _mmCalibs.end());
    const MMCalibData * cMMCalibs = entryIt->second;
    
    double r0(0), t0(0), a0(0);
    
    if (mode == 0) {
		r0 = cMMCalibs->a02timeParam[0]; t0 = cMMCalibs->a02timeParam[1];
		a0 = cMMCalibs->a02timeParam[2];	
	} else if (mode == 1) {
		r0 = cMMCalibs->a12timeParam[0]; t0 = cMMCalibs->a12timeParam[1];
		a0 = cMMCalibs->a12timeParam[2];	
	}
	
	return t0 + a0 * ::log((r0 / ratio) - 1); 
}

// TEMPORARY FUNCTION
double
APVCalcHitTimes::_get_hit_timeRise( DetID_t did , const APVHit & cHit, double & ratio, double time ) const {
	
	// obtain APV detector ID without wire number {{{
	DetID stationID(did);
	WireID pl(stationID.payload());
	pl.unset_wire_no();
	stationID.payload(pl.id);
	// }}}
	auto entryIt = _mmCalibs.find(stationID);
	assert(entryIt != _mmCalibs.end());
    const MMCalibData * cMMCalibs = entryIt->second;
    
    double r0(0), t0(0), a0(0);
    
    r0 = cMMCalibs->a02timeParam[0]; t0 = cMMCalibs->a02timeParam[1];
	a0 = cMMCalibs->a02timeParam[2];	
	
	return (time - t0) / ::log((r0 / ratio) - 1); 
}

double
APVCalcHitTimes::_get_hit_sigmaMM( DetID_t did , const APVHit & cHit, double & ratio, int mode ) const {
    
    // obtain APV detector ID without wire number {{{
	DetID stationID(did);
	WireID pl(stationID.payload());
	pl.unset_wire_no();
	stationID.payload(pl.id);
	// }}}
	auto entryIt1 = _mmCalibs.find(stationID);
	assert(entryIt1 != _mmCalibs.end());
    const MMCalibData * cMMCalibs = entryIt1->second;
    
	auto entryIt2 = _mmWireCalibs.find(stationID);
	assert(entryIt2 != _mmWireCalibs.end());
    const MMWireCalibData * cMMWireCalibs = entryIt2->second;
    
    double r0(0), a0(0);
    double r02(0), t02(0), a02(0), r0t0(0), r0a0(0), t0a0(0);
    double sigmaN(0), sigmaR(0), sigmaT(0), dtdr(0), dtda0(0), dtdt0(0), dtdr0(0);
    double A(0), B(0);
    
    // TODO: Check wire number type
    auto wireNo = cHit.rawData.wireNo;
	
	sigmaN = cMMWireCalibs->chCalib[wireNo];	
    
    if (mode == 0) {
		r0 = cMMCalibs->a02timeParam[0]; 
		a0 = cMMCalibs->a02timeParam[2];
		
		r02 = cMMCalibs->a02timeError[0]; t02 = cMMCalibs->a02timeError[1];
		a02 = cMMCalibs->a02timeError[2]; r0t0 = cMMCalibs->a02timeError[3];
		r0a0 = cMMCalibs->a02timeError[4]; t0a0 = cMMCalibs->a02timeError[5];
		
		A = cHit.rawData.samples[0];
		B = cHit.rawData.samples[2];
				
	} else if (mode == 1) {
		r0 = cMMCalibs->a12timeParam[0];
		a0 = cMMCalibs->a12timeParam[2];

		r02 = cMMCalibs->a12timeError[0]; t02 = cMMCalibs->a12timeError[1];
		a02 = cMMCalibs->a12timeError[2]; r0t0 = cMMCalibs->a12timeError[3];
		r0a0 = cMMCalibs->a12timeError[4]; t0a0 = cMMCalibs->a12timeError[5];
		
		A = cHit.rawData.samples[1];
		B = cHit.rawData.samples[2];
	}

	sigmaR = ( sigmaN + (1 / sqrt(12) ) ) *
	         ( ( sqrt(pow(A,2) + pow(B,2)) ) / pow(B,2) );
	         
	dtdr =   ( a0 * r0 ) /
	         pow(ratio,2) * ( r0 / ratio - 1 );
	         
	dtda0 = ::log(( r0 / ratio )-1);
	
	dtdt0 = 1;
	
	dtdr0 = a0 / ( ratio * ( r0 / ratio - 1 ) );
	
	sigmaT = sqrt( ( pow(dtdr,2) * pow(sigmaR,2) ) + ( pow(dtdr0,2) * r02 )
	             + ( pow(dtdt0,2) * t02 ) + ( pow(dtda0,2) * a02 ) 
	             + 2 * ( dtdt0 * dtdr0 * r0t0 ) + 2 * ( dtda0 * dtdr0 * r0a0 )
	             + 2 * ( dtda0 * dtdt0 * t0a0 ) );
	
	return sigmaT;
}

}

REGISTER_HANDLER( APVCalcHitTimes, banks, ch, cfg
                , "Calculates time and errors for APV-based detectors" ) {
    return new handlers::APVCalcHitTimes(ch, aux::retrieve_det_selection(cfg));
}

}

