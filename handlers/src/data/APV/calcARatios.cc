#include "data/APV/calcARatios.hh"

namespace na64dp {
namespace handlers {

APVCalcARatios::APVCalcARatios( calib::Dispatcher & ch
                              , const std::string & only
                              ) : AbstractHitHandler<APVHit>(ch, only) {}

/** If $A_2$ is null or NaN, the `APVHit::a12` will be left intact.
 *  If $A_3$ is null or NaN, the `APVHit::a13` will be left intact.
 *
 * \returns always `true`.
 */
bool
APVCalcARatios::process_hit( EventID
                           , DetID_t
                           , APVHit & cHit ) {
    const double & A0 = cHit.rawData.samples[0]
               , & A1 = cHit.rawData.samples[1]
               , & A2 = cHit.rawData.samples[2]
               ;
    if( A0 && ! isnan(A2) )
        cHit.a02 = A0/A2;
    if( A1 && ! isnan(A2) )
        cHit.a12 = A1/A2;
    return true;
}

}

REGISTER_HANDLER( APVCalcARatios, banks, ch, yamlNode
                , "Calculates a12 and a23 ratios for APV-based detectors" ) {
    return new handlers::APVCalcARatios(ch, aux::retrieve_det_selection(yamlNode));
}

}

