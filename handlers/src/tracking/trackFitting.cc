#include "tracking/trackFitting.hh"

#ifdef GenFit_FOUND

#include <EventDisplay.h>

#include <Track.h>
#include <AbsTrackRep.h>
#include <RKTrackRep.h>

// Kalman reference fitter routine and associated info
#include <KalmanFitterRefTrack.h>
#include <KalmanFitter.h>
#include <DAF.h>

#include <KalmanFitterInfo.h>

namespace na64dp {
namespace handlers {

TrackFitting::TrackFitting( calib::Dispatcher & cdsp
                          , const std::string & only
                          , int minIter
                          , int maxIter
                          , double minMomCut
                          , double maxMomCut
                          , int verbose )
        : AbstractHandler()
        , _minIter(minIter)
        , _maxIter(maxIter)
        , _minMomCut(minMomCut)
        , _maxMomCut(maxMomCut)
        , _verbose(verbose) {}

TrackFitting::ProcRes
TrackFitting::process_event(Event * evPtr) {
    
    /* Unfortunatly we can't use process hit for tracks since it
     * has no DetID_t. This handler is the redesign of original
     * idea of implementation of genfit into pipeline structure.
     * However, it turns out, that the genfit has ownership over
     * most of its objects and it creates memleakage as well as
     * numerous segfaults.
     */
        
	genfit::AbsKalmanFitter * fitter
			= new genfit::DAF(false);

	fitter->setMinIterations( _minIter ); 
	fitter->setMaxIterations( _maxIter );
    
    std::cout << (evPtr->tracks).size() << std::endl;
    
    for ( auto & trackCand : evPtr->tracks ) {
		
		Track & track = (*trackCand.second);
		
		//std::cout << track.pdg << std::endl;
		
		genfit::AbsTrackRep * rep = new genfit::RKTrackRep(track.pdg);
		
		TVector3 pos( track.seed[0]
		            , track.seed[1]
		            , track.seed[2] );
		
		TVector3 mom( track.mom[0]
		            , track.mom[1]
		            , track.mom[2] );
		
		genfit::Track * gTrack = new genfit::Track( rep, pos, mom );
		
		for ( auto & tp : track.trackPoints ) {
			genfit::TrackPoint * gTP = new genfit::TrackPoint((*tp).genfitTrackPoint);
			gTrack->insertPoint( gTP );
		}
		
		try {
			fitter->processTrack( gTrack );
							
			gTrack->checkConsistency();
		}
		catch(genfit::Exception& e) {
			std::cerr << e.what();          
		}
		
		
		genfit::FitStatus* fit = gTrack->getFitStatus();
        
        const genfit::MeasuredStateOnPlane * stLast = &(gTrack->getFittedState(0));
        
        const int particle = gTrack->getCardinalRep()->getPDG();    
          
        #if 0  
        if ( fit->isFitConverged() ) {
            if ( fit->isFitConvergedFully() ) {
                std::cout << "GenFit track converged in all points" << std::endl;
            }
            else if ( fit->isFitConvergedPartially() ) {
                std::cout << "Track failed to converge in all points. " 
                          << "Number of failed points: " 
                          << fit->getNFailedPoints() << std::endl;
            }
        } else {
            std::cout << "Track is not converged. Next track! " << std::endl;
            //continue;
        }
		
        std::cout << "GenFit track" << " particle PDG = " << particle
				  << " number of points = " << track.genfitTrackRepr->getNumPoints()
                  << " momentum = " << stLast->getMomMag()
                  << " fit converged = " << fit->isFitConverged()
                  << " fitted charge = " << fit->getCharge()
                  << " fit Chi^2 = " << fit->getChi2()
                  << " fit Ndf = " << fit->getNdf()
                  << " fit Chi^2/Ndf = " << fit->getChi2() / fit->getNdf() << std::endl;
		
		#endif
		
		track.momentum = stLast->getMomMag();
		
		track.chi2ndf = fit->getChi2() / fit->getNdf();
		
		delete gTrack;
		gTrack = nullptr;
		
    }
    
    delete fitter;
	fitter = nullptr;
	
    return kOk;
}

REGISTER_HANDLER( TrackFitting, banks, ch, cfg
                , "Handler to perform fitting of track candidates" ) {
    
    return new TrackFitting( ch
                           , aux::retrieve_det_selection(cfg)
                           , cfg["iterations"][0].as<int>()
                           , cfg["iterations"][1].as<int>()
                           , cfg["momCut"][0].as<double>()
                           , cfg["momCut"][1].as<double>()
                           , cfg["verbose"].as<int>()
                           );
}
}
}
#endif  // defined(GenFit_FOUND)
