#include "tracking/trackDestruct.hh"

#ifdef GenFit_FOUND

#include <EventDisplay.h>

#include <Track.h>

namespace na64dp {
namespace handlers {

TrackDestruct::TrackDestruct( calib::Dispatcher & cdsp
                              , const std::string & only
                              , ObjPool<Track> & bank
                              )
        : AbstractHitHandler<TrackPoint>(cdsp, only)
        , TDirAdapter(cdsp)
        , _tracksBank(bank) {
}

TrackDestruct::ProcRes
TrackDestruct::process_event(Event * evPtr) {
    
    Track & track = *(evPtr->tracks.begin()->second);

    delete track.genfitTrackRepr;
    
    return kOk;
}

void
TrackDestruct::finalize(){
    //genfit::EventDisplay::getInstance()->open();
}

REGISTER_HANDLER( TrackDestruct, banks, ch, cfg
                , "Handler to destruct genfit::Track" ) {
    
    return new TrackDestruct( ch
                             , aux::retrieve_det_selection(cfg)
                             , banks.of<Track>() 
                             );
}
}
}
#endif  // defined(GenFit_FOUND)
