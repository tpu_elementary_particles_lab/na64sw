#include "tracking/trackSeed.hh"

#include "na64detID/wireID.hh"

#ifdef GenFit_FOUND

namespace na64dp {
namespace handlers {

TrackSeed::TrackSeed( calib::Dispatcher & ch
                    , const std::string & only
                    , ObjPool<TrackPoint> & obTP
                    , ObjPool<Track> & bank
                    , double magPosition
                    , int missingHit )
		: AbstractHitHandler<TrackPoint>(ch, only)
		, _magPosition(magPosition)
		, _missingHit(missingHit) {}

bool
TrackSeed::process_hit( EventID
					  , DetID_t did
                      , TrackPoint & tp ) {

	// allows to ignore missing {proj}
	//DetID station(did);
	
	std::map<std::string, std::string> substDict;
	naming().append_subst_dict_for( did, substDict );
	std::string station = util::str_subst( "{kin}{statNum}", substDict, true );	

	// Create cells for a cellular automaton in up and downstreams
	// For test only for x planes. Sorting is based on z position
	
	Cell cell;
	cell.trackPoint = _current_event().trackPoints.pool().get_ref_of(tp);
	cell.planeName = did;
	cell.name = station;
	cell.x = tp.gR[0];
	cell.y = tp.gR[1];
	cell.z = tp.gR[2];
	cell.state = 1;
	cell.update = false;
	cell.charge = tp.charge;
		
	auto idxIt = _cells.find( cell.z );
	if( _cells.end() == idxIt ) {
		auto ir = _cells.emplace( cell.z, CellMap() );
		assert( ir.second );
		idxIt = ir.first;
	}
			
	CellMap & cellByDet = idxIt->second;
	
	cellByDet.emplace( did, cell );
		   
	return true;
}

TrackSeed::ProcRes
TrackSeed::process_event(Event * evPtr) {
	
	AbstractHitHandler<TrackPoint>::process_event( evPtr );
	
	std::vector<std::vector<Cell>> downCells;
	std::vector<std::vector<Cell>> upCells;

	// We are using multimap to sort our trackpoints in correct order
	// there is possible much more elegante solution
	
	for ( auto it : _cells ) {
		_create_space_points( it.second, downCells, upCells);
	}
			
	// main routine
	_cellular_automaton(downCells);
	_cellular_automaton(upCells);
	
	_cells.clear();
	
	downCells.clear();
	upCells.clear();
		
	return kOk;

}


void
TrackSeed::_create_space_points( CellMap & map 
							   , std::vector<std::vector<Cell>> & spDown
							   , std::vector<std::vector<Cell>> & spUp  ) {
	
	std::vector<Cell> pointsInDownLayer;
	std::vector<Cell> pointsInUpLayer;
	
	// Collect every point in detector plane
	for ( auto it : map ) {
		//std::cout << naming()[it.first] << std::endl;
		if ( it.second.z > _magPosition ) {
			it.second.layer = spUp.size();
			pointsInUpLayer.push_back( it.second );
		} else {
			it.second.layer = spDown.size();
			pointsInDownLayer.push_back( it.second );
		}		
	}
	
	if ( !pointsInUpLayer.empty() ) {
		spUp.push_back( pointsInUpLayer );
	}
	
	if ( !pointsInDownLayer.empty() ) {
		spDown.push_back( pointsInDownLayer );
	}
	
	pointsInDownLayer.clear();
	pointsInUpLayer.clear();
}

void
TrackSeed::_calculate_cell_state( std::vector<Cell> & curCell
							    , std::vector<Cell> & prevCell ) {
	
	// Cut for maximal distance between layers.
	// Seems to be correct but might be optimazed
	double maxDistance(400);
	double maxAngle(0.05); // approximately 2.5 Degrees
	
	double distance, brAngleX, brAngleY, x, y, z;
			
	// Function to update cell states in neighbour layers.
	// As we have possibility to have missing hits in detector
	// layers, we might want tolerate up to several missing hits
	
	for ( auto & curPoint : curCell ) {
		for ( auto & prevPoint : prevCell ) {
			
			distance = abs( curPoint.z - prevPoint.z );
			
			x = ( curPoint.x - prevPoint.x );
			y = ( curPoint.y - prevPoint.y );
			z = ( curPoint.z - prevPoint.z );
				
			brAngleX = atan2(x, z);
			brAngleY = atan2(y, z);			
					
			if ( distance < maxDistance && abs(brAngleX) < maxAngle && abs(brAngleY) < maxAngle) {
				// If at lease one cell updated it state we continue
				if ( curPoint.state == prevPoint.state ) 
					curPoint.update = true;	
			}
		}
	}
}


bool
TrackSeed::_update_cell_state( std::vector<std::vector<Cell>> & cell ) {

	bool updateInStep(false);
	// Itterate over all of detector planes (aka superlayers)
	// and update status of cells. Otherwise: return false if
	// system in stable state.
	
	for ( unsigned int layer = 0
	    ; layer < cell.size()
	    ; ++layer ) {

		for ( auto & hit : cell[layer] ) {
			// assign all hits to certain layer
						
			if ( hit.update ) {
				updateInStep = true;
				hit.state++;
				hit.update = false;
			}
		}
	}
	return updateInStep;
}


#if 0
// TODO: Continue with this function
void
TrackSeed::_track_seeding( std::vector<std::vector<Cell>> & cell ) {
	
	std::vector<Cell> tempVector;
	
	//std::vector<Cell> trackSeed;
	
	for ( auto it : cell ) {
		for ( auto ir : it ) {
			tempVector.push_back(ir);
		}
	}
	// sort vector with respect to highest cell - state
	std::sort(tempVector.begin(), tempVector.end(), [](Cell a, Cell b) {
        return a.state > b.state;
    });
    
    for ( auto & it : tempVector ) {
		std::cout << it.state << std::endl;
	}

	std::vector<Cell> trackCand;
	
	// Iterate for all of the cells;
	for ( unsigned int layer = cell.size(); layer > 0; --layer ) {
		
		// consider one cell until all of the possible lines will not be accounted
		for ( unsigned int cur = 0 ; cur < cell[layer].size(); cur++ ) {
			// check if this cell state doesn't equal to 1
			if ( cell[layer][cur].state != 1 ) {
				// check cells in first and second neighborhood layers
				
				bool hasMatch(true);
				
				while ( cont ) {
					
					if (cell[la
					
				
				
				
				
				
				
				for ( auto prev1 : cell[layer-1] ) {
					if ( (cell[layer][cur].state == prev1.state - 1) && cell[layer-1].size() == 1 ) {
						trackCand.push_back( prev1.state );
					}
				}
					
				for ( auto prev2 : 
				
}
#endif


void
TrackSeed::_cellular_automaton( std::vector<std::vector<Cell>> & cell ) {

	// 100 is a randomly choosed value to ensure that the system will
	// stabilzed in computationally acceptable time
	int numOfSteps(100);
	
	unsigned int numSuperLayers(cell.size());
	
	bool updateInStep(false);
			
	// Cycle for time evolution of our dynamic system
	
	for ( int k = 0; k < numOfSteps; ++k ) {
		
		for ( unsigned int sLayer = 1; sLayer < numSuperLayers; ++sLayer ) {
								
			/* for each space-point the automaton finds its neighbour
			 * in the previous layer based on certain criteria.
			 * For now we will use angle and distance to consider "real" hit/
			 * https://www.star.bnl.gov/~gorbunov/main/node24.html
			 */
		
			_calculate_cell_state( cell[sLayer], cell[sLayer-1] );
			
			// option to tolerate one missing hit
			if ( sLayer != 1 ) {
				_calculate_cell_state( cell[sLayer], cell[sLayer-2] );
			}
			
			if ( sLayer > 2 ) {
				//_calculate_cell_state( cell[sLayer], cell[sLayer-3] );
			}
	
		}
		// After end of the loop - update status of all cells
		updateInStep = false;
		
		updateInStep = _update_cell_state( cell );
	
		if ( !updateInStep ) {
			//std::cout<< "Cellular automaton reached final state in " << k+1
		    // 		 << " steps" << std::endl;
			break;
		}
	}
	
	#if 1
	for ( auto & superLayer : cell ) {
		for ( auto & hit : superLayer ) {
				std::cout << "Detector: " << hit.name << ","
						  << "hit state: " << hit.state << "," 
						  << "hit layer: " << hit.layer << std::endl;
		}
	}
	#endif
	
	//_track_seeding( cell );
	
}

}

REGISTER_HANDLER( TrackSeed, banks, ch, cfg
                , "Handler for hit processing and simple itterative seeding" ) {
    
    return new handlers::TrackSeed( ch
                             , aux::retrieve_det_selection(cfg)
                             , banks.of<TrackPoint>()
                             , banks.of<Track>()
                             , cfg["magPosition"].as<double>()
                             , cfg["missingHits"].as<int>(0) );
}
}

#endif  // defined(GenFit_FOUND)
