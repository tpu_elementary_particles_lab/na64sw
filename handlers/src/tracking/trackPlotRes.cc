#include "tracking/trackPlotRes.hh"

#include <iostream>
#include <fstream>

#ifdef GenFit_FOUND

#include <Track.h>
#include <MeasuredStateOnPlane.h>

#include <KalmanFitterInfo.h>

#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TApplication.h>
#include <TH2F.h>

namespace na64dp {
namespace handlers {

TrackPlotRes::TrackPlotRes( calib::Dispatcher & cdsp
                          , const std::string & only
                          , ObjPool<Track> & bank
                              )
        : AbstractHitHandler<TrackPoint>(cdsp, only)
        , TDirAdapter(cdsp)
        , _tracksBank(bank) {

    //resFile.open("residuals.txt");
    
    //resFile << "Residuals for NA64 2017 run cdr01001-003199.dat\n";
}

TrackPlotRes::~TrackPlotRes() {
	for(auto p : _histograms) {
		delete p.second;
    }
}

TrackPlotRes::ProcRes
TrackPlotRes::process_event(Event * evPtr) {
    
    Track & track = *(evPtr->tracks.begin()->second);
    
    genfit::Track * cTrack = track.genfitTrackRepr;
       
    for (unsigned int i = 0; i < cTrack->getNumPoints(); ++i) {
    
        genfit::TrackPoint* tp 
				= cTrack->getPointWithFitterInfo( i 
                                                , cTrack->getCardinalRep());
    
		genfit::AbsFitterInfo * fitterInfo 
				= tp->getFitterInfo(cTrack->getCardinalRep());
		
		genfit::KalmanFitterInfo* kFitterInfo 
				= static_cast<genfit::KalmanFitterInfo*>(fitterInfo);
		
		genfit::AbsMeasurement * measurement 
					= tp->getRawMeasurement();

		DetID_t did = measurement->getDetId();
		
		auto it = _histograms.find(did);
		if ( _histograms.end() == it ) {
			std::string hstName = "residuals";
			std::string hstDescription = "residuals of reconstruction";
			auto substCtx = subst_dict_for(did, hstName);
            std::string description = util::str_subst( hstDescription, substCtx );
            auto p = dir_for( did, substCtx, _overridenPath);
            p.second->cd();
            TH2F * newHst = new TH2F( p.first.c_str()
                                    , description.c_str()
                                    , 500, -10, 10
                                    , 500, -10, 10 );
            it = _histograms.emplace(did, newHst).first;
		}

		unsigned int nMeasurements = kFitterInfo->getNumMeasurements();
		
		
		for ( unsigned int i = 0; i < nMeasurements; ++i ) {
        
			const genfit::MeasurementOnPlane & residual 
					= kFitterInfo->getResidual(i);
			
			TVectorD res = residual.getState();
			TMatrixDSym covM = residual.getCov();
			
			it->second->Fill( res[0], res[1] );
		}
		
		
    }
    
    return kOk;
}

void
TrackPlotRes::finalize(){
	
	for( auto idHstPair : _histograms ) {
		tdirectories()[idHstPair.first]->cd();
		idHstPair.second->Write();
	}    
    
    //resFile.close();

}

REGISTER_HANDLER( TrackPlotRes, banks, ch, cfg
                , "Handler to plot residuals" ) {
    
    return new TrackPlotRes( ch
                             , aux::retrieve_det_selection(cfg)
                             , banks.of<Track>() 
                             );
}
}
}
#endif  // defined(GenFit_FOUND)
