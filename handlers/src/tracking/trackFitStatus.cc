#include "tracking/trackFitStatus.hh"

#ifdef GenFit_FOUND

// Tracking engine and track representatives
#include <AbsTrackRep.h>
#include <RKTrackRep.h>
#include <Track.h>
#include <TrackPoint.h>

// Measurements, planes and states on plane
#include <MeasuredStateOnPlane.h>
#include <StateOnPlane.h>

// Kalman reference fitter routine and associated info
#include <KalmanFitterRefTrack.h>

namespace na64dp {
namespace handlers {

TrackFitStatus::TrackFitStatus( calib::Dispatcher & cdsp
                              , const std::string & only
                              , ObjPool<Track> & bank
                              , int verbose )
        : AbstractHitHandler<TrackPoint>(cdsp, only)
        , TDirAdapter(cdsp)
        , _tracksBank(bank)
        , _verbose(verbose) {}

TrackFitStatus::ProcRes
TrackFitStatus::process_event(Event * evPtr) {
    
    Track & track = *(evPtr->tracks.begin()->second);
    
    if (_verbose > 0) {
                    
        genfit::FitStatus* fit = track.genfitTrackRepr->getFitStatus();
        
        genfit::MeasuredStateOnPlane stLast = track.genfitTrackRepr->getFittedState(0);
        
        const int particle = track.genfitTrackRepr->getCardinalRep()->getPDG();    
            
        if ( fit->isFitConverged() ) {
            if ( fit->isFitConvergedFully() ) {
                std::cout << "GenFit track converged in all points" << std::endl;
            }
            else if ( fit->isFitConvergedPartially() ) {
                std::cout << "Track failed to converge in all points. " 
                          << "Number of failed points: " 
                          << fit->getNFailedPoints() << std::endl;
            }
        } else {
            std::cout << "Track is not converged. Next track! " << std::endl;
            return kDiscriminateEvent;
        }
		std::cout << "ECAl energy deposition: " << evPtr->ecalEdep << std::endl;
        std::cout << "GenFit track" << " particle PDG = " << particle
				  << " number of points = " << track.genfitTrackRepr->getNumPoints()
                  << " momentum = " << stLast.getMomMag()
                  << " fit converged = " << fit->isFitConverged()
                  << " fitted charge = " << fit->getCharge()
                  << " fit Chi^2 = " << fit->getChi2()
                  << " fit Ndf = " << fit->getNdf()
                  << " fit Chi^2/Ndf = " << fit->getChi2() / fit->getNdf() << std::endl;
    }

    return kOk;
}

REGISTER_HANDLER( TrackFitStatus, banks, ch, cfg
                , "Handler for main tracking routine" ) {
    
    return new TrackFitStatus( ch
                             , aux::retrieve_det_selection(cfg)
                             , banks.of<Track>()
                             , cfg["verbose"].as<int>()  
                             );
}
}
}
#endif  // defined(GenFit_FOUND)
