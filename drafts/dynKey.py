
count = 1e7
keyLen = 23

m = {}

while count:
    k = (rand() for i in range(keyLen))
    if k not in m:
        m[k] = 1
    else:
        m[k] += 1
    count -= 1

