# TODO

Issues and plans of somewhat fundamental nature that can not be embedded into
auto-generated TODO-list are listed here.

* The `na64ee` library introduces too much fuss with the initial deployment.
Consider adding it as custom ebuild of `q-crypt-hep` overlay. This will be
reasonable to implement together with support of
[`lazyc++`](https://www.lazycplusplus.com/) and custom ebuild for `p348-daq`.
