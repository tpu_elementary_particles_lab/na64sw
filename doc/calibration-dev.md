# NA64DP Calibration Subsystem Concepts {#NA64DPCalibrationConcepts}

By term "calibration data" (in na64sw) we define here *any kind of data that
depends on the event number* including not only the typical kinds of calibration
information, but also such things as detector naming and geometry.

## Concepts

* We expect that full set of all the calibration data needed for the every kind
of processing has not be kept in RAM entirely. From this assumption one may
immediately derive a necessity of some kind of runtime "state" or "cache" where
the currently loaded information is kept (i.e. the amount of calibration data
being actually "loaded" from the "existing" superset).
* This "state" has to be synchronized with the event number under
consideration. Typical source provoking state change -- data source
object representation that notifies "state" with the newly acquired event's ID.
For the MC case this is the hooks that are called when new event arrives (i.e.
`G4UserEventAction` subclass, for Geant4 API).
* "State" maintains a subscription for entities interested in particular types
of calibration information via "observer-notifier" (or "pub/sub") pattern. I.e.
changes in state (induced by changing the event ID) is propagated among objects
who are interested in certain pieces of information with subscription mechanism.

So far we have the following core structure for calibration data maintenance
at a runtime:

* `Manager` class maintains the subscription;
* `iCalibDataLoader` performs loading of the prescribed data (means);
* `iCalibRunIndex` interface implementation defines which kind of the
calibration information have to be updated for every event and with which
means by forwarding the update call to concrete `iCalibDataLoader`'s subclass
instance.

Resulting system built on these classes is still quite generic and foresees
some sophisticated usecases like keeping the *run indexes* at the database,
overriding it with configuration file (both within a `iCalibRunIndex`) and
involve a mixed loading scheme where the data may be retrieved from ASCII CSV
file, local database, ROOT file or fetched with RESTful API.

## Use Case: Intorducing New Calibration Data Type

1. Define the calibration data type. It may be a simple type, POD struct or
complex class. Single instance of this type must contain only the data needed
for certain *validity period*.

2. Use (existing or implement new) loader. In case of adding new loader
one have to subclass the vanilla `iCalibDataLoader` (and add the appropriate
entry within run index). In more common case one may rely on a generic loader
and subclass `GenericLoader::AbstractCalibDataIndex`. In case of simple data
trivially constructible from YAML description consider usage of
`SimpleYAMLDataIndex<T>` that defines stateless, single-function conversions.
For more complex or a bit more bulky data, consider subclassing from
`iCSVFilesDataIndex<T, TupleTypesT ...>` -- users have to only define a
tuple-to-data-type conversion method in their subclass.

3. If you have subclassed the `iCalibDataLoader` -- instantiate your loader and
add it to the application's  manager with `Manager::add_loader()` method. If,
on the contrary, you have subclassed the `GenericLoader::AbstractCalibDataIndex`
instantiate and add the data index to `GenericLoader` instance
mentioning new data field (see, e.g., pipeline function). Defining a runtime
data loader constructor for YAML is somewhat tricky (a small
intermediate structure is used) -- refer to `calib::CalibInfoCtr`'s docs.

## Use Case: Using Calibration Sub-system in User Applications

The central object that user's app must maintain is an isntance of calibration
data manger: `na64dp::calib::Manager` object. It might be instantiated with no
additional preparations:

        na64dp::calib::Manager mgr;

Having this object, user's classes may then subscribe to calibration update.
E.g., having calibration data type, say `MyCalibData` and
class `MyCalibDataUser`, one may utilise the subscription by implementing
the `Observable<MyCalibData>::iObserver` interface, and calling
`subscribe()` method of the `Manager` instance:

        class MyCalibDataUser : public na64dp::util::Observable<MyCalibData>::iObserver {
            // ...
            MyCalibDataUser( na64dp::calib::Manager & mgr ) {
                mgr.subscribe<MyCalibData>(*this, "default");
            }
            // ...
        protected:
            virtual void handle_update( const MyCalibData & ) override;
            // ...
        };

Here `subscribe()` takes a string argument (`"default"` in example) that,
together with C++ type info, identifies the type of calibration entry.

Note that the calibration data user class has only one way to recieve the
updated data instance -- a protected method `handle_update()`. This reference
is guaranteed to be valid until next `handle_update()` call, so
subscriber may safely cache it.

To make the `Manager` isntance do something meaningful, one have to bind the
*run index* instance.

## Range-overriding Run Index

The `RangeOverrideRunIndex` class implements a straightforward way of
mapping the event identifier over a subsets of calibration sources. In this
class we assume that certain calibration data object is valid *starting from*
some event ID *till* the next event ID that has another calibration data
object. This way a data signed with event ID `5` will override
(or *substitute*) the entity signed with event ID `2`. It is the simplest yet
useful mapping scheme to define range matching.

## Generic Loader

Most of the practical usecases do not require a level of generalization that
vanilla `iCalibDataLoader` offers (loading the data of any type for any event).

The `GenericLoader` class implements a calibration data loader with dynamic
composition. Once loading data of certain type is requested (by `load_data()`
method), it forwards execution to one of its *data index* instances
(subclassing `GenericLoader::AbstractCalibDataIndex`) that performs actual
loading. The "data index" instance is specific for certain data type while
`GenericLoader` is not (so `GenericLoader` instance is just a collection
of type-specific loaders) and that is the reason for this level of
composition.

New instances of data indexes are added by `GenericLoader::add_data_index()`
at the startup.

### YAML Calibration Info

The `iYAMLDataIndex<T>`, a template subclass of `GenericLoader::add_data_index()`
represents data index where information needed
to load calibration data for particular run is stored as YAML node (in RAM).
It might be either a entire input neede to construct a new calibration data
object (see `SimpleYAMLDataIndex<T>`), or just some description needed to load
a relatevely large pice of data (`iCSVFilesDataIndex`).

## Practical usage

We hereby assume that user is familiar with writing handler basics and now will
consider a following use-case:

1. The calibration data is needed in handler `MyHandler`
2. The calibration data is valid during the standard lifespan: from certain run
3. The calibration data object is defined by C-struct `MyCalibData` tagged as
`"myCalibTag"`.

Steps to accomplish the task:

1. Include the `MyCalibData` struct declaration into handler's definition and
introduce the updating hook by implementing the `iObserver` interface:

    class MyHandler : public na64dp::util::Observable<MyCalibData>::iObserver {
        protected:
            virtual void handle_update( const MyCalibData & ) override;
            // ...
        public:
            MyHandler( na64dp::calib::Manager & mgr ) {
                mgr.subscribe<MyCalibData>(*this, "myCalibTag");
            }
    };

2. Register the calibration loader ... 

(TODO)

