# Notes on TBName Mapping {#TBNameMapping}

Raw data in NA64 is provided in format treated by `DaqDataDecoding` (DDD). This
lib came from COMPASS (NA58) experiment and provides versatile integration with
hardware DAQ (data acquizition) system inherited by NA64 from COMPASS.

In this document the brief description on the hierarchy is given. For detailed
information, see the original code of NA58 DAQ -- the `DaqDataDecoding` library
is a part of CORAL software distribution.

## Hardware Electronics Chips

The key element of any DAQ system is the particular chip that provides
measurements in certain digital form for dispatching and subsequent storage.
One type of chip may serve a number of detectors.

E.g. the SADC (stands for "sampling ADC") is a chip widely used by both
experiments to acquire data from
numerous type of detectors: calorimeters, drift chambers, veto detectors and so
on. It mostly consists of two samplers (sampling rate is about 40ns) running in
parallel with shift of 20ns. Thus, singe SADC hip provides samling rate of
20ns. Typically, time window of SADC chip is limited by 32 samples.

Another important chip is APV. Originally developed for CMS detectors, it
serves for many experiments at CERN as an analog de-multiplexer gathering
signals from multiple inputs and transmitting their read-out by single output
line so that multiple inputs may be measured by one sampling ADC.

Assumption that we made during architecturing our software system is that one
detector is managed by a chip of certain type.

## Detector Naming

DDD has concept of "*TBName*". The *TBName* is a short string usually
corresponding to a single detector or assembly part of the detector. Particular
naming scheme is usually choosen by means of DAQ convenience. E.g. the
electromagnetic calorimeter of NA64 consists of two parts (preshower and main
part) with corresponding distinct TBNames: `ECAL0` for pre-shower part and
`ECAL1` for main.

Generally, TBName refers to a detector of certain type: `ECAL0`, `ECAL1` are
for electromagnetic calorimeters, `HCAL0`, `HCAL1`, `HCAL2`, `HCAL3` are for
hadronic calorimeter, `GM01X__`, `GM02Y__` are for GEMs (gas electronic
multiplier tracking detector) and so on.

So far, one may assume that TBName consists of at least two semantic units:
the detector *kin* (like `ECAL`, `GM`, `HCAL`, `DC`, etc) and the station
number. Third, optional part refers to arbitrary naming *postfix* (`X`, `Y__`,
`Ybl`, etc) bearing various assembly-specific information.

For example:

* `ECAL0` consists of detector *kin* name -- `ECAL` and *station number* -- `0`
* `MM03U` consists of three parts: *kin* -- `MM` (micromega detector),
*station number* -- `03`, and postfix -- `U` denoting that this entry is for
the U-projection plane identification.
* `ST03Ybl` (from NA58) is for straw detectors kin (`ST`) for station number
`03` and assembly part for Y-plane at its "bottom left" (`bl`).

Such a partitioning allows trigger hardware to refer to
certain part important for triggering, while at the same time leads to some
unambiguity in analysis tools, causing data treatment code to consider a
special case for multiple parts while it logically a single detector assembly.
Moreover, the `TBName` is rarely refers to finer partitioning. E.g.
calorimeters usually consists of cells and tracking detectors are the arrays of
wires. In native DDD it is a subject of *digits* -- an information about
triggered event part. Logically, however it is more about *location* of the hit
and thus should be affiliated to detector ID.

## Detectors Identifiers in NA64DP

In this software we propose another approach to identify detectors. Instead of
TBNames, the numerical identification is used. By the means of bit
partitioning, the full detector ID is immersed into a single integer identifier,
bearing information of *chip*, *kin*, *station number* and detector-specific
entity number that we further refer as *payload*. Note that the *payload* here
refers not to a detector digit (payload of hit information), but has to be
interpreted as a *payload* of detector ID.

The mapping of TBnames to numerical ids is controlled at a runtime from special
object providing coding shortcuts for conversion from names to numbers and
vise-versa (we foresee setup changes involving changes in TBNames naming
nomenclature).

## TBNames and Numerical ID Correspondance

\todo describe here a format of `nameMaps` object in `presets/calibration.yaml`
once it'll become settled

## Two-Staged Conversion Procedure

\todo describe here a role of `postfix` argument in
`TBNameMappings::detector_id_by_ddd_name()` and details on what is called
"full name" contrary to TBName once the per-detector selection procedure will
be finalized within handlers

## String Templates and TDirAdapter Class {#TDirAdapterDetails}

\todo write this up when the path naming templates will be clearly understood
and implemented not only for the particular detector planes, but rather for
detector assemblies and so on

# Detector Selection DSL {#DSuL}

To apply handlers to certain detectors, the small domain-specific language
is involved. To reflect the fact that this language does not impose
sophisticated functionality that one would expect from any artifical grammar
we some times call it "detector selection domain specific language" or DSuL
where the "u" stands for "micro-" (language).

The basic grammar is somewhat resembling C++ logic expressions where some
amount of variables are "defined" externally. Exact description of grammar
may be found at `presets/ds-dsl.y` file which contains assets needed to
automatically generate parsing and evaluation routines for this language.

Examples:

* `kin==MM && (station==7 || station==2)` -- filters out only the MuMegas hits
from stations #7 and #2.
* `kin==HCAL && number==2 && (xIdx==2 && yIdx==2) ` -- filters out only hits
from second module of hadronic calorimeter (HCAL) in cell 2x2.

One can notice that besides of common C-like logical expression syntax with
integer literals, there is an important knowledge necessary for user to compose
such expressions: list of definitions.

## DSuL: comparison expressions

Following comparison expressions are supported:

* EQALS `==`
* DOES NOT EQUAL `!=`
* GREATER `>`
* GREATER OR EQUAL `>=`
* LESSER `<`
* LESS OR EQUAL `<=`

This operators can not be applied to the result of an expression -- another
comparison or logic expression. I.e. the following statements are invalid:

* `(kin == GM) != 0` (might be reduced it to `kin == GM`)
* `!(kin == GM || kin == MM) == 0 && wire=12` (might be reduce it
to `!(kin == GM || kin == MM) && wire=12`).

This is deliberate choice since most of selector expressions used in practice
may be efficiently expressed with pure boolean algebra over value comparison
results.

## DSuL: operators

Following set of logic operators are supported (sorted by the precedence):

* Unary NOT (`!`) operator
* Binary AND (`&&`) operator
* Binary OR (`||`) operator
* Binary XOR (`^^`) operator

Note about XOR: C does not support this operator. At the places were its
meaning becomes handy common C practice suggests using `!=`. In our DSuL we
can not use this idiom as DSuL does not support comparison operators over logic
result.

## List of Definitions

All the chip (`SADC`, `APV`) and kin (`GM`, `MM`, `HCAL` etc) names defined in
detector naming mapping are available in the expression context as a variables
that can be resolved to their numeric identifier. Additionaly, the projection
identifier letters (`U`, `V`, `X`, `Y`) will also be resolved to their numeric
equivalent.

Besides of these identifiers, the following properties related to detector
identifier are available:

* `chip` -- will be resolved to current detector chip ID
* `kin` -- resolved to current detector kin ID
* `number` -- resolved to current detector station number
* (for SADC detectors sonly) `xIdx`, `yIdx` -- resolved to current
hit's X/Y indexes (typically, calorimeter cell)
* (for APV detectors only) `projection` -- projection identifier
* (for APV detectors only) `wireNo` -- APV detector wire number

