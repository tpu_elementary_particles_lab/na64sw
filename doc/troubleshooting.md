# Troubleshooting

### No network connection during docker build invocation

On some systems, during execution of `$ docker build -t hepfarm4ecal-edu .`
command an error may appear of the form: 

`!!! Error fetching binhost package info from 'http://hepsoft.crank.qcrypt.org/amd64.opt.20200222/' !!! <urlopen error [Errno 110] Connection timed out>`

Indicating that build container is failed to fetch the pre-compiled package
from remote repository. Try to provide `docker build` command with the
`--network=host` to disable docker network bridging during the build to
overcome this troble:

    $ docker build -t hepfarm4ecal-edu . --network=host

Alternatively, you might need to finely tune your docker networking. However,
this behavious seems to not affect the running container, so this
straightforward approach may be a better solution.

### An error with "Fix map file problem(s) first" prevents from reading raw data

If you have followed the standard container installation procedure, check that
the environment variable `NA64DP_CFG_ROOT` is set to current repository
path (in container it will be `/var/src/ecal-edu`).

The `DaqDataDecoding` needs .xml maps that for NA64 are usually supplied with
`na64-daq` repo. We, therefore, rely on standard installation scheme. If you
have followed the standard repository layout (and `na64-daq` lies at the same
level of current repo).

If standard sources layout is not the case, verify the configuration file
provided to application with `-i,--input-cfg` and availability of the file
defined by its parameter anmed `CORALMapsDir`.

### Annoying error messages of missing file entries appear on startup

Messages of the form

    Error in cling::AutoLoadingVisitor::InsertIntoAutoLoadingState:
       Missing FileEntry for GblTrajectory.h
       requested to autoload type gbl::GblData

are related to known issue which ROOT developers
"[expected](https://root-forum.cern.ch/t/error-in-cling-insertintoautoloadingstate/29347) to get rid only in 2019".
Nevertheless this messages most probably refer to GenFit headers and may
be cured with

    `export ROOT_INCLUDE_PATH=/usr/include/genfit`

on container (consider to add it to collector's `.bashrc`).

