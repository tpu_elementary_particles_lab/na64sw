# Handlers Developer Guidelines {#HandlersDevGuide}

Handlers development principles:

1. Each handler has to be intended for a single and clear purpose. Please, try
to avoid handlers that do few things. I.e. tasks of deriving some value based
on the event data and depicting it on a histogram would be better to keep in
two dedicated handlers: a deriving one and a plotting one, passing the
derived value via the event structure. This rule has resemblance to
the well-known [UNIX principle](https://en.wikipedia.org/wiki/Unix_philosophy),
and has a long history of confirmation by practical experience.
2. As a rule, the event structure must not contain (or refer to) data allocated
directly on heap. Instead, use the _banks_ object. Besides of the performance
reasons (using banks is generally faster than allocating on heap), this idea
helps to avoid leaks and unintended dependencies. See "Hit Banks Technique"
below for details and a [dedicated section](\ref NA64DPPMemModel) for even more
details with exhaustive rationale.
3. Try to avoid hardcoding values in the handlers. Any parameter, if possible,
must be provided via the configuration file. For instance, if handler plots a
histogram, the binning an ranges must be provided as configuration values to
constructor of this class, for the rare exception when they defined by some
hardware or physics features.

This principles are given as a recommendation for generic case. Practical task
often put certain restrictions to these principles, making handler developers
to prefer, for example, to put a parameterised discrimination within some
sophisticated algorithm to avoid significant performance loss or swelling of
the event object. Algorithm implemented by handler may also rely on some
externally-defined constraints (e.g. hardware design) in which case hardcoding
the values may be cheaper than making a dedicated config. Nevertheless one have
to take into account this consideration as _architectural principles_ when
making decisions on how to imlement certain data analysis feature.

## Note on Hit Banks Technique  {#HitBanksNotes}

The "banks" are merely the pre-allocated blocks of memory. Similar to what
is being called ["pool allocators"](https://en.wikipedia.org/wiki/Memory_pool)
they provide faster (re-)allocation of smaller memory pieces, providing better
performance with respect to pure-heap
allocation. More detailed overview of the corresponding memory model should be
given in a [dedicated section](\ref NA64DPPMemModel).

Involving them into pipeline processing has few side benefits:

1. Access from multiple threads is naturally isolated, concerning such a 
parallelism model that individual banks are allocated for each processing
thread devoted to single pipeline object.
2. Possibility for memory leaks at the level of events is almost eliminated. No
entity is responsible for freeng the banks after each event has been
processed -- all the objects associated with particular event are removed at
once.
3. Event processing becomes more deterministic because of reproducible layout
within each bank.

Significant drawback for hit banks technique is that one need to perform some
additional work for those banks to allocate/free the data (custom pointer
type). To minimize the impact we have tried to hide this usage under the
modern C++ syntax mixins (syntactic sugar).

Effectively, the data allocated using banks is still located on heap, but
management of the objects in memory is no more the kernel's task. Since object
descriptors are simpler and more deterministic, there is an opportunity to
significantly speed up the frequent re-creation of objects during data
processing.

# Pipeline Execution

... (refer here to a dedicated page about `na64dp-pipe` app)

# Writing Own Handler

Technically, the handler has to fullfill the following requirements to become
an entity available for insertion in the pipeline via standard NA64DP
procedures:

1. Has to be expressed as class inheriting `AbstractHandler` abstract base and
implementing its pure-virtual `process_event()` function
2. To be registered in a global index of handlers within the library (usually
with `REGISTER_HANDLER` macro).
3. To be linked within the application execution context.

There are multiple ways to accomplish these requirements implementing the task,
allowing the pipeline to be constructed within various application contexts
like a dedicated app for data processing, Python extesnion or any other user
application. For most common use case (add handler to `libna64dp.so`), the
easiest way would be:

1. Create declaration (`.hh` usually in `include/NA64DPHandlers`) and
implementation (`.cc` usually in `src/NA64DPHandlers`) files
2. Reference the implementation file path to `CMakeLists.txt` file in the root
of repository within the list of `NA64DP_LIB_SOURCES` variable.

For instance let's consider a generic-case handler first.

## Defining Generic Handler Class

Minimal code snippets for some handler named `MyHandler` is given in this
section. Disclaimer: it does not follow a couple of the traits of a
_good style_, but shall be pretty close to the way averaged user writes the
things, so most of the audience are probably familiar with these syntactic
constructions.

* declaration `include/NA64DPHandlers/MyHandler.hh`

~~~cpp
    #pragma once
    #include "NA64DPAbstractHandler.hh"

    using namespace na64dp;

    class MyHandler : public AbstractHandler {
    public:
        ProcRes process_event( Event * event ) {
            // ... your event processing code here
            return kOk;
        }
    };
~~~

It is a standard C++ header file. It is protected from repeatative inclusion
with `#pragma once` macro (but one may prefer `#ifndef`-approach), it include
the `NA64DPAbstractHandler.hh` header file for `AbstractHandler` declaration.
To simplify this code we have also imported `na64dp` namespace with `using`
keyword. Next we defined a class named `MyHandler` inherited from
`AbstractHandler` and implemented pure-virtual method `process_event()` within
our descendant class. The implemntation of `process_event()` does nothing
except for returning a special result code `kOk` to indicate that event
processing was done ormally.

* implementation `src/NA64DPHandlers/MyHandler.cc`

~~~cpp
    #include "NA64DPHandlers/MyHandler.hh"

    REGISTER_HANDLER( MyHandler, banks, ch, cfg, "A dummy handler" ) {
        return new MyHandler();
    }
~~~

In this standard C++ implementation file we have just included our header file
to provide compiler with implementation necessary for linking. Only uncommon
thing here is a `REGISTER_HANDLER()` and function body next to it (we will
discuss it below). Overall, this is a common C++ file, similar to what one
writes within a `main.cpp`. Handler developer may put arbitrary `#includes`
here, implement their own procedures and so on.

This code will define a generic purpose handler base available for all the
NA64DP routines. Its `process_event()` method will be called with every event
passing through the pipeline. After re-building the `na64dp` project (invoke
`make` in the build directory) the handler named `MyHandler` should appear in
the `na64dp-pipe -l` output and become a meaningful entry in the `.yaml`
pipe run-configuration files.

# REGISTER\_HANDLER Macro

`REGISTER_HANDLER` macro hides some amount of code that is responsible for
automated adding of the defined handler class into the registry available for
runtime creation.
This is a preprocessor macro meaning that it will be substituted by a C/C++
code by compiler after initial processing. Right after this macro a function
body that performs run-time construction is provided. It is expected that _this
function will allocate the handler of certain type on heap and return the
pointer to an instance_.

This macro accepts few arguments:

1. First argument denotes the name of the handler to be added. It is not
obligated to have a name similar to C++ class definition. But is desirable to
since users will deal with this name at a runtime an have to know at which
doc page or at which `.hh`/`.cc` files they may find it in case of troubles.
2. Second argument shall denote the name of an argument of type `HitBanks` that
is needed by some handlers at the construction time. In our example we do not
use it, but if one would implement a handler that performs dynamic memory
operations on the event instance, this object is very important. Name that
will be provided as a second argument here will be the name of variable
referencing `HitBanks` instance. See [notes on hit banks above](\ref HitBanksNotes)
for details on this technique.
3. Third argument will be the name of variable of type `iCalibHandle *`. This
variable refers to calibration handle that shall be available for certain
handlers in order them to retrieve calibration data. In our current example
we do not use it. Name that
will be provided as a third argument here will be the name of variable
referencing `HitBanks` instance.
4. Fourth argument is a name of configuration node variable. In NA64DP the
`yamlcpp` as a versatile configuration carrier, so this is a common mechanism
to configure many objects created a runtime.
5. Fifth argument is a description string that will be shown to users as a
context reference. It shall briefly describe what handler is intended for.
For example the `na64dp-pipe` application has an interface to show this
references by a special command-line option.

\todo more examples on registering handlers: simple ones, using the arguments,
registering template handlers

## Notes On the Generic Handler Class

For the sake of simplicity in the above example a couple of things were
omitted: placing the handler at the namespace and moving the implementation
of the `process_event()` method from declaration file to implementation file.

First, the good name space for handlers is `na64dp::handlers`. It is somehat a
standard place for them. One of the reasons why handlers have to have their own
namespace is to group them together at the auto-generated documentation page.
So instead of writing

~~~cpp
    using namespace na64dp;
~~~

one should rather write

~~~cpp
    namespace na64dp {
    namespace handlers {
    // ...
    }
    }
~~~

and put all the things within it (except for `#include`) between itnernal `{`
and `}`.

Second, the implementation of method(s) have to be placed at the implementation
file. Such a separation has a long history in C++ and comes with strong
benefits:

1. It improves readability of declarations files making the code easier to
understand (and debug, and maintain and so on)
2. It helps to avoid linkage clashes
3. It makes definitions to be _reentant_ meaning tha multiple implementation
files may access the same definition from single header file
4. It speeds up the compilation process (often significantly)

Taking into account this two points, the files should look like:

* Declaration

~~~cpp
    #pragma once
    #include "NA64DPAbstractHandler.hh"

    namespace na64dp {
    namespace handlers {

    class MyHandler : public AbstractHandler {
    public:
        ProcRes process_event( Event * event ) override;
    };

    }
    }
~~~

* Implementation

~~~cpp
    #include "NA64DPHandlers/MyHandler.hh"

    namespace na64dp {
    namespace handlers {

    AbstractHandler::ProcRes MyHandler::process_event(Event * event) {
        // ... users event data processing code here
        return kOk;
    }

    }

    REGISTER_HANDLER( MyHandler, banks, ch, cfg, "A dummy handler" ) {
        return new MyHandler();
    }

    }
~~~

Albeit this is a bare minima for composing a basic common-purpose handler,
typical custom handler is less generic by purpose in the sense we would like
to access some specific data wihin the event like hits being affiliated to
certain chip or detector. There are helper classes and specialized
`AbstractHandler`'s subclasses that complements its original functionality with
some useful routines:

* `TDirAdapter` helper class that might be included into inheritance chain
of particular handler subclass to provide convenient and standardized mechanism
to address various ROOT entities (histograms, arrays, trees) to certain
`TDirectory` within global output `TFile`. For details see the [page devoted to
detector naming scheme resolution](\ref TDirAdapterDetails).
* `iCalibSubscriber` helper class defining an interface for the entities that
depends on calibration information. It provides efficient updating lifecycle
for calibration-derived data on calibration information update based on event
numbering. For details, see [a dedicated page](\ref NA64DPCalibrationConcepts).
* `AbstractHitHandler` template class descendant of `AbstractHandler`
implementing iteration over hits of certain type. Since, generally speaking,
the hit identification depends on the setup version (particular detector set),
selection of hits within an event is a calibration-dependant information and
`iCalibSubscriber` is included into this subclass. See below for the
detailed reference.

There also may be some detector-specific handlers that operates with certain
detector stations only.

## AbstractHitHandler

This class is designed to iterate over hits of certain type within an event.
It is less generic than `AbstractHandler`, but provides some common code to
perform iteration over hits within an event.

It is a template class parameterised with requested hit type. To reach the
particular _hits map_ within  an event, the type traits technique is used (see
below).

\todo example on subclassing this handler (refer to existing ones).

# Extending Event Structure

\todo this is a bit advanced topic as it sometimes implies using of hit banks
and I still did not make enough synctactic sugar to make it look less horrible
for unexperienced user

## Hit Type Traits Technique {#HitsTypeTraits}

Type traits technique relies on
[C++ partial template specialization](https://en.cppreference.com/w/cpp/language/partial_specialization) --
a technique that allows the templated `class`, `struct` or `union` to have an
individual subset of nested definitions, members, `constexpr`s  and so on. One
may think on it as extrapolation of class einheritance, but done at the level
of metaprogramming.

Effectively it allows us to significantly shorten the amount of work needed to
define new types within the event object model, to implement automated
procedures for event data setting and retriaval and eventually decrease the
duplicity within handlers.

Practically, let's consider the `EvFieldTraits` template. By default this
template has no definition at all (see `NA64DPHitTraits.hh`):

~~~cpp
    template<typename T> struct EvFieldTraits;
~~~

However, for some newly-defined event field (say, `SADCHit`) we provide a
_specialized_ definition of `EvFieldTraits` struct parameterised with `SADCHit`
type (see `NA64DPEvFields/SADC.hh`):

~~~cpp
    template<>
    struct EvFieldTraits<SADCHit> {
        /// Returns reference to a SADC hits map from Event object
        static ObjMap<SADCHit> & map( Event & e );
        ...
    };
~~~

Then, imagine we have some function that has to implement type-dependent
selection of the procedure needed to accomplish some common task. For instance,
we may have to retrieve the _hits map_ from event that corresponds to hit of
certain type. Then it is sufficient to write only:

~~~cpp
    templae<typename T>
    void insert_hit( Event & eventRef, T & hitRef ) {
        EvFieldTraits<T>::map(e).insert( e );
    }
~~~

And it will automatically pick up the specialization for newly-defined types
when `insert_hit(e, hitRef)` is called. Contrary, some non-template
implementation will demand populating of new conditional `if() {}` statement
each time the new type is added:

~~~cpp
    if( typeid(hit) == SADCTypeID ) {
        e.sadcHits.insert( hit );
    } else if(typeid(hit) == APVTypeID) {
        e.apvHits.insert( hit );
    } else if(typeid(hit) == whateverID) {
        e.whateverHits.insert(hit);
    } ...
~~~

Type traits is a great technique to implement type-dependant behaviour while
the code is expected to grow as such places will multiply, scattered across the
code. Providing traits will decrease the code and eliminate the need of taking
into account all such forks.

For more comprehensive reference on generic uage see e.g.
the ["Design Patterns: Elements of Reusable Object-Oriented Software"](https://www.modernescpp.com/index.php/tag/type-traits)
book.

