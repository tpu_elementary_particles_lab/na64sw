#pragma once

#include "na64dp/abstractHandler.hh"
#include "na64event/hitTraits.hh"
#include "na64calib/manager.hh"
#include "na64util/selector.hh"
#include "na64detID/TBName.hh"

#include <algorithm>

namespace na64dp {

/// Small non-template shim providing hit handler statistics for monitoring
class HitHandlerStats : public AbstractHandler {
protected:
    /// Number of considered hits
    size_t _nHitsConsidered;
    /// Number of discriminated hits
    size_t _nHitsDiscriminated;
public:
    HitHandlerStats() : AbstractHandler()
                      , _nHitsConsidered(0)
                      , _nHitsDiscriminated(0)
                      {}
    /// Returns number of hits being considered by this handler
    size_t n_hits_considered() const { return _nHitsConsidered; }
    /// Returns number of hits being discriminated by this handler
    size_t n_hits_discriminated() const { return _nHitsDiscriminated; }
};

/**\brief Helper class implementing frequent functions on name slections
 *
 * This class is designed for handlers that depends on "current" instance of
 * `util::TBNameMappings` calibration information entry, thus reducing
 * genericity of handler to single dispatcher instance.
 * */
class SelectiveHitHandler : protected calib::Handle<nameutils::DetectorNaming> {
public:
    typedef std::pair<std::string, DetSelect *> Selector;
private:
    /// Selection limits detector IDs this handler is applicable to.
    Selector _selector;
public:
    ///\brief Performs basic conversion of the names set to detector IDs set.
    ///
    /// At the level of AbstractHandler<> template, performs conversion of
    /// string set _selector to set _onlyDetectors of numerical IDs.
    /// Subclasses may call this parent function to avoid duplication.
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    /// Returns current naming instance
    const nameutils::DetectorNaming & naming() const;
    /// Initializes hit selection mixing
    SelectiveHitHandler( calib::Dispatcher & dspt
                       , const std::string & selExpr );
    ~SelectiveHitHandler();
    /// Returns true if detector ID matches selection expression
    bool matches( DetID ) const;
    /// Returns true if selection expression is set.
    bool is_selective() const { return !_selector.first.empty(); }
    /// Returns selector expression that this handler is set to process. Empty
    /// for all hits of certain type.
    ///\todo rename to detector_selection_str()
    const Selector & detector_selection() const { return _selector; }
};

///\todo rename to `iHitHandler`
template<typename HitT>
class AbstractHitHandler : public HitHandlerStats
                         , public SelectiveHitHandler {
private:
    /// process_hit() may set this to the resulting code of process_event()
    ProcRes _pr;
    /// process_hit() may set this to remove current entry from map
    bool _removeFieldEntry;
    /// process_hit() may retrieve current event reference
    Event * _cEvPtr;
protected:
    ///process_hit() may call this to retrieve reference to the current event
    Event & _current_event() { assert( _cEvPtr ); return *_cEvPtr; }
    /// process_hit() should call this to set the return value of the
    /// process_event()
    virtual void _set_event_processing_result( ProcRes r ) { _pr = r; }
    /// process_hit() should call this to erase current hit entry from
    /// processing
    virtual void _set_hit_erase_flag() { _removeFieldEntry = true; }
public:
    AbstractHitHandler(calib::Dispatcher & dsp) : HitHandlerStats()
                                                , SelectiveHitHandler(dsp, "")
                                                , _cEvPtr(nullptr) {}
    /// Initializes discriminative hit handler (handles only certain detector
    /// IDs).
    AbstractHitHandler( calib::Dispatcher & dsp
                      , const std::string & sel) : HitHandlerStats()
                                                 , SelectiveHitHandler(dsp, sel)
                                                 , _cEvPtr(nullptr) {}

    /// Performs iteration over hits of certain type within an event and
    /// delegates particular treatment to process_hit(). Will return true
    /// if all the process_hit() invocations returned true AND there were at
    /// least one process_hit() invocation.
    virtual ProcRes process_event( Event * e ) override;
    /// Shall perform treatment of particular hit and return `false' to abrupt
    /// an iteration over event hits.
    virtual bool process_hit( EventID eventID
                            , DetID_t detID
                            , HitT & hit ) = 0;
};

template<typename HitT> AbstractHandler::ProcRes
AbstractHitHandler<HitT>::process_event( Event * e ) {
    _pr = kOk;
    bool doEFProc = false;
    _cEvPtr = e;  // set current event ptr
    std::vector< typename EvFieldTraits<HitT>::MapType::Iterator > hitsToRemove;
    if( !is_selective() ) {
        auto & hm = EvFieldTraits<HitT>::map(*e);
        for( auto it = hm.begin(); hm.end() != it; ++it ) {
            ++_nHitsConsidered;
            auto hitEntry = *it;
            _removeFieldEntry = false;
            doEFProc = process_hit( e->id, hitEntry.first, *hitEntry.second );
            if( _removeFieldEntry ) {
                hitsToRemove.push_back( it );
            }
            if( ! doEFProc ) break;
        }
    } else {
        auto & hm = EvFieldTraits<HitT>::map(*e);
        for( auto hitEntry = hm.begin(); hm.end() != hitEntry; ++hitEntry ) {
            if( ! matches((*hitEntry).first) ) continue;
            ++_nHitsConsidered;
            _removeFieldEntry = false;
            doEFProc = process_hit( e->id, (*hitEntry).first, *(*hitEntry).second );
            if( _removeFieldEntry ) {
                hitsToRemove.push_back( hitEntry );
            }
            if( ! doEFProc ) break;
        }
    }
    for( auto it : hitsToRemove ) {
        EvFieldTraits<HitT>::remove_from_event( *e, it
                        , naming() );
    }
    _nHitsDiscriminated += hitsToRemove.size();
    _cEvPtr = nullptr;  // unset current event ptr
    return _pr;
}

namespace aux {

/// Parses the "applyTo" list of strings if given within the node
std::string retrieve_det_selection( const YAML::Node & );

}

}  // namespace na64dp

