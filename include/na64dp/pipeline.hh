#pragma once

#include "na64dp/abstractHandler.hh"
#include "na64calib/manager.hh"

/**\namespace na64dp
 * \brief Main project's namespace
 *
 * NA64DP project (DP stands for "data processing") is written on C++ and all
 * the declarations (types, classes and variables) must be declared within
 * this namespace.
 * */
namespace na64dp {

class iEvProcInfo;  // FWD

/**\brief Ordered handlers array with utility methods
 *
 * For details explaining what is the behind of the pipeline idea, see
 * "Key concepts" section in "related pages" of the generated doc.
 *
 * This implementation of the pipeline concepts assumes that all handlers are
 * to be an objects sublassing `AbstractHandler` class. The `Pipeline` stores
 * their pointers in the array, subsequently calling ther `process_event()`
 * on each event instance provided within `process()` method.
 * */
class Pipeline : public std::vector<AbstractHandler*> {
protected:
    /// Reference to logger instance
    log4cpp::Category & _log;
    /// Pointer to the running monitoring handle
    iEvProcInfo * _procInfoPtr;
public:
    /// Constructs empty pipeline.
    Pipeline(iEvProcInfo * pi=nullptr);
    /// Recieves YAML configuration file name, parses it and uses
    /// HandlerConstructors singleton to assemble a handlers chain.
    /// Provide a ptr to EventProcessingInfo instance to gain useful runtime
    /// information.
    Pipeline( YAML::Node, HitBanks &, calib::Dispatcher &, iEvProcInfo * pi=nullptr );
    /// Deletes all the associated handlers.
    ~Pipeline();
    /// Guide the event instance through the pipeline, feeding it to each
    /// handler in chain unless one of the handlers will return
    /// `kDiscriminateEvent' code. Returns `true' if at least one of the
    /// handlers have set the `kStopProcessing' flag.
    bool process(Event &);
};

}

