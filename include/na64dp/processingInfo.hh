# pragma once

#include "na64sw-config.h"

# include <map>
# include <vector>
# include <thread>
# include <mutex>

namespace na64dp {

class AbstractHandler;
struct HitBanks;

namespace aux {
/// Tracks basic statisticas on the running processor within the pipeline.
struct HandlerStatisticsEntry {
    /// Name of the handler.
    std::string name;
    /// Counter corresponding to discriminated events on certain handler in
    /// pipeline
    size_t nDiscriminated;
    // For hit handler -- number of hits being considered
    //size_t nHitsConsidered;
    // For hit handler -- number of hits being discriminated
    //size_t nHitsDiscriminated;
    /// Time elapsed on computation.
    clock_t elapsed
          , _started;

    HandlerStatisticsEntry(const std::string & nm) : name(nm)
                                                   , nDiscriminated(0)
                                                   , elapsed(0) {}
};
}  // namespace aux

/** \brief Event processing info registry
 *
 * Holds index of `aux::HandlerStatisticsEntry`
 * by handler pointer, provides basic interface for querying.
 *
 * \note descendants usually protect access to registry members with mutex, so
 * by default most of the getters are qualified as `protected'. */
struct iEvProcInfo : protected std::map< const AbstractHandler *
                                       , aux::HandlerStatisticsEntry> {
private:
    size_t _nEvsProcessed  ///< number of processed events
         , _nEvsToProcess  ///< number of discriminated events
         ;
    clock_t _started;  ///< processing started at
    /// ordered list of handlers stats
    std::vector<const aux::HandlerStatisticsEntry *> _ordered;
protected:
    /// Interface for pipeline: adds pipeline's handler
    virtual void register_handler( const AbstractHandler *, const std::string & );
    /// Returns statistics entry for handler
    aux::HandlerStatisticsEntry & stats_for( const AbstractHandler * );

    /// Interface for pipeline: increases overall events counter. Returns
    /// `false' if events counter exceeded.
    virtual bool notify_event_read();
    /// Interface for pipeline: increases particular handler discriminated
    /// events counter
    virtual void notify_event_discriminated( const AbstractHandler * hPtr );
    /// Interface for pipeline: notifies certain handler started to process
    /// event
    virtual void notify_handler_starts( const AbstractHandler * hPtr );
    /// Interface for pipeline: notifies certain handler done to process event
    virtual void notify_handler_done( const AbstractHandler * hPtr );

    iEvProcInfo( size_t nEvsToProcess ) : _nEvsProcessed(0)
                                        , _nEvsToProcess(nEvsToProcess)
                                        , _started(0){}

    /// Returns number of events being processed
    size_t n_events_processed() const { return _nEvsProcessed; }
    /// Returns number of events being discriminated within the pipeline
    size_t n_events_to_process() const { return _nEvsToProcess; }
    /// Returns clock() value when processing started
    clock_t started() const { return _started; }
    /// Returns ordered list of processor stats
    const std::vector<const aux::HandlerStatisticsEntry *> & ordered_stats() {
        return _ordered; }

    friend class Pipeline;
};

/** \brief Wrapper for parallel dispatching of events processing info.
 *
 * Tracks the event processing speed, guarantees reasonable refresh rate while
 * dispatching some runtime information: events being read and processed,
 * average events processing rate, events discrimination statistics, etc.
 *
 * The rationale is to prevent significant slowdown appearing on frequent I/O
 * operations (FIFO, TTY, network connections, etc) by yielding of a thread
 * dedicated to these operations. Though, synchronization via mutex may still
 * introduce noticeable impact. */
class EvProcInfoDispatcher : public iEvProcInfo {
private:
    /// Refresh interval, msec
    const unsigned int _rrMSec;
    /// A thread for periodical print-out of the info
    std::thread * _dispatcherThread;
    /// True, when periodical print-out shall proceed
    bool _keep;
    /// Info synchronization mutex
    std::mutex _m;
    /// In-thread runner
    void _dispatch_f();
protected:
    /// Reference to banks instance
    const HitBanks & _banks;
    /// A record storing the starting time of the processing procedure
    clock_t _processingStartTime;  // XXX?

    /// Prints a short report by given file descriptor.
    virtual void _update_event_processing_info() = 0;
    /// Initializes basic state.
    EvProcInfoDispatcher( size_t nMaxEvs
                        , unsigned int mSecRefreshInterval
                        , const HitBanks & banks );
public:
    virtual ~EvProcInfoDispatcher();
    /// Issues a monitoring thread
    void start();
    /// Stops monitoring thread
    void finalize();

    /// Guarded version: increases "passed events" counter
    virtual bool notify_event_read() override;
    /// Guarded version: increases "discriminated events" counters
    virtual void notify_event_discriminated( const AbstractHandler * hPtr ) override;
    /// Guarded version: handler has been invoked
    virtual void notify_handler_starts( const AbstractHandler * hPtr ) override;
    /// Guarded version: handler has finished
    virtual void notify_handler_done( const AbstractHandler * hPtr ) override;

    /// Returns capnp entry describing current state
    virtual void excerpt( std::vector<char> & );
};

/// Prints handlers I/O stats as a simple unbuffered ASCII display.
class PrintEventProcessingInfo : public EvProcInfoDispatcher {
private:
    int _fd;  ///< A C file descriptor where info is printed to.
protected:
    virtual void _update_event_processing_info() override;
public:
    /// Accepts a C-file descriptor and refresh interval in msec.
    PrintEventProcessingInfo( int fileDescriptor
                            , const HitBanks & banks
                            , size_t nMaxEvents=0
                            , int mSecRefreshInterval=200 ) : EvProcInfoDispatcher( nMaxEvents, mSecRefreshInterval, banks )
                                                            , _fd(fileDescriptor) {}
};

}  // namespace na64dp

