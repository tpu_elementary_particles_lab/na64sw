#pragma once

#include "na64util/YAMLAssertions.hh"
#include "na64util/str-fmt.hh"
#include "na64util/vctr.hh"
#include "na64event/event.hh"

#include "log4cpp/Category.hh"

#include <map>
#include <yaml-cpp/yaml.h>

namespace na64dp {

namespace calib {
class Dispatcher;  // fwd
}

/**\brief An abstract handler class
 * 
 * Defines an interface for all the handlers implemented in the pipeline.
 */
class AbstractHandler {
public:
    ///\brief Type of the numerical code returned by handler adter processing done.
    enum ProcRes : uint8_t {
        /// This is the default return code meaning that event should be
        /// propagated further by the pipeline.
        kOk = 0x0,
        /// This flag in returning result stops the caller code to
        /// propagate the event to next handlers in pipeline. After that the
        /// caller code may continue with next event from source.
        kDiscriminateEvent = 0x2,
        /// This flag must be considered by the caller code as a terminative
        /// signal -- no more event must be processed by the caller code.
        /// Together with `kDiscriminateEvent' flag it stops the further
        /// propagation of current event as well
        kStopProcessing = 0x4,
        /// A shortcut for instant stop of processing.
        kAbortProcessing = kDiscriminateEvent | kStopProcessing,
    };
protected:
    /// Handlers logger category
    log4cpp::Category & _log;
public:
    /// Default constructor takes reference to event banks and calibration
    /// banks.
    AbstractHandler();
    /// Abstract method processing the event. Doing most of the handler's work.
    /// Returning `false' will cause to prevent this event from propagation
    /// further by the pipeline.
    virtual ProcRes process_event(Event * event) = 0;
    /// Optional interface method performing some operations after all the
    /// events were processed, before the output ROOT file is closed.
    virtual void finalize();
    /// Dtr made virtual to provide generic deletion of handler subclass
    /// instances
    virtual ~AbstractHandler() {}
    /// Returns logger instance affiliated to handlers objects.
    log4cpp::Category & log() { return _log; }
};

template<> struct CtrTraits<AbstractHandler> {
    typedef AbstractHandler * (*Constructor)
        ( HitBanks &, calib::Dispatcher &, const YAML::Node & );
};

namespace utils {  // TODO: rename to "util"

/// Returns getter instance by name
template<typename T> typename EvFieldTraits<T>::ValueGetter
value_getter( const std::string & nm ) {
    for( auto ge = EvFieldTraits<T>::getters; ge->getter; ++ge ) {
        if( std::string(ge->name) == nm ) return ge->getter;
    }
    NA64DP_RUNTIME_ERROR( "No getter named \"%s\".", nm.c_str() );
}

}  // namespace na64dp::utils

}  // namespace na64dp

/**\brief Registers new event handler type for pipeline data processing
 * \ingroup vctr-defs
 *
 * Registers subclasses of `AbstractHandler` base to be created with `VCtr`.
 *
 * \param clsName The name of the handler type
 * \param banks The name for "banks" argument to be used in ctr code
 * \param calibDsp The name of calibations dispatcher instance to be used in ctr
 * \param ni The name of the config (YAML) node to be used in the ctr
 * \param desc An event handler type description string
 */
# define REGISTER_HANDLER( clsName, banks, calibDsp, ni, desc )                 \
static AbstractHandler * _new_ ## clsName ## _instance( na64dp::HitBanks &      \
                                                      , na64dp::calib::Dispatcher &  \
                                                      , const YAML::Node & );    \
static bool _regResult_ ## clsName = \
    ::na64dp::VCtr::self().register_class<AbstractHandler>( # clsName, _new_ ## clsName ## _instance, desc ); \
static AbstractHandler * _new_ ## clsName ## _instance( __attribute__((unused)) na64dp::HitBanks & banks \
                                                      , __attribute__((unused)) na64dp::calib::Dispatcher & calibDsp  \
                                                      , __attribute__((unused)) const YAML::Node & ni )

