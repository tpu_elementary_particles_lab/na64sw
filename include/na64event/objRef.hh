#pragma once

#include "na64util/str-fmt.hh"

#include <vector>
#include <limits>

#include <log4cpp/Category.hh>

namespace na64dp {

template<typename T> class PoolRef;  // fwd

/**\brief A single-typed bank array
 *
 * Implements a "pool allocator" object. Maintains a single memory block
 * holding the subset of homogeneously-typed object for faster access.
 *
 * \todo implement nicer syntax for new object allocation
 * \todo rename `new_object()` method as it is not applied to only hits anymore
 * \todo possibly, all this stuff will possibly be re-implemented
 */
template<typename HitT>
class ObjPool : private std::vector<HitT> {
public:
    /// Reserves some amount of hits for allocation.
    ObjPool( size_t nHitsReserved ) {
        std::vector<HitT>::reserve( nHitsReserved );
    }

    /// Returns offset to a new uninitialized instance
    size_t increment_capacity() {
        std::vector<HitT>::push_back( HitT() );
        return std::vector<HitT>::size() - 1;
    }

    /// Returns new entry
    PoolRef<HitT> create() {
        return PoolRef<HitT>( *this, increment_capacity() );
    }

    /// Performs reverse mapping, from normal pointer to `PoolRef`.
    PoolRef<HitT> get_ref_of( const HitT * ptr );
    PoolRef<HitT> get_ref_of( const HitT & ref ) { return get_ref_of( &ref ); }

    /// For diagnostic and debugging purposes
    const std::vector<HitT> & get_storage() const { return *this; }

    using std::vector<HitT>::capacity;
    using std::vector<HitT>::size;
    using std::vector<HitT>::at;
    using std::vector<HitT>::clear;
};

/// A "pointer-like" entity referencing hit within a bank.
///
/// This helper object represents a reference to an object allocated with
/// "memory bank" (pool allocator). By minor price of some performance, it
/// remains valid after pool allocator being resized.
template<typename T>
class PoolRef {
private:
    ObjPool<T> * _ownerPtr;  ///< object owning bank ptr
    size_t _offset;  ///< reference within an owning bank
public:
    /// Ctr for uninitialized pointer (null)
    PoolRef() : _ownerPtr( nullptr), _offset( std::numeric_limits<size_t>::max()) {}
    /// Ctr for existing object pointer
    PoolRef( ObjPool<T> & owner, size_t n ) : _ownerPtr( &owner)
                                            , _offset(n) {}
    /// Returns mutable reference to the object.
    T & dereference() {
        try {
            return _ownerPtr->at(_offset);
        } catch( std::exception & e ) {
            log4cpp::Category::getInstance( "banks" )
                .error( "Dereference failure : %p:%zu (real size is %zu)"
                      , _ownerPtr, _offset
                      , _ownerPtr ? _ownerPtr->size() : 0 );
            throw;
        }
    }
    /// Returns const-protected reference to the object.
    const T & dereference() const {
        try {
            return _ownerPtr->at(_offset);
        } catch( std::exception & e ) {
            log4cpp::Category::getInstance( "banks" )
                .error( "Dereference failure (const) : %p:%zu (real size is %zu)"
                      , _ownerPtr, _offset
                      , _ownerPtr ? _ownerPtr->size() : 0 );
            throw;
        }
    }
    /// Mutable dereferencing operator.
    T & operator*() { return dereference(); }
    /// Constant dereferencing operator.
    const T & operator*() const { return dereference();}
    /// Makes arrow operator to dereference the associated object
    T * operator->() { return &dereference(); }
    /// Makes arrow operator to dereference the associated object (const)
    const T * operator->() const { return &dereference(); }
    /// Returns mutable reference to the object owner (bank).
    ObjPool<T> & owner() { assert( _ownerPtr); return *_ownerPtr; }
    /// Returns constant reference to the object owner (bank).
    const ObjPool<T> & owner() const { assert( _ownerPtr); return *_ownerPtr; }
    /// Returns object identifier within the bank.
    size_t offset_id() const { return _offset; }
    /// Returns `true' if pointer refers to an existing entry
    bool is_valid() const { return nullptr != _ownerPtr; }
    /// Wipes reference, dropping instance to invalid state.
    void clear() { _ownerPtr = nullptr; _offset = std::numeric_limits<size_t>::max(); }

    bool operator<(const PoolRef<T> & r ) const {
        return _ownerPtr <= r._ownerPtr && _offset < r._offset;
    }
    
    bool operator==(const PoolRef<T> & r ) const {
		return _ownerPtr == r._ownerPtr && _offset == r._offset;
	}
	
	bool operator!=(const PoolRef<T> & r ) const {
		return !this->operator==(r);
	}
};


template<typename T> PoolRef<T>
ObjPool<T>::get_ref_of( const T * ptr ) {
    auto distance = ptr - this->data();
    if( distance < 0 || (long unsigned int) distance > this->size() ) {
        NA64DP_RUNTIME_ERROR(
                , "Object pointer %p does not belong to allocator %p."
                , ptr, this );
    }
    return PoolRef<T>( *this, distance );
}


/// A "pointer-like" entity referencing hit within a bank.
///
/// This helper object represents a reference to an object allocated with
/// "memory bank" (pool allocator). By minor price of some performance, it
/// remains valid after pool allocator being resized.
template<typename T>
class ConstPoolRef {
private:
    const ObjPool<T> * _ownerPtr;  ///< object owning bank ptr
    size_t _offset;  ///< reference within an owning bank
public:
    /// Ctr for uninitialized pointer (null)
    ConstPoolRef() : _ownerPtr( nullptr), _offset( std::numeric_limits<size_t>::max()) {}
    /// Ctr for existing object pointer
    ConstPoolRef( const ObjPool<T> & owner, size_t n ) : _ownerPtr( &owner)
                                                       , _offset(n) {}
    ConstPoolRef( const PoolRef<T> & vo ) : _ownerPtr(&(vo.owner()))
                                          , _offset(vo.offset_id()) {}
    /// Returns const-protected reference to the object.
    const T & dereference() const {
        try {
            return _ownerPtr->at(_offset);
        } catch( std::exception & e ) {
            log4cpp::Category::getInstance( "banks" )
                .error( "Dereference failure (const) : %p:%zu (real size is %zu)"
                      , _ownerPtr, _offset
                      , _ownerPtr ? _ownerPtr->size() : 0 );
            throw;
        }
    }
    /// Constant dereferencing operator.
    const T & operator*() const { return dereference();}
    /// Makes arrow operator to dereference the associated object (const)
    const T * operator->() const { return &dereference(); }
    /// Returns constant reference to the object owner (bank).
    const ObjPool<T> & owner() const { assert( _ownerPtr); return *_ownerPtr; }
    /// Returns object identifier within the bank.
    size_t offset_id() const { return _offset; }
    /// Returns `true' if pointer refers to an existing entry
    bool is_valid() const { return nullptr != _ownerPtr; }
    /// Wipes reference, dropping instance to invalid state.
    void clear() { _ownerPtr = nullptr; _offset = std::numeric_limits<size_t>::max(); }

    bool operator<(const ConstPoolRef<T> & r ) const {
        return _ownerPtr <= r._ownerPtr && _offset < r._offset;
    }
};

}

