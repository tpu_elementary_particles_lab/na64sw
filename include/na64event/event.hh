#pragma once

#include "na64sw-config.h"
#include "na64event/pools.hh"
#include "na64event/hitsMap.hh"
#include "na64event/fields/SADC.hh"
#include "na64event/fields/StwTDC.hh"
#include "na64event/fields/CaloEDep.hh"
#include "na64event/fields/track.hh"

#include <na64ee/event-id.hh>

#ifndef NA64DP_EVENT_API
#   error "NA64DP_EVENT_API undefined."
#endif

/**\file NA64DPEvent.hh
 * \brief Event structure declaration.
 *
 * The event structure is the principal declaration affecting every handler
 * within the pipeline. Handlers take and modify the data from/within an event
 * instance, by accessing its members.
 *
 * Refer to [handlers development guide](\ref HandlersDevGuide) for insights of
 * how to modify the events composition.
 * */

namespace na64dp {

/* \brief NA64 event representation.
 *
 * This struct maps hit instances over various identification schemes for
 * faster access. Instance of this structure is the major subject of
 * modification and data retrieval for all the handlers composing the pipeline.
 *
 * Refer to [handlers development guide](\ref HandlersDevGuide) for insights of
 * how to modify the events structure.
 *
 * Type is not copyable as creation of copies must require explicitly defined
 * hit creation.
 * */
struct Event {
    Event(const Event &) = delete;
    Event & operator=(const Event &) = delete;

    /// Event unique ID
    EventID id;
    /// Event time: in seconds since epoch, microseconds
    std::pair<time_t, uint32_t> time;

    /// SADC-based detectors hits (ECAL, WCAL, HCAL, VETO, etc.)
    EvFieldTraits<SADCHit>::MapType sadcHits;

    /// APV-based detector hits (MicroMegas, GEMs)
    EvFieldTraits<APVHit>::MapType apvHits;
    
    /// NA64TDC-based detector hits (Straw)
    EvFieldTraits<StwTDCHit>::MapType stwtdcHits;

    /**\brief 1D clusters (contains references to the APV hits)
     * 
     * Cluster contains references to hits located in neighbouring group of
     * wires.
     * Clusters here are indexed by "incomplete" detector ID that contains only
     * chip ID, kin ID, number of station and projection ID (no wire number). */
    APVClustersIndex apvClusters;
    
    /// Energy deposition in calorimetric detectors (e.g. ECAL, WCAL, HCAL, etc)
    EvFieldTraits<CaloEDep>::MapType caloEDeps;

    /// (experimental) track points index
    EvFieldTraits<TrackPoint>::MapType trackPoints;

    /// Particle tracks
    EvFieldTraits<Track>::MapType tracks;

    /// Main constructor, forwards pool allocator references to SADC hits
    Event( HitBanks & b ) : id(0x0)
                          , sadcHits( b.of<SADCHit>() )
                          , apvHits( b.of<APVHit>() )
                          , stwtdcHits ( b.of<StwTDCHit>() )
                          , apvClusters( b.of<APVCluster>() )
                          , caloEDeps( b.of<CaloEDep>() )
                          , trackPoints( b.of<TrackPoint>() )
                          {}
                          
    /// TODO: depreciate EDep replaced by CaloEDep
    double ecalEdep;
    double preShower;
    double hcalEdep;
    double vetoEdep;
    double srdEdep;
    
    double masterTime;

    /// Resets event for reentrant usage
    void clear() {
        id = 0x0;
        ecalEdep = 0;
        preShower = 0;
        hcalEdep = 0;
        vetoEdep = 0;
        srdEdep = 0;
        masterTime = 0;
        
        // TODO: reset time
        sadcHits.clear();
        apvHits.clear();
        stwtdcHits.clear();
        apvClusters.clear();
        caloEDeps.clear();
        trackPoints.clear();
        tracks.clear();
    }
    
    /// Hodoscope hits
    //HitsMap<SADCHit> viewHodoHits;
};

}  // namespace na64dp

