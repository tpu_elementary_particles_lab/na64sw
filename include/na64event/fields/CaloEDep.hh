#pragma once

#include <memory>
#include "na64event/fields/SADC.hh"

namespace na64dp {

/**\brief Field for energy deposition in calorimetric systems
 **/
struct CaloEDep {
	
	std::map<DetID_t, PoolRef<SADCHit>> stationHits;
	
	DetID_t station;
	
	double eDepMeV;
};

typedef ObjMap<DetID_t, CaloEDep, aux::Multimap> CaloEDepIndex;

template<>
struct EvFieldTraits<CaloEDep> {
    typedef CaloEDepIndex MapType;
    /// Returns reference to a SADC hits map from Event object
    static MapType & map( Event & e );
    /// Returns certain bank for the type
    static ObjPool<CaloEDep> & bank( HitBanks & bs ) { return bs.bankCaloEDeps; }
    /// Returns a list of maps specific for certain SADC detector station id
    static std::vector<MapType*>
        maps_for( Event & e, DetID did, const nameutils::DetectorNaming & );
    /// Erases hit entry from all the maps within an event
    static void remove_from_event( Event & e
                                 , MapType::Iterator
                                 , const nameutils::DetectorNaming & );

    static void reset_hit( CaloEDep & hit );
    
    static DetID_t uniq_detector_id(DetID);

    typedef double (*ValueGetter)(const CaloEDep &);
    static struct GetterEntry {
        ValueGetter getter;
        const char * name;
        const char * description;
    } * getters;
};

}
