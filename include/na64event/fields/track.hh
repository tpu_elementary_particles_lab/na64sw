#pragma once

#include "na64event/fields/trackPoint.hh"

namespace genfit {  // fwd decls
class Track;
}

namespace na64dp {

/**\brief Track numerical identifier type
 *
 * This type name is reserved for future extesnions, implying that event may
 * contain multiple reconstructed tracks. Currently, only the track with
 * track ID =0 is used for incident particle track.
 *
 * \todo move to a dedicated header with, traits, etc. */
typedef int TrackID_t;

/**\brief Particle track representation
 *
 * \todo this is a draft, yet containing only the tracking points
 */
struct Track : public std::vector<PoolRef<TrackPoint>> {
    
    /// Initial track position
    double seed[3];
    /// Initial track direction
    double mom[3];
    /// Momentum hypothesis based on
    double momentum;
    /// Particle identifier
    int pdg;
    /// Chi2/Ndf
    double chi2ndf;
    /// Energy deposition
    double edep;
    /// Vector of founded track points
    std::vector<PoolRef<TrackPoint>> trackPoints;
    
    genfit::Track * genfitTrackRepr;
    // ...
};

template<>
struct EvFieldTraits<Track> {
    typedef std::map<TrackID_t, PoolRef<Track> > MapType;
    
    /// Returns reference to a APV hits map from Event object
    static MapType & map( Event & e );
    // Returns reference to the main track points container within an event
    // TrackPoints & map(Event & e);
    /// Returns certain bank for the type
    static ObjPool<Track> & bank( HitBanks & bs ) { return bs.bankTracks; }
    /// Erases track entry from all the maps within an event

    /// Returns a list of maps specific for certain APV detector station id
    static std::vector<MapType*>
        maps_for( Event & e
                , DetID did
                , const nameutils::DetectorNaming & );
    static void reset_hit( Track & track);

    typedef double (*ValueGetter)(const Track &);
    static struct GetterEntry {
        ValueGetter getter;
        const char * name;
        const char * description;
    } * getters;

};

}

