#pragma once

#include "na64event/fields/APV.hh"

#if defined(CapnProto_FOUND) && CapnProto_FOUND
#include "event.capnp.h"
#endif

namespace na64dp {

namespace serialization {
    struct StatePacking;  // fwd
    struct StateUnpacking;  // fwd
}

/// Representation of the hit cluster
struct APVCluster : public std::map<APVStripNo_t, PoolRef<APVHit>> {
    /// Coordinate of cluster's center, of wire number units
    double position;
    /// Cluster center position weighted by charge sum
    double wPosition;
    /// Cluster charge sum (unnormed), sum of all hit's amplitude measurements
    double charge;
    /// xxx?
    double sparseness;
    /// Cluster time
    double time;
    /// Cluster's time error
    double timeError;
};

/// Associative array listing 1D clusters by plane ID
typedef ObjMap<DetID_t, APVCluster, aux::Multimap> APVClustersIndex;

template<>
struct EvFieldTraits<APVCluster> {
    typedef APVClustersIndex MapType;
    /// Returns reference to a APV hits map from Event object
    static MapType & map( Event & e );
    /// Returns a list of hits maps specific for certain APV detector id
    static std::vector<MapType*>
        maps_for( Event & e, DetID did, const nameutils::DetectorNaming & );
    /// Erases cluster entry from all the maps within an event
    static void remove_from_event( Event &
                                 , MapType::Iterator
                                 , const nameutils::DetectorNaming & );

    /// Returns certain bank for the type
    static ObjPool<APVCluster> & bank( HitBanks & bs ) { return bs.bankAPVClusters; }

    /// Drops the hit to "uninitialized" state
    static void reset_hit( APVCluster & );
    /// Returns unique detector ID without variable payload; for APV detectors
    /// it means getting rid of wire number; this function delegates execution
    /// to EvFieldTraits<APVHit>::uniq_detector_id()
    static DetID_t uniq_detector_id(DetID did) {
        return EvFieldTraits<APVHit>::uniq_detector_id(did);
    }

    typedef double (*ValueGetter)(const APVCluster &);
    static struct GetterEntry {
        ValueGetter getter;
        const char * name;
        const char * description;
    } * getters;

    #if defined(CapnProto_FOUND) && CapnProto_FOUND
    typedef capnp::na64dp::APVCluster PackedType;
    static void pack( const APVCluster &
                    , PackedType::Builder
                    , serialization::StatePacking & );
    static void unpack( PackedType::Reader
                      , APVCluster &
                      , serialization::StateUnpacking & );
    #endif
};

}

