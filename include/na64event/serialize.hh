#pragma once

#include "na64event/event.hh"

#if defined(CapnProto_FOUND) && CapnProto_FOUND
#include <capnp/message.h>
#include <capnp/serialize-packed.h>
#endif

namespace na64dp {
namespace serialization {

#if defined(CapnProto_FOUND) && CapnProto_FOUND
/**\brief An event field serialization, writing raw bytes to string buffer
 *
 * Reciprocal to `from_bytes()` function.
 * To write the serialized data, constructs an intermediate message builder,
 * packs the object and converts the data via the `kj::Array` instance to
 * word-aligned string.
 *
 * \note This function is not basically a very performant one as it copies some
 * redundant copies. For performant operation consider using of native
 * serialization routines provided by Cap'n'proto (streams, buffers, etc)
 */
template<typename T> std::string
to_bytes( const T & hit, StatePacking & S ) {
    ::capnp::MallocMessageBuilder msg;
    typename EvFieldTraits<T>::PackedType::Builder bHit
            = msg.initRoot<typename EvFieldTraits<T>::PackedType>();
    EvFieldTraits<T>::pack( hit, bHit, S );
    kj::Array<capnp::word> dataArr = capnp::messageToFlatArray(msg);
    kj::ArrayPtr<kj::byte> bytes = dataArr.asBytes();
    return std::string(bytes.begin(), bytes.end());
}

/**\brief An event field de-serialization, reading raw bytes from string buffer
 *
 * Reciprocal to `to_bytes()` function.
 *
 * \note This function is not basically a very performant one as it copies some
 * redundant copies. For performant operation consider using of native
 * serialization routines provided by Cap'n'proto (streams, buffers, etc)
 */
template<typename T> void
from_bytes( const std::string & serialized
          , T & dest
          , StateUnpacking & S ) {
    ::capnp::MallocMessageBuilder msg;
    // check the string is word-aligned
    assert(reinterpret_cast<uintptr_t>(serialized.data()) % sizeof(void*) == 0);
    const kj::ArrayPtr<const capnp::word> view(
        reinterpret_cast<const capnp::word*>(&(*serialized.begin())),
        reinterpret_cast<const capnp::word*>(&(*serialized.end()))
        );
    capnp::FlatArrayMessageReader msgReader(view);
    typename EvFieldTraits<T>::PackedType::Reader rHit
                = msgReader.getRoot<typename EvFieldTraits<T>::PackedType>();
    EvFieldTraits<T>::unpack( rHit, dest, S );
}
#endif

/**\brief Event serialization state
 *
 * The process of event serialization involves some cross-fields references
 * that has to be memorized during event serialization. This kind of
 * information requires a stateful approach.
 *
 * Reciprocal class is `StateUnpacking`.
 * */
struct StatePacking {
    /// Allocators
    HitBanks & banks;
    /// APV hit numbers by wire ID, for `APVCluster`
    std::map<APVStripNo_t, size_t> apvHits;
    /// APV cluster by its pointer, for `TrackPoint`
    std::map<PoolRef < APVCluster>, size_t> apvClusters;
    /// List of track points, for `Track`
    std::map<PoolRef < TrackPoint>, size_t> trackPoints;
    // ...

    StatePacking(HitBanks & banks_) : banks(banks_) {}
};

/**\brief Event de-serialization state
 *
 * The process of event deserialization involves some cross-fields references
 * that has to be memorized during event serialization. This kind of
 * information requires a stateful approach.
 * 
 * Reciprocal class is `StatePacking`.
 * */
struct StateUnpacking {
    /// Allocators
    HitBanks & banks;
    /// APV hits pointers by number within serialized list
    std::map<size_t, PoolRef < APVHit>> apvHits;
    /// Ptrs to deserialized clusters by their number within a serialized list
    std::map<size_t, PoolRef < APVCluster> > apvClusters;
    /// List of track points, for `Track`
    std::map<size_t, PoolRef < TrackPoint> > trackPoints;
    // ...
    
    StateUnpacking(HitBanks & banks_) : banks(banks_) {}
};

/**\brief Serializes event using Cap'n'Proto
 *
 * \todo Signature will change
 * */
void serialize_event( Event & e );

}
}

