#pragma once

#include "na64event/objRef.hh"

namespace na64dp {

// fwd, NA64DPEvFields/SADC.hh
struct SADCHit;
struct WfFittingData;
// NA64DPEvFields/APV.hh
struct APVHit;
struct StwTDCHit;
struct APVCluster;
struct CaloEDep;
struct TrackPoint;
struct Track;

template<typename T> struct EvFieldTraits;

/**\brief A set of memory banks maintained by single pipeline.
 *
 * This structure keeps a number of object banks responsible for single event
 * processing.
 *
 * At the start of individual event processing, the data source shall impose
 * hits information in the `Event` structure, allocating memory via the hit
 * banks provided by an instance of this structure.
 *
 * During the event processing, handlers shall allocate/free new data pieces
 * by means of this structure.
 *
 * After the each single event processing, the pipeline instance wipes out all
 * the data allocated with this banks.
 *
 * A dedicated instance of this structure has to be allocated for each new
 * pipeline execution thread if they do run in parallel.
 * */
struct HitBanks {
    ObjPool<SADCHit>   bankSADC;
    ObjPool<APVHit>    bankAPV;
    ObjPool<StwTDCHit> bankStwTDC;
    // ... (other hits)

    /// ObjPool of SADC waveform fitting objects
    ObjPool<WfFittingData> bankSADCFitting;
    /// ObjPool of APV clusters
    ObjPool<APVCluster> bankAPVClusters;
    /// ObjPool of SADC hits in calorimeters
    ObjPool<CaloEDep>  bankCaloEDeps;
    /// ObjPool of track points
    ObjPool<TrackPoint> bankTrackPoints;
    /// ObjPool of track objects
    ObjPool<Track> bankTracks;
    // ...

    /// Default ctr, initializes all banks by 10
    HitBanks();

    /// Ctr pre-allocates the memory banks for graceful start.
    HitBanks( size_t nSADCHits
            , size_t nAPVHits
            , size_t nStwTDCHits
            , size_t nSADCFittingEntries
            , size_t nAPVClusters
            , size_t nCaloEDeps
            , size_t nTrackPoints
            , size_t nTracks
            // ...
            );
    /// Clears the allocated memory blocks for subsequent re-using.
    void clear();

    /// Helper function using traits to acquire the bank reference
    template<typename T> ObjPool<T> & of() {
        return EvFieldTraits<T>::bank( *this );
    }
};

}
