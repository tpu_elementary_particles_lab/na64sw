#pragma once

#include "na64sw-config.h"

#ifdef DaqDataDecoding_FOUND

#include <DaqEventsManager.h>

#include "na64dp/abstractEventSource.hh"

namespace na64dp {

namespace ddd {

template<typename CSDigitT> struct CSDigitTraits;  // fwd

/**\brief DaqDataDecoding library CS::EventsManager adapter.
 *
 * Represents a DDD events data source.
 *
 * \todo Get rid of DaqEventsManager class in favor of custom file/xrootd/date interface.
 * \todo Generate offsets index (na64ee) by option.
 * \todo Resolve issue with wrong kin "STT"
 */
class DDDEventSource : public AbstractEventSource
                     , protected calib::Handle<nameutils::DetectorNaming> {
public:
    struct NameCache {
        DetChip_t kSADC, kAPV, kStwTDC;
        DetKin_t kECAL;
        DetKin_t kMM, kGEM;
        DetKin_t kSt, kStt;
        // ... others used by ChipTraits
    };
protected:
    /// Calib manager reference
    calib::Manager & _calibMgr;
    /// Ptr to DaqDataDecoding events manager instance.
    CS::DaqEventsManager * _dddMgr;
    /// Number of decoding failures occured
    size_t _decodingFailures;
    /// Names of unknown detectors and times they were met duing the iteration
    std::map<std::string, size_t> _unknownDets;
    /// Internal detector naming cache
    NameCache _detIDsCache;
    /// Set of permitted event types
    std::set<CS::DaqEvent::EventType> _permitTypes;
    /// Set of tbnames to be ignored
    ///\todo make here a more complex object that will perform partial/wildcard match
	std::set<std::string> _tbnames2ignore;
protected:
    virtual void handle_update( const nameutils::DetectorNaming & ) override;

    void _event_fill( Event &, CS::Chip::Digits & );

    /// On successfull insertion returns complete detector ID, on failure - 0x0
    template<typename DigitT>
    DetID_t _fill_hit( Event & e
                     , DetID incompleteDID
                     , HitsInserter<typename ddd::CSDigitTraits<DigitT>::HitType> & inserter
                     , const CS::Chip::Digit * abstractDigitPtr
                     ) {
        const DigitT & dgt = dynamic_cast<const DigitT &>(*abstractDigitPtr);
        typedef CSDigitTraits<DigitT> Traits;
        // note: complete_detector_id() modifies incompleteDID
        if( Traits::complete_detector_id( _detIDsCache, incompleteDID, dgt ) ) {
            Traits::impose_hit_data( dgt
                                   , *inserter(e, incompleteDID, this->operator*())
                                   //, inserter.put_hit(e, incompleteDID, this->operator*())
                                   //, *inserter(e, incompleteDID, calibs().naming())
                                   );
            return incompleteDID;
        }
        return 0x0;
    }
public:
    /// Default ctr: immediately instantiates DDD manager with all the
    /// neccessary assets: CORAL maps directory and list of inputs (files or
    /// network sockets)
    DDDEventSource( HitBanks &
                  , calib::Manager & mgr
                  , const std::string & coralMapsDir
                  , const std::vector<std::string> & inputs
                  , std::vector<std::string> permittedEvTypes
                  , const std::set<std::string> & tbnames2ignore
                  );
    /// Interface implementation: reads an event with DDD manager.
    virtual bool read( Event & ) override;
    /// Getter for DDD manager
    CS::DaqEventsManager & cs_manager_ref() { return *_dddMgr; }
    /// Getter for DDD manager (const version)
    const CS::DaqEventsManager & cs_manager_ref() const { return *_dddMgr; }
    /// Returns number of event decoding failures
    size_t decoding_failures() const { return _decodingFailures; }
    /// Returns map of TBName vs occurance counter
    const std::map<std::string, size_t> & unknown_detectors() const { return _unknownDets; }
};

}  // namespace ddd
}  // namespace na64dp

#endif  // SUPPORT_DDD_FORMAT
