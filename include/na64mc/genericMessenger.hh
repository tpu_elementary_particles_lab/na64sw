#pragma once

#include <G4UImessenger.hh>

#include <G4UIcmdWithAnInteger.hh>
#include <G4UIcmdWithADouble.hh>
#include <G4UIcmdWithABool.hh>
#include <G4UIcmdWithAString.hh>

#include <cassert>

namespace na64dp {
namespace mc {

template<typename T> struct G4UIArgTraits;  // default is not defined

template<>
struct G4UIArgTraits<G4int> {
    typedef G4UIcmdWithAnInteger CommandType;
    static constexpr char typeCharG4 = 'i';
};

template<>
struct G4UIArgTraits<G4String> {
    typedef G4UIcmdWithAString CommandType;
    static constexpr char typeCharG4 = 's';
};

template<>
struct G4UIArgTraits<G4double> {
    typedef G4UIcmdWithADouble CommandType;
    static constexpr char typeCharG4 = 'd';
};

template<>
struct G4UIArgTraits<G4bool> {
    typedef G4UIcmdWithABool CommandType;
    static constexpr char typeCharG4 = 'b';
};

/**\brief A helper for shortening creation of new commands for Geant4 macros
 *
 * Subclasses may utilize this class by inheriting it and binding commands
 * with appropriate methods. This way is shorter for simple use cases than
 * direct creation of the messengers and commands with native Geant4 classes.
 *
 * \todo broadcasting support
 * \todo stateful parsing support ("current" parameter is not considered)
 * \todo add usage examples to doc
 */
class GenericG4Messenger : public G4UImessenger
                         , protected std::vector<G4UIcommand *> {
public:
    /// Bitmask type for `G4ApplicationState`
    typedef unsigned char AllowedState;
    // Same meaning as `G4ApplicationState`, but in a form of bitflags
    static const AllowedState preinit,      init,       idle
                            , geomClosed,   eventProc,  quit
                            , abort,        default_,   anyAppState;
    typedef void (*CallableTarget)(GenericG4Messenger *, const G4String &);
protected:
    const G4String _root;
    std::vector< std::pair<G4String, G4UIcommand *> > _stack;
    std::map<G4UIcommand *, CallableTarget> _callables;

    G4String _path(const G4String & lastToken) const;
public:
    GenericG4Messenger( const G4String & rootPath="/" ) : _root(rootPath) {}
    GenericG4Messenger(const GenericG4Messenger &) = delete;
    // todo: no copy assignable
    ~GenericG4Messenger();

    /// Returns root path for the messenger
    const G4String & GetRootPath() const { return _root; }

    /// Invokes an operation bound to command
    virtual void SetNewValue( G4UIcommand *, G4String ) override;

    /// Adds a dir on top of the stack
    GenericG4Messenger & dir( const G4String & name
                            , const G4String & descr
                            );

    /// Adds a command on top of the stack, the callback and parameters added further
    GenericG4Messenger & cmd( const G4String & name
                            , const G4String & descr
                            , CallableTarget target
                            , AllowedState states=default_
                            );

    /// Pops the stack assuring that topmost command has the name provided
    GenericG4Messenger & end( const G4String & name );

    /// A helper function setting the permitted states with corresponding
    /// `G4UIcommand::AvailableForStates()` calls
    static void set_permitted_states( G4UIcommand *, AllowedState );

    /// Adds a command without parameter
    GenericG4Messenger & cmd_nopar( const G4String & name
                            , const G4String & descr
                            , CallableTarget target
                            , AllowedState s=default_
                            );

    /// Adds one of typed shortcut commands (no stack appending)
    template<typename T>
    GenericG4Messenger & cmd( const G4String & name
                            , const G4String & descr
                            , const G4String & parameterName
                            , CallableTarget target
                            , AllowedState s=default_
                            );

    /// Adds one of typed shortcut commands with default value (no stack appending)
    template<typename T>
    GenericG4Messenger & cmd( const G4String & name
                            , const G4String & descr
                            , const G4String & parameterName
                            , const T & dft
                            , CallableTarget target
                            , AllowedState s=default_ );

    /// Adds one of typed parameters to the topmost command
    template<typename T>
    GenericG4Messenger & par( const G4String & name
                            , const G4String & desc 
                            );

    /// Adds one of typed parameters to the topmost command, with default value 
    template<typename T>
    GenericG4Messenger & par( const G4String & name
                            , const G4String & desc
                            , const T & df
                            );
};

//                                                     ________________________
// __________________________________________________/ Template Implementation

template<typename T> GenericG4Messenger &
GenericG4Messenger::cmd( const G4String & name
                       , const G4String & descr
                       , const G4String & parameterName
                       , CallableTarget target
                       , AllowedState s
                       ) {
    auto p = new typename G4UIArgTraits<T>::CommandType( _path(name), this );
    p->SetGuidance(descr);
    p->SetParameterName(parameterName, false, false);
    set_permitted_states(p, s);
    this->push_back(p);
    _callables[p] = target;
    return *this;
}

template<typename T> GenericG4Messenger &
GenericG4Messenger::cmd( const G4String & name
                       , const G4String & descr
                       , const G4String & parameterName
                       , const T & dft
                       , CallableTarget target
                       , AllowedState s
                       ) {
    auto p = new typename G4UIArgTraits<T>::CommandType( _path(name), this );
    p->SetGuidance(descr);
    p->SetParameterName(parameterName, true, false);
    p->SetDefaultValue(dft);
    set_permitted_states(p, s);
    this->push_back(p);
    _callables[p] = target;
    return *this;
}

template<typename T> GenericG4Messenger &
GenericG4Messenger::par( const G4String & name
                       , const G4String & desc
                       ) {
    if( _stack.rbegin()->second->GetCommandName().empty() ) {
        char errBf[128];
        snprintf( errBf, sizeof(errBf)
                , "Can not associate parameter \"%s\" with a dir \"%s\"."
                , name.c_str()
                , _stack.rbegin()->second->GetCommandPath().c_str()
                );
        G4Exception( __FUNCTION__
                   , "GEODE003"
                   , FatalErrorInArgument
                   , errBf
                   );
    }

    G4UIcommand * cmdPtr = _stack.rbegin()->second;
    // todo: does Geant4 deletes command together with its parameters?
    auto p = new G4UIparameter( name, G4UIArgTraits<T>::typeCharG4, false );
    p->SetGuidance(desc);
    cmdPtr->SetParameter(p);
    return *this;
}

/// Adds one of typed parameters to the topmost command, with default value 
template<typename T> GenericG4Messenger &
GenericG4Messenger::par( const G4String & name
                       , const G4String & desc
                       , const T & dft
                       ) {
    if( _stack.rbegin()->second->GetCommandName().empty() ) {
        char errBf[128];
        snprintf( errBf, sizeof(errBf)
                , "Can not associate parameter \"%s\" with a dir \"%s\"."
                , name.c_str()
                , _stack.rbegin()->second->GetCommandPath().c_str()
                );
        G4Exception( __FUNCTION__
                   , "GEODE003"
                   , FatalErrorInArgument
                   , errBf
                   );
    }

    G4UIcommand * cmdPtr = _stack.rbegin()->second;
    // todo: does Geant4 deletes command together with its parameters?
    auto p = new G4UIparameter( name, G4UIArgTraits<T>::typeCharG4, true );
    p->SetGuidance(desc);
    p->SetDefaultValue(dft);
    cmdPtr->SetParameter(p);
    return *this;
}

}
}


