#pragma once

#include "globals.hh"
#include "G4VEmProcess.hh"
#include "G4ParticleDefinition.hh"
#include "G4Step.hh"

namespace na64dp {
namespace mc {

class StepMax : public G4VEmProcess {
    G4double fMaxChargedStep;
    G4bool isInitialised;
public:
    StepMax();
    virtual ~StepMax();

    virtual G4bool IsApplicable(const G4ParticleDefinition&) override;
    virtual void PreparePhysicsTable(const G4ParticleDefinition&) override;
    virtual void BuildPhysicsTable(const G4ParticleDefinition&) override;
    virtual void InitialiseProcess(const G4ParticleDefinition*) override;
    virtual G4double PostStepGetPhysicalInteractionLength(const G4Track& track
            , G4double previousStep, G4ForceCondition* cond);
    virtual G4VParticleChange* PostStepDoIt(const G4Track&, const G4Step&) override;
};

}
}
