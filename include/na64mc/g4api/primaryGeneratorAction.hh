#include "na64sw-config.h"
#include "na64mc/modules.hh"

# include <G4VUserPrimaryGeneratorAction.hh>

class G4Event;
class G4GeneralParticleSource;

namespace na64dp {
namespace mc {

/// Minimal primary generator action, utilizes GPS.
class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction {
public:
    PrimaryGeneratorAction();
    ~PrimaryGeneratorAction();
    virtual void GeneratePrimaries(G4Event* anEvent) override;
private:
    G4GeneralParticleSource * _gps;
};

template<>
struct UIModuleTraits<G4VUserPrimaryGeneratorAction> {
    static const std::string command
                           , description
                           , default_;
    struct Config {};
    static G4VUserPrimaryGeneratorAction * instantiate( const G4String & name 
                                                      , ModularConfig & msgr );
};

}
}

