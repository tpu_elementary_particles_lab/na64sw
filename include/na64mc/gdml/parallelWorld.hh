#pragma once

#include <G4VUserParallelWorld.hh>

class G4GDMLParser;  // fwd

namespace na64dp {
namespace mc {

class DetectorConstruction;  // fwd

/**\brief A parallel world class, read from GDML
 *
 * This class is designed for GDML geometry compatibility. It is parameterised
 * with externally-managed GDML parser (typically, user's detector
 * construction), the name of this world and the setup name to be used from
 * a GDML document the parser manages.
 *
 * \note: contrary to the usual (mass) world, the subordinate parallel "setup"
 * will be placed inside a world volume provided by Geant4 itself.
 *
 * \todo: it is possible (but seems redundant) to accept a position&rotation
 * placements for this parallel world (currently we assume no translation or
 * rotation).
 * */
class GDMLParallelWorld : public G4VUserParallelWorld {
protected:
    const G4String _setupName;
    G4GDMLParser * _gdmlParser;
public:
    GDMLParallelWorld( const G4String & pwName
                     , const G4String & setupName
                     , G4GDMLParser * gdmlParserPtr
                     // ...
                     );
    virtual ~GDMLParallelWorld();

    virtual void Construct() override;
    virtual void ConstructSD() override;
};

}
}

