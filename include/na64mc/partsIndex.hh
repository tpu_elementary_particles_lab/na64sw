#pragma once

#include <G4Types.hh>
#include <G4String.hh>
#include <G4TouchableHistory.hh>

#include <map>
#include <cassert>
#include <regex>

#include <gtest/gtest_prod.h>

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4Step;

namespace na64dp {
namespace mc {

/**\brief A mixin class building the "hit index" for complex detectors
 *
 * This is a helper class forming numerical part identifier for detectors
 * assembled with multiple sensitive sub-volumes. This is a frequent case for
 * detectors assemblied from tubes, cells, etc.
 *
 * The numerical identifier (of type `Index_t`) that uses replica numbers as
 * parts of its digits might be naturally used for caching the hit summators,
 * histograms identifiers and other things.
 *
 * Include this class into inheritance chain of the sensitive detector and use
 * corresponding GDML `<auxinfo/>` processor assign index completion instances
 * (appenders) to the certain level of volume hierarchy.
 */
class PartsIndex {
public:
    /// Sub-entity type addressing certain hit generator/collection procedure.
    /// Suitable for addressing particular scorers and procedures within maps.
    typedef uint64_t Index_t;
    /// Type of a single index
    typedef uint16_t SubIndex_t;
    /// Type of dimensions index
    typedef uint8_t NDim_t;

    /// Maximum index (within each single dimension) value
    constexpr static Index_t indexMax = 0x3ff - 1;
    /// Number of dimensions supported
    constexpr static NDim_t nDimMax = 6;

    /// An index of the segment -- helper type for more readable code
    struct Index {
        /// Plain index value this instance is built over
        Index_t code;
        /// Constructs instance over the plain numerical value
        Index(Index_t idx) : code(idx) {}
        /// Constructs instance with all dims unset
        Index() : code(0x0) {}
        /// Implicit cast operator "to plain number"
        operator Index_t() const { return code; }

        /// Returns index value of a dimension
        SubIndex_t get(NDim_t nDim) const;
        /// Returns `true` if index of a specific dimension is set
        bool is_set(NDim_t nDim) const;
        /// Drops the certain dimesnion value to "unset" state
        void reset(NDim_t nDim);
        /// Sets certain dimension number
        void set(NDim_t nDim, SubIndex_t value);

        struct ConversionComparator;

        /**\brief Index conversion rule
         *
         * This helper class provides a way to manipulate cells indexing at the
         * runtime. Constructor accepts string in a form of N (N < `PartsIndex::nDimMax`)
         * tokens delimeted by commas (optionally, followed by spaces). Number of
         * tokens must correspond to the number of segmentation dimesnions. Supported
         * tokens:
         *
         *      keep -- index within a dimension will be preserved
         *      ignore -- (index will be unset)
         *      /[[:digit:]]+ -- the result of division index by number
         *  
         * For instance, consider the rule: "keep, ignore, /5". Then, indexing
         * triplets will be translated as follows:
         *      0,  0,  0  ->  0,  ?,  0
         *     12, 34, 78  -> 12,  ?, 15
         * Note that "unset" index in most cases will be interpreted as an index that
         * does not mean anything and thus will merge all the cells in that dimesnion.
         *
         * Rationale is to provide basic cells-merging capabilities to accumulate
         * spectra within adjoin valumes.
         *
         * \note Has to be copy-constructible
         *
         * \todo Consider support for selectors like `=0, =5-10` etc
         * */
        class Conversion {
        public:
            /// Returns list of tokens
            static std::vector<std::string> tokenize_expression(const std::string &);
        protected:
            /// Regular expression checking the tokens validity
            static const std::regex rxExpression;
            /// Single dimension index conversion rule type
            typedef std::pair<bool, PartsIndex::SubIndex_t> (*ConvertFunction)(PartsIndex::SubIndex_t, size_t);
            /// Array of conversion functions by indexing dimension
            std::pair<ConvertFunction, size_t> cnvFs[PartsIndex::nDimMax];
        public:
            /// Creates trivial conversion rule (keep, keep, ...)
            Conversion();
            /// Creates conversion rule
            Conversion(const std::string & expr);
            /// Converts index, first in pair is whether the index matches the rule
            std::pair<bool, PartsIndex::Index> operator()(PartsIndex::Index i) const;

            bool operator==(const Conversion &) const;
            bool operator!=(const Conversion & a) const { return ! ((*this) == a); }

            struct Hash {
                size_t operator()(const Conversion &) const;
            };
        };
    };

    /// Basic interface of routine completing index with certain dimension
    /// sub-index
    class iIndexAppender {
    public:
        /// Appends provided index instance with information retrieved from
        /// certain touchable
        virtual void append_index( Index &
                                 , const G4VPhysicalVolume *
                                 , G4int replicaNo ) const = 0;
    };
protected:
    /// Names of dimensions
    std::vector<std::string> _names;
    /// A set of index appenders indexed by the logical volume ptr they
    /// correspond to
    std::map<const G4LogicalVolume *, iIndexAppender *> _idxAppenders;
public:
    /// Returns index based on step touchable history and appenders defined.
    Index get_index(const G4Step * aStep) const;

    /// Binds the geometry hierarchy level with certain index appending
    /// operation. Instantiates the particular index appender instance wrt
    /// given `idxAppenderArg` that must correspond to one of `iIndexAppender`
    /// subclasses.
    void define_indexing_level( const G4LogicalVolume * lvPtr
                              , const G4String & idxStrID
                              , const G4String & idxAppenderArg );

    ///\brief String substitution wrt provided index object
    ///
    /// Substitutes occurences of `$1`, `$2` etc in a string with corresponding
    /// numbers from index value. If `unset` is set to terminative symbol,
    /// raises a `std::runtime_error` if referenced index is not set. Avoid
    /// unintentional combinations like `$$2` as substitution is recursive
    static std::string subst_index( const std::string fmt, Index, char unset='\0' );

    ///\brief Makes index from touchable history wrt appenders provided.
    ///
    /// Traverses the touchables hierarchy for a pre-step point, building the
    /// index object of certain type with prescribed `iIndexAppender` hierarchy.
    /// Invokes `append_hits()` once traversing is done.
    ///
    ///\todo Avoid copy construction
    static Index_t obtain_index( G4TouchableHistory
                               , const std::map<const G4LogicalVolume *, iIndexAppender *> & );
};

//
// Basic index appenders
///////////////////////

/**\brief Simple appender class setting N-th dimension to certain const number
 *
 * Assigns the constant number to one of the index dimensions. Useful in case
 * of not-replicated parts of the volumes hierarchy.
 *
 * \todo Custom exceptions
 * \todo ensure the nDim < 4 in ctr
 * \todo ensure the number < 2&&16 in `append_index()`
 */
class ConstantIndexAppender : public PartsIndex::iIndexAppender {
private:
    G4int _nDim, _N;
public:
    ConstantIndexAppender( G4int nDim, G4int number ) : _nDim(nDim), _N(number) {}
    virtual void append_index( PartsIndex::Index & idx
                             , const G4VPhysicalVolume *
                             , G4int ) const override;
};

/**\brief Appender class setting N-th dimension to replica number
 *
 * Assigns replica number to one of the index dimesnions.
 *
 * \todo Custom exceptions
 * \todo ensure the nDim < 4 in ctr
 * \todo ensure the number < 2&&16 in `append_index()`
 * */
class NReplicaIndexAppender : public PartsIndex::iIndexAppender {
private:
    G4int _nDim;
public:
    NReplicaIndexAppender( G4int nDim ) : _nDim(nDim) {}
    virtual void append_index( PartsIndex::Index & idx
                             , const G4VPhysicalVolume * phVol
                             , G4int replicaNo ) const override;
};

}
}

