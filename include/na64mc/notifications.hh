#pragma once

#include <vector>
#include <cstdint>

namespace na64dp {
namespace mc {

class Notifier;  // fwd

/**\brief Notification type structure
 *
 * Geant4 offers a strongly bound system of interfaces that should be
 * implemented to handle few types of important events during its MC
 * simulation workflow:
 *  - Run started
 *  - Event simulation started
 *  - Event simulation ended
 *  - Run ended
 * Additionally NA64SW introduces an event "postprocessing" stage that also has
 * start/end.
 *
 * To facilitate the handling of such an events within a number of destinations
 * that have to be created and attached at the runtime we introduce the
 * notification system.
 *
 * \todo Add mutex for MT case?
 * \todo Add payload to notification?
 * */
struct Notification {
    typedef uint8_t Flags_t;
    enum Type : uint8_t {
        eventSimulationStarted = 0x1,
        eventSimulationEnded = 0x2,
        eventProcessingStarted = 0x4,
        eventProcessingEnded = 0x8,
        runSimulationStarted = 0x10,
        runSimulationEnded = 0x20,
        applicationDone = 0x40,
    };
};

/// Provides an interface of the notification receiver (subscriber) to hook
/// with certain types of MC workflow stages
class iNotificationDestination {
private:
    /// Notification flags
    const Notification::Flags_t _flags;
    /// Ptr to notifier to free the subscription
    Notifier & _notifierRef;
protected:
    /// Subsccribes the destination for certain typed of events
    iNotificationDestination( Notification::Flags_t, Notifier & );
    /// Removes subscription
    virtual ~iNotificationDestination();
    /// Called when the event of certain type occurs
    virtual void receive_notification( Notification::Flags_t ) = 0;
public:
    /// Returns notification flags specified for this subscriber
    Notification::Flags_t notification_flags() const { return _flags; }

    friend class Notifier;
};

/// Notifications emitter broadcasting incoming message among all the
/// subscribers who are interested in particular type of notifications
class Notifier {
private:
    /// List of destinations; keeps the order of insertion
    std::vector<iNotificationDestination *> _destinations;
protected:
    /// Typically called by ctrs to subscribe new instances to certain type(s)
    /// of notifications
    void subscribe(iNotificationDestination * addressee);
    /// Typically called by dtrs of destination instances to remove the subscription
    void remove_subscription(iNotificationDestination * addressee);
public:
    /// Dispatches notifications to subscribed destinations.
    void notify( Notification::Flags_t );

    friend class iNotificationDestination;
};

}
}

