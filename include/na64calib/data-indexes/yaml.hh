#pragma once

#include "na64calib/config-yaml.hh"
#include "na64detID/TBName.hh"

namespace na64dp {
namespace calib {

template<typename T>
class iYAMLDataIndex : public iYAMLIndex
                     , public std::map<EventID, YAML::Node> {
public:
    virtual T yaml_to_data(const YAML::Node &) = 0;

    /// Returns true if entry exists
    virtual bool has( EventID eid ) override
            { return std::map<EventID, YAML::Node>::end()
                  != std::map<EventID, YAML::Node>::find(eid); }
    /// Appends current naming index
    virtual void append( EventID eid, const YAML::Node & node ) override {
        auto ir = std::map<EventID, YAML::Node>::emplace( eid, node );
        if( ! ir.second ) {
            NA64DP_RUNTIME_ERROR( "Failed to emplace information starting"
                    " from event %s -- entry already exists."
                    , eid.to_str().c_str()
                    );
        }
    }
    /// Sets particular data to dispatcher using proxy constructed from
    /// stored YAML node
    virtual void put( EventID eid
                    , GenericLoader::DispatcherProxy & proxy ) override {
        auto it = iYAMLDataIndex<T>::find(eid);
        assert( iYAMLDataIndex<T>::end() != it );
        proxy.set<T>( yaml_to_data(it->second) );
    }
};


/**\brief Run index representation of YAML nodes periodized with event number
 *
 * This is useful template for cases when data index may be entirely loaded
 * into RAM. This might be the case of small, lightweight hierarchical data
 * that rarely changes between runs. In this case loading the entire data
 * index together with data payload is easier than retrieving it by demands.
 * */
template<typename T>
class SimpleYAMLDataIndex : public iYAMLDataIndex<T> {
public:
    typedef T (*Translator)(const YAML::Node &);
private:
    Translator _translator;
public:
    SimpleYAMLDataIndex(Translator t) : _translator(t) {
        assert(t);
    }
    virtual T yaml_to_data( const YAML::Node & node ) override {
        return _translator(node);
    }
};

}
}

