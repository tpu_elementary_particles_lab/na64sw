#pragma once

#include "na64calib/data-indexes/yaml.hh"
#include "na64util/csv-io.hh"
#include "na64util/uri.hh"

#include <fstream>

namespace na64dp {
namespace calib {

/**\brief Reads CSV files into complex calibration data index
 * */
template<typename T, typename ... TupleTypesT>
class iCSVFilesDataIndex : public iYAMLDataIndex<T> {
public:
    typedef std::tuple<TupleTypesT...> Tuple;

    virtual void append_index_from_tuple( T &
                                        , const Tuple & ) = 0;

    virtual T yaml_to_data( const YAML::Node & node ) override {
        const std::string tokenRegexStr = node["tokenRegex"]
                                        ? node["tokenRegex"].as<std::string>()
                                        : std::string("[^\\s,]+");

        #if 0
        util::uriparser::URI srcURI( node["source"].as<std::string>() );
        if( !(srcURI.scheme().empty() || "file" == srcURI.scheme()) ) {
            NA64DP_RUNTIME_ERROR( "Retrieving CSV data by scheme or"
                    " protocol \"%s\" is not implemented."
                    , srcURI.scheme().c_str() );
        }
        // TODO: code below will be moved once we implement another data
        // fetching scheme or protocol
        assert( !srcURI.path().empty() );
        auto names = util::expand_names( srcURI.path() );
        # else
        std::string path = node["source"].as<std::string>();
        auto names = util::expand_names( path );
        # endif
        if( names.size() > 1 ) {
            NA64DP_RUNTIME_ERROR( "Path expression \"%s\" was expanded to"
                    " %d(!=1) names."
                    , path.c_str()
                    , names[0].c_str() );
        } else if(names.empty()) {
            NA64DP_RUNTIME_ERROR( "Can not interpret path \"%s\"."
                    , path.c_str() );
        }

        std::ifstream ifs( names[0] );
        na64dp::CSVReader vr(ifs, 0, tokenRegexStr);
        std::tuple<TupleTypesT...> t;
        T dataIndex;
        while( vr.read<TupleTypesT...>(t) ) {
            append_index_from_tuple( dataIndex, t );
        }
        return dataIndex;
    }
};

}
}
