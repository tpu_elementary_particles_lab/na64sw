#pragma once

#include "na64util/str-fmt.hh"
#include "na64util/observer.hh"
#include "na64util/demangle.hh"

#include <log4cpp/Category.hh>
#include <typeindex>

namespace na64dp {
namespace errors {

/// Thrown if no calibration found
class NoCalibrationInfoEntry : public std::runtime_error {
private:
    const std::type_info & _ti;
    const std::string _name;
public:
    NoCalibrationInfoEntry( const std::string & nm
                          , const std::type_info & ti ) throw();
    const std::string name() const { return _name; }
    const std::type_info & type_info() const { return _ti; }
};

/// Raised when observable is not set yet, but handle dereferencing requested
class PrematureDereferencing : public std::runtime_error {
private:
    const std::type_info & _ti;
    const std::string _name;
public:
    PrematureDereferencing( const std::string & nm
                          , const std::type_info & ti ) throw();
    const std::string name() const { return _name; }
    const std::type_info & type_info() const { return _ti; }
};

}  // namespace ::na64dp::error

namespace calib {

/**\brief Container of calibration data observables
 *
 * Maintains runtime storages of the calibration data. 
 * Dispatches update calls when `set()` method is called among subscribed
 * observers via the `Observable` interface.
 *
 * Helper methods to add and remove the calibration info consumers added --
 * instances that subclassing the `Client` class to maintain (un-)binding
 * on construction / destruction.
 *
 * \see Client
 * */
class Dispatcher {
public:
    struct BaseEntry {};
    typedef std::pair<std::type_index, std::string> CIDataID;

    template<typename T> static CIDataID
    info_id( const std::string & nm ) {
        return std::make_pair( std::type_index(typeid(T)), nm );
    }

protected:
    /// Reference to logger
    log4cpp::Category & _log;
    /// Concrete type of calibration data providing implementing collection
    /// entry for calibration data
    template<typename T>
    struct ConcreteEntry : public BaseEntry
                         , public util::Observable< T > {
        T data;
        ConcreteEntry() {}
        virtual ~ConcreteEntry() {}
        ConcreteEntry( const T & data_ ) : data(data_) {}
        ConcreteEntry( T && data_ ) : data(data_) {}
    };

    typedef std::unordered_map<CIDataID, BaseEntry *, util::PairHash> Index;

    /// Indexes observables by their `std::type_index`
    Index _ciDicts;

    template<typename T> Index::iterator
    _find( const std::string & name ) {
        auto it = _ciDicts.find( info_id<T>(name) );
        if( _ciDicts.end() == it ) {
            throw errors::NoCalibrationInfoEntry( name, typeid(T) );
        }
        return it;
    }

    template<typename T> Index::iterator
    _find_or_create( const std::string & name ) {
        auto it = _ciDicts.find( info_id<T>(name) );
        if( _ciDicts.end() == it ) {
            auto ir = _ciDicts.emplace( info_id<T>(name), new ConcreteEntry<T>() );
            assert(ir.second);
            it = ir.first;
        }
        return it;
    }
public:
    /// Default ctr, initializes logger
    Dispatcher() : _log(log4cpp::Category::getInstance(std::string("calib"))) {}

    /// Dtr, if not all the observables were wiped, prints warning
    ~Dispatcher() {
        if( ! _ciDicts.empty() ) {
            _log.warn( "%d calibration info observables remain on"
                       " dispatcher destruction; possible memory leak."
                     , _ciDicts.size() );
        }
    }

    ///\brief Adds new calibration info client
    ///
    /// Optionally, introduces calibration observable if it does not exist
    /// Returns `false` if observable was already added.
    template<typename T> bool
    subscribe( typename util::Observable<T>::iObserver & client
             , const std::string & ciName ) {
        auto it = _find_or_create<T>( ciName );
        auto entryPtr = static_cast<ConcreteEntry<T>*>(it->second);
        bool ir = entryPtr->bind_observer(client);
        msg_debug( _log
                 , "Observer %p has been bound to"
                   " observable %p %s."
                   , &client, &(entryPtr->data)
                   , util::calib_id_to_str(info_id<T>(ciName)).c_str() );
        return ir;
    }

    ///\brief Removes calibation info client
    ///
    /// Optionally, removes calibration observable, if it was the last client
    template<typename T> void
    unsubscribe( typename util::Observable<T>::iObserver & client
               , const std::string & ciName ) {
        Index::iterator it;
        try {
            it = _find<T>( ciName );
        } catch(errors::NoCalibrationInfoEntry & e) {
            _log.warn( "Unable to unbind observer %p"
                       " from non-existing calibration observable %s."
                     , &client
                     , util::calib_id_to_str(info_id<T>(ciName)).c_str() );
            return;
        }
        auto entryPtr = static_cast<ConcreteEntry<T>*>(it->second);
        entryPtr->unbind_observer(client);
        msg_debug( _log
                 , "Unbinding observer %p from"
                   " observable %p %s."
                 , &client, &(entryPtr->data)
                 , util::calib_id_to_str(info_id<T>(ciName)).c_str() );
        if( entryPtr->n_observers() ) {
            // there are some subscribers, keep observable
            return;
        }
        msg_debug( _log
                 , "Unbinding last observer from"
                   " observable %p %s causes deletion of observable."
                 , &(entryPtr->data)
                 , util::calib_id_to_str(info_id<T>(ciName)).c_str() );
        delete entryPtr;
        _ciDicts.erase(it);
    }

    /**\brief Sets calibration data currently in use, notifying observables
     *
     * For non-existing observables, raises a `NoCalibrationInfoEntry`
     * exception, so caller has to be sure that observable exists.
     * */
    template<typename T> void
    set( const std::string & name, const T & data ) {
        auto it = _find<T>(name);
        auto ce = static_cast<ConcreteEntry<T>*>(it->second);
        ce->data = data;
        _log.debug( "Calibration data"
                " observable of type %s has been updated (copy)."
                , util::calib_id_to_str(info_id<T>(name)).c_str() );
        ce->notify_observers( ce->data );
    }

    template<typename T> void
    set( const std::string & name, T && data ) {
        auto it = _find<T>(name);
        auto ce = static_cast<ConcreteEntry<T>*>(it->second);
        ce->data = std::move(data);
        _log.debug( "Calibration data"
                " observable of type %s has been updated (move by rvalue)."
                , util::calib_id_to_str(info_id<T>(name)).c_str() );
        ce->notify_observers( ce->data );
    }

    bool observable_exists( const CIDataID & k ) const {
        return _ciDicts.end() != _ciDicts.find(k);
    }

    #if 0  // xxx, we barely need it according to the design
    /// Returns calibration data currently in use
    template<typename T> const T &
    get( const std::string & name ) const {
        auto it = _find<T>(name);
        auto ce = static_cast<ConcreteEntry<T>*>(it->second);
        return ce->data;
    }
    #endif
};

/**\brief Read-only reference to calibration data
 *
 * Instance of this class represents "cached" reference to the calibration data
 * for cases when no sophisticated updating procedure is required. Note, that
 * it's usecase is restricted to single dispatcher class as it's
 * biding/unbinding mechanism is controlled by creation/destroying calls, so
 * every instance of this class has to have its lifetime lesser than their
 * dispatcher.
 */
template<typename T>
class Handle : protected util::Observable<T>::iObserver {
private:
    /// Dispatcher reference
    Dispatcher & _d;
    /// Subscription name
    const std::string _name;
    /// Info reference
    const T * _dataPtr;
protected:
    virtual void handle_update( const T & newRef ) override {
        _dataPtr = &newRef;
    }
public:
    /// Construct handle to particular calibration data info entry.
    /// It remains invalid till next recaching.
    Handle( const std::string & nm, Dispatcher & d ) : _d(d)
                                                     , _name(nm)
                                                     , _dataPtr(nullptr) {
        _d.subscribe<T>(*this, nm);
    }
    /// Copy ctr adds new subscriber
    Handle( const Handle<T> & orig ) : _d(orig._d)
                                     , _name(orig._name)
                                     , _dataPtr(orig._dataPtr) {
        _d.subscribe<T>(*this, _name);
    }
    /// rvalue-constructor prevents unsubscription
    Handle( Handle<T> && orig ) : _d(orig.d)
                                , _name(orig.name)
                                , _dataPtr(orig.dataPtr) {
        _d.subscribe<T>(*this, _name);
    }
    /// Destroy calibration data, unbinding the observer.
    ~Handle() {
        _d.unsubscribe<T>(*this, _name);
    }
    /// Retrieve referenced variable
    const T & operator*() const {
        if( !_dataPtr ) {
            throw errors::PrematureDereferencing( _name, typeid(T) );
        }
        return *_dataPtr;
    }
    const T * operator->() const {
        if( !_dataPtr ) {
            throw errors::PrematureDereferencing( _name, typeid(T) );
        }
        return _dataPtr;
    }
};

}
}

