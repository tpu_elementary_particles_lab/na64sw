#pragma once

#include "na64sw-config.h"

#include "na64ee/event-id.hh"

#include "na64calib/manager.hh"

#include <map>
#include <cassert>

namespace na64dp {

namespace errors {

/// Indexing container has no valid range wrt given value
class NoValidForRange : public std::runtime_error {
public:
    NoValidForRange()
            : std::runtime_error( "No valid entry has been found for event." ) {}
};

/// At least two different loaders specified for same type by same event
class CalibrationDataConflict : public std::runtime_error {
private:
    EventID _bgnEvID;
    std::type_index _ti;
    std::string _calibTypeName
              , _loader1  ///< loader specified by current call
              , _loader2  ///< loader specified previously
              ;
public:
    CalibrationDataConflict( EventID evID
                           , const std::type_index & ti
                           , const std::string & ctn
                           , const std::string & l1
                           , const std::string & l2 )
            : std::runtime_error( "Conflict in calibration loader specs." )
            , _bgnEvID(evID)
            , _ti(ti)
            , _calibTypeName(ctn)
            , _loader1(l1)
            , _loader2(l2)
            {}
    virtual ~CalibrationDataConflict() {}
    // TODO: getters ...
};

#if 0  // XXX
class UninitIndexState : public std::runtime_error {
public:
    UninitIndexState()
            : std::runtime_error( "Index state was not initialized." ) {}
};
#endif
}  // namespace na64dp::error

namespace calib {

/**\brief A runs validity range index helper class.
 *
 * Maps objects that are valid _from_ certain run number. This helper class
 * incapsulates `std::upper_bound()` over sorted associative array to provide
 * a bit more intuitive way of retreiving the data associated with range.
 *
 * Ususal application is to index calibration data wrt runs ranges. */
template<typename KeyT, typename T>
class RangeIndex : public std::map<KeyT, T> {
public:
    typedef std::map<KeyT, T> Parent;
public:
    typename Parent::iterator
    get_valid_entry_for(KeyT rn) {
        assert( !Parent::empty() );
        // it->first > rn, i.e. get iterator to the next element
        auto it = Parent::upper_bound( rn );
        if( it == Parent::begin() ) {
            return Parent::end();
        }
        // Return element previous to upper bound
        return --it;
    }
};

/**\brief A basic externally-configured calibration index class.
 *
 * Indexes calibration entries by run number. Each entry of certain
 * calibration type considered valid *from* certain run number *until* the
 * next run number.
 *
 * \note A notorious drawback of this type of indexing is that it does not
 * allow to specify discrete validity periods with gaps in between. This
 * situation may represent an important case.
 */
class RangeOverrideRunIndex : public iCalibRunIndex {
private:
    struct CalibDataDescription {
        std::string loaderName;
        // ...?
    };
    /// Collection of run range sub-indexes, grouped by calibration data type
    std::unordered_map< Dispatcher::CIDataID
                      , RangeIndex< EventID
                                  , CalibDataDescription>
                      , util::PairHash > _types;
public:
    virtual ~RangeOverrideRunIndex() {}
    /// Collects calibration information differences between two runs
    virtual void updates( EventID oldEventID
                        , EventID newEventID
                        , UpdatesList & ) override;
    
    /// Adds new calibration data entry into index
    void add_entry( EventID bgnEvID
                  , const Dispatcher::CIDataID & ciType
                  , const std::string & loaderName
                  );
};

#if 0
/**\brief Stateful wrapper around `RunsRangeIndex` template.
 *
 * Implements lazy behaviour of re-setting state on run number changes.
 *
 * \note §23.1.2.8 in the standard states that insertion/deletion operations on
 * a set/map will not invalidate any iterators to those objects (except
 * iterators pointing to a deleted element).*/
template<typename T>
class StatefulRunsRangeIndex : public RunsRangeIndex<T> {
public:
    typedef RunsRangeIndex<T> Parent;
protected:
    typename Parent::iterator _current;
public:
    /// Constructor. Initializes current state to invalid state.
    StatefulRunsRangeIndex() {
        _current = Parent::end();
    }
    /// Considers given run ID. If it corresponds to a range different from
    /// previous, current state will be changed to new one and `true' will be
    /// returned, otherwise only returns `false'.
    bool set_run_no(na64ee::runNo_t nr) {
        auto it = Parent::get_valid_entry_for(nr);
        if( it == _current )
            return false;
        _current = it;
        return true;
    }
    /// Current value instance getter.
    T & current() {
        if( Parent::end() == _current )
            throw error::UninitIndexState();
        return _current->second;
    }
};
#endif

}  // namespace calib
}  // namespace na64dp

