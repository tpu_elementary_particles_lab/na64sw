#pragma once

#include "na64util/uri.hh"
#include "na64detID/detectorID.hh"
#include "na64calib/dispatcher.hh"

#include <log4cpp/Category.hh>

#include <na64ee/event-id.hh>

namespace na64dp {
namespace nameutils {
class DetectorNaming;  // fwd
}  // namespace na64dp::utils

namespace error {
/// Exception raises if third party submits a duplicating calibration entry
class DuplicatingCalibrationEntry : public std::runtime_error {
private:
    const std::type_info & _typeInfo;
    const std::string & _name;
public:
    DuplicatingCalibrationEntry( const char * what_
                               , const std::type_info & ti
                               , const std::string & detName_ ) throw()
        : std::runtime_error(what_)
        , _typeInfo(ti)
        , _name(detName_) {}
    const std::type_info & type_info() const throw() { return _typeInfo; }
    const std::string & det_name() const throw() { return _name; }
};
}  // namespace na64dp::error

template<typename T> struct CalibrationTraits;  // default is not defined

namespace calib {

/**\brief Decides what calibration data have to be updated based on run number
 *
 * Class implementing this interface has to decide which calibration data types
 * has to be updated between runs.
 */
struct iCalibRunIndex {
    /// Type that uniquely identies a record of calibration data that has to be
    /// loaded
    struct CIUpdateID {
        /// Name of the loader that has to fetch the calibration data
        std::string loaderName;
        /// Run number that often (but not obligatory) used by the loader to
        /// identify the calibration record
        EventID eventID;
    };
    /// List of updates that shall be performed, in order
    struct UpdatesList : public std::unordered_map< Dispatcher::CIDataID
                                                  , std::vector<CIUpdateID>
                                                  , util::PairHash
                                                  > {
        /// Appends updates vector
        void enqueue_update( Dispatcher::CIDataID key
                           , const std::string & loaderName
                           , EventID calibEventID );

        /// Appends updates vector (template shortcut)
        template<typename T>
        void enqueue_update_of_type( const std::string & calibTypeName
                                   , const std::string & loaderName
                                   , EventID calibEventID
                                   ) {
            auto key = Dispatcher::info_id<T>(calibTypeName);
            enqueue_update( key, loaderName, calibEventID );
        }
    };
    /// Returns list of the run to be updated between events
    virtual void updates( EventID oldEventID
                        , EventID newEventID
                        , UpdatesList & ) = 0;
};

/**\brief Calibration data loader interface
 *
 * Loader subclasses incapsulates details of how the certain data type has to
 * be loaded using dispatcher isntance. `iLoader` subclasses implement the
 * concrete protocol of data retrieval.
 *
 * For instance performing a database request for certain data type and
 * certain event is the subject of loader instance. Retrieving data from a
 * file or fetching RESTful data are the other important examples.
 */
struct iCalibDataLoader {
    virtual ~iCalibDataLoader() {}
    /// Loads calibration data into given dispatcher instance
    virtual void load_data( EventID eventID
                          , Dispatcher::CIDataID
                          , Dispatcher & ) = 0;
    /// Shall return whether the specified calibration is available
    virtual bool has( EventID eventID
                    , Dispatcher::CIDataID ) = 0;
};

template<typename T> class User;  // fwd

/**\brief Runtime calibration data manager
 *
 * Manages runtime calibration data fetch, retrieval, storage and
 * on-change notifications.
 *
 * Has multiple instances of run indexes and calibration data loader imposed
 * at a runtime. The first kind of entities implements interface
 * `iCalibRunIndex` that shall impose what kind of calibration data should be
 * updated and from where. The former will load it.
 *
 * Order of insertion of the run indexes matters in case of collisions:
 * typically the last inserted will displace updates imposed by previous. This
 * behaviour is not strict: `iCalibRunIndex` instances get the reentrant
 * updates dictionary and may abstain from displacing calibration data,
 * depending on implementation (though, displacing is implied).
 *
 * Manager does not maintain the lifetime run indexes and loaders.
 *
 * There must be a single instance of the manager in process.
 * */
class Manager : public Dispatcher {
private:
    /// Calibration info index used to retrieve updates list
    std::vector<iCalibRunIndex *> _ciIndexes;
    /// Calibration data loader uset to fetch updates
    std::unordered_map<std::string, iCalibDataLoader *> _ciLoaders;
    /// Current event ID
    EventID _cEventID;

    /// Internal method collecting updates from runs indexes
    bool _collect_updates( iCalibRunIndex::UpdatesList &, EventID);
    /// Internal method performing loading of relevant updates
    void _apply_updates( const iCalibRunIndex::UpdatesList & );
public:
    Manager();
    virtual ~Manager();

    /// Sets current event ID, used by data sources
    void event_id( EventID );
    /// Returns current run number
    EventID event_id() const { return _cEventID; }

    /// Adds calibration data loader instance
    virtual void add_loader( const std::string & loaderName
                           , iCalibDataLoader * loaderPtr );
    /// Adds run index instance
    virtual void add_run_index( iCalibRunIndex * runIndex );
};

}
}  // namespace na64dp

