#pragma once

#include "na64calib/mean-value.hh"
#include "na64detID/detectorID.hh"
#include "na64calib/data-indexes/csv.hh"

#include <unordered_map>

namespace na64dp {
namespace calib {

/// Indexes SADC pedestal pairs by detector ID -- calibration data type
typedef std::unordered_map< DetID_t, std::pair<NDValue, NDValue> > Pedestals;

/**\brief Pedestals CSV indexes file reader
 *
 * ASCII CSV files referenced by YAML nodes shall be in format:
 *  - Full name of SADC detector entity (e.g. "ECAL0:0-2-0")
 *  - "Odd"/"Even" token denoting ADC channel
 *  - Pedestal value (float)
 *  - Pedestal error (sigma for normal distribution)
 *  - Number of entries
 * */
class PedestalsCSVDataIndex :
    public iCSVFilesDataIndex< Pedestals
                             , std::string, std::string
                             , double, double, size_t> {
protected:
    /// Naming handle to dereference detector name IDs into numerical ones
    Handle<nameutils::DetectorNaming> _naming;
    /// Logger reference
    log4cpp::Category & _log;
public:
    /// Binds name handle to dispatcher
    PedestalsCSVDataIndex( Dispatcher & cdsp )
            : _naming("default", cdsp)
            , _log(log4cpp::Category::getInstance("calibrations.pedestals")) {}
    /// Appends data index with tuple read from CSV file
    virtual void append_index_from_tuple( Pedestals &
                                        , const Tuple & t ) override;
};

}
}
