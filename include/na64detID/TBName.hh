#pragma once

#include "na64detID/detectorID.hh"
#include "na64util/selector.hh"

#include "na64detID/chips.hh"
#include "na64detID/kins.hh"

/**\file NA64DPTBName.hh
 *
 * The TBName comes from NA58 (COMPASS) nomenclature. This short string ID
 * usually looks like "ECAL0", "GM01X1", "DC05Y2__" and should uniquely
 * identify a particular detector assembly. This function returns detector ID
 * numerical value without a `payload' section that sometimes refers to
 * particular detector channel (e.g. NA64 ECAL has a composite segmentation
 * scheme where X and Y segmentation is determined not in TBName).
 */

namespace YAML {
class Node;  //fwd
}

namespace na64dp {
namespace nameutils {

/// \brief Defines mapping of detector textual ID to numerical ones.
///
/// This class seems to be more complex that simple task of one-to-one
/// correspondance would require, but actually its goal is to expose all the guts
/// of two-staged naming conversion scheme to user code. Considering two
/// use-cases:
///
/// - Conversion of naitve DDD TBNames, like `ECAL0`, `GM03X__` to numerical ID
/// - Conversion of NA64DP extended TBnames like `ECAL0:1-1`, `HCAL3:2-2` to
/// numerical ID.
///
/// Instance of this object is usually associated with calibration data (see
/// class CalibHandle).
class DetectorNaming : protected ChipFeaturesTable
                     , protected KinFeaturesTable {
protected:
    /// Internal version of subst dictionary appending function that relies
    /// on chin&kin features being already found in tables
    static void _append_subst_dict_for( DetID did
                                      , std::map<std::string, std::string> & m
                                      , const ChipFeaturesTable::Features * chipFtsPt=nullptr
                                      , const KinFeaturesTable::Features * kinFtsPtr=nullptr );
public:
    typedef ChipFeaturesTable::Features ChipFeatures;
    using ChipFeaturesTable::chip_id;
    using ChipFeaturesTable::chip_features;
    using ChipFeaturesTable::chip_add;
    using ChipFeaturesTable::has_chip;

    typedef KinFeaturesTable::Features KinFeatures;
    using KinFeaturesTable::KinID;
    using KinFeaturesTable::kin_id;
    using KinFeaturesTable::kin_features;
    using KinFeaturesTable::define_kin;
    using KinFeaturesTable::has_kin;

    /// Wipes out all added entries.
    void clear();

    /// Defines new detector kin entry with chip given by its name
    KinFeatures & define_kin( const std::string & kinName
                            , DetKin_t kinID
                            , const std::string & chipName
                            , const std::string & fmt="{kin}{statNum}"
                            , const std::string & kinDescription=""
                            , const std::string & histsTPath="" );

    /// Returns DAQ detector entity name by numerical detector identifier.
    std::string name( DetID did ) const;
    /// Shortcut for "name()"
    std::string operator[]( DetID did ) const { return name(did); }

    /// Returns detector numerical identifier by given DAQ name
    DetID id(const std::string & nm, bool noThrow=false) const;
    /// Shortcut for "id()"
    DetID operator[]( const std::string & nm ) const { return id(nm); }

    /// Appends given map with substitution entries
    void append_subst_dict_for(DetID, std::map<std::string, std::string> &) const;

    /// Returns string template for histograms
    const std::string & name_template(DetID) const;

    /// Returns path string template for detector (by ID)
    const std::string & path_template(DetID detID) const;
};


namespace detail {
template<std::string KinFeaturesTable::Features::*attr> struct DetStrFts;
// nameFormat
template<>
struct DetStrFts<&KinFeaturesTable::Features::nameFormat> {
    constexpr static std::string ChipFeaturesTable::Features::*chipPtr
            = &ChipFeaturesTable::Features::nameFormat;
};
// dddNameFormat
template<>
struct DetStrFts<&KinFeaturesTable::Features::dddNameFormat> {
    constexpr static std::string ChipFeaturesTable::Features::*chipPtr
            = &ChipFeaturesTable::Features::dddNameFormat;
};
// pathFormat
template<>
struct DetStrFts<&KinFeaturesTable::Features::pathFormat> {
    constexpr static std::string ChipFeaturesTable::Features::*chipPtr
            = &ChipFeaturesTable::Features::pathFormat;
};
};

/// Returns detector string pattern according to order of overriding
template<std::string KinFeaturesTable::Features::*attr> const std::string &
detector_string_patern( const ChipFeaturesTable::Features & chipFts
                      , const KinFeaturesTable::Features & kinFts ) {
    if( ! ((kinFts.*attr).empty()) ) return kinFts.*attr;
    return chipFts.*(detail::DetStrFts<attr>::chipPtr);
}

}  // namespace na64dp::utils


/// Alias for common detector ID selection type
typedef dsul::Selector<DetID, nameutils::DetectorNaming> DetSelect;

}  // namespace na64dp
