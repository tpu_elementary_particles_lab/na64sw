#pragma once

#include "na64sw-config.h"
#include "na64detID/detectorIDSelect.h"

#include <cstdint>
#include <cassert>
#include <stddef.h>  // for size_t in global ns

/**\file NA64DPDetectorID.hh
 * \brief Numerical detector id accessories.
 * \todo use C bitfields in addition
 * */

namespace na64dp {

/// Fully determines detector within the NA64 experiment.
typedef uint32_t DetID_t;
/// Defines detector chip (e.g. SADC, APV, F1, etc)
typedef uint8_t DetChip_t;
/// Defines detector kin (e.g. ECAL, MM, GM, LYSO, etc)
typedef uint8_t DetKin_t;
/// Defines the orderly number type within the DDD naming scheme, i.e. the
/// postfix in names like "HCAL2" (2), "GM05" (5).
typedef uint8_t DetNumber_t;
/// Additional detector-identifying information (like cell number, x/y-index
/// and so on) may be also encoded in the payload part of the DetectorID.
typedef uint16_t DetIDPayload_t;

namespace aux {

template< typename T
        , uint8_t OffsetP
        , DetID_t MaxP
        , DetID_t MaskP> struct DetIDFieldHelper {
    static void unset(DetID_t & id) {
        id &= ~MaskP;
    }
    static void set(DetID_t & id, T val) {
        assert( val <= MaxP );
        unset(id);
        id |= ((val+1) << OffsetP);
    }
    static T get(const DetID_t & id) {
        return ((id & MaskP) >> OffsetP)-1;
    }
    static bool is_set(const DetID_t & id) {
        return (id & MaskP) >> OffsetP;
    }
};

/* Nasty implementation details:
 *  3 bits: chip number (1-7)
 *  5 bits: detector kin (1-31)
 *  8 bits: detector number (1-255)
 * 16 bits: payload
 */
constexpr uint8_t gChipIDBOffset    = 29
                , gKinIDBOffset     = 24
                , gDetNumOffset     = 16
                , gPayloadOffset    = 0
                ;

constexpr DetID_t gChipIDMax    = 0b110
                , gKinIDMax     = 0b11110
                , gDetNumMax    = 0b11111110
                , gPayloadMax   = 0xfffe
                ;

constexpr DetID_t gMaskChipID   = (0b111 << gChipIDBOffset)
                , gMaskKinID    = (0b11111 << gKinIDBOffset)
                , gMaskDetNum   = (0b11111111 << gDetNumOffset)
                , gMaskPayload  = (0xffff << gPayloadOffset)
                ;

typedef DetIDFieldHelper<DetChip_t,      gChipIDBOffset, gChipIDMax,  gMaskChipID>  ChipID;
typedef DetIDFieldHelper<DetKin_t,       gKinIDBOffset,  gKinIDMax,   gMaskKinID>   KinID;
typedef DetIDFieldHelper<DetNumber_t,    gDetNumOffset,  gDetNumMax,  gMaskDetNum>  DetNumID;
typedef DetIDFieldHelper<DetIDPayload_t, gPayloadOffset, gPayloadMax, gMaskPayload> DetPlID;

}  // namespace aux

///\brief An OO-wrapper over the detector number.
///
/// This struct wraps DetID_t value offering a bit more convenient way to
/// operate with its bit-encoded information.
struct DetID {
    /// Calue stored within the wrapper.
    DetID_t id;

    /// Construct the zeroed id.
    DetID() : id(0x0) {}
    /// Construct the wrapper around value.
    DetID(DetID_t id_) : id(id_) {}
    /// A dedicated constructor accepting all the members: chip, kin, number
    /// and payload as separated arguments. Default values causes corresponding
    /// fields to not be initialized.
    /// \note for number "unint" marker is 255 (0xff)
    DetID( DetChip_t chipID
         , DetKin_t kinID
         , DetNumber_t num=0xff
         , DetIDPayload_t pl=0x0
         ) : id(0x0) {
        if(chipID) chip(chipID);
        if(kinID) kin(kinID);
        if(0xff != num) number(num);
        if(pl) payload(pl);
    }

    /// Sets chip code
    void chip( DetChip_t v ) { assert(v); aux::ChipID::set(id, v); }
    /// Returns true if chip value is set
    bool is_chip_set() const { return aux::ChipID::is_set(id); }
    /// Clears chip code
    void unset_chip() { aux::ChipID::unset(id); }
    /// Returns chip code
    DetChip_t chip() const { assert(is_chip_set()); return aux::ChipID::get(id); }

    /// Sets detector kin
    void kin( DetKin_t v ) { assert(v); aux::KinID::set(id, v); }
    /// Returns true if kin value is set
    bool is_kin_set() const { return aux::KinID::is_set(id); }
    /// Clears kin code
    void unset_kin() { aux::KinID::unset(id); }
    /// Returns detector kin
    DetKin_t kin() const { assert(is_kin_set()); return aux::KinID::get(id); }

    /// Sets detector orderly number (e.g. 2 for HCAL2, 6 for GM06, etc)
    void number( DetNumber_t v )  { aux::DetNumID::set(id, v); }
    /// Returns true if number value is set
    bool is_number_set() const { return aux::DetNumID::is_set(id); }
    /// Clears number
    void unset_number() { aux::DetNumID::unset(id); }
    /// Returns detector orderly number (e.g. 2 for HCAL2, 6 for GM06, etc)
    DetNumber_t number() const { assert(is_number_set()); return aux::DetNumID::get(id) ; }

    /// Sets the fine identifier value (for certain station)
    void payload(DetIDPayload_t v)  { assert(v); aux::DetPlID::set(id, v); }
    /// Returns true if payload value is set
    bool is_payload_set() const { return aux::DetPlID::is_set(id); }
    /// Clears payload
    void unset_payload() { aux::DetPlID::unset(id); }
    /// Returns the fine identifier value (for certain station)
    DetIDPayload_t payload() const  { assert(is_payload_set()); return aux::DetPlID::get(id); }

    /// Type-casting operator, simplifying the conversion.
    operator DetID_t () { return id; }
};

namespace utils {
/// Common selectors set for detector selection DSL, NULL-terminated
extern const na64dpsu_SymDefinition * gDetIDGetters;
}

}

