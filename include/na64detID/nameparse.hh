#pragma once
#include "na64sw-config.h"

#include <string>

namespace na64dp {
namespace nameutils {

/**\brief Returns true if given token may be splitted into name components
 *
 * Uses regular expression internally to split the provided token into
 * detector name components. If string does not match, returns false. This
 * function defines only basic (lexical) match -- it does not guarantee that
 * given string is really valid DDD TBName or full detector name.
 *
 * \param[in] str String that has to be interpreted as detector name
 * \param[out] detName detector name part. Can not be empty if `str` matches.
 * \param[out] number detector number part. Can be empty even on match.
 * \param[out] payload detector name payload part. Can be empty.
 * \return whether provided string matches the naming scheme.
 * */
bool
tokenize_name_str( const std::string & str
                 , std::string & detName
                 , std::string & number
                 , std::string & payload );

}
}
