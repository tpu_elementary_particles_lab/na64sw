#include "na64sw-config.h"
#include "na64detID/detectorID.hh"

#include <map>

/**\file CellID.hh
 *
 * File contains common payload definition for SADC detectors.
 *
 * \todo move from ECAL dir
 * */

namespace na64dp {

/// Numeric type keeping encoded cell identifier
typedef DetIDPayload_t CellID_t;

/**\brief Spatial coordinates of detector cell
 *
 * Represents shortened identifier for the cell of spatially-segmentated
 * detectors: x, y, z (whether it is preshower).
 *
 * One has to consider difference between "unset" cell identifier and
 * identifier corresponding to 0x0x0: both will return 0 with `get_x()`,
 * `get_y()`, `get_z()`, but meaning of this zeroes is different. Consider
 * additional check with `is_set()` method to catch the difference.
 */
struct CellID {
    /// Maximum index value
    constexpr static DetIDPayload_t idxMax = 0x1e;

    /// Public member, keeping the encoded information about x/y/z cell idxs.
    CellID_t cellID;
    /// Constructs "unset" cell ID
    CellID() : cellID(0x0) {}
    /// Copies cell ID. "Unset" identifier will be kept.
    CellID( CellID_t id_ ) : cellID(id_) {}
    /// Sets the cell ID
    CellID( int x, int y, int z );
    /// Returns X index of cell. For both, unset and x=0 cell the 0 will be returned.
    unsigned int get_x() const;
    /// Returns `true` if X index is set.
    bool is_x_set() const;
    /// Returns Y index of cell. For both, unset and y=0 cell the 0 will be returned.
    unsigned int get_y() const;
    /// Returns `true` if Y index is set.
    bool is_y_set() const;
    /// Returns Z index of cell. For both, unset and z=0 cell the 0 will be returned.
    unsigned int get_z() const;
    /// Returns `true` if Z index is set.
    bool is_z_set() const;
    /// Sets X index of the cell
    void set_x(unsigned int x);
    /// Sets Y index of the cell
    void set_y(unsigned int y);
    /// Sets Z index of the cell
    void set_z(unsigned int z);
    /// Returns true if at least one index was set
    bool is_set() const { return cellID; }

    /// Appends string values text template rendering context
    static void append_completion_context( DetID, std::map<std::string, std::string> & );
    /// Converts from WireID to string
    static void to_string( DetIDPayload_t, char *, size_t available );
    /// Converts from string to WireID 
    static DetIDPayload_t from_string( const char * );
};

}  // namespace na64dp
