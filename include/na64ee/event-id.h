# ifndef H_NA64_EVENT_IDENTIFIERS_H
# define H_NA64_EVENT_IDENTIFIERS_H

# include "na64sw-config.h"

/**@file na64_event_id.h
 *
 * @brief This file declares structures and uxilliary routines for identifying
 *        and event in the NA64 middleware routines.
 *
 * Considering the need to accumulate <=1e14 events per NA64 experiment this
 * structures were carefully crafted in order to provide event identification
 * mechanizm minimizing computational cost.
 *
 * One can make a distinction between event number identifying certain event
 * instance inside all the run or inside particular chunk in run. By technical
 * reasons it is desirable to identify events inside the chunk as it seems to
 * be cheaper to read the event from particular file (which is a chunk itself)
 * then to look up it inside all the files of a particular run.
 *
 * Here we provide both ways to identify event.
 * */

# include <limits.h>
# include <stdio.h>
# include <stdint.h>

# ifdef __cplusplus
extern "C" {
# endif  /*__cplusplus*/

/** Plain numeric type identifying the certain event in NA64 experiment. */
typedef uint64_t na64sw_EventID_t;

/**@macro for_all_eventID_fields
 * @brief X-Macro indexing all the bit fields of the event ID.
 * */
#define NA64SW_FOR_ALL_EVENT_ID_FIELDS( m, ... )                               \
    m( uint32_t, eventNo,       30,    0, 0x3fffffff,         __VA_ARGS__ )   \
    m( uint16_t, spillNo,       12,   30, 0x3ffc0000000,      __VA_ARGS__ )   \
    m( uint8_t,  reservedBit,   1,    42, 0x40000000000,      __VA_ARGS__ )   \
    m( uint32_t, runNo,         21,   43, 0xfffff80000000000, __VA_ARGS__ )

#define define_event_id_field_type( type, name, nBits, offset, bitmask, ... ) \
    typedef type na64sw_ ## name ## _t;
NA64SW_FOR_ALL_EVENT_ID_FIELDS( define_event_id_field_type )
#undef define_event_id_field_type

#define declare_event_id_get_set( type, name, nBits, offset, bitmask, ... ) \
na64sw_ ## name ## _t na64sw_get_ ## name ( na64sw_EventID_t eid ); \
void na64sw_set_ ## name ( na64sw_EventID_t * eidPtr, na64sw_ ## name ## _t v );
NA64SW_FOR_ALL_EVENT_ID_FIELDS( declare_event_id_get_set )
#undef declare_event_id_get_set

#define declare_event_field_limits( type, name, nBits, offset, bitmask, ... ) \
extern const na64sw_ ## name ## _t na64sw_g_ ## name ## _max;
NA64SW_FOR_ALL_EVENT_ID_FIELDS( declare_event_field_limits )
#undef declare_event_field_limits

/** Commoin ID constructor without available chunk number. */
na64sw_EventID_t
na64sw_assemble_event_id(
        na64sw_runNo_t    runNo,
        na64sw_spillNo_t  spillNo,
        na64sw_eventNo_t  eventInSpillNo );

/** Encodes event ID into given string buffer */
size_t na64sw_eid2str(na64sw_EventID_t eid, char * buf, size_t buflen);

/** Decodes event ID from given string buffer */
na64sw_EventID_t na64sw_str2eid( const char * bf, char ** eventEnd );

# ifdef __cplusplus
}
# endif  /*__cplusplus*/

# endif  /* H_NA64_EVENT_IDENTIFIERS_H */




