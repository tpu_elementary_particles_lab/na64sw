/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64ee_errorHandle.h"

# include <exception>
# include <stdexcept>

namespace na64ee {

typedef NA64EE_EnumScope::NA64EE_ErrorCode ErrorCode;

class Exception : public std::runtime_error {
private:
    ErrorCode _code;
public:
    Exception() = delete;
    Exception( ErrorCode code_, const std::string & details ) :
                std::runtime_error(details), _code(code_) {}

    ErrorCode code() const { return _code; }
};  // class Exception

# define na64ee_raise( codeName, ... )                          \
while(true) {                                                   \
    char errBf[128];                                            \
    snprintf( errBf, sizeof(errBf), __VA_ARGS__ );              \
    throw Exception( ErrorCode::na64ee_ ## codeName, errBf );   \
break; }

}  // namespace na64ee

