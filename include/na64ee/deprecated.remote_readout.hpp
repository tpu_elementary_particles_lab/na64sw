/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_EVENT_EXCHANGE_DDD_REMOTE_READOUT_H
# define H_NA64_EVENT_EXCHANGE_DDD_REMOTE_READOUT_H

# include "na64_event_id.hpp"

# ifndef SUPPORT_DDD_FORMAT
# warning "This header declares routines and structs designed for dealing with " \
"DaqDataDecoding (DDD) library, however your build of NA64EE lib seems to be "   \
"not configured against DDD library and these routines are disabled."
# endif  // SUPPORT_DDD_FORMAT

# include "na64ee_readout.hpp"
# include <iosfwd>
# include <map>
# include <list>
# include <memory>

namespace CS {
class DaqEvent;  // FWD
}  // namespace CS

namespace na64ee {

std::shared_ptr<CS::DaqEvent>
fetch_event( const char * url,
             DDDIndex::RunNo_t runNo,
             DDDIndex::SpillNo_t chunkNo,
             DDDIndex::EventNo_t eventNo );

}  // namespace na64ee

# endif  // H_NA64_EVENT_EXCHANGE_DDD_REMOTE_READOUT_H

