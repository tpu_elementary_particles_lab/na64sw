#pragma once

namespace na64dp {
namespace util {

/**\brief Adds support for some auxiliary logging facilities
 *
 * Will add following entities to `log4cpp` object factories:
 *  - Appenders:
 *      -- (TODO) "ostream-stdout" -- present in native `log4cpp` properties
 *          configurator, but weirdly absent in factory
 *      -- (TODO) "Geant4 UI" -- does nothing, if there is no running Geant4 session
 *
 * \todo JSON layout
 * \todo ZMQ/remote/EPI appender
 * */
void inject_extras_to_log4cpp();

}
}

