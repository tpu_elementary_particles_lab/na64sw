#ifndef H_NA64DP_STRIP_LAYOUT_H
#define H_NA64DP_STRIP_LAYOUT_H

#include "na64sw-config.h"

/**\file Layout.h
 * For certain "DAQ wire number" the orderly number of the strip (wire) has to
 * be found in order to perform spatial reconstruction. In principle it is a
 * 1-to-1 relation.
 * However for ETH(Z) Micromegas detector, the wiring layout is somewhat more
 * sophisticated, and one DAQ wire number corresponds to pentet of wires being
 * galvanically connected (64 pentets covers 320 wires in 64 APV channels).
 * */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/// Real (physical) wire number number type
typedef uint16_t NA64DP_APVStripNo_t;
/// DAQ (channel) wire number type
typedef uint16_t NA64DP_APVWireNo_t;
/// Number of wires type
typedef uint8_t NA64DP_APVNWires_t;

extern const NA64DP_APVStripNo_t na64dp_MM_JointWires[64][5];

/**\brief APV demultiplexing callback function type
 *
 * Callback function type to translate the DAQ wire number into physical wire
 * number for APV detectors */
typedef const NA64DP_APVStripNo_t * (*NA64DP_DemultiplexingMapping)( NA64DP_APVWireNo_t, NA64DP_APVNWires_t * );

/**\brief Demultiplexing mapping for MuMegas */
const NA64DP_APVStripNo_t * na64_APV_strip_mapper__micromegas_joints( NA64DP_APVWireNo_t, NA64DP_APVNWires_t * );
/**\brief Demultiplexing mapping for identity detectors */
const NA64DP_APVStripNo_t * na64_APV_strip_mapper__identity( NA64DP_APVWireNo_t, NA64DP_APVNWires_t * );
/**\brief Demultiplexing mapping for triple GEMs. */
const NA64DP_APVStripNo_t * na64_APV_strip_mapper__GEM_demultiplexing_scheme( NA64DP_APVWireNo_t, NA64DP_APVNWires_t * );

#ifdef __cplusplus
}  // extern "C"
#endif

# endif  /* H_NA64DP_STRIP_LAYOUT_H */


