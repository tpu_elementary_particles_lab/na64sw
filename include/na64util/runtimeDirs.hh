#pragma once

#include <string>
#include <vector>
#include <sstream>

namespace na64dp {
namespace util {

/**\brief Class represents a cached state for accessible directories.
 *
 * Helper class keeping a set of directories to perform file lookup. Designed
 * for reentrant usage to speedup momentary usage (does not update itself
 * automatically).
 * */
class RuntimeDirs : public std::vector<std::string> {
private:
    std::stringstream _mlLog;
public:
    /**\brief Accepts colon-separated list of directories
     *
     * During instantiation, filters existing, accessible directories and puts
     * them into parental class (vector) for subsequent usage.
     * */
    RuntimeDirs( const char * locations="." );

    /**\brief Performs lookup of the file given by 
     * */
    std::vector<std::string> locate_files( const std::string & ) const;

    /// Returns internal log reference for 
    std::string dir_lookup_log() const { return _mlLog.str(); }
};


}
}

