#ifndef H_NA64DP_SUMS_H
#define H_NA64DP_SUMS_H

#ifdef __cplusplus
#include <cmath>  // needed for C++ section below
#endif

/**\brief Summation routines
 * \file NA64DPSums.h
 *
 * This C header defines various single-pass (online) statistical algorithms
 * that may be used in handlers to avoid repeatitive invocations of the
 * pipeline. References:
 *
 *  - http://www.numericalexpert.com/articles/single_pass_stat/
 *  - doi:https://doi.org/10.1007%2Fs00607-005-0139-x
 *  - https://www.johndcook.com/blog/2008/11/05/how-to-calculate-pearson-correlation-accurately/
 *  - https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_Online_algorithm
 *  - https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online
 * */

#ifdef __cplusplus
extern "C" {
#endif

/**\brief A generalized Kahan-Babuška-Summation-Algorithm state instance
 *
 * Declares reentrant state object for the Klein's summation algorithm present
 * in doi:https://doi.org/10.1007%2Fs00607-005-0139-x .
 * An \f$i\f$-th algorithm have the
 * error \f$ O(n(log_2 (n) \epsilon)^{i+1}) \f$.
 *
 * Usage implies initialization, multiple iterative increment operations and
 * result retreival with corresponding functions.
 * */
typedef struct na64dp_KleinScorer {
    double s, cs, ccs;
} na64dp_KleinScorer_t;

/**\brief (Re-)initializes Klein scorer */
void na64dp_sum_klein_init(na64dp_KleinScorer_t * S);
/**\brief Adds a value to Klein scorer */
void na64dp_sum_klein_add(na64dp_KleinScorer_t * S, double v );
/**\brief Returns sum of Klein scorer */
double na64dp_sum_klein_result(const na64dp_KleinScorer_t * S);


/**\brief A running covariance coefficient calculus for two variables
 *
 * Explots the running approach to calculate co-moment of the relative
 * distribution.
 *
 * For summation, the Klein's scorer is applied as a reentrant state.
 * */
typedef struct na64dp_Covariance {
    unsigned long n;  /**< Number of pairs being considered */
    na64dp_KleinScorer_t xMean  /**< Current mean of X samples */
                       , yMean  /**< Current mean of Y samples */
                       ;
    na64dp_KleinScorer_t d2x  /**< Sum of X deviation squares */
                       , d2y  /**< Sum of Y deviation squares */
                       ;
    na64dp_KleinScorer_t rS;  /**< Sum of relative deviation products */
} na64dp_Covariance_t;

/** (Re-)initializes running Pearson's covariance calculus */
void na64dp_cov_init( na64dp_Covariance_t * ptr );
/** Adds variable pair to consideration */
void na64dp_cov_account( na64dp_Covariance_t * ptr, double x, double y );
/** Get the (population) covariance result from current state */
double na64dp_covariance_get( const na64dp_Covariance_t * ptr );

/**\brief Unweighted covariance matrix implementation
 *
 * Number of independent components obeys triangular number series.
 * */
typedef struct na64dp_CovarianceMatrix {
    /** Number of components in vector */
    unsigned short d;
    /** Number of considered samples */
    unsigned long n;
    /** Array of mean scorers */
    na64dp_KleinScorer_t * means;
    /** Array of squared deviation scorers */
    na64dp_KleinScorer_t * sqds;
    /** Sum of relative deviation products (co-moments) */
    na64dp_KleinScorer_t * covs;
} na64dp_CovarianceMatrix_t;

/** Initializes covariance matrix for vector of `d` components */
void na64dp_mcov_init( na64dp_CovarianceMatrix_t * ptr, unsigned short d );
/** Adds vector to consideration */
void na64dp_mcov_account( na64dp_CovarianceMatrix_t * ptr, const double * v );
/** Returns covariance of i-th and j-th variables */
double na64dp_mcov( na64dp_CovarianceMatrix_t * ptr, int i, int j );
/** Frees covariance matrix */
void na64dp_mcov_free( na64dp_CovarianceMatrix_t * ptr );

#ifdef __cplusplus
}
#endif

#endif
