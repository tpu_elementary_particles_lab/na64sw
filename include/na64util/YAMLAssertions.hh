#pragma once

#define YAML_ASSERT_SEQ( node, name )  assert(node[name].IsSequence());
#define YAML_ASSERT_MAP( node, name )  assert(node[name].IsMap());
#define YAML_ASSERT_SCLR( node, name )  assert(node[name].IsMap());
