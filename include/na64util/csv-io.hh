#pragma once

#include <istream>
#include <tuple>
#include <regex>

#include <iostream>  // XXX

namespace na64dp {

namespace error {

/** \brief Tabular data (CSV or similar) parsing error exception.
 *
 * Exception is thrown from within CSV parsing utility code when some violation
 * of basic CSV syntax occurs. Typically bears information on exact place and
 * reason of an error.
 * */
class CSVParsingError : public std::runtime_error {
private:
    size_t _nLine, _nToken;
public:
    /// Accepts error text description, optionally line number and token
    /// (column) number
    CSVParsingError( const char * what_
                   , size_t nLine=0
                   , size_t nToken=0 ) throw();
    /// Sets line number of an error (for re-throw)
    void n_line(size_t nLine) throw() { _nLine = nLine; }
    /// Sets token number of an error (for re-throw)
    void n_token(size_t nToken) throw() { _nToken = nToken; }
    /// Returns line number of an error
    size_t n_line() const throw() { return _nLine; }
    /// Returns token (column) number within an error
    size_t n_token() const throw() { return _nToken; }
};

}  // namespace error

namespace aux {
namespace meta {

// Various auxiliary code for operating with tuples

template<int... Is> struct seq {};
template<int N, int... Is> struct gen_seq : gen_seq<N - 1, N - 1, Is...> { };
template<int... Is> struct gen_seq<0, Is...> : seq<Is...> { };

template<typename T, typename F, int... Is>
void for_each(T & t, F & f, seq<Is...>) {
    auto l = { (f(std::get<Is>(t)), 0)... };
    (void) l;
}

template<typename... Ts, typename F>
void for_each_in_tuple(std::tuple<Ts...> & t, F & f) {
    meta::for_each(t, f, meta::gen_seq<sizeof...(Ts)>());
}

// Use to retrieve first element from tuple (completes `tail()`)
template < typename T , typename... Ts >
auto head( std::tuple<T,Ts...> t ) {
   return  std::get<0>(t);
}

// aux specification for `tail()`
template < std::size_t... Ns , typename... Ts >
auto tail_impl( std::index_sequence<Ns...> , std::tuple<Ts...> t ) {
   return  std::make_tuple( std::get<Ns+1u>(t)... );
}

// Use it to retrieve "all except first" elements from a tuple
template < typename... Ts >
auto tail( std::tuple<Ts...> t ) {
   return  tail_impl( std::make_index_sequence<sizeof...(Ts) - 1u>() , t );
}

}  // namespace meta

/// ASCII/CSV parsing traits implementations for CSV IO
template<typename T> struct ValueParser;

template<>
struct ValueParser<std::string> {
    static void read( const std::string & token, std::string & value ) {
        value = token;
    }
};

template<>
struct ValueParser<double> {
    static void read( const std::string & token, double & value ) {
        value = stod(token);
    }
};

template<>
struct ValueParser<int> {
    static void read( const std::string & token, int & value ) {
        value = stoi(token);
    }
};

template<>
struct ValueParser<size_t> {
    static void read( const std::string & token, size_t & value ) {
        value = stoul(token);
    }
};

}  // namespace aux

/**\brief Reader for the ASCII streams that holds tabular entries
 *
 * \todo Detailed description shall be added once API will become distilled.
 * */
class CSVReader {
private:
    size_t _lineCount;  ///< line counter
    size_t _tokCount;  ///< tokens (columns) counter
    std::istream & _is;  ///< stream ref to be read
    std::regex _wrx;
    std::sregex_iterator _i
                       , _begin
                       , _end;
protected:
    virtual std::string _get_next_line();
    std::string _tail;
    size_t _lastMatchPos;
public:
    /**\brief Constructs reader instance over a stream */
    CSVReader( std::istream & is
             , size_t lineCountStart=0
             , const std::string & tokRx="[^\\s]+" );

    /**\brief Entry-line reading method, handy for loop usage
     *
     * One may use it easily within a `while()` expression to read a sequence
     * of homogeneous entries from the stream */
    template<typename ... ArgsT>
    bool read( std::tuple<ArgsT...> & values ) {
        _tail.clear();
        _lastMatchPos = std::string::npos;
        auto line = _get_next_line();
        if( line.empty() ) return false;
        _begin = std::sregex_iterator( line.begin()
                                     , line.end()
                                     , _wrx );
        _i = _begin;
        _tokCount = 0;
        try {
            aux::meta::for_each_in_tuple( values, *this );
        } catch( error::CSVParsingError & e ) {
            e.n_line( _lineCount );
            throw e;
        }
        if( _lastMatchPos < line.size() ) {
            _tail = line.substr( _lastMatchPos );
            _lastMatchPos = _tail.find_first_not_of(" \t");
            if( _lastMatchPos == std::string::npos ) {
                _tail.clear();  // wipe empty line
            } else {
                _tail = _tail.substr(_lastMatchPos);
            }
        }
        return true;
    }

    /// Returns string after last token that correspondied to parsed line
    std::string tail() const { return _tail; }

    /// Convenient to use in "one entry per call" case
    template<typename ... ArgsT>
    CSVReader & operator>>(std::tuple<ArgsT...> & values) {
        read( values );
        return *this;
    }

    // note: shall be used only by each_in_tuple(), but it would be too messy
    // to make them friends
    template<typename T>
    void operator()( T & value ) {
        ++_tokCount;
        try {
            if( _end == _i ) {
                throw error::CSVParsingError("Bad tokens (columns) count in line.");
            }
            aux::ValueParser<T>::read( _i->str(), value );
        } catch( error::CSVParsingError & e ) {
            e.n_token( _tokCount );
            throw e;
        }
        _lastMatchPos = _i->position() + _i->length();
        ++_i;
    }
};

}

