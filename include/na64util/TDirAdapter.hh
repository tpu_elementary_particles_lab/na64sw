#pragma once

#include "na64detID/TBName.hh"
#include "na64calib/dispatcher.hh"

#ifdef ROOT_FOUND

#include <TDirectory.h>
#include <map>

namespace na64dp {

/**\brief A ROOT's TDirectory manager helper class.
 *
 * Implements lyfecycle support for TDirectory instance for
 * classes that use ROOT histograming functions, etc. Helps to organize
 * entities in the output ROOT file, by catalogues. Relies on structure
 * provided by naming.
 *
 * \todo recaching (`handle_update()` implem)
 * \todo parallel access guarding locks?
 * */
class TDirAdapter : public util::Observable<nameutils::DetectorNaming>::iObserver {
protected:
    std::map<DetID_t, TDirectory *> _rootDirs;
    /// Recached existing `_rootDirs` to correspond new detector IDs
    virtual void handle_update( const nameutils::DetectorNaming & ) override;
    /// Ptr to the name mappings (null until first update)
    const nameutils::DetectorNaming * _names;
public:
    /// Creates an instance with certain layout scheme.
    TDirAdapter( calib::Dispatcher & );
    /// Returns full substitution dictionary for certain detectors to be used
    /// in string templates
    std::map<std::string, std::string>
        subst_dict_for( DetID_t
                      , const std::string &) const;
    /// Creates (or returns if exists) a TDirectory for entity identified
    /// by certain detector ID.
    /// \arg did DetectorID
    /// \arg hstNm histogram name string that may be superseded by naming template
    /// \arg nm Current name mappings
    TDirectory *
        dir_for( DetID_t did
               , std::string & hstNm
               , const std::string & overridenTemplate="" );
    /// Overloaded version of `dir_for()` accepting external string context for
    /// formatting
    std::pair<std::string, TDirectory *>
        dir_for( DetID_t did
               , const std::map<std::string, std::string> & ctx
               , const std::string & overridenTemplate="" );
    /// Returns a map of directories indexed by detector IDs and created by
    /// this class
    std::map<DetID_t, TDirectory *> & tdirectories() { return _rootDirs; }
    /// Returns reference to a naming object
    const nameutils::DetectorNaming & naming() const;

    /// Creates full path of `TDirectory`(es) recursively, similar as
    /// `mkdir -p` does in shell, starting from `gFile`'s root
    static TDirectory * mkdir_p( const std::string & );
};

}  // namespace na64dp

#endif

