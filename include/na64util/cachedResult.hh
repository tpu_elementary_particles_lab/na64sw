#pragma once

namespace na64dp {
namespace util {

template<typename CallableT> struct CacheTraits;

/**\brief A (not memoizing) cached result helper class
 *
 * This template structure implements a common logic of cached data. It does
 * not provide the argument comparison, but rather just lazily returns a
 * result (even for the mismatching or unequal arguments).
 * */
template<typename CallableT>
class Cached {
protected:
    /// Cache validity flag
    bool _isCacheValid;
    /// Cached result value
    typename CacheTraits<CallableT>::Result _cache;
    /// Callable instance used to calc the cache
    CallableT _callable;
public:
    /// Ctr accepting the callable instance
    Cached( CallableT c ) : _isCacheValid(false)
                          , _cache(CacheTraits<CallableT>::initialValue)
                          , _callable(c)
                          {}
    /// (Re-)caching getter
    template<typename ... ArgsT > typename CacheTraits<CallableT>::ReturnType
    get( ArgsT ... args ) {
        if( _isCacheValid ) {
            CacheTraits<CallableT>::recache(_cache, _callable, args ... );
            _isCacheValid = true;
        }
        return CacheTraits<CallableT>::derive_result(_cache);
    }
    /// Invalidates the cache value
    void invalidate() { _isCacheValid = false; }
};

}
}

