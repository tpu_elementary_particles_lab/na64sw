#pragma once

#include "na64mc/g4api/sensitiveDetector.hh"
#include "na64mc/stepDispatcher.hh"

namespace na64dp {
namespace mc {

/**\brief A base class for sensitive detector consisting of multiple enumerable
 *        objects
 *
 * This sensitive detector subtype implements retrieving of indexes within a
 * complex geometry with enumerated replication using `mc::PartsIndex`.
 * */
class AbstractSegmentedSensitiveDetector
            : public SensitiveDetector
            , public AbstractStepDispatcher<PartsIndex::Index_t>
            , public PartsIndex
            {
private:
    /// A step handlers destination. Might be filtered one.
    AbstractStepDispatcher<PartsIndex::Index_t> * _handlers;
public:
    /// Index conversion defined for this SD. Applied before dispatching.
    PartsIndex::Index::Conversion * conversionPtr;
protected:
    /// Implements `SensitiveDetector` interface by forwarding call to 
    /// `AbstractStepDispatcher<PartsIndex::Index_t>`
    virtual G4bool ProcessHits(G4Step* aStep, G4TouchableHistory*) override {
        handle_step(aStep);
        return true;  // return value consideration is officially deprecated in Geant4
    }
    /// Derives index using `PartsIndex::get_key()`
    PartsIndex::Index_t derive_key(const G4Step * aStep) {
        Index i = PartsIndex::get_index(aStep);
        if( conversionPtr ) {
            auto p = (*conversionPtr)(i);
            assert(p.first);  // TODO?
            return p.second;
        }
        return i;
    }
    // From `AbstractStepDispatcher<PartsIndex::Index_t>` interface
    //virtual iStepHandler * new_handler(const Index_t &, const G4Step *) = 0;
public:
    AbstractSegmentedSensitiveDetector( const G4String & sdName )
            : SensitiveDetector(sdName) {}
};

///\brief Type trats for step handler
///
/// Default is implemented as somewhat generic case.
template<typename T>
struct StepHandlerTraits {
    static T * new_handler( const PartsIndex::Index_t & i
                          , const G4Step * s
                          , AbstractSegmentedSensitiveDetector * sd
                          ) {
        return new T(i, s, sd);
    }
    static void delete_handler( const PartsIndex::Index_t & i
                              , T * h
                              , AbstractSegmentedSensitiveDetector * sd
                              ) {
        delete h;
    }
};

/**\brief Concrete segmented SD class with sub-handlers of certain type
 *
 * For cases when segmented SD is parameterised with some single type of step
 * handler type this class provides an automatic implementation based on type
 * traits technique (see `StepHandlerTraits`).
 * */
template<typename HandlerT>
class SegmentedSensitiveDetector : public AbstractSegmentedSensitiveDetector {
protected:
    /// Implements `AbstractStepDispatcher<PartsIndex::Index_t>` interface
    /// using handler traits
    virtual iStepHandler * new_handler(const Index_t & i, const G4Step * s) override {
        return StepHandlerTraits<HandlerT>::new_handler(i, s, this);
    }
public:
    SegmentedSensitiveDetector( const G4String & sdName
                              , const G4String & msgrPath )
            : AbstractStepDispatcher(sdName, msgrPath) {}
    ~SegmentedSensitiveDetector() {
        for( auto & p :
                *static_cast<std::unordered_map<PartsIndex::Index_t, iStepHandler *>*>(*this) ) {
            StepHandlerTraits<HandlerT>::delete_handler(p.first, p.second, this);
        }
    }
};

}
}
