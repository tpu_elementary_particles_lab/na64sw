#pragma once

#include "sd/segmented.hh"
#include "na64mc/geode/genericMessenger.hh"
#include "na64util/cachedResult.hh"
#include "na64util/numerical/online.hh"

namespace na64dp {
namespace mc {

/**\brief A generic sensitive detector class for segmented detectors
 *
 * Provides functionality similar to native Geant4 scorers and filters
 * subsystem with runtime extensibility.
 *
 * User code defines a destination class and dynamically registers it using
 * a helper macro/runtime call. Then this destination class becomes available
 * to instantiation using factory constructor and binding within the UI macros.
 *
 * Various destination classes may provide versatile features, from simple
 * energhy scoring to sophisticated data collection logic.
 *
 * For macro command path starting with `<rootPath>`, the fillowing commands are
 * available:
 *
 *   * <rootPath>/<sdName>/add <destClassName> <destInstanceName>
 *     creates new named destination instance
 *   * <rootPath>/<sdName>/setIndexConversionRule <destInstanceName> <ruleExpr>
 *     assigns index conversion rule to a destination named there
 *   * <rootPath>/<sdName>/setFilter <destInstanceName> <fltrClass> ...
 *     TODO: adds filter to a destination
 *
 * Notes on the internal implementation:
 *  - A destination represents a single "class" of the entities -- summators,
 *  scorers, histograms, etc, NOT the single instance
 *  - Filters are named (many of them are derived from standard Geant4 ones)
 *  - Conversions aren't named; they are a relatively simple data structs that
 *  may be compared directly
 *  - thus the destinations and their set of filters are grouped within a
 *  multimap by a conversion rule applied
 */
class CompositeSensitiveDetector : public SegmentedSensitiveDetector
                                 , public geode::GenericG4Messenger {
public:
    /** Basic interface for set of scoring destinations
     *
     * For the optimiztion reasons destination interface does not imply any
     * particular collection structure (i.e. map, array, etc). Implemnting
     * classes have to define their own allocation/addressing logic.
     */
    struct iDestination {
        /// Shall account a step with (optionally converted) part's index
        virtual void account_step(Index, G4Step * aStep) = 0;
        /// Usually finalizes the summation scorers
        virtual void finalize_event() {}
    };

    /// Step-filtering base class
    struct iFilter {
        /// Returns true if filter accepts the step (similar to `G4VSDFilter::Accept()`)
        virtual bool passes( Index, const G4Step * aStep ) = 0;
    };
protected:
    /// Container type for named filters
    typedef std::map<std::string, util::Cached<iFilter *> > FiltersByName;

    /// Helper type joining a set of filters (even empty) and destination
    struct FilteredDestination : public std::vector<util::Cached<iFilter*>*> {
        iDestination & dest;
        FilteredDestination(iDestination & dest_) : dest(dest_) {}
        void operator()( Index i, G4Step * aStep );
    };

    /// Messenger path for destinations
    std::string _basePath;
    /// All the destinations by name (owns destinations)
    std::map<std::string, FilteredDestination> _destsByName;
    /// All the filters by name (owns filters)
    FiltersByName _filters;
    /// Destinations by conversion rules (not owning container)
    std::unordered_multimap< Index::Conversion
                           , FilteredDestination *
                           , Index::Conversion::Hash > _dests;
protected:
    /// Performs event filling
    virtual void process_hits_for( const Index_t & idx
                                 , G4Step * aStep
                                 ) override;
public:
    virtual void EndOfEvent(G4HCofThisEvent*) override;

    /// Creates new composite sensitive detector instance and adds
    /// corresponding UI macro commands
    CompositeSensitiveDetector( const std::string & sdName
                              , const std::string & msgrPath );
    /// Instantiates new destination as a plain one
    void add_destination( const std::string & clsName
                        , const std::string & dstName );
    /// Makes plain destination to accept converted index
    void set_conversion_rule_for( const std::string & dstName
                                , const Index::Conversion & cnv );

    /// Adds new destination; recieves dest class name and dest name ID
    static void ui_cmd_add_destination(geode::GenericG4Messenger*, const G4String &);
    /// Sets a conversion rule for existing destination
    static void ui_cmd_make_converted(geode::GenericG4Messenger*, const G4String &);
};

template<typename T> struct DestinationTraits;

/// Shimmering class defining access/create logic for destination entries of
/// composite detector
template<typename T>
class SparseDestination : public CompositeSensitiveDetector::iDestination
                        , public std::unordered_map<PartsIndex::Index_t, T> {
public:
    typedef DestinationTraits<T> Traits;
    typedef std::unordered_map<PartsIndex::Index_t, T> Container;
public:
    /// Forwards call to trait's `new_entry()` (if need) and to `account()`
    virtual void account_step( PartsIndex::Index i
                             , G4Step * aStep
                             ) override {
        auto it = Container::find(i);
        if( Container::end() == it ) {
            it = Container::emplace(i, Traits::create(*this, i, aStep )).first;
        }
        Traits::account(*this, it->second, i, aStep);
    }
    /// Forwards call to trait's `finalize()`
    virtual void finalize_event() override {
        for( auto & e : static_cast<Container &>(*this) ) {
            Traits::finalize( *this
                            , e.first
                            , e.second );
        }
    }
};

// todo ArrayDestination ?

}  // namespace ::na64dp::mc

namespace util {

/// Caching traits for filter result
template<>
struct CacheTraits<mc::CompositeSensitiveDetector::iFilter*> {
    typedef bool Result;
    typedef Result ReturnType;  // same as cache
    static constexpr Result initialValue = false; // = Result(false, mc::PartsIndex::Index());
    
    static bool derive_result( bool r ) { return r; }
    static void recache( bool & r
                       , mc::CompositeSensitiveDetector::iFilter * fltrPtr
                       , mc::PartsIndex::Index i
                       , G4Step * aStep
                       ) {
        r = fltrPtr->passes(i, aStep);
    }
};

}  // namespace na64dp::util

template<> struct CtrTraits<mc::CompositeSensitiveDetector::iDestination> {
    typedef mc::CompositeSensitiveDetector::iDestination * (*Constructor)(
            const G4String &, const G4String & );
};

}  // namespace na64dp

/// Registers new composite sensitive detector's destination subclass
# define REGISTER_COMPOSITE_SENSITIVE_DETECTOR_DESTINATION( clsName, name, subMsgrPath, desc )  \
    static ::na64dp::mc::CompositeSensitiveDetector::iDestination * _new_ ## clsName ## _instance(     \
          const G4String &, const G4String & );  \
static bool _regResult_ ## clsName = \
    ::na64dp::VCtr::self().register_class<::na64dp::mc::CompositeSensitiveDetector::iDestination>( \
        # clsName, _new_ ## clsName ## _instance, desc ); \
static ::na64dp::mc::CompositeSensitiveDetector::iDestination * _new_ ## clsName ## _instance( \
            __attribute__((unused)) const G4String & name, \
            __attribute__((unused)) const G4String & subMsgrPath \
          ) 


