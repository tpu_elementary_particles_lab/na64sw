#pragma once

// TODO
#if 0
#include "na64mc/gdml/processor.hh"
#include "na64mc/geode/genericMessenger.hh"
#include "na64mc/notifications.hh"
#include "na64mc/stepClassifier.hh"
#include "stepping/classifierSegmSD.hh"

#include <regex>

namespace na64dp {
namespace mc {

/**\brief Assigns the segmentation level for segmented detectors
 *
 * Uses (creates or appends) instances of `BySegmentID` class (a special kind
 * of steps classifier) by adding the "segmentation levels" to them. The
 * technique:
 *
 *  - logical volume in GDML refers to a named classifier with some kind of
 *    expression defining how this volume shall identify a "segment"
 *  - this processor parses this expression and, having the pointer to a
 *    logical volume, imposes the rule into classifier
 *  - the classifier is then to be used by sensitive detector instance(s) to
 *    resolve the proper steps handler
 * */
class SetSegmentID : public iAuxInfoProcessor
                   , public std::map<std::string, BySegmentID> {
public:
    /// A regular expression to parse the classifier association syntax
    static const std::regex rxSegmentID;

    /**\brief Obtains segmentation level description tokens from expression str
     *
     * \todo document syntax
     *
     * \returns `true` if parsing suceed
     * */
    static bool parse_segmentation_level_description( const std::string & expr
            , std::string & classifierName
            , std::string & variableName
            , std::string & value );
private:
    const G4String _uiCmdClassifiersRoot;
public:
    /// Ctr
    SetSegmentID( const G4String & uiCmdClassifiersRoot );
    /// Adds the index appender to a classifier; may create new classifier of
    /// type `BySegmentID`
    virtual void process_auxinfo( const G4GDMLAuxStructType & auxStruct
                                , G4LogicalVolume * lvPtr ) override;
};

}
}
#endif
