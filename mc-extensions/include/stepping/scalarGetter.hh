#pragma once

#include "na64util/numerical/online.hh"
#include "na64mc/genericMessenger.hh"

#include <map>

class G4Step;

namespace na64dp {
namespace mc {

/** An aux data structure accumulating sums and summed squares of a value
 * during the MC run. Used for partial template specialization of
 * `CompositeSensitiveDetector` subclass and for `SteppingAction::iHandler`
 * subclassing.
 * */
struct CumulativeValue {
    numerical::KleinScorer fromThisEvent
                         , overall
                         , sqOverall;
};

/**\brief Keeps a getter function that obtains a value from `G4Step` instance
 *
 * A mixin class managing the "getter" function that must be set at a runtime.
 * Used by composite SD destinations and by `SteppingAction::iHandler`
 * subclasses.
 *
 * \note Getter dictionary `StepGetterMixin::getters`is defined and statically
 * filled in conjugated implementation file.
 * */
class StepGetterMixin {
public:
    /// A type of getter function -- recieves a ptr to `G4Step`, returns a
    /// scalar value
    typedef double (*Getter)(const G4Step * aStep);
    /// A dictionary of named getter functions. Some are initialized statically.
    static std::map<std::string, Getter> getters;
protected:
    /// Ptr to func under supervision
    Getter _getter;
public:
    /// Constructs scorers factory with no getter being set
    StepGetterMixin() : _getter(nullptr) {}
    /// Returns current getter for the destination instance (may be nullptr)
    double get_value(const G4Step * aStep) const { return _getter(aStep); }

    /// Prints list of available getters
    static void ui_cmd_list_getters( GenericG4Messenger*
                                   , const G4String & );
};

// TODO: think on traits class defining key extraction from step to facilitate
// both SD and SA. something like
//
//      template<typename T>
//      struct StepKeyTraits {
//          static T derive_key(const G4Step * aStep);
//      };
//
// The key type may be Index_t (for SDs only), the PDG code, the material and
// their combinations.
// Ugly `SparseDestination` may be re-written as too specific one?

}
}

