#pragma once

# include "na64mc/stepDispatcher.hh"

namespace na64dp {
namespace mc {

/** ...
 */
class ScalarValueStepHandler : public iStepHandler
                             , public StepGetterMixin {
protected:
    virtual void handle_value(G4double) = 0;
public:
    virtual void handle_step(const G4Step * step);
};

}
}

