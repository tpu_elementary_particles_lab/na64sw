#pragma once

#include "na64mc/stepDispatcher.hh"

namespace na64dp {
namespace mc {

/**\brief Passes only steps leaving the world
 *
 * This kind of filter is useful for determining the energy leaks, e.g. for
 * standalone detector testing -- calorimeter hermiticity, unaccounted energy,
 * etc.
 * */
class WorldLeavingStepFilter : public iStepFilter {
public:
    /**\brief Returns true if particle track leaves the world
     *
     * Post step point for last MC step in track leaving the world has target
     * logical volume set to NULL. We use this feature to determine this steps.
     * 
     * \returns true if track leaves the World volume
     * */
    virtual bool passes(const G4Step *) const override;
};

}
}

