#pragma once

#include "na64mc/stepClassifier.hh"

// TODO
#if 0
#include "na64mc/geode/genericMessenger.hh"

/**\file
 * \brief Declaration of SD-only classifier for segmented detector volumes.
 * */

namespace na64dp {
namespace mc {

/**\brief An MC track classifier for segmented SD
 *
 * Stateful configurable classifier retrieving the spatial segment ID from
 * geometry according the rules set from a runtime.
 *
 * Usually, instances of this class are created via dedicated GDML processing
 * class (see `SetSegmentID`) where the binding against certain Geant4 logical
 * volume is defined.
 *
 * \note this type of classifier must be instantiated *only* by aux info processor
 */
class BySegmentID : public StepClassifier::iStepFeatureExtractor 
                  , public PartsIndex
                  , public geode::GenericG4Messenger {
public:
    /// Creates no UI commands
    BySegmentID( const G4String & name
               , const G4String & path );
    /// Returns composed index
    virtual EncodedStepFeature get_feature(const G4Step &) override;
    /**\brief Writes the segment index
     *
     * \todo document formatting
     * */
    virtual void key_to_str(EncodedStepFeature, char *, std::size_t) override;
};

}
}
#endif
