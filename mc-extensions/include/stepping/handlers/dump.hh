#pragma once

#include "na64mc/stepDispatcher.hh"

// TODO
#if 0
#include "na64mc/geode/genericMessenger.hh"

#include <fstream>

namespace na64dp {
namespace mc {

/**\brief Dump sub-handler referring to a particular file
 * */
class StepsDumpHandler : public iStepHandler {
public:
    enum OutputInfoTypes : unsigned int {
        kPreStepPoint_mm = 0,
        kPostStepPoint_mm = 1,
        kParticlePDGCode = 2,
        kTotalEnergy_MeV = 3,
        kMomentumDir = 4,
        kGlobalTime_nsec = 5,
        kMaterialName = 6,
        kAllColumns = 0x7f
        // ...
    };
private:
    const size_t & _flags;
    /// Output file stream object
    std::ofstream _os;
public:
    /// Ctr; opens ASCII file for writing
    StepsDumpHandler( const size_t & flags
                    , const std::string & filename );
    /// Virtual dtr (need by polymorphic interface realization)
    virtual ~StepsDumpHandler() {}
    /// Writes step data as the ASCII
    virtual void handle_step(const G4Step *) override;
};



/**\brief A simple ASCII dump for the MC steps
 *
 * This handler useful for debugging purposes.
 *
 * \todo Control over the set of information written: position, momentum, PDG
 *      code, etc
 * */
class StepsDump : public iStepHandler
                , public geode::GenericG4Messenger {
protected:
    /// Flags steering the output composition
    size_t _flags;
    /// Single (for non-classifying dump) file destination
    StepsDumpHandler * _destination;
public:
    /// Ctr -- no file opened initially
    StepsDump( const G4String & name
             , const G4String & path );
    /// Implements `iStepHandler` -- dumps the step to a file
    virtual void handle_step(const G4Step *) override;
    /// Opens file for writing
    static void ui_cmd_open_file( geode::GenericG4Messenger *, const G4String & );
    /// Closes the file
    static void ui_cmd_close_file( geode::GenericG4Messenger *, const G4String & );
};

/**\brief Classifying ASCII dump handler
 *
 * This handler useful for debugging purposes.
 *
 * \todo Control over the set of information written: position, momentum, PDG
 *      code, etc
 * */
class ClassifyingStepsDump : public AbstractRuntimeStepDispatcher
                           , public geode::GenericG4Messenger {
protected:
    size_t _flags;
    std::string _filenamePat;
public:
    /// Creates the new handler instance wrt file name pattern being set
    virtual iStepHandler * new_handler( const StepHandlerKey &
                                      , const G4Step *
                                      ) override;

    /// Ctr -- no file opened initially
    ClassifyingStepsDump( const G4String & name
                        , const G4String & path
                        , StepClassifier & kc );

    /// Erases all created streams closing corresponding files
    void clear_destinations();
    /// Sets the filename pattern for newly created files
    static void ui_cmd_set_filename_pattern( geode::GenericG4Messenger *, const G4String & );
    /// Forawards call to `clear_destinations()`
    static void ui_cmd_close_all( geode::GenericG4Messenger *, const G4String & );
};

}
}
#endif
