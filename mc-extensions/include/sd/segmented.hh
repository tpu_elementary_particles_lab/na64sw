#pragma once

#include "na64mc/stepDispatcher.hh"
#include "na64mc/g4api/sensitiveDetector.hh"

namespace na64dp {
namespace mc {

/**\brief A basic SD class supporting segmented classifier
 *
 * This SD class is designed for generic composite sensitive detector that
 * makes distinction between it's parts. The parts assumed to be subdivided
 * (segmented) according to geometrical description. Corresponding classifier
 * adds indexes into the subdivison to facilitate identification of individual
 * handlers connected to the sub-volumes.
 *
 * The step handler instances are constructed in a lazy way.
 * */
class SegmentedSensitiveDetector : public SensitiveDetector
                                 , public AbstractRuntimeStepDispatcher {
protected:
    /// Creates new handler instance of desired type for a new entry
    virtual iStepHandler * new_handler( const StepHandlerKey &
                                      , const G4Step *
                                      ) override;
    //virtual G4bool ProcessHits(G4Step * aStep, G4TouchableHistory *) override;
public:
    SegmentedSensitiveDetector( const G4String & sdName
                              , StepClassifier & classifier );
};

}
}

