#include "stepping/classifierSegmSD.hh"

// TODO
#if 0
namespace na64dp {
namespace mc {

BySegmentID::BySegmentID( const G4String & name
                        , const G4String & path ) : geode::GenericG4Messenger(path) {
    dir(name, "Parameters for segmented classifier.")
        // ... conversions?
    .end(name);
}

EncodedStepFeature
BySegmentID::get_feature( const G4Step & aStep ) {
    return { .segmentIndex = PartsIndex::get_index(&aStep) };
}

void
BySegmentID::key_to_str( EncodedStepFeature
                       , char *
                       , std::size_t ) {
    throw std::runtime_error("TODO to-str conversion segment ID");
}

}
}

#if 0
REGISTER_GDML_AUX_INFO_TYPE( segmentID, msgrPath, dc, calibMgr, mcNfr
        , "Associates the sensitive detector instance with certain volume" ) {
    return new na64dp::mc::SetSD( msgrPath
                                , calibMgr
                                , mcNfr
                                );
}
#endif
#endif
