#include "stepping/scalarGetter.hh"

#include <G4Step.hh>

namespace na64dp {
namespace mc {

std::map<std::string, StepGetterMixin::Getter> StepGetterMixin::getters;

// simple step getters {{{
static double _edep_getter( const G4Step * aStep ) {
    return aStep->GetTotalEnergyDeposit()
         * aStep->GetTrack()->GetWeight()
         / CLHEP::MeV;
}

// Taken from http://www.apc.univ-paris7.fr/~franco/g4doxy/html/G4PSCellCharge_8cc-source.html
//  just to demonstrate the idea
//  TODO: check this!
static double _charge_getter( const G4Step * aStep ) {
    G4double res = 0.;
    // Enter or First step of primary.
    if( aStep->GetPreStepPoint()->GetStepStatus() == fGeomBoundary 
        || ( aStep->GetTrack()->GetParentID() == 0 &&
             aStep->GetTrack()->GetCurrentStepNumber() == 1 ) ){
        G4double cellCharge = aStep->GetPreStepPoint()->GetCharge();
        cellCharge *= aStep->GetPreStepPoint()->GetWeight();
        //G4int index = GetIndex(aStep);
        //EvtMap->add(index, cellCharge);
        res += cellCharge;
    }

    // Exit
    if( aStep->GetPostStepPoint()->GetStepStatus() == fGeomBoundary) {
        G4double cellCharge = aStep->GetPreStepPoint()->GetCharge();
        cellCharge *= - aStep->GetPreStepPoint()->GetWeight();
        //G4int index = GetIndex(aStep);
        //EvtMap->add(index, CellCharge);
        res += cellCharge;
    }
    return res;
}
// }}}

static bool _register_std_getters() {
    StepGetterMixin::getters["edep"] = _edep_getter;
    StepGetterMixin::getters["charge"] = _charge_getter;
    return true;
}

static bool _rr = _register_std_getters();

void
StepGetterMixin::ui_cmd_list_getters( GenericG4Messenger*
                                    , const G4String & ) {
    for( const auto & p : getters ) {
        G4cout << " * " << p.first << std::endl;
    }
}

}
}

