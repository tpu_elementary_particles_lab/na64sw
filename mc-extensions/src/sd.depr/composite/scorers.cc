#include "sd/composite/scorers.hh"

namespace na64dp {
namespace mc {

ScalarScorers::ScalarScorers( const G4String & name
                            , const G4String & msgrPath
                            ) : geode::GenericG4Messenger(msgrPath)
                    , _name(name)
                    , _nEvents(0)
                    {
    dir( name
       , "Commands relevant to eponymous destination within a composite"
         " sensitive detector (scalar value scoring)." )
        .cmd<G4String>( "useValue"
                      , "Sets the value to be accumulated"
                      , "getterName"
                      , ui_cmd_set_getter
                      , anyAppState
                      )
        .cmd<G4String>( "write"
                      , "Writes scored sums and squares of sums into an ASCII"
                        " file"
                      , "filename"
                      , ui_cmd_write_ascii
                      , anyAppState
                      )
        .cmd_nopar( "list"
                  , "Prints list of scalar value getters available."
                  , ui_cmd_list_getters
                  , anyAppState
                  )
    .end( name );
}

void
ScalarScorers::ui_cmd_set_getter( geode::GenericG4Messenger* msgr_
                                , const G4String & strExpr ) {
    ScalarScorers & msgr = dynamic_cast<ScalarScorers &>(*msgr_);
    auto it = getters.find(strExpr);
    if( getters.end() == it ) {
        NA64DP_RUNTIME_ERROR( "Can not find G4Step getter \"%s\""
                " (scorers for scalar values).", strExpr.c_str() );
    }
    msgr._getter = it->second;
}

void
ScalarScorers::ui_cmd_write_ascii( geode::GenericG4Messenger * msgr_
                                 , const G4String & fnm ) {
    ScalarScorers & msgr = dynamic_cast<ScalarScorers &>(*msgr_);
        std::ofstream oFile(fnm);

    size_t nEntries = 0;
    double sum = 0, sqs = 0;
    oFile << "# " << msgr._nEvents << std::endl;
    for( auto & p : dynamic_cast<const SparseDestination<CumulativeValue>&>(msgr) ) {
        PartsIndex::Index i(p.first);
        const auto & cval = p.second;

        for( unsigned char j = 0; j < PartsIndex::nDimMax; ++j ) {
            if( i.is_set(j) ) {
                oFile << std::setw(5) << (int) i.get(j);
            }
        }
        double val = cval.overall
             , sqv = cval.sqOverall
             ;
        oFile << std::setw(15) << std::scientific << val
              << std::setw(15) << sqv
              << std::endl;
        sum += val;
        val /= msgr._nEvents;
        sqs += sqv / msgr._nEvents + val*val;
        ++nEntries;
    }
    
    log4cpp::Category::getInstance("na64mc.scorers")
            << log4cpp::Priority::INFO << "File \""
            << fnm << "\" written; avrg.value: " << sum/msgr._nEvents
            << " +/- " << sqrt(sqs) << " from " << nEntries << " entries.";
}

REGISTER_COMPOSITE_SENSITIVE_DETECTOR_DESTINATION( Scorers, nm, msgrPath,
        "Collects sum and sum of squares per event of certain value" ) {
    return new ScalarScorers(nm, msgrPath);
}


}
}

