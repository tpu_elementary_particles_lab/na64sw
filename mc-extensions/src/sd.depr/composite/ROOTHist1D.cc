#include <G4UnitsTable.hh>
#include <TFile.h>

#include "sd/composite/ROOTHist1D.hh"
#include "na64util/str-fmt.hh"

namespace na64dp {
namespace mc {

ROOTHist1DValue::ROOTHist1DValue( const char * name
                                , const char * title
                                , size_t nBins, double vMin, double vMax
                                ) {
    hst = new TH1F( name, title, nBins, vMin, vMax );
    log4cpp::Category::getInstance("na64mc.sd.composite") << log4cpp::Priority::DEBUG
            << "TH1F \"" << name << "\" created.";
}

ROOT1DHists::ROOT1DHists( const G4String & name
                        , const G4String & msgrPath)
            : geode::GenericG4Messenger(msgrPath)
            , _name(name)
            , _nameTemplate("unset")
            , _titleTemplate("unset")
            , _nBins(0)
            , _range{0, 0}
            , _unit(1.) {
    dir( name
       , "Commands relevant to eponymous histogramming destination within a"
         " composite sensitive detector." )
        .cmd<G4String>( "useValue"
                      , "Sets the value to be accumulated"
                      , "getterName"
                      , ui_cmd_set_getter
                      )
        .cmd_nopar( "list"
                  , "Prints list of scalar value getters available."
                  , ui_cmd_list_getters
                  )
        .cmd<G4String>( "setName"
                      , "Set name pattern for ROOT histograms, use {i1}, {i2} etc for"
                        " index number"
                      , "namepat"
                      , ui_cmd_set_name
                      )
        .cmd<G4String>( "setTitle"
                      , "Set name pattern for ROOT histograms, use {i1}, {i2} etc for"
                        " index number"
                      , "titlepat"
                      , ui_cmd_set_title
                      )
        .cmd( "setBinning"
            , "Sets histogram binning parameters"
            , ui_cmd_set_hst_pars
            )
            .par<G4int>( "nBins", "Number of bins" )
            .par<G4double>( "valueMin", "Minimum value in range" )
            .par<G4double>( "valueMax", "Maximum value in range" )
            .par<G4String>( "valueUnits", "Units for the range values" )
        .end( "setBinning" )
        .cmd_nopar( "write"
                  , "Writes histograms to currently open ROOT file"
                  , ui_cmd_write
                  , anyAppState
                  )
    .end( name );
}

void
ROOT1DHists::ui_cmd_set_getter( geode::GenericG4Messenger* msgr_
                              , const G4String & strExpr ) {
    ROOT1DHists & msgr = dynamic_cast<ROOT1DHists &>(*msgr_);
    auto it = getters.find(strExpr);
    if( getters.end() == it ) {
        NA64DP_RUNTIME_ERROR( "Can not find G4Step getter \"%s\""
                " (scorers for scalar values).", strExpr.c_str() );
    }
    msgr._getter = it->second;
}

void
ROOT1DHists::ui_cmd_set_name( geode::GenericG4Messenger* msgr_
                            , const G4String & strExpr ) {
    ROOT1DHists & msgr = dynamic_cast<ROOT1DHists &>(*msgr_);
    msgr._nameTemplate = strExpr;
    msgr._nameTemplate.erase( remove( msgr._nameTemplate.begin()
                                    , msgr._nameTemplate.end(), '\"' )
            , msgr._nameTemplate.end());
    //std::cout << "XXX " << msgr._nameTemplate << std::endl;  // XXX
}

void
ROOT1DHists::ui_cmd_set_title( geode::GenericG4Messenger* msgr_
                             , const G4String & strExpr ) {
    ROOT1DHists & msgr = dynamic_cast<ROOT1DHists &>(*msgr_);
    msgr._titleTemplate = strExpr;
    msgr._titleTemplate.erase( remove( msgr._titleTemplate.begin()
                                     , msgr._titleTemplate.end(), '\"' )
            , msgr._titleTemplate.end());
    //std::cout << "XXY " << msgr._titleTemplate << std::endl;  // XXX
}

void
ROOT1DHists::ui_cmd_set_hst_pars( geode::GenericG4Messenger* msgr_
                                , const G4String & strExpr ) {
    ROOT1DHists & msgr = dynamic_cast<ROOT1DHists &>(*msgr_);
    std::istringstream iss(strExpr);
    std::string rangeUnits;

    {
        iss >> msgr._nBins >> msgr._range[0] >> msgr._range[1];
        iss >> rangeUnits;
    }
    //std::cout << "XXX \"" << strExpr << "\" => "
    //          << "nBins=" << msgr._nBins << ", vMin=" << msgr._range[0]
    //          << ", vMax=" << msgr._range[1] << "m, units=\"" << rangeUnits << "\"."
    //          << std::endl;  // XXX
    if(!rangeUnits.empty()) {
        msgr._unit = G4UnitDefinition::GetValueOf(rangeUnits.c_str());
    } else {
        msgr._unit = 1;
    }
}

void
ROOT1DHists::ui_cmd_write( geode::GenericG4Messenger* msgr_
                         , const G4String & //strExpr
                         ) {
    if( !gFile ) {
        throw std::runtime_error("Unable to write histograms: no ROOT"
                " file opened.");
    }
    ROOT1DHists & msgr = dynamic_cast<ROOT1DHists &>(*msgr_);
    for( auto & p : dynamic_cast<SparseDestination<ROOTHist1DValue>&>(msgr) ) {
        assert(p.second.hst);
        //p.second.hst->Write();  // XXX
    }
}

ROOTHist1DValue
DestinationTraits<ROOTHist1DValue>::create(
            const SparseDestination<ROOTHist1DValue> & dst_
          , const PartsIndex::Index i
          , const G4Step * //aStep
          ) {
    const auto & dst = static_cast<const ROOT1DHists&>(dst_);
    // generate substitution dictionary using index
    return ROOTHist1DValue( PartsIndex::subst_index( dst._nameTemplate, i ).c_str()
                          , PartsIndex::subst_index( dst._titleTemplate, i ).c_str()
                          , dst._nBins
                          , dst._range[0], dst._range[1]
                          );
}

REGISTER_COMPOSITE_SENSITIVE_DETECTOR_DESTINATION( ROOT1DHists, nm, msgrPath,
        "Builds a 1D histogram" ) {
    return new ROOT1DHists(nm, msgrPath);
}

}
}

