#include "sd/ECAL.hh"

#include "na64mc/g4api/eventInformation.hh"

#include "na64detID/cellID.hh"
#include "na64detID/detectorID.hh"

#include <G4EventManager.hh>

namespace na64dp {
namespace mc {

ECALSD::ECALSD( const G4String & sdName
              , calib::Dispatcher & dsp ) : SegmentedSensitiveDetector( sdName )
                                          , _namingPtr(nullptr)
                                          , _totalSum(0) {
    dsp.subscribe<nameutils::DetectorNaming>(*this, "default");
}

void
ECALSD::handle_update( const nameutils::DetectorNaming & naming ) {
    _ecalID = naming["ECAL0"];
    _namingPtr = &naming;
    //std::cout << " -- " << (int) did.chip()
    //          << ", " << (int) did.kin()
    //          << ", " << (did.is_payload_set() ? did.payload() : 0)
    //          << std::endl;
}

void
ECALSD::process_hits_for( const Index_t & idx
                        , G4Step * aStep
                        ) {
    auto it = _hits.find(idx);
    if( _hits.end() == it ) {
        auto ir = _hits.emplace(idx, CaloHit());
        assert(ir.second);
        it = ir.first;
    }
    // collect energy deposit taking into account track weight
    it->second += aStep->GetTotalEnergyDeposit() * aStep->GetTrack()->GetWeight() / CLHEP::MeV;
    _totalSum += aStep->GetTotalEnergyDeposit() / CLHEP::MeV;  // XXX
    // NOTE: other useful
    //G4double eDeposit = aStep->GetTotalEnergyDeposit();
    //G4double sLength = aStep->GetStepLength();
    //G4ThreeVector displace = aStep->GetDeltaPosition();
    //G4double tof = aStep->GetDeltaTime(); 
}

void
ECALSD::EndOfEvent(G4HCofThisEvent * hcofe) {
    SegmentedSensitiveDetector::EndOfEvent(hcofe);
    // Put hit in the event
    EventInformation & einfo = *get_einfo();
    #if 0   // Assure we are dealing with clean event, XXX
    std::cout << " xxx " << &(einfo.eventBuilder.current_event())
              << ", " << einfo.eventBuilder.current_event().sadcHits.size()
              << std::endl;
    assert(einfo.eventBuilder.current_event().sadcHits.empty());
    #endif
    for(const auto & p : _hits) {
        Index_t idx_ = p.first;
        const CaloHit & eSum = p.second;

        // Compose cell index and complete detector ID
        PartsIndex::Index idx(idx_);
        assert(idx.is_set(0) && idx.is_set(1) && idx.is_set(2));
        CellID cid(idx.get(2), idx.get(1), idx.get(0));
        // ^^^ cell indexing order is reverted with respect to ordinary XYZ
        //     order because for our `SegmentedSensitiveDetector` class the
        //     lowest hierarchy level must have the 0-th dimension
        DetID detID(_ecalID);
        detID.payload(cid.cellID);

        #if 0
        std::cout << " ..." << idx.get(2) << "x" << idx.get(1)
                  << "x" << idx.get(0) << " -> "
                  << (*_namingPtr)[detID] << " : " << (double) eSum << std::endl;
        #endif

        // Insert hit and set edep
        PoolRef<SADCHit> hitRef = einfo.eventBuilder.create_hit<SADCHit>(detID);
        hitRef->eDep = eSum;
    }
    //std::cout << "xxx (" << _totalSum << ") from " << _hits.size()
    //          << " cells." << std::endl;  // XXX
    // Clear summators cache
    _hits.clear();
    _totalSum = 0;
}

}

REGISTER_SENSITIVE_DETECTOR( ECALSD
                           , name, msgrPath, dsp, nfr
                           , "Sensitive detector for e/m calorimeter" ) {
    return new na64dp::mc::ECALSD( name, dsp );
}

}

