#include "gdml/visStyle.hh"

#include <G4Exception.hh>
#include <G4LogicalVolume.hh>
#include <G4VisAttributes.hh>

#include <cassert>

namespace na64dp {
namespace mc {

const std::regex SetVisualAttributes::rxStyle(
        "\\s*(\\*|\\w+)\\s*\\:\\s*(hidden|wireframe|surface)?\\s*(#[\\da-fA-F]{3,8})?\\s*");

SetVisualAttributes::SetVisualAttributes( const G4String & uiRootPath
                                        , const G4String * defaultSetupPtr )
        : GenericG4Messenger(uiRootPath)
        , _defaultSetupPtr(defaultSetupPtr)
        , _default { VolumeStyle::undefined, 0x0 } {
    dir( "style", "NA64SW GDML auxinfo style management." )
        .cmd<G4String>( "setDefault"
            , "Sets default drawing style for volumes tagged with \"style\""
              " auxinfo tag. Syntax is same as for GDML attribute except for"
              " spaces. Oktothorp will be passed within quotation marks or"
              " without leading space (vis syntax considers it to be a comment"
              " mark otherwise)."
            , "defaultStyle"
            , ui_cmd_set_default_style
            , anyAppState
            )
        .cmd( "apply"
            , "(Re-)applies styles to logical volumes on scene w.r.t. the"
              " \"style\" auxinfo tag."
            , ui_cmd_apply_styles
            , anyAppState
            )
            .par<G4String>( "setupSelector"
                , "Setup selector for styles to apply."
                , "-" )
            .par<G4String>( "lvPattern"
                , "Logic volume name pattern to apply."
                , "*" )
        .end("apply")
    .end( "style" );
}

std::pair<std::string, SetVisualAttributes::VolumeStyle>
SetVisualAttributes::parse(const std::string & expr) {
    VolumeStyle vs;
    std::smatch m;
    if( !std::regex_match( expr, m, rxStyle ) )
        return std::make_pair("", vs);  // setupName is empty, must be interpreted as error by caller

    std::string setupName = m[1];

    if( m[2] == "surface" ) vs.drawingStyle = VolumeStyle::surface;
    else if( m[2] == "wireframe" ) vs.drawingStyle = VolumeStyle::wireframe;
    else if( m[2] == "hidden" ) vs.drawingStyle = VolumeStyle::hidden;
    else vs.drawingStyle = VolumeStyle::undefined;

    // Color may be provided as (+1 for leading oktothorp):
    //  - 3 hex digits RGB, same meaning as in CSS, i.e. #345 -> #334455FF
    //  - 4 hex digits RGBA, same meaning as in CSS, i.e. #2345 -> #22334455
    //  - 6 hex digits RGB
    //  - 8 hex digits RGBA
    const std::string sm(m[3]);
    vs.rgba = 0x0;
    if( sm.empty() ) {
        return std::make_pair(setupName, vs);  // color is not set
    }
    if( sm.size() == 4 || sm.size() == 5) {
        for( unsigned int i = 1; i < sm.size(); ++i ) {
            char c = sm[i];
            if( c >= '0' && c <= '9' ) {
                c -= '0';
            } else if( c >= 'a' && c <= 'f' ) {
                c -= 'a';
                c += 10;
            } else if( c >= 'A' && c <= 'F' ) {
                c -= 'A';
                c += 10;
            } else {
                assert(false);
            }
            vs.rgba |= ((c << 4) | (c)) << (4-i)*8;
        }
        if( sm.size() == 4 ) vs.rgba |= 0xff;
    } else if( sm.size() == 7 || sm.size() == 9 ) {
        vs.rgba = strtol( sm.data() + 1, NULL, 16 );
        if( sm.size() == 7 ) {
            vs.rgba <<= 8;
            vs.rgba |= 0xff;
        }
    } else {
        // wrong number of digits in color -- clear setupName to indicate
        // error
        setupName.clear();
    }
    return std::make_pair(setupName, vs);
}

void
SetVisualAttributes::override_style( VolumeStyle & t, const VolumeStyle & n ) {
    if( VolumeStyle::undefined != n.drawingStyle ) {
        t.drawingStyle = n.drawingStyle;
    }
    if( n.rgba ) {
        t.rgba = n.rgba;
    }
}

SetVisualAttributes::VolumeStyle
SetVisualAttributes::get_style_for( const StylesDict & dict
                                  , const VolumeStyle & defaultStyle
                                  , const std::string & setupName ) {
    VolumeStyle base = defaultStyle;
    auto it = dict.find("*");
    if( dict.end() != it ) {
        override_style( base, it->second );
    }
    it = dict.find(setupName);
    if( dict.end() != it ) {
        override_style( base, it->second );
    }
    return base;
}

SetVisualAttributes::StylesDict
SetVisualAttributes::parse_styles(const std::string & expr) {
    SetVisualAttributes::StylesDict dct;
    if(expr.empty()) return dct;
    for( size_t bgn = 0, end = expr.find(';')
       ; bgn != std::string::npos
       ; bgn = end != std::string::npos ? end+1 : std::string::npos
       , end = expr.find(';', bgn) ) {
        auto tok = expr.substr(bgn, end-bgn);
        auto p = parse(tok);
        if( p.first.empty() ) {
            char errbf[128];
            snprintf(errbf, sizeof(errbf), "Bad styles token: \"%s\"", tok.c_str());
            G4Exception( __FUNCTION__, "NA64SW003", JustWarning, errbf);
            continue;
        }
        dct.insert(p);
    }
    return dct;
}

void
SetVisualAttributes::process_auxinfo( const G4GDMLAuxStructType & auxStruct
                                    , G4LogicalVolume * lvPtr ) {
    if( !auxStruct.unit.empty() ) {
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Unused \"unit\" attribute for styles auxinfo of volume \"%s\""
                , lvPtr->GetName().c_str() );
        G4Exception( __FUNCTION__, "NA64SW004", JustWarning, errbf);
    }
    auto it = _index.find(lvPtr);
    if( _index.end() != it ) {
        auto newStyles = parse_styles(auxStruct.value);
        // append styles dict
        for( auto p : newStyles ) {
            it->second.insert(p);
        }
    } else {
        _index.emplace( lvPtr, parse_styles(auxStruct.value) );
    }
}

void
SetVisualAttributes::apply_style_to( const VolumeStyle & style
                                   , G4LogicalVolume * target ) {
    G4VisAttributes va;
    // color
    if( style.rgba ) {
        G4Colour clr( ((style.rgba >> 24) & 0xff)/255.  // red
                    , ((style.rgba >> 16) & 0xff)/255.  // green
                    , ((style.rgba >>  8) & 0xff)/255.  // blue
                    , ((style.rgba      ) & 0xff)/255.  // alpha
                    );
        va.SetColour(clr);
    }
    // geometry drawing style
    if( VolumeStyle::undefined != style.drawingStyle ) {
        if( VolumeStyle::hidden == style.drawingStyle ) {
            va.SetVisibility(false);
        } else if( VolumeStyle::wireframe == style.drawingStyle ) {
            va.SetVisibility(true);
            va.SetForceWireframe(true);
        } else if( VolumeStyle::surface == style.drawingStyle ) {
            va.SetVisibility(true);
            va.SetForceSolid(true);
        } else assert(false);
        
    }
    // ... va.SetLineWidth()
    // ... va.SetLineStyle() (unbroken, dashed, dotted)
    // ... va.SetDaughtersInvisible()
    // ... va.ForceAuxEdgeVisible()
    // ... va.SetForceLineSegmentsPerCircle()
    // apply attributes
    target->SetVisAttributes(va);
}

void
SetVisualAttributes::set_default( const VolumeStyle & vsDft ) {
    _default = vsDft;
}

void
SetVisualAttributes::apply_styles_for( const std::string & setupName
                                     , const std::string & lvNamePat ) {
    if( _index.empty() ) {
        G4Exception( __FUNCTION__, "NA64SW005", JustWarning
                , "Apply styles for scene with 0 styled volumes called"
                " (was the geometry constructed?)" );
    }
    const bool all = "*" == lvNamePat;
    for( auto sp : _index ) {
        if( !all && std::string(sp.first->GetName()) != lvNamePat ) continue;
        VolumeStyle vs = get_style_for( sp.second, _default, setupName );
        apply_style_to(vs, sp.first);
    }
}

void
SetVisualAttributes::ui_cmd_set_default_style( GenericG4Messenger * msgr_
                                             , const G4String & strVal_
                                             ) {
    G4String strVal = strVal_;
    SetVisualAttributes & msgr = *static_cast<SetVisualAttributes*>(msgr_);
    // trim quotation marks, if provided:
    strVal.erase(remove( strVal.begin(), strVal.end(), '\"' ), strVal.end());

    auto sp = SetVisualAttributes::parse("default:" + strVal);
    // ^^^ mock the setup name to satisfy regex, we won't use it further
    if( sp.first.empty() ) {
        char errBf[128];
        snprintf( errBf, sizeof(errBf)
                , "Unable to parse default style expression: \"%s\"."
                , strVal.c_str() );
        G4Exception( __FUNCTION__
                   , "NA64SW193"
                   , FatalErrorInArgument
                   , errBf
                   );
    }
    msgr.set_default( sp.second );
}

void
SetVisualAttributes::ui_cmd_apply_styles( GenericG4Messenger * msgr_
                                        , const G4String & strVal
                                        ) {
    SetVisualAttributes & msgr = *static_cast<SetVisualAttributes*>(msgr_);
    // argument here provided as a string, e.g. "Default ones", "Default *",
    // "- *". We must parse it manually here as Geant4 parameter's
    // prescriptions seem not be doing the semantical parsing.
    std::string setupname, volumepat;
    {
        std::istringstream iss(strVal);
        iss >> setupname >> volumepat;
    }
    if( "-" == setupname ) {
        setupname = msgr._defaultSetupPtr ? *msgr._defaultSetupPtr : "Default";
    }
    msgr.apply_styles_for( setupname, volumepat );
}

}
}

REGISTER_GDML_AUX_INFO_TYPE( style, msgrPath, state
        , "Visual attributes styling auxinfo type of tag" ) {
    return new na64dp::mc::SetVisualAttributes( msgrPath
                    , &(state.setupName) );
}

