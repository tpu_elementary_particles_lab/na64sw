#include "gdml/sensDet.hh"
#include "na64calib/manager.hh"
#include "sd/segmented.hh"

#include <G4VSensitiveDetector.hh>
#include <G4SDManager.hh>

#define UI_MACRO_DIR_SD "sd"

namespace na64dp {
namespace mc {

SetSD::SetSD( const G4String rootPath
            , calib::Dispatcher & calibDsp
            , Notifier & nfr
            )
        : GenericG4Messenger(rootPath)
        , _calibDispatcherRef(calibDsp)
        , _mcNotifier(nfr)
        {
    dir(UI_MACRO_DIR_SD, "Macro commands related to bindings of sensitive detector"
              " to GDML instances.")
        .cmd( "create"
            , "Instantiates an instance of sensitive detector"
              " class with given name, optionally assigning MC hit"
              " MC conversion scheme."
            , ui_cmd_create_sd
            , preinit | init | idle
            )
            .par<G4String>( "sdClass", "SD class to instantiate." )
            .par<G4String>( "sdName", "Name of the SD isntance (in standard Geant4 notation)." )
            //.par<G4String>( "hitConverter", "Name of the MC hit conversion scheme to apply."
            //                    " Use \"none\" for no conversion"
            //              , "none" )
        .end( "create" )
    .end(UI_MACRO_DIR_SD);
}

void
SetSD::process_auxinfo( const G4GDMLAuxStructType & auxStruct
                      , G4LogicalVolume * lvPtr ) {
    assert(lvPtr);
    if( !auxStruct.unit.empty() ) {
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Unused \"unit\" attribute for sensitive detector auxinfo"
                  " of volume \"%s\""
                , lvPtr->GetName().c_str() );
        G4Exception( __FUNCTION__, "NA64SW004", JustWarning, errbf);
    }

    if( auxStruct.value.empty() ) {
        G4Exception( __FUNCTION__, "NA64SW115", FatalErrorInArgument
                , "Empty sensitive detector name." );
    }
    
    auto sdPtr = G4SDManager::GetSDMpointer()->FindSensitiveDetector( auxStruct.value );
    if( !sdPtr ) {
        // TODO: is this redundant? The FindSensitiveDetector seems to
        // throw its own exception
        char errbf[128];
        snprintf( errbf, sizeof(errbf)
                , "Unable to associate volume \"%s\" with non-existing"
                  " sensitive detector \"%s\""
                , lvPtr->GetName().c_str(), auxStruct.value.c_str() );
        G4Exception( __FUNCTION__, "NA64SW007", JustWarning, errbf);
        return;
    }
    log4cpp::Category & L
        = log4cpp::Category::getInstance(std::string("na64mc.gdml.auxinfo"));

    if( lvPtr->GetSensitiveDetector() ) {
        L << log4cpp::Priority::WARN << "Sensitive detector \""
            << auxStruct.value << "\" assigned to volume \""
            << lvPtr->GetName() << "\" (" << (void*) lvPtr
            << ") overrides \"" << lvPtr->GetSensitiveDetector()
            << "\".";
    }
    lvPtr->SetSensitiveDetector( sdPtr );
    L << log4cpp::Priority::INFO
      << "Sensitive detector \"" << auxStruct.value << "\" assigned to volume \""
      << lvPtr->GetName() << "\" (" << (void*) lvPtr << ")";
}

void
SetSD::ui_cmd_create_sd( GenericG4Messenger * msgr_
                       , const G4String & strVal
                       ) {
    SetSD & msgr = *static_cast<SetSD*>(msgr_);
    std::string sdClass, sdName;
    {
        std::istringstream iss(strVal);
        iss >> sdClass >> sdName;
    }
    const std::string uiCmdPath = msgr.GetRootPath() + UI_MACRO_DIR_SD "/";
    SensitiveDetector * newSDPtr
        = VCtr::self().make<SensitiveDetector>( sdClass
                                              , sdName, uiCmdPath
                                              , msgr._calibDispatcherRef
                                              , msgr._mcNotifier
                                              );
    G4SDManager::GetSDMpointer()->AddNewDetector( newSDPtr );

    log4cpp::Category & L = log4cpp::Category::getInstance(std::string("na64mc.gdml.auxinfo"));
    L << log4cpp::Priority::INFO
        << "New SensitiveDetector subclass (" << sdClass
        << ") instance created. Name: \"" << sdName << "\", ptr="
        << newSDPtr << ".";
}

}
}

REGISTER_GDML_AUX_INFO_TYPE( sensitiveDetector, msgrPath, state
        , "Associates the sensitive detector instance with certain volume" ) {
    #if 1
    throw std::runtime_error("TODO: SD set aux info.");  // TODO
    #else
    return new na64dp::mc::SetSD( msgrPath
                                , state.calibrationsManager
                                , state.notifier
                                );
    #endif
}


