#include "sd/segmented.hh"

namespace na64dp {
namespace mc {

iStepHandler *
SegmentedSensitiveDetector::new_handler( const StepHandlerKey &
                                       , const G4Step *
                                       ) {
    throw std::runtime_error("TODO: instantiate handler for generic segmented det");
    // ...
}

//G4bool
//SegmentedSensitiveDetector::ProcessHits(G4Step * aStep, G4TouchableHistory *) {
//    return true;
//}

SegmentedSensitiveDetector::SegmentedSensitiveDetector( const G4String & sdName
                                                      , StepClassifier & classifier
        ) : SensitiveDetector(sdName)
          , AbstractRuntimeStepDispatcher(classifier) {
    // ...
}

}
}

REGISTER_SENSITIVE_DETECTOR( SegmentedGeneric
                           , name, msgrPath, calibDsp, nfr
                           , "A general-purpose segmented sensitive detector"
                             " supporting standard handlers."
                           ) {
    throw std::runtime_error("TODO: vctr for GenericSegmented");
    return nullptr;  //new na64dp::mc::SegmentedSensitiveDetector( name );
}

